//
//  DexcomCGMManagerTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-10-01.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
import UserNotifications
import LoopKit
import TransmitterCore
@testable import DexcomCGMKit

class MockKeychainManager: SecurePersistAuthenticationKey {
    var storage: [String:String] = [:]
    func setDexcomCGMTransmitterKey(_ key: String?, for keyService: String?) throws {
        if let keyService = keyService {
            storage[keyService] = key
        }
    }
    
    func getDexcomCGMTransmitterKey(for keyService: String?) -> String? {
        guard let keyService = keyService else { return nil }
        return storage[keyService]
    }
}

class DexcomCGMManagerTests: XCTestCase {
    
    private let transmitterID = "8XXXXX"
    private var mockTxComm = MockTransmitterBleCommunication()
    private var cgmManager: DexcomCGMManager!
    
    private var newEGVExpectation: XCTestExpectation!
    private var newEGVs: [NewGlucoseSample]!

    private struct LoggedEvent: Equatable {
        let deviceIdentifier: String?
        let type: DeviceLogEntryType
        let message: String
        init(deviceIdentifier: String? = DexcomCGMManager.managerIdentifier, type: DeviceLogEntryType, message: String) {
            self.deviceIdentifier = deviceIdentifier
            self.type = type
            self.message = message
        }
    }
    private var loggedEvents: [LoggedEvent] = []
    private var loggedEventExpectation: XCTestExpectation!
    private let expectedDeviceUUID = UUID(uuidString: "DCF262A8-B79D-424F-B970-02D4ACF3E149")! // Just a UUID I made up
    private let expectedDate = Date(timeIntervalSince1970: 0)
    private let scanTimerTimeout = 0.1 // seconds
    
    private var alertIdentifiersScheduled: [DeviceAlert.Identifier] = []
    private var alertIdentifiersRemoved: [DeviceAlert.Identifier] = []

    private let mockKeychainManager = MockKeychainManager()
    private var originalKeychainManager: (() -> SecurePersistAuthenticationKey)!
    
    fileprivate func setUpCgmManager(_ state: DexcomCGMManagerState) {
        // Setup CGM Manager
        cgmManager = DexcomCGMManager(state: state,
                                      initialTransmitterCommunication: mockTxComm,
                                      findNewTransmitterCommunication: { _,_ in self.mockTxComm },
                                      reconnectTransmitterCommunication: { _,_ in self.mockTxComm },
                                      newUUIDFunc: { self.expectedDeviceUUID }, nowFunc: { self.expectedDate },
                                      scanTimerTimeout: scanTimerTimeout)
        // Set Mock Comms Delegate
        mockTxComm.connectionDelegate = cgmManager
        mockTxComm.cgmDelegate = cgmManager
        
        // Set ourselves as the CGM Manager Delegate
        cgmManager.cgmManagerDelegate = self
    }
    
    override func setUp() {
        originalKeychainManager = DexcomCGMManagerState.keychainManager
        DexcomCGMManagerState.keychainManager = {
            return self.mockKeychainManager
        }

        var cgmManagerState = DexcomCGMManagerState(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!)
        let keyServiceID = "DexcomKeyService"
        cgmManagerState.authenticationKeyServiceIdentifier = keyServiceID
        cgmManagerState.transmitterStaticInfo.maxRuntimeDays = 110 // G6 default value
        cgmManagerState.transmitterStaticInfo.sessionTimeDays = 10 // G6 default value
        cgmManagerState.transmitterStaticInfo.activatedOn = Date()
        setUpCgmManager(cgmManagerState)

        // Clear any prior logged events
        loggedEvents = []
        loggedEventExpectation = self.expectation(description: "loggedEventExpect")
        loggedEventExpectation.assertForOverFulfill = false
        alertIdentifiersScheduled = []
        alertIdentifiersRemoved = []
    }
    
    override func tearDown() {
        DexcomCGMManagerState.keychainManager = originalKeychainManager
    }
        
    // MARK: - Connection Delegate Tests
    
    func testOnConnectReplaceTransmitter() {
        // after a connection is made, if the transmitter is found and there is a pending transmitter found action, the CGM manager will execute this aciton
        // set the sensor code to starting the session after the transmitter is found
        let sensorCode = SensorCode(rawValue: "7171")
        cgmManager.pendingSensorCode = sensorCode
                
        // trigger connection callback
        mockTxComm.connectionEstablished()
        
        let startSessionCmd = CgmCommand.start(uuid: expectedDeviceUUID,
                                               systemTime: expectedDate,
                                               transmitterTime: nil,
                                               code: sensorCode,
                                               response: nil)
        XCTAssertEqual(startSessionCmd, mockTxComm.commandQueue.first)

        checkLoggedEvents(expected: [
            LoggedEvent(type: .connection, message: "onConnect(): "),
            LoggedEvent(type: .connection, message: "onConnect(): connectionState: awaitingScan ->  transmitterFound"),
            LoggedEvent(type: .send, message: "startSession(_:): start(uuid: DCF262A8-B79D-424F-B970-02D4ACF3E149, systemTime: 1970-01-01 00:00:00 +0000, transmitterTime: nil, code: Optional(TransmitterCore.SensorCode.code7171), response: nil)")
        ])
    }

    func testNotFoundAlertScheduledAndCanceled() {
        cgmManager.startCommunication()
        waitOnMain()
        
        // Make sure the "not found" notification was scheduled (for the future).  Note that startCommunication
        // actually attempts to remove the .transmitterNotFound alert twice, on purpose (once by the fact that it calls
        // "closeCommunication", and another when it calls "scanForTransmitter"
        checkAlertWasRemoved(forAlert: .transmitterNotFound, count: 2)
        checkAlertWasScheduled(forAlert: .transmitterNotFound)

        // trigger connection callback
        mockTxComm.connectionEstablished()
        waitOnMain()

        checkAlertWasRemoved(forAlert: .transmitterNotFound)

        ignoreLoggedEvent()
    }

    func testStoringAuthenticationData() {
        // trigger connection callback
        let authenticationConnectionData = TestAuthenticationConnectionData()
        mockTxComm.authenticationDataAvailable(authenticationConnectionData)
        
        XCTAssertEqual(cgmManager.state.transmitterAuthenticationKeyTimestamp, authenticationConnectionData.authenticationKeyTimestamp)
        noLoggedEvent()
    }
    
    func testOnTransmitterFound() {
        // set the sensor code to starting the session after the transmitter is found
        let sensorCode = SensorCode(rawValue: "7171")
        cgmManager.pendingSensorCode = sensorCode
        
        // trigger connection callback
        let transmitterStaticInfo = TransmitterStaticInfo(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                                          deviceUUID: expectedDeviceUUID.description,
                                                          transmitterVersion: "1.0.1",
                                                          softwareNumber: 1,
                                                          apiVersion: 2,
                                                          storageTimeDays: 30,
                                                          maxStorageTimeDays: 120,
                                                          maxRuntimeDays: 90,
                                                          sessionTimeDays: 10,
                                                          isDiagnosticDataDownloadSupported: true,
                                                          isEgvBackfillSupported: true,
                                                          isPredictedEgvSupported: true,
                                                          activatedOn: expectedDate)
        mockTxComm.transmitterFound(transmitterStaticInfo)
        
        let startSessionCmd = CgmCommand.start(uuid: expectedDeviceUUID,
                                               systemTime: expectedDate,
                                               transmitterTime: nil,
                                               code: sensorCode,
                                               response: nil)
        XCTAssertEqual(startSessionCmd, mockTxComm.commandQueue.first)
        XCTAssertEqual(cgmManager.state.connectionState, .transmitterFound)
        // transmitterStaticInfo activatedOn is set in the transmitterFound callback, so it cannot be compared
        XCTAssertEqual(cgmManager.state.transmitterStaticInfo.transmitterId, transmitterStaticInfo.transmitterId)
        XCTAssertEqual(cgmManager.state.transmitterStaticInfo.deviceUUID, transmitterStaticInfo.deviceUUID)
        XCTAssertEqual(cgmManager.state.transmitterStaticInfo.transmitterVersion, transmitterStaticInfo.transmitterVersion)
        
        checkLoggedEvents(expected: [
            LoggedEvent(type: .connection, message: "onTransmitterFound(_:): TransmitterStaticInfo(dexcomTransmitterID: DexcomCGMKit.DexcomTransmitterID(transmitterID: \"8XXXXX\"), deviceUUID: \"DCF262A8-B79D-424F-B970-02D4ACF3E149\", transmitterVersion: \"1.0.1\", softwareNumber: 1, apiVersion: 2, storageTimeDays: 30, maxStorageTimeDays: 120, maxRuntimeDays: 90, sessionTimeDays: 10, isDiagnosticDataDownloadSupported: true, isEgvBackfillSupported: true, isPredictedEgvSupported: true, activatedOn: Optional(1970-01-01 00:00:00 +0000))"),
            LoggedEvent(type: .connection, message: "onTransmitterFound(_:): connectionState: awaitingScan ->  transmitterFound"),
            LoggedEvent(type: .send, message: "startSession(_:): start(uuid: DCF262A8-B79D-424F-B970-02D4ACF3E149, systemTime: 1970-01-01 00:00:00 +0000, transmitterTime: nil, code: Optional(TransmitterCore.SensorCode.code7171), response: nil)")
        ])
    }
    
    func testTransmitterNotFound() {
        cgmManager.startCommunication()

        checkLoggedEvents(expected: [
            LoggedEvent(type: .connection, message: "closeCommunication(): communication closed"),
            LoggedEvent(type: .connection, message: "startCommunication(): connectionState: awaitingScan"),
            LoggedEvent(type: .connection, message: "scanForTransmitter(): connectionState: awaitingScan ->  scanning"),
            LoggedEvent(type: .connection, message: "startScanningForTransmitter(): communication started"),
        ])
    }
    
    // MARK: - Command Tests
    
    func testStartSessionCommandWithSensorCode() {
        let sensorCode = SensorCode(rawValue: "7171")
        cgmManager.startSession(sensorCode)
        
        let startSessionCmd = CgmCommand.start(uuid: expectedDeviceUUID,
                                               systemTime: expectedDate,
                                               transmitterTime: nil,
                                               code: sensorCode,
                                               response: nil)
        XCTAssertTrue(mockTxComm.isCurrentCommand(command: startSessionCmd))
        checkLoggedEvent(expected: LoggedEvent(type: .send, message: "startSession(_:): start(uuid: DCF262A8-B79D-424F-B970-02D4ACF3E149, systemTime: 1970-01-01 00:00:00 +0000, transmitterTime: nil, code: Optional(TransmitterCore.SensorCode.code7171), response: nil)"))
    }
    
    func testStartSessionCommandWithoutSensorCode() {
        cgmManager.startSession(nil)
        
        let startSessionCmd = CgmCommand.start(uuid: expectedDeviceUUID,
                                               systemTime: expectedDate,
                                               transmitterTime: nil,
                                               code: nil,
                                               response: nil)
        XCTAssertTrue(mockTxComm.isCurrentCommand(command: startSessionCmd))
        noLoggedEvent()
    }
    
    func testStopSessionCommand() {
        cgmManager.stopSession()
        
        let stopSessionCmd = CgmCommand.stop(uuid: UUID(),
                                             systemTime: expectedDate,
                                             transmitterTime: nil,
                                             response: nil)
        XCTAssertTrue(mockTxComm.isCurrentCommand(command: stopSessionCmd))
        checkLoggedEvent(expected: LoggedEvent(type: .send, message: "stopSession(): stop(uuid: DCF262A8-B79D-424F-B970-02D4ACF3E149, systemTime: 1970-01-01 00:00:00 +0000, transmitterTime: nil, response: nil)"))
    }
    
    func testCalibrationCommand() {
        let meterValue: Int32 = 80
        let calibrationValue = HKQuantity(unit: HKUnit.milligramsPerDeciliter, doubleValue: Double(meterValue))
        cgmManager.calibrate(value: calibrationValue)
        
        let calibrateCmd = CgmCommand.calibrate(uuid: UUID(),
                                                systemTime: expectedDate,
                                                transmitterTime: nil,
                                                meterValue: meterValue,
                                                response: nil)
        XCTAssertTrue(mockTxComm.isCurrentCommand(command: calibrateCmd))
        checkLoggedEvent(expected: LoggedEvent(type: .send, message: "calibrate(value:): calibrate(uuid: DCF262A8-B79D-424F-B970-02D4ACF3E149, systemTime: 1970-01-01 00:00:00 +0000, transmitterTime: nil, meterValue: 80, response: nil)"))
    }
    
    // MARK: - CGM Delegate Tests
    
    func testNewCGMData() {
        // create CGM data
        let cgmReading = TestTransmitterCGMReading(recordedSystemTime: expectedDate,
                                                   transmitterId: transmitterID,
                                                   egvTransmitterTime: TestTransmitterTime(),
                                                   egvSystemTime: expectedDate,
                                                   sequenceNumber: 1,
                                                   algorithmState: AlgorithmState.inCalibration,
                                                   egv: 80,
                                                   predictedEgv: nil,
                                                   rate: 1.0,
                                                   isBgGeneratedOnTransmitter: false,
                                                   aberrationState: AberrationState.none)
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: TestCalibrationBounds(),
                                              autoCalibration: AutoCalibrationState.auto,
                                              sessionDurationDays: 10,
                                              sensorWarmupDuration: 2)
        
        // trigger the new data callback
        mockTxComm.egvAvailable(transmitterData)
        
        let latestTransmitterData = cgmManager.latestTransmitterData!
        XCTAssertEqual(latestTransmitterData.transmitterId, transmitterData.transmitterId)
        XCTAssertNil(latestTransmitterData.session)
        XCTAssertEqual(latestTransmitterData.cgmReading.egv, transmitterData.cgmReading.egv)
        XCTAssertTrue(latestTransmitterData.backfillCgmReading.isEmpty)
        XCTAssertNil(latestTransmitterData.lastCalibration)
        XCTAssertEqual(latestTransmitterData.calibrationBounds as! TestCalibrationBounds, transmitterData.calibrationBounds as! TestCalibrationBounds)
        XCTAssertEqual(latestTransmitterData.autoCalibration, transmitterData.autoCalibration)
        XCTAssertEqual(latestTransmitterData.sessionDurationDays, transmitterData.sessionDurationDays)
        XCTAssertEqual(latestTransmitterData.sensorWarmupDuration, transmitterData.sensorWarmupDuration)
        checkLoggedEvent(expected: LoggedEvent(type: .receive, message: "onCgmData(_:): TransmitterData(dexcomTransmitterID: DexcomCGMKit.DexcomTransmitterID(transmitterID: \"8XXXXX\"), session: nil, cgmReading: DexcomCGMKitTests.TestTransmitterCGMReading(recordedSystemTime: 1970-01-01 00:00:00 +0000, transmitterId: \"8XXXXX\", egvTransmitterTime: DexcomCGMKitTests.TestTransmitterTime(seconds: 1), egvSystemTime: 1970-01-01 00:00:00 +0000, sequenceNumber: 1, algorithmState: TransmitterCore.AlgorithmState.inCalibration, egv: 80, predictedEgv: nil, rate: 1.0, isBgGeneratedOnTransmitter: false, aberrationState: TransmitterCore.AberrationState.none), backfillCgmReading: [], lastCalibration: nil, calibrationBounds: DexcomCGMKitTests.TestCalibrationBounds(bgWeight: 180.0, linearityBoundLow: 1, linearityBoundHigh: 10, wedgeBoundError0: 1, wedgeBoundError1: 10), autoCalibration: TransmitterCore.AutoCalibrationState.auto, sessionDurationDays: 10, sensorWarmupDuration: 2)"))
    }
    
    func testNewCalibration() {
        newEGVExpectation = expectation(description: "Calibration")
        
        // create CGM data
        let testTransmitterTime = TestTransmitterTime()
        let testTransmitterCalibration = TestTransmitterCalibration(calibrationSystemTime: expectedDate, calibrationTransmitterTime: testTransmitterTime)
        let cgmReadingUnReliable = TestTransmitterCGMReading(recordedSystemTime: expectedDate,
                                                             transmitterId: transmitterID,
                                                             egvTransmitterTime: testTransmitterTime,
                                                             egvSystemTime: expectedDate,
                                                             sequenceNumber: 1,
                                                             algorithmState: AlgorithmState.calibrationError1,
                                                             egv: 80,
                                                             predictedEgv: nil,
                                                             rate: 1.0,
                                                             isBgGeneratedOnTransmitter: false,
                                                             aberrationState: AberrationState.none)
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReadingUnReliable,
                                              backfillCgmReading: [],
                                              lastCalibration: testTransmitterCalibration,
                                              calibrationBounds: TestCalibrationBounds(),
                                              autoCalibration: AutoCalibrationState.auto,
                                              sessionDurationDays: 10,
                                              sensorWarmupDuration: 2)
        
        // trigger the new data callback
        mockTxComm.egvAvailable(transmitterData)
        
        wait(for: [newEGVExpectation], timeout: 1)
        XCTAssertNotNil(newEGVs)
        if let newEGVs = newEGVs {
            XCTAssertEqual(newEGVs[0].quantity.doubleValue(for: HKUnit.milligramsPerDeciliter), Double(TestTransmitterCalibration().meterValue))
        }
        checkLoggedEvent(expected: LoggedEvent(type: .receive, message: "onCgmData(_:): TransmitterData(dexcomTransmitterID: DexcomCGMKit.DexcomTransmitterID(transmitterID: \"8XXXXX\"), session: nil, cgmReading: DexcomCGMKitTests.TestTransmitterCGMReading(recordedSystemTime: 1970-01-01 00:00:00 +0000, transmitterId: \"8XXXXX\", egvTransmitterTime: DexcomCGMKitTests.TestTransmitterTime(seconds: 1), egvSystemTime: 1970-01-01 00:00:00 +0000, sequenceNumber: 1, algorithmState: TransmitterCore.AlgorithmState.calibrationError1, egv: 80, predictedEgv: nil, rate: 1.0, isBgGeneratedOnTransmitter: false, aberrationState: TransmitterCore.AberrationState.none), backfillCgmReading: [], lastCalibration: Optional(DexcomCGMKitTests.TestTransmitterCalibration(calibrationSystemTime: 1970-01-01 00:00:00 +0000, calibrationTransmitterTime: DexcomCGMKitTests.TestTransmitterTime(seconds: 1), meterValue: 65)), calibrationBounds: DexcomCGMKitTests.TestCalibrationBounds(bgWeight: 180.0, linearityBoundLow: 1, linearityBoundHigh: 10, wedgeBoundError0: 1, wedgeBoundError1: 10), autoCalibration: TransmitterCore.AutoCalibrationState.auto, sessionDurationDays: 10, sensorWarmupDuration: 2)"))
    }

    func testNewEGV() {
        newEGVExpectation = expectation(description: "GlucoseMeasurement")
        
        // create CGM data
        let cgmReading = TestTransmitterCGMReading(recordedSystemTime: expectedDate,
                                                   transmitterId: transmitterID,
                                                   egvTransmitterTime: TestTransmitterTime(),
                                                   egvSystemTime: expectedDate,
                                                   sequenceNumber: 1,
                                                   algorithmState: AlgorithmState.inCalibration,
                                                   egv: 80,
                                                   predictedEgv: nil,
                                                   rate: 1.0,
                                                   isBgGeneratedOnTransmitter: false,
                                                   aberrationState: AberrationState.none)
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: TestCalibrationBounds(),
                                              autoCalibration: AutoCalibrationState.auto,
                                              sessionDurationDays: 10,
                                              sensorWarmupDuration: 2)
        
        // trigger the new data callback
        mockTxComm.egvAvailable(transmitterData)
        
        wait(for: [newEGVExpectation], timeout: 1)
        XCTAssertNotNil(newEGVs)
        if let newEGVs = newEGVs {
            XCTAssertEqual(newEGVs[0].quantity.doubleValue(for: HKUnit.milligramsPerDeciliter), Double(cgmReading.egv))
        }
        checkLoggedEvent(expected: LoggedEvent(type: .receive, message: "onCgmData(_:): TransmitterData(dexcomTransmitterID: DexcomCGMKit.DexcomTransmitterID(transmitterID: \"8XXXXX\"), session: nil, cgmReading: DexcomCGMKitTests.TestTransmitterCGMReading(recordedSystemTime: 1970-01-01 00:00:00 +0000, transmitterId: \"8XXXXX\", egvTransmitterTime: DexcomCGMKitTests.TestTransmitterTime(seconds: 1), egvSystemTime: 1970-01-01 00:00:00 +0000, sequenceNumber: 1, algorithmState: TransmitterCore.AlgorithmState.inCalibration, egv: 80, predictedEgv: nil, rate: 1.0, isBgGeneratedOnTransmitter: false, aberrationState: TransmitterCore.AberrationState.none), backfillCgmReading: [], lastCalibration: nil, calibrationBounds: DexcomCGMKitTests.TestCalibrationBounds(bgWeight: 180.0, linearityBoundLow: 1, linearityBoundHigh: 10, wedgeBoundError0: 1, wedgeBoundError1: 10), autoCalibration: TransmitterCore.AutoCalibrationState.auto, sessionDurationDays: 10, sensorWarmupDuration: 2)"))
    }
    
    func testBackfillData() {
        newEGVExpectation = expectation(description: "Backfill")
        
        // create CGM data
        let backfillReading = TestTransmitterCGMReading(recordedSystemTime: expectedDate,
                                                   transmitterId: transmitterID,
                                                   egvTransmitterTime: TestTransmitterTime(),
                                                   egvSystemTime: expectedDate.addingTimeInterval(-30),
                                                   sequenceNumber: 1,
                                                   algorithmState: AlgorithmState.inCalibration,
                                                   egv: 80,
                                                   predictedEgv: nil,
                                                   rate: 1.0,
                                                   isBgGeneratedOnTransmitter: false,
                                                   aberrationState: AberrationState.none)
        let cgmReading = TestTransmitterCGMReading(recordedSystemTime: expectedDate,
                                                             transmitterId: transmitterID,
                                                             egvTransmitterTime: TestTransmitterTime(seconds: 30),
                                                             egvSystemTime: expectedDate,
                                                             sequenceNumber: 2,
                                                             algorithmState: AlgorithmState.inCalibration,
                                                             egv: 90,
                                                             predictedEgv: nil,
                                                             rate: 1.0,
                                                             isBgGeneratedOnTransmitter: false,
                                                             aberrationState: AberrationState.none)
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading,
                                              backfillCgmReading: [backfillReading],
                                              lastCalibration: nil,
                                              calibrationBounds: TestCalibrationBounds(),
                                              autoCalibration: AutoCalibrationState.auto,
                                              sessionDurationDays: 10,
                                              sensorWarmupDuration: 2)
        
        // trigger the new data callback
        mockTxComm.egvAvailable(transmitterData)
        
        wait(for: [newEGVExpectation], timeout: 1)
        XCTAssertNotNil(newEGVs)
        if let newEGVs = newEGVs {
            // the backfill EGV is provided after the current EGV, such that the current EGV is also provided to the user as soon as possible
            XCTAssertEqual(newEGVs[0].quantity.doubleValue(for: HKUnit.milligramsPerDeciliter), Double(cgmReading.egv))
            XCTAssertEqual(newEGVs[1].quantity.doubleValue(for: HKUnit.milligramsPerDeciliter), Double(backfillReading.egv))
        }
        checkLoggedEvent(expected: LoggedEvent(type: .receive, message: "onCgmData(_:): TransmitterData(dexcomTransmitterID: DexcomCGMKit.DexcomTransmitterID(transmitterID: \"8XXXXX\"), session: nil, cgmReading: DexcomCGMKitTests.TestTransmitterCGMReading(recordedSystemTime: 1970-01-01 00:00:00 +0000, transmitterId: \"8XXXXX\", egvTransmitterTime: DexcomCGMKitTests.TestTransmitterTime(seconds: 30), egvSystemTime: 1970-01-01 00:00:00 +0000, sequenceNumber: 2, algorithmState: TransmitterCore.AlgorithmState.inCalibration, egv: 90, predictedEgv: nil, rate: 1.0, isBgGeneratedOnTransmitter: false, aberrationState: TransmitterCore.AberrationState.none), backfillCgmReading: [DexcomCGMKitTests.TestTransmitterCGMReading(recordedSystemTime: 1970-01-01 00:00:00 +0000, transmitterId: \"8XXXXX\", egvTransmitterTime: DexcomCGMKitTests.TestTransmitterTime(seconds: 1), egvSystemTime: 1969-12-31 23:59:30 +0000, sequenceNumber: 1, algorithmState: TransmitterCore.AlgorithmState.inCalibration, egv: 80, predictedEgv: nil, rate: 1.0, isBgGeneratedOnTransmitter: false, aberrationState: TransmitterCore.AberrationState.none)], lastCalibration: nil, calibrationBounds: DexcomCGMKitTests.TestCalibrationBounds(bgWeight: 180.0, linearityBoundLow: 1, linearityBoundHigh: 10, wedgeBoundError0: 1, wedgeBoundError1: 10), autoCalibration: TransmitterCore.AutoCalibrationState.auto, sessionDurationDays: 10, sensorWarmupDuration: 2)"))
    }
    
    // MARK: - lifecycle (init, raw state init, deinit, etc)
    
    // MARK: - comms (scan, reconnect, close connection, replace transmitter, etc)
    func testStartCommunication() {
        cgmManager.startCommunication()
        
        // Should have called "close" on any existing connection.
        XCTAssertTrue(mockTxComm.closeCalled)

        // Should be in .awaitingScan.  Make sure it scans.
        XCTAssertEqual(.scanning, cgmManager.state.connectionState)
        
        // Should have called "start" on the connection.
        XCTAssertTrue(mockTxComm.startCalled)

        checkLoggedEvents(expected: [
            LoggedEvent(type: .connection, message: "closeCommunication(): communication closed"),
            LoggedEvent(type: .connection, message: "startCommunication(): connectionState: awaitingScan"),
            LoggedEvent(type: .connection, message: "scanForTransmitter(): connectionState: awaitingScan ->  scanning"),
            LoggedEvent(type: .connection, message: "startScanningForTransmitter(): communication started"),
        ])
    }
        
    func testReconnect() {
        let transmitterAuthenticationKey = "ABC123"
        var state = DexcomCGMManagerState(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!, connectionState: .awaitingReconnect)
        state.transmitterStaticInfo.maxRuntimeDays = 110 // G6 default value
        state.transmitterStaticInfo.sessionTimeDays = 10 // G6 default value
        state.transmitterStaticInfo.activatedOn = Date()
        state.authenticationKeyServiceIdentifier = transmitterID
        state.transmitterAuthenticationKey = transmitterAuthenticationKey
        state.transmitterAuthenticationKeyTimestamp = expectedDate
        XCTAssertEqual(transmitterAuthenticationKey, state.transmitterAuthenticationKey)
        XCTAssertNotNil(state.transmitterAuthenticationKeyTimestamp)
        setUpCgmManager(state)
        XCTAssertEqual(transmitterAuthenticationKey, cgmManager.state.transmitterAuthenticationKey)
        XCTAssertEqual(expectedDate, cgmManager.state.transmitterAuthenticationKeyTimestamp)

        cgmManager.startCommunication()
        
        // Should have called "close" on any existing connection.
        XCTAssertTrue(mockTxComm.closeCalled)

        // Should be in .awaitingReconnect.
        XCTAssertEqual(.awaitingReconnect, cgmManager.state.connectionState)
        
        // Should have called "start" on the connection.
        XCTAssertTrue(mockTxComm.startCalled)

        checkLoggedEvents(expected: [
            LoggedEvent(type: .connection, message: "closeCommunication(): communication closed"),
            LoggedEvent(type: .connection, message: "startCommunication(): connectionState: awaitingReconnect"),
            LoggedEvent(type: .connection, message: "startScanningForTransmitter(): communication started"),
        ])
    }
    
    func testStartCommunicationThenCloseCommunication() {
        cgmManager.startCommunication()
        waitOnMain()
        
        checkLoggedEvents(expected: [
            LoggedEvent(type: .connection, message: "closeCommunication(): communication closed"),
            LoggedEvent(type: .connection, message: "startCommunication(): connectionState: awaitingScan"),
            LoggedEvent(type: .connection, message: "scanForTransmitter(): connectionState: awaitingScan ->  scanning"),
            LoggedEvent(type: .connection, message: "startScanningForTransmitter(): communication started"),
        ])

        // reset everything because start also closes:
        mockTxComm.closeCalled = false
        clearLoggedEvents()
        alertIdentifiersRemoved = []
        
        cgmManager.closeCommunication()
        waitOnMain()
        
        XCTAssertTrue(mockTxComm.closeCalled)
        XCTAssertNil(mockTxComm.cgmCommandDelegate)
        XCTAssertNil(mockTxComm.cgmDelegate)
        XCTAssertNil(mockTxComm.connectionDelegate)
        checkAlertWasRemoved(forAlert: .transmitterNotFound)
        checkLoggedEvents(expected: [
            // The two "removeDeliveredAlert" logs here is expected.
            LoggedEvent(type: .delegate, message: "removeDeliveredAlert(identifier:): 15"),
            LoggedEvent(type: .delegate, message: "removeDeliveredAlert(identifier:): 15"),
            LoggedEvent(type: .connection, message: "closeCommunication(): communication closed"),
        ])
    }
    
    func testReplaceTransmitter() {
        cgmManager.replaceTransmitter(DexcomTransmitterID("8YYYYY")!)
        checkLoggedEvents(expected: [
            LoggedEvent(type: .connection, message: "replaceTransmitter(_:): replacing transmitter with DexcomTransmitterID(transmitterID: \"8YYYYY\")"),
            LoggedEvent(type: .connection, message: "closeCommunication(): communication closed"),
            LoggedEvent(type: .connection, message: "startCommunication(): connectionState: awaitingScan"),
            LoggedEvent(type: .connection, message: "scanForTransmitter(): connectionState: awaitingScan ->  scanning"),
            LoggedEvent(type: .connection, message: "startScanningForTransmitter(): communication started"),
            LoggedEvent(type: .connection, message: "replaceTransmitter(_:): started communication with transmitter replacement"),
        ])
        XCTAssertEqual("8YYYYY", cgmManager.state.transmitterStaticInfo.dexcomTransmitterID.transmitterID)
    }
    
    // MARK: - locked properties
    
    // MARK: - observers
    
    // MARK: - cgm manager delegation
    
    // MARK: - logging
    
    func testLoggingOnCriticalError() {
        cgmManager.onCriticalError("badness")
        checkLoggedEvent(expected: LoggedEvent(type: .error, message: "onCriticalError(_:): reason: badness"))
    }
    
    func testLoggingOnInfo() {
        cgmManager.onInfo("something happened")
        checkLoggedEvent(expected: LoggedEvent(type: .delegate, message: "onInfo(_:): something happened"))
    }
    
    func testLoggingOnLowBattery() {
        cgmManager.onTransmitterLowBattery()
        checkLoggedEvent(expected: LoggedEvent(type: .delegate, message: "onTransmitterLowBattery(): "))
    }
    
    func testLoggingOnTransmitterUnrecoverableError() {
        cgmManager.onTransmitterUnrecoverableError(1234)
        checkLoggedEvent(expected: LoggedEvent(type: .error, message: "onTransmitterUnrecoverableError(_:): statusCode: 1234"))
    }
}

// MARK: - CGM Manage Delegate Methods

extension DexcomCGMManagerTests: CGMManagerDelegate {
    func scheduleNotification(for manager: DeviceManager, identifier: String, content: UNNotificationContent, trigger: UNNotificationTrigger?) {
    }
    
    func clearNotification(for manager: DeviceManager, identifier: String) {
    }
    
    func removeNotificationRequests(for manager: DeviceManager, identifiers: [String]) {
    }
    
    func issueAlert(_ alert: DeviceAlert) {
        alertIdentifiersScheduled.append(alert.identifier)
    }
    
    func removePendingAlert(identifier: DeviceAlert.Identifier) {
        
    }
    
    func removeDeliveredAlert(identifier: DeviceAlert.Identifier) {
        alertIdentifiersRemoved.append(identifier)
    }
    
    func deviceManager(_ manager: DeviceManager, logEventForDeviceIdentifier deviceIdentifier: String?, type: DeviceLogEntryType, message: String, completion: ((Error?) -> Void)?) {
        loggedEvents.append(LoggedEvent(deviceIdentifier: deviceIdentifier, type: type, message: message))
        loggedEventExpectation.fulfill()
    }
    
    func startDateToFilterNewData(for manager: CGMManager) -> Date? { return nil }
    
    func cgmManager(_ manager: CGMManager, didUpdateWith result: CGMResult) {
        // used to test reporting of new glucose values (both egvs and calibration)
        switch result {
        case .newData(let glucoseArray):
            newEGVs = glucoseArray
            newEGVExpectation?.fulfill()
        default:
            break
        }
    }
    
    func cgmManagerWantsDeletion(_ manager: CGMManager) { }
    
    func cgmManagerDidUpdateState(_ manager: CGMManager) { }
    
    func credentialStoragePrefix(for manager: CGMManager) -> String { return "" }
}

struct TestAuthenticationConnectionData: AuthenticationConnectionDataProtocol {
    var authenticationKey: String = "testAuthenticationKey"
    var authenticationKeyTimestamp: Date = Date(timeIntervalSince1970: 2)
}

// MARK: - Log event test helpers
extension DexcomCGMManagerTests {
    
    private func checkLoggedEvent(expected: LoggedEvent, file: StaticString = #file, line: UInt = #line) {
        wait(for: [loggedEventExpectation], timeout: 1)
        XCTAssertEqual([expected], loggedEvents, file: file, line: line)
    }
    
    private func checkLoggedEvents(expected: [LoggedEvent], file: StaticString = #file, line: UInt = #line) {
        wait(for: [loggedEventExpectation], timeout: 1)
        XCTAssertEqual(expected, loggedEvents, file: file, line: line)
    }

    private func clearLoggedEvents() {
        loggedEvents = []
        loggedEventExpectation = self.expectation(description: "loggedEventExpect")
        loggedEventExpectation.assertForOverFulfill = false
    }
    
    private func noLoggedEvent(file: StaticString = #file, line: UInt = #line) {
        loggedEventExpectation.fulfill()
        wait(for: [loggedEventExpectation], timeout: 1)
        XCTAssertEqual(0, loggedEvents.count, file: file, line: line)
    }
    
    private func ignoreLoggedEvent() {
        wait(for: [loggedEventExpectation], timeout: 1)
    }
}

// MARK: - Notification check helpers
extension DexcomCGMManagerTests {
    
    private func checkAlertWasScheduled(forAlert alert: DexcomAlertType, file: StaticString = #file, line: UInt = #line) {
        XCTAssertEqual([DeviceAlert.Identifier(managerIdentifier: DexcomCGMManager.managerIdentifier, alertIdentifier: "\(alert.rawValue)")], alertIdentifiersScheduled, file: file, line: line)
        alertIdentifiersScheduled = []
    }
    
    private func checkAlertWasRemoved(forAlert alert: DexcomAlertType, count: Int = 1, file: StaticString = #file, line: UInt = #line) {
        let alertIdentifier = DeviceAlert.Identifier(managerIdentifier: DexcomCGMManager.managerIdentifier, alertIdentifier: "\(alert.rawValue)")
        XCTAssertEqual([DeviceAlert.Identifier](repeating: alertIdentifier, count: count), alertIdentifiersRemoved, file: file, line: line)
        alertIdentifiersRemoved = []
    }
    
}

extension XCTestCase {
    
    func waitOnMain() {
        let exp = expectation(description: "waitOnMain")
        DispatchQueue.main.async {
            exp.fulfill()
        }
        wait(for: [exp], timeout: 1.0)
    }

}
