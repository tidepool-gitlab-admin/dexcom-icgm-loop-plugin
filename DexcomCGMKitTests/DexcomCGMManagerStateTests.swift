//
//  DexcomCGMManagerStateTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-10-01.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
@testable import DexcomCGMKit

class DexcomCGMManagerStateTests: XCTestCase {
    private var transmitterID = "8XXXXX"
    private let deviceUUID = "deviceUUID"
    private let transmitterVersion = "1.0.1"
    private let softwareNumber: UInt32 = 1
    private let apiVersion: UInt32 = 2
    private let storageTimeDays: UInt32 = 30
    private let maxStorageTimeDays: UInt32 = 120
    private let maxRuntimeDays: UInt32 = 90
    private let sessionTimeDays: UInt32 = 10
    private let isDiagnosticDataDownloadSupported = true
    private let isEgvBackfillSupported = true
    private let isPredictedEgvSupported = true
    private let activatedOn = Date()
    private let authKeyServiceID = "DexcomKeyServiceIdentifier"
    private let keyTimestamp = Date()
    
    func testInitailizationNoConnectionState() {
        let cgmManagerState = DexcomCGMManagerState(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!)
        let transmitterInfo = cgmManagerState.transmitterStaticInfo
        XCTAssertEqual(transmitterInfo.transmitterId, transmitterID)
        XCTAssertEqual(transmitterInfo.deviceUUID, "")
        XCTAssertEqual(transmitterInfo.transmitterVersion, "")
        XCTAssertEqual(transmitterInfo.softwareNumber, 0)
        XCTAssertEqual(transmitterInfo.apiVersion, 0)
        XCTAssertEqual(transmitterInfo.storageTimeDays, 0)
        XCTAssertEqual(transmitterInfo.maxStorageTimeDays, 0)
        XCTAssertEqual(transmitterInfo.maxRuntimeDays, 0)
        XCTAssertEqual(transmitterInfo.sessionTimeDays, 0)
        XCTAssertFalse(transmitterInfo.isDiagnosticDataDownloadSupported)
        XCTAssertFalse(transmitterInfo.isEgvBackfillSupported)
        XCTAssertFalse(transmitterInfo.isPredictedEgvSupported)
        XCTAssertNil(transmitterInfo.activatedOn)
        XCTAssertEqual(cgmManagerState.connectionState, .awaitingScan)
    }
    
    func testInitailizationWithConnectionState() {
        let cgmManagerState = DexcomCGMManagerState(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!, connectionState: .scanning)
        let transmitterInfo = cgmManagerState.transmitterStaticInfo
        XCTAssertEqual(transmitterInfo.transmitterId, transmitterID)
        XCTAssertEqual(cgmManagerState.connectionState, .scanning)
    }
    
    func testRestoreFromRawValueValid() {
        let transmitterInfo = TransmitterStaticInfo(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                                    deviceUUID: deviceUUID,
                                                    transmitterVersion: transmitterVersion,
                                                    softwareNumber: softwareNumber,
                                                    apiVersion: apiVersion,
                                                    storageTimeDays: storageTimeDays,
                                                    maxStorageTimeDays: maxStorageTimeDays,
                                                    maxRuntimeDays: maxRuntimeDays,
                                                    sessionTimeDays: sessionTimeDays,
                                                    isDiagnosticDataDownloadSupported: isDiagnosticDataDownloadSupported,
                                                    isEgvBackfillSupported: isEgvBackfillSupported,
                                                    isPredictedEgvSupported: isPredictedEgvSupported,
                                                    activatedOn: activatedOn)
        let encodedTransmitterInfo = try! PropertyListEncoder().encode(transmitterInfo)
        let rawValue: [String:Any] = ["transmitterInfo": encodedTransmitterInfo, "transmitterKeyTimestamp": keyTimestamp, "authenticationKeyServiceIdentifier": authKeyServiceID]
        let cgmManagerState = DexcomCGMManagerState.init(rawValue: rawValue)!
        XCTAssertEqual(cgmManagerState.transmitterStaticInfo, transmitterInfo)
        XCTAssertEqual(cgmManagerState.authenticationKeyServiceIdentifier, authKeyServiceID)
        XCTAssertEqual(cgmManagerState.transmitterAuthenticationKeyTimestamp, keyTimestamp)
        XCTAssertEqual(cgmManagerState.connectionState, .awaitingReconnect)
    }
    
    func testRestoreFromRawValueInvalid() {
        let transmitterInfo = TransmitterStaticInfo(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                                    deviceUUID: deviceUUID,
                                                    transmitterVersion: transmitterVersion,
                                                    softwareNumber: softwareNumber,
                                                    apiVersion: apiVersion,
                                                    storageTimeDays: storageTimeDays,
                                                    maxStorageTimeDays: maxStorageTimeDays,
                                                    maxRuntimeDays: maxRuntimeDays,
                                                    sessionTimeDays: sessionTimeDays,
                                                    isDiagnosticDataDownloadSupported: isDiagnosticDataDownloadSupported,
                                                    isEgvBackfillSupported: isEgvBackfillSupported,
                                                    isPredictedEgvSupported: isPredictedEgvSupported,
                                                    activatedOn: activatedOn)
        let rawValue: [String:Any] = ["transmitterInfo": transmitterInfo, "transmitterKeyTimestamp": keyTimestamp, "authenticationKeyServiceIdentifier": authKeyServiceID]
        let cgmManagerState = DexcomCGMManagerState.init(rawValue: rawValue)
        XCTAssertNil(cgmManagerState)
    }
    
    func testRawValue() {
        var cgmManagerState = DexcomCGMManagerState(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!)
        cgmManagerState.authenticationKeyServiceIdentifier = authKeyServiceID
        let alertType = DexcomAlertType.glucoseLow
        let lowAlertConfiguration = DexcomAlertConfiguration(type: alertType,
                                                             enabled: true,
                                                             alwaysSoundEnabled: true,
                                                             sound: .defaultSound)
        cgmManagerState.userMutableAlertConfigurations = [.glucoseLow: lowAlertConfiguration]
        cgmManagerState.alertSchedule = DexcomAlertSchedule.defaultSchedule()
        let rawValue = cgmManagerState.rawValue
        let encodedTransmitterInfo = try! PropertyListEncoder().encode(cgmManagerState.transmitterStaticInfo)
        let encodedAlertConfigurations = try! PropertyListEncoder().encode(cgmManagerState.userMutableAlertConfigurations)
        let encodedAlertSchedule = try! PropertyListEncoder().encode(cgmManagerState.alertSchedule)
        XCTAssertEqual(encodedTransmitterInfo, rawValue["transmitterInfo"] as? Data)
        XCTAssertEqual(encodedAlertConfigurations, rawValue["userMutableAlertConfigurations"] as? Data)
        XCTAssertEqual(encodedAlertSchedule, rawValue["alertSchedule"] as? Data)
        XCTAssertNil(rawValue["transmitterKeyTimestamp"])
        XCTAssertEqual(authKeyServiceID, rawValue["authenticationKeyServiceIdentifier"] as? String )
    }
}
