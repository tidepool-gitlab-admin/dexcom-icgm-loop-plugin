//
//  FloatingPointTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import XCTest
@testable import DexcomCGMKit

class FloatingPointTests: XCTestCase {

    func testRoundedToTenths() {
        // average number (round down)
        var number = 12.34958
        XCTAssertEqual(number.roundedToTenths, 12.3)
        
        // average number (round up)
        number = 12.35958
        XCTAssertEqual(number.roundedToTenths, 12.4)
        
        // small number
        number = 0.1235958
        XCTAssertEqual(number.roundedToTenths, 0.1)
        
        // smaller number
        number = 0.0001235958
        XCTAssertEqual(number.roundedToTenths, 0)
        
        // larger number
        number = 12034
        XCTAssertEqual(number.roundedToTenths, 12034)
    }

}
