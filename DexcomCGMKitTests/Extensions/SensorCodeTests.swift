//
//  SensorCodeTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-11-21.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
import TransmitterCore
@testable import DexcomCGMKit

class SensorCodeTests: XCTestCase {

    func testSensorCodeLength() {
        XCTAssertEqual(SensorCode.codeLength, 4)
    }

}
