//
//  TransmitterCalibrationTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-09-25.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
import TransmitterCore
@testable import DexcomCGMKit

class TransmitterCalibrationTests: XCTestCase {
    
    func testGlucoseValue() {
        let transmitterCalibration = TestTransmitterCalibration()
        let glucoseValue: HKQuantity = transmitterCalibration.glucoseValue
        XCTAssertEqual(glucoseValue.doubleValue(for: HKUnit.milligramsPerDeciliter), 65)
    }
    
    func testIsDisplayOnly() {
        let transmitterCalibration = TestTransmitterCalibration()
        // calibration values are always for display only
        XCTAssertTrue(transmitterCalibration.isDisplayOnly)
    }
    
    func testSyncIdentifier() {
        let transmitterID = "8XXXXX"
        let transmitterCalibration = TestTransmitterCalibration()
        let syncID = transmitterCalibration.syncIdentifier(forTransmitter: transmitterID)
        XCTAssertEqual(syncID, "\(transmitterID) \(transmitterCalibration.calibrationTransmitterTime.seconds)")
    }
    
    func testIsValid() {
        let validCalibration = TestTransmitterCalibration()
        XCTAssertTrue(validCalibration.isValid)
        
        var invalidCalibrationValue = TestTransmitterCalibration()
        invalidCalibrationValue.meterValue = 0
        XCTAssertFalse(invalidCalibrationValue.isValid)
        
        var invalidCalibrationTime = TestTransmitterCalibration()
        invalidCalibrationTime.calibrationTransmitterTime = TestTransmitterTime(seconds: 0)
        XCTAssertFalse(invalidCalibrationValue.isValid)
    }
    
}
