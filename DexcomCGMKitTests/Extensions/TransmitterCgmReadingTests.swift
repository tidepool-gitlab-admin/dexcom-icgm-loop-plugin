//
//  TransmitterCgmReadingTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-10-01.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
import TransmitterCore
@testable import DexcomCGMKit

class TransmitterCgmReadingTests: XCTestCase {

    private var cgmReading: TestTransmitterCGMReading!
    private var transmitterID: String = "8XXXXX"
    private var autoCalibration = AutoCalibrationState.auto
    private var sensorDurationDays = 10
    private var sensorWarmupDurationHours = 2
    private var calibrationBounds = TestCalibrationBounds()
    
    override func setUp() {
        super.setUp()
        cgmReading = TestTransmitterCGMReading(recordedSystemTime: Date(),
                                               transmitterId: transmitterID,
                                               egvTransmitterTime: TestTransmitterTime(),
                                               egvSystemTime: Date(),
                                               sequenceNumber: 1,
                                               algorithmState: AlgorithmState.inCalibration,
                                               egv: 80,
                                               predictedEgv: nil,
                                               rate: 1.0,
                                               isBgGeneratedOnTransmitter: false,
                                               aberrationState: AberrationState.none)
    }
    
    func testGlucoseValueReliable() {
        let glucoseValue: HKQuantity = cgmReading.glucoseValue!
        XCTAssertEqual(glucoseValue.doubleValue(for: HKUnit.milligramsPerDeciliter), 80)
    }
    
    func testGlucoseValueUnReliable() {
        cgmReading.algorithmState = AlgorithmState.calibrationError1
        let glucoseValue = cgmReading.glucoseValue
        XCTAssertNil(glucoseValue)
    }
    
    func testTrendRateValid() {
        let trendRate: HKQuantity = cgmReading.trendRate!
        XCTAssertEqual(trendRate.doubleValue(for: HKUnit.milligramsPerDeciliterPerMinute), 1.0)
    }
    
    func testTrendRateInvalid() {
        cgmReading.rate = Double.nan
        let trendRate = cgmReading.trendRate
        XCTAssertNil(trendRate)
    }
    
    func testTrendRateInvalid12_7() {
        cgmReading.rate = 12.7
        let trendRate = cgmReading.trendRate
        XCTAssertNil(trendRate)
    }
    
    func testTrendRateInvalid12_7Close() {
        cgmReading.rate = 12.700000000000001
        let trendRate = cgmReading.trendRate
        XCTAssertNil(trendRate)
    }

    func testIsDisplayOnly() {
        // cgm reading values are never used for calibration. So always false
        XCTAssertFalse(cgmReading.isDisplayOnly)
    }

    func testSyncIdentifier() {
        XCTAssertEqual(cgmReading.syncIdentifier, "\(transmitterID) \(TestTransmitterTime().seconds)")
    }
    
}
