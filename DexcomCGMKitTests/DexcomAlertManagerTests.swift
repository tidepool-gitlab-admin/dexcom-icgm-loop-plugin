//
//  DexcomAlertManagerTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-11-21.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
import TransmitterCore
import GlucoseCore
@testable import DexcomCGMKit

class DexcomAlertManagerTests: XCTestCase {

    private var updatedConfigurationsExpectation: XCTestExpectation!
    private var alertTriggeredExpectation: XCTestExpectation!
    private var newConfigurations: [DexcomAlertConfiguration]!
    private var content: UNMutableNotificationContent!
    private var identifier: String!
    private var configurations: [DexcomAlertConfiguration]!
    private var cgmReading: TestTransmitterCGMReading!
    private let expectedIdentifier = DexcomAlertManager.cgmAlertNotificationIdentifier
    private var sessionsRemaining: Int?
    
    override func setUp() {
        configurations = []
        for alertType in DexcomAlertType.allCases {
            let configuration = DexcomAlertConfiguration(type: alertType, enabled: true, alwaysSoundEnabled: false, sound: .defaultSound)
            configurations.append(configuration)
        }
        cgmReading = TestTransmitterCGMReading(recordedSystemTime: Date(),
                                               transmitterId: "8YYYYY",
                                               egvTransmitterTime: TestTransmitterTime(),
                                               egvSystemTime: Date(),
                                               sequenceNumber: 1,
                                               algorithmState: AlgorithmState.inCalibration,
                                               egv: 120,
                                               predictedEgv: nil,
                                               rate: 1.0,
                                               isBgGeneratedOnTransmitter: false,
                                               aberrationState: AberrationState.none)
        updatedConfigurationsExpectation = expectation(description: "New Configurations")
    }
    
    func testInitialization() {
        let alertManager = DexcomAlertManager()
        XCTAssertTrue(alertManager.alwaysSoundEnabled)
        alertManager.delegate = self
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        XCTAssertNotNil(newConfigurations)
        XCTAssertEqual(newConfigurations.count, 7)
        XCTAssertTrue(newConfigurations.contains(where: { $0.type == DexcomAlertType.glucoseLow }))
        XCTAssertTrue(newConfigurations.contains(where: { $0.type == DexcomAlertType.glucoseUrgentLow }))
        XCTAssertTrue(newConfigurations.contains(where: { $0.type == DexcomAlertType.glucoseHigh }))
        XCTAssertTrue(newConfigurations.contains(where: { $0.type == DexcomAlertType.glucoseRateRise }))
        XCTAssertTrue(newConfigurations.contains(where: { $0.type == DexcomAlertType.glucoseRateFall }))
        XCTAssertTrue(newConfigurations.contains(where: { $0.type == DexcomAlertType.signalLoss }))
        XCTAssertTrue(newConfigurations.contains(where: { $0.type == DexcomAlertType.noReadings }))
    }
    
    func testRestoreConfigurableAlertConfigurations() {
        configurations.remove(at: 3)
        configurations.remove(at: 0)
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        XCTAssertNotNil(newConfigurations)
        XCTAssertEqual(newConfigurations.count, DexcomAlertType.allCases.count-2)
        for (index, alertType) in DexcomAlertType.allCases.enumerated() {
            if index == 0 || index == 3 {
                XCTAssertFalse(newConfigurations.contains(where: { $0.type == alertType }))
            } else {
                XCTAssertTrue(newConfigurations.contains(where: { $0.type == alertType }))
            }
        }
    }
    
    func testAlertGlucoseUrgentLow() {
        alertTriggeredExpectation = expectation(description: "Alert")
        let alertType = DexcomAlertType.glucoseUrgentLow
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        cgmReading.egv = 40
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertGlucoseLow() {
        alertTriggeredExpectation = expectation(description: "Alert")
        let alertType = DexcomAlertType.glucoseLow
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        cgmReading.egv = 70
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertGlucoseHigh() {
        alertTriggeredExpectation = expectation(description: "Alert")
        let alertType = DexcomAlertType.glucoseHigh
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        cgmReading.egv = 400
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertRateRising() {
        alertTriggeredExpectation = expectation(description: "Alert")
        let alertType = DexcomAlertType.glucoseRateRise
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        cgmReading.rate = 4.0
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertRateFalling() {
        alertTriggeredExpectation = expectation(description: "Alert")
        let alertType = DexcomAlertType.glucoseRateFall
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        cgmReading.rate = -4.0
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertSignalLoss() {
        alertTriggeredExpectation = expectation(description: "Alert")
        let alertType = DexcomAlertType.signalLoss
        var signalLossConfiguration = DexcomAlertConfiguration(type: alertType, enabled: true, alwaysSoundEnabled: false, sound: .defaultSound)
        signalLossConfiguration.threshold = HKQuantity(unit: signalLossConfiguration.type.unit, doubleValue: 0)
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.updateAlert(withConfiguration: signalLossConfiguration)
        alertManager.delegate = self
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertNoReadings() {
        alertTriggeredExpectation = expectation(description: "Alert No Readings First Detection")
        let alertType = DexcomAlertType.noReadings
        var noReadingsConfiguration = DexcomAlertConfiguration(type: alertType, enabled: true, alwaysSoundEnabled: false, sound: .defaultSound)
        // allow the no readings alert to re-alert right away
        noReadingsConfiguration.alertFrequency = DexcomAlertFrequency(timeInterval: TimeInterval(seconds: 0))
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.updateAlert(withConfiguration: noReadingsConfiguration)
        alertManager.delegate = self
        // set no readings alert delay value is 20 mins
        XCTAssertEqual(alertManager.noReadingsAlertDelay, TimeInterval(minutes: 20))
        // set no readings alert delay in the past to allow fasting testing
        alertManager.noReadingsAlertDelay = TimeInterval(minutes: -1)
        cgmReading.algorithmState = AlgorithmState.temporarySensorIssue
        cgmReading.aberrationState = AberrationState.powerAberration
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
        alertTriggeredExpectation = expectation(description: "Alert No Readings Re-alerting")
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }

    func testAlertWarmupComplete() {
        alertTriggeredExpectation = expectation(description: "Alert")
        // for some reason the alert is trigger twice back to back
        alertTriggeredExpectation.expectedFulfillmentCount = 2
        let alertType = DexcomAlertType.warmupComplete
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        cgmReading.algorithmState = AlgorithmState.sensorWarmup
        alertManager.newCGMReading(cgmReading)
        cgmReading.algorithmState = AlgorithmState.inCalibration
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }

    func testAlertSessionExpired() {
        alertTriggeredExpectation = expectation(description: "Alert")
        // for some reason the alert is trigger twice back to back
        alertTriggeredExpectation.expectedFulfillmentCount = 2
        let alertType = DexcomAlertType.sensorExpired
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        let sessionPeriod = CGMSessionPeriodStruct(currentTime: Date(), sessionStartTime: Date(), sessionDuration: Int(TimeInterval(days: 10)))
        cgmReading.algorithmState = AlgorithmState.inCalibration
        alertManager.newCGMReading(cgmReading, forSession: sessionPeriod)
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertSessionExpiresSoon24H() {
        alertTriggeredExpectation = expectation(description: "Alert")
        // for some reason the alert is trigger twice back to back
        alertTriggeredExpectation.expectedFulfillmentCount = 2
        let alertType = DexcomAlertType.sensorExpiresSoon24H
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        let sessionPeriod = CGMSessionPeriodStruct(currentTime: Date(), sessionStartTime: Date(), sessionDuration: Int(TimeInterval(hours: 23)))
        cgmReading.algorithmState = AlgorithmState.inCalibration
        alertManager.newCGMReading(cgmReading, forSession: sessionPeriod)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertSessionExpiresSoon6H() {
        alertTriggeredExpectation = expectation(description: "Alert")
        // for some reason the alert is trigger twice back to back
        alertTriggeredExpectation.expectedFulfillmentCount = 2
        let alertType = DexcomAlertType.sensorExpiresSoon6H
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        let sessionPeriod = CGMSessionPeriodStruct(currentTime: Date(), sessionStartTime: Date(), sessionDuration: Int(TimeInterval(hours: 5)))
        cgmReading.algorithmState = AlgorithmState.inCalibration
        alertManager.newCGMReading(cgmReading, forSession: sessionPeriod)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertSessionExpiresSoon2H() {
        alertTriggeredExpectation = expectation(description: "Alert")
        // for some reason the alert is trigger twice back to back
        alertTriggeredExpectation.expectedFulfillmentCount = 2
        let alertType = DexcomAlertType.sensorExpiresSoon2H
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        let sessionPeriod = CGMSessionPeriodStruct(currentTime: Date(), sessionStartTime: Date(), sessionDuration: Int(TimeInterval(minutes: 110)))
        cgmReading.algorithmState = AlgorithmState.inCalibration
        alertManager.newCGMReading(cgmReading, forSession: sessionPeriod)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertSessionExpiresSoon30M() {
        alertTriggeredExpectation = expectation(description: "Alert")
        // for some reason the alert is trigger twice back to back
        alertTriggeredExpectation.expectedFulfillmentCount = 2
        let alertType = DexcomAlertType.sensorExpiresSoon30M
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        let sessionPeriod = CGMSessionPeriodStruct(currentTime: Date(), sessionStartTime: Date(), sessionDuration: Int(TimeInterval(minutes: 20)))
        cgmReading.algorithmState = AlgorithmState.inCalibration
        alertManager.newCGMReading(cgmReading, forSession: sessionPeriod)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertFailedSensor() {
        alertTriggeredExpectation = expectation(description: "Alert")
        // for some reason the alert is trigger twice back to back
        alertTriggeredExpectation.expectedFulfillmentCount = 2
        let alertType = DexcomAlertType.sensorFailed
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        cgmReading.algorithmState = AlgorithmState.sensorFailedDueToCountsAberration
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertFailedTransmitter() {
        alertTriggeredExpectation = expectation(description: "Alert")
        let alertType = DexcomAlertType.transmitterFailed
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        cgmReading.algorithmState = AlgorithmState.sessionFailedDueToTransmitterError
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }
    
    func testAlertTransmitterEOLSessionStarted() {
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        
        let alertTypes = [DexcomAlertType.sessionsRemaining3, DexcomAlertType.sessionsRemaining2, DexcomAlertType.lastSessionStarted]
        for alertType in alertTypes {
            alertTriggeredExpectation = expectation(description: "Alert")
            switch alertType {
            case .sessionsRemaining3:
                sessionsRemaining = 3
            case .sessionsRemaining2:
                sessionsRemaining = 2
            case .lastSessionStarted:
                sessionsRemaining = 1
            default:
                sessionsRemaining = nil
            }
                        
            cgmReading.algorithmState = AlgorithmState.sessionStopped
            alertManager.newCGMReading(cgmReading)
            cgmReading.algorithmState = AlgorithmState.sensorWarmup
            alertManager.newCGMReading(cgmReading)
            wait(for: [alertTriggeredExpectation], timeout: 1)
            XCTAssertEqual(identifier, expectedIdentifier)
            XCTAssertEqual(content.title, alertType.localizedTitle)
            XCTAssertEqual(content.body, alertType.localizedMessage)
        }
    }
    
    func testAlertTransmitterEOLSessionEnded() {
        alertTriggeredExpectation = expectation(description: "Alert")
        let alertType = DexcomAlertType.lastSessionEnded
        let alertManager = DexcomAlertManager(mutableAlertConfigurations: configurations)
        alertManager.delegate = self
        sessionsRemaining = 0
        cgmReading.algorithmState = AlgorithmState.inCalibration
        alertManager.newCGMReading(cgmReading)
        cgmReading.algorithmState = AlgorithmState.sessionStopped
        alertManager.newCGMReading(cgmReading)
        wait(for: [alertTriggeredExpectation, updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(content.title, alertType.localizedTitle)
        XCTAssertEqual(content.body, alertType.localizedMessage)
    }

}

extension DexcomAlertManagerTests: DexcomAlertManagerDelegate {
    func remainingSessionsForTransmitter() -> Int? {
        return sessionsRemaining
    }
    
    func scheduleNotification(withIdentifier identifier: String, content: UNMutableNotificationContent, trigger: UNTimeIntervalNotificationTrigger?) {
        if trigger == nil {
            self.content = content
            self.identifier = identifier
            alertTriggeredExpectation.fulfill()
        }
    }
    
    func alertConfigurationsUpdated(_ mutableAlertConfigurations: [DexcomAlertConfiguration]) {
        newConfigurations = mutableAlertConfigurations
        updatedConfigurationsExpectation.fulfill()
    }
    
    func removeNotificationRequests(withIdentifiers identifiers: [String]) {
        XCTAssertEqual(identifiers, [DexcomAlertManager.cgmAlertNotificationIdentifier])
    }
    
}
