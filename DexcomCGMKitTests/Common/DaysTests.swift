//
//  DaysTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-11-26.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
@testable import DexcomCGMKit

class DaysTests: XCTestCase {
    
    func testEveryDay() {
        let sunMonTueWedThuFriSat: Days = [.sunday, .monday, .tuesday, .wednesday, .thursday, .friday, .saturday]
        let everyday = Days.everyday
        XCTAssertTrue(everyday.contains(.sunday))
        XCTAssertTrue(everyday.contains(.monday))
        XCTAssertTrue(everyday.contains(.tuesday))
        XCTAssertTrue(everyday.contains(.wednesday))
        XCTAssertTrue(everyday.contains(.thursday))
        XCTAssertTrue(everyday.contains(.friday))
        XCTAssertTrue(everyday.contains(.saturday))
        XCTAssertEqual(everyday.localizedValue, "Every day")
        XCTAssertEqual(everyday, sunMonTueWedThuFriSat)
    }
    
    func testWeekdays() {
        let monTueWedThuFri: Days = [.monday, .tuesday, .wednesday, .thursday, .friday]
        let weekdays: Days = .weekdays
        XCTAssertFalse(weekdays.contains(.sunday))
        XCTAssertTrue(weekdays.contains(.monday))
        XCTAssertTrue(weekdays.contains(.tuesday))
        XCTAssertTrue(weekdays.contains(.wednesday))
        XCTAssertTrue(weekdays.contains(.thursday))
        XCTAssertTrue(weekdays.contains(.friday))
        XCTAssertFalse(weekdays.contains(.saturday))
        XCTAssertEqual(weekdays.localizedValue, "Weekdays")
        XCTAssertEqual(weekdays, monTueWedThuFri)
    }
    
    func testWeekend() {
        let sunSat: Days = [.sunday, .saturday]
        let weekend: Days = .weekend
        XCTAssertTrue(weekend.contains(.sunday))
        XCTAssertFalse(weekend.contains(.monday))
        XCTAssertFalse(weekend.contains(.tuesday))
        XCTAssertFalse(weekend.contains(.wednesday))
        XCTAssertFalse(weekend.contains(.thursday))
        XCTAssertFalse(weekend.contains(.friday))
        XCTAssertTrue(weekend.contains(.saturday))
        XCTAssertEqual(weekend.localizedValue, "Weekend")
        XCTAssertEqual(weekend, sunSat)
    }
    
    func testNoDaysSelected() {
        let noDays: Days = []
        XCTAssertFalse(noDays.contains(.sunday))
        XCTAssertFalse(noDays.contains(.monday))
        XCTAssertFalse(noDays.contains(.tuesday))
        XCTAssertFalse(noDays.contains(.wednesday))
        XCTAssertFalse(noDays.contains(.thursday))
        XCTAssertFalse(noDays.contains(.friday))
        XCTAssertFalse(noDays.contains(.saturday))
        XCTAssertEqual(noDays.localizedValue, "No days selected")
    }
    
    func testSunMonTueLocalizedValue() {
        let days: Days = [.sunday, .monday, .tuesday]
        XCTAssertEqual(days.localizedValue, "Sun Mon Tue")
    }
    
    func testTueThuFriLocalizedValue() {
        let days: Days = [.friday, .thursday, .tuesday]
        XCTAssertEqual(days.localizedValue, "Tue Thu Fri")
    }
    
    func testSunWedThuSatLocalizedValue() {
        let days: Days = [.thursday, .saturday, .sunday, .wednesday]
        XCTAssertEqual(days.localizedValue, "Sun Wed Thu Sat")
    }
    
    func testContainsDayOfDate() {
        let days: Days = [.sunday, .wednesday]
        let today = Date()
        let sunday = today.previous(.sunday)
        let thursday = today.next(.thursday)
        XCTAssertTrue(days.contains(dayOfDate: sunday))
        XCTAssertFalse(days.contains(dayOfDate: thursday))
    }
    
    func testContainsYesterdayOfDate() {
        let days: Days = [.sunday, .wednesday]
        let today = Date()
        let sunday = today.previous(.sunday)
        let thursday = today.next(.thursday)
        XCTAssertFalse(days.contains(yesterdayOfDate: sunday))
        XCTAssertTrue(days.contains(yesterdayOfDate: thursday))
    }
    
    func testExpandingDays() {
        let days: Days = [.sunday, .wednesday]
        let daysExpanded: Set<Days> = [.sunday, .wednesday]
        XCTAssertEqual(days.expandOptions(), daysExpanded)
    }
    
    
    func testCombiningDays() {
        let daysExpanded: Set<Days> = [.monday, .saturday]
        let daysCombined: Days = [.monday, .saturday]
        XCTAssertEqual(Days.combineOptions(daysExpanded), daysCombined)
    }
    
    func testDescriptionOfDays() {
        let multipleDays: Days = [.sunday, .wednesday]
        let singleDay: Days = [.monday]
        XCTAssertEqual(String(describing: multipleDays), "Sunday Wednesday")
        XCTAssertEqual(String(describing: singleDay), "Monday")
    }
}
