//
//  WeeklyScheduleTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-11-27.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
@testable import DexcomCGMKit

class WeeklyScheduleTests: XCTestCase {

    func testWeeklyScheduleSameDay() {
        let calendar = Calendar(identifier: .gregorian)
        let today = Date()
        let seconds = 0
        let minutes = 10
        let startHour = 14
        let endHour = 20
        let duration = endHour - startHour
        let startTime = calendar.date(bySettingHour: startHour, minute: minutes, second: seconds, of: today)!
        let endTime = calendar.date(bySettingHour: endHour, minute: minutes, second: seconds, of: today)!
        let startTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: startTime)
        let endTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: endTime)
        
        let days: Days = [.sunday, .tuesday]
        let weeklySchedule = WeeklySchedule(days: days, startTimeComponents: startTimeComponents, endTimeComponents: endTimeComponents)
        
        let aSunday = today.previous(.sunday)
        let aSundayInScheduleStart = calendar.date(bySettingHour: startHour, minute: minutes, second: seconds, of: aSunday)!
        let aSundayInScheduleMiddle = calendar.date(bySettingHour: startHour + duration/2, minute: minutes, second: seconds, of: aSunday)!
        let aSundayInScheduleEnd = calendar.date(bySettingHour: endHour, minute: minutes, second: seconds, of: aSunday)!
        let aSundayOutOfScheduleEarlier = calendar.date(bySettingHour: startHour, minute: minutes - 1, second: seconds, of: aSunday)!
        let aMonday = today.previous(.monday)
        let aTuesday = today.next(.tuesday)
        let aTuesdayInSchedule = calendar.date(bySettingHour: startHour, minute: minutes + 2, second: seconds, of: aTuesday)!
        let aTuesdayOutOfScheduleLater = calendar.date(bySettingHour: endHour, minute: minutes + 1, second: seconds, of: aTuesday)!
        
        XCTAssertEqual(calendar.dateComponents([.hour, .minute], from: weeklySchedule.startTime(usingCalendar: calendar)), startTimeComponents)
        XCTAssertEqual(calendar.dateComponents([.hour, .minute], from: weeklySchedule.endTime(usingCalendar: calendar)), endTimeComponents)
        
        XCTAssertEqual(weeklySchedule.days, days)

        XCTAssertTrue(weeklySchedule.contains(aSundayInScheduleStart, usingCalendar: calendar))
        XCTAssertTrue(weeklySchedule.contains(aSundayInScheduleMiddle, usingCalendar: calendar))
        XCTAssertTrue(weeklySchedule.contains(aSundayInScheduleEnd, usingCalendar: calendar))
        XCTAssertFalse(weeklySchedule.contains(aSundayOutOfScheduleEarlier, usingCalendar: calendar))
        XCTAssertFalse(weeklySchedule.contains(aMonday, usingCalendar: calendar))
        XCTAssertTrue(weeklySchedule.contains(aTuesdayInSchedule, usingCalendar: calendar))
        XCTAssertFalse(weeklySchedule.contains(aTuesdayOutOfScheduleLater, usingCalendar: calendar))
    }
    
    func testWeeklyScheduleOvernight() {
        let calendar = Calendar(identifier: .gregorian)
        let today = Date()
        let seconds = 0
        let minutes = 10
        let startHour = 20
        let middleHour = 2
        let endHour = 8
        let startTime = calendar.date(bySettingHour: startHour, minute: minutes, second: seconds, of: today)!
        let endTime = calendar.date(bySettingHour: endHour, minute: minutes, second: seconds, of: today)!
        let startTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: startTime)
        let endTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: endTime)
        
        let days: Days = [.sunday]
        let weeklySchedule = WeeklySchedule(days: days, startTimeComponents: startTimeComponents, endTimeComponents: endTimeComponents)

        let aSunday = today.previous(.sunday)
        let aSundayInScheduleStart = calendar.date(bySettingHour: startHour, minute: minutes, second: seconds, of: aSunday)!
        let aSundayOutOfScheduleEarlier = calendar.date(bySettingHour: startHour, minute: minutes - 1, second: seconds, of: aSunday)!
        let aMonday = today.previous(.monday)
        let aMondayInScheduleMiddle = calendar.date(bySettingHour: middleHour, minute: minutes, second: seconds, of: aMonday)!
        let aMondayInScheduleEnd = calendar.date(bySettingHour: endHour, minute: minutes, second: seconds, of: aMonday)!
        let aMondayOutOfScheduleLater = calendar.date(bySettingHour: endHour, minute: minutes + 1, second: seconds, of: aMonday)!
        let aTuesday = today.next(.tuesday)
        
        XCTAssertEqual(calendar.dateComponents([.hour, .minute], from: weeklySchedule.startTime(usingCalendar: calendar)), startTimeComponents)
        XCTAssertEqual(calendar.dateComponents([.hour, .minute], from: weeklySchedule.endTime(usingCalendar: calendar)), endTimeComponents)
        
        XCTAssertEqual(weeklySchedule.days, days)

        XCTAssertTrue(weeklySchedule.contains(aSundayInScheduleStart, usingCalendar: calendar))
        XCTAssertFalse(weeklySchedule.contains(aSundayOutOfScheduleEarlier, usingCalendar: calendar))
        XCTAssertTrue(weeklySchedule.contains(aMondayInScheduleMiddle, usingCalendar: calendar))
        XCTAssertTrue(weeklySchedule.contains(aMondayInScheduleEnd, usingCalendar: calendar))
        XCTAssertFalse(weeklySchedule.contains(aMondayOutOfScheduleLater, usingCalendar: calendar))
        XCTAssertFalse(weeklySchedule.contains(aTuesday, usingCalendar: calendar))
    }

    
    func testEquality() {
        let calendar = Calendar(identifier: .gregorian)
        let today = Date()
        let originalSeconds = 0
        let originalminutes = 10
        let originalStartHour = 20
        let originalEndHour = 8
        var startTime = calendar.date(bySettingHour: originalStartHour, minute: originalminutes, second: originalSeconds, of: today)!
        var endTime = calendar.date(bySettingHour: originalEndHour, minute: originalminutes, second: originalSeconds, of: today)!
        var startTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: startTime)
        var endTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: endTime)
        
        let originalDays: Days = [.sunday]
        let sameSchedule1 = WeeklySchedule(days: originalDays, startTimeComponents: startTimeComponents, endTimeComponents: endTimeComponents)
        var sameSchedule2 = WeeklySchedule(days: originalDays, startTimeComponents: startTimeComponents, endTimeComponents: endTimeComponents)
        XCTAssertEqual(sameSchedule1, sameSchedule2)
        
        // change seconds (still equal since the user can only select minutes and hours)
        startTime = calendar.date(bySettingHour: originalStartHour, minute: originalminutes, second: originalSeconds + 1, of: today)!
        startTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: startTime)
        sameSchedule2 = WeeklySchedule(days: originalDays, startTimeComponents: startTimeComponents, endTimeComponents: endTimeComponents)
        XCTAssertEqual(sameSchedule1, sameSchedule2)
        
        // different days
        let differentDays: Days = [.monday, .tuesday]
        var differentSchedule = WeeklySchedule(days: differentDays, startTimeComponents: startTimeComponents, endTimeComponents: endTimeComponents)
        XCTAssertNotEqual(sameSchedule1, differentSchedule)
        
        
        // different start time
        startTime = calendar.date(bySettingHour: originalStartHour, minute: originalminutes + 1, second: originalSeconds, of: today)!
        startTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: startTime)
        differentSchedule = WeeklySchedule(days: originalDays, startTimeComponents: startTimeComponents, endTimeComponents: endTimeComponents)
        XCTAssertNotEqual(sameSchedule1, differentSchedule)
        
        // different end time
        endTime = calendar.date(bySettingHour: originalEndHour + 1, minute: originalminutes, second: originalSeconds, of: today)!
        endTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: endTime)
        differentSchedule = WeeklySchedule(days: originalDays, startTimeComponents: startTimeComponents, endTimeComponents: endTimeComponents)
        XCTAssertNotEqual(sameSchedule1, differentSchedule)
    }
    

}
