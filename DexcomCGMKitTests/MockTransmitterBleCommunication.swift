//
//  MockTransmitterBleCommunication.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-10-01.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation
import DexcomCGMKit
import TransmitterCore

extension CgmCommand: Equatable {
    public static func == (lhs: CgmCommand, rhs: CgmCommand) -> Bool {
        switch (lhs, rhs) {
        case (.stop(_), .stop(_)) :
            return true
        case let (.start(a), .start(b)):
            return a.code == b.code
        case let (.calibrate(a), .calibrate(b)):
            return a.meterValue == b.meterValue
        default:
            return false
        }
    }
}

public class MockTransmitterBleCommunication: TransmitterBleCommunicationProtocol {
    
    public var commandQueue: [CgmCommand] = []
    
    public var connectionDelegate: ConnectionDelegate?
    
    public var cgmDelegate: CgmDelegate?
    
    public var debugMessageDelegate: DebugMessageDelegate?
    
    public var cgmCommandDelegate: CgmCommandDelegate?
    
    public var transmitterStatusDelegate: TransmitterStatusDelegate?
    
    public var communicationStreamQueue: DispatchQueue

    var startCalled = false
    public func start() throws {
        startCalled = true
    }
    
    var closeCalled = false
    public func close() {
        closeCalled = true
    }
    
    public func addCgmCommand(_ command: CgmCommand) {
        commandQueue.append(command)
    }
    
    public func removeCgmCommand(_ command: CgmCommand) {
        if let index = commandQueue.firstIndex(of: command) {
            commandQueue.remove(at: index)
        }
    }

    func isCurrentCommand(command: CgmCommand) -> Bool {
        return command == commandQueue.first
    }
    
    init(communicationStreamQueue: DispatchQueue = .main) {
        self.communicationStreamQueue = communicationStreamQueue
    }
    
    func connectionEstablished() {
        connectionDelegate?.onConnect()
    }
    
    func authenticationDataAvailable(_ authenticationConnectionData: AuthenticationConnectionDataProtocol) {
        connectionDelegate?.onAuthenticationConnectionData(authenticationConnectionData)
    }
    
    func transmitterFound(_ transmitterStaticInfo: TransmitterInfo) {
        connectionDelegate?.onTransmitterFound(transmitterStaticInfo)
    }
    
    func egvAvailable(_ cgmData: TransmitterCgmData) {
        cgmDelegate?.onCgmData(cgmData)
    }
    
}
