//
//  TestTransmitterTime.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import TransmitterCore

struct TestTransmitterTime: TransmitterTime {
    var seconds: UInt
    init(seconds: UInt = 1) {
        self.seconds = seconds
    }
}
