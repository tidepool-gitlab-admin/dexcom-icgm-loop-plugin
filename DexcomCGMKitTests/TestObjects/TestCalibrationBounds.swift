//
//  TestCalibrationBounds.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import TransmitterCore

struct TestCalibrationBounds: CalibrationBounds, Equatable {
    var bgWeight: Double = 180.0
    var linearityBoundLow: Int = 1
    var linearityBoundHigh: Int = 10
    var wedgeBoundError0: Int = 1
    var wedgeBoundError1: Int = 10
}
