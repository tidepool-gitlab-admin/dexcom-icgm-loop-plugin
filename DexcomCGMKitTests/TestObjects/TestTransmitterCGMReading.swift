//
//  TestTransmitterCGMReading.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import TransmitterCore

struct TestTransmitterCGMReading: TransmitterCgmReading, Equatable {
    static func == (lhs: TestTransmitterCGMReading, rhs: TestTransmitterCGMReading) -> Bool {
        return lhs.recordedSystemTime == rhs.recordedSystemTime &&
            lhs.transmitterId == rhs.transmitterId &&
            lhs.egvTransmitterTime.seconds == rhs.egvTransmitterTime.seconds &&
            lhs.egvSystemTime == rhs.egvSystemTime &&
            lhs.sequenceNumber == rhs.sequenceNumber &&
            lhs.algorithmState == rhs.algorithmState &&
            lhs.egv == rhs.egv &&
            lhs.predictedEgv == rhs.predictedEgv &&
            lhs.rate == rhs.rate &&
            lhs.isBgGeneratedOnTransmitter == rhs.isBgGeneratedOnTransmitter &&
            lhs.aberrationState == rhs.aberrationState
    }
    
    var recordedSystemTime: Date
    var transmitterId: String
    var egvTransmitterTime: TransmitterTime
    var egvSystemTime: Date
    var sequenceNumber: UInt
    var algorithmState: AlgorithmState
    var egv: UInt
    var predictedEgv: UInt?
    var rate: Double
    var isBgGeneratedOnTransmitter: Bool
    var aberrationState: AberrationState
}
