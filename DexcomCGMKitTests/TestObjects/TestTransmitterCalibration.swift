//
//  TestTransmitterCalibration.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import TransmitterCore

struct TestTransmitterCalibration: TransmitterCalibration {
    var calibrationSystemTime: Date = Date()
    var calibrationTransmitterTime: TransmitterTime = TestTransmitterTime()
    var meterValue: Int = 65
}
