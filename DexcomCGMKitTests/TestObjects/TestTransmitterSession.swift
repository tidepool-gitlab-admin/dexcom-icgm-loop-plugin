//
//  TestTransmitterSession.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import TransmitterCore

struct TestTransmitterSession: TransmitterSession {
    var sessionStartTransmitterTime: TransmitterTime
    var sessionStartSystemTime: Date
    
    init(startDate: Date) {
        self.sessionStartTransmitterTime = TestTransmitterTime()
        self.sessionStartSystemTime = startDate
    }
}
