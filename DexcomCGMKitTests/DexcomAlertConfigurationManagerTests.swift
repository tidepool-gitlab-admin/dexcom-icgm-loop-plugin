//
//  DexcomAlertConfigurationManagerTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-11-21.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
import TransmitterCore
import GlucoseCore
import LoopKit
@testable import DexcomCGMKit

class DexcomAlertConfigurationManagerTests: XCTestCase {
    
    private var alertManager: DexcomAlertConfigurationManager!
    private var updatedConfigurationsExpectation: XCTestExpectation!
    private var alertsIssued = [DeviceAlert]()
    private var alertIdentifier: String!
    private var newConfigurations: [DexcomAlertType: DexcomAlertConfiguration]!
    private var configurations: [DexcomAlertType: DexcomAlertConfiguration]!
    private var cgmReading: TestTransmitterCGMReading!
    private let expectedIdentifier = DexcomAlertConfigurationManager.cgmAlertNotificationIdentifier
    private var newAlertSchedule: DexcomAlertSchedule!
    private var alertSchedule = DexcomAlertSchedule.defaultSchedule()
    private var sessionsRemaining: Int?
    private var pendingAlertIdentifiersRemoved: [String] = []
    private var deliveredAlertIdentifiersRemoved: [String] = []
    
    override func setUp() {
        configurations = [:]
        for alertType in DexcomAlertType.allCases {
            let configuration = DexcomAlertConfiguration(type: alertType, enabled: true, alwaysSoundEnabled: false, sound: .defaultSound)
            configurations[alertType] = configuration
        }
        cgmReading = TestTransmitterCGMReading(recordedSystemTime: Date(),
                                               transmitterId: "8YYYYY",
                                               egvTransmitterTime: TestTransmitterTime(),
                                               egvSystemTime: Date(),
                                               sequenceNumber: 1,
                                               algorithmState: AlgorithmState.inCalibration,
                                               egv: 120,
                                               predictedEgv: nil,
                                               rate: 1.0,
                                               isBgGeneratedOnTransmitter: false,
                                               aberrationState: AberrationState.none)
        let now = Date()
        let startTimeComponents = Calendar.current.dateComponents(WeeklySchedule.timeComponents, from: now)
        let endTimeComponents = Calendar.current.dateComponents(WeeklySchedule.timeComponents, from: Calendar.current.date(byAdding: .day, value: 1, to: now)!)
        alertSchedule.weeklySchedule = WeeklySchedule(days: Days.everyday, startTimeComponents: startTimeComponents, endTimeComponents: endTimeComponents)
        alertSchedule.configurations[.glucoseLow]?.threshold = HKQuantity.init(unit: DexcomAlertType.glucoseLow.unit, doubleValue: 90)
        alertSchedule.configurations[.glucoseHigh]?.threshold = HKQuantity.init(unit: DexcomAlertType.glucoseLow.unit, doubleValue: 150)
        updatedConfigurationsExpectation = expectation(description: "New Configurations")
        pendingAlertIdentifiersRemoved = []
        deliveredAlertIdentifiersRemoved = []

        alertsIssued = [DeviceAlert]()
    }
    
    func testInitialization() {
        let alertManager = DexcomAlertConfigurationManager()
        XCTAssertTrue(alertManager.alwaysSoundEnabled)
        alertManager.delegate = self
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        XCTAssertNotNil(newConfigurations)
        XCTAssertEqual(newConfigurations.count, 7)
        XCTAssertEqual(newConfigurations[DexcomAlertType.glucoseLow]?.type, DexcomAlertType.glucoseLow)
        XCTAssertEqual(newConfigurations[DexcomAlertType.glucoseUrgentLow]?.type, DexcomAlertType.glucoseUrgentLow)
        XCTAssertEqual(newConfigurations[DexcomAlertType.glucoseHigh]?.type, DexcomAlertType.glucoseHigh)
        XCTAssertEqual(newConfigurations[DexcomAlertType.glucoseRateRise]?.type, DexcomAlertType.glucoseRateRise)
        XCTAssertEqual(newConfigurations[DexcomAlertType.glucoseRateFall]?.type, DexcomAlertType.glucoseRateFall)
        XCTAssertEqual(newConfigurations[DexcomAlertType.signalLoss]?.type, DexcomAlertType.signalLoss)
        XCTAssertEqual(newConfigurations[DexcomAlertType.noReadings]?.type, DexcomAlertType.noReadings)
    }
    
    func testRestoreConfigurableAlertConfigurations() {
        configurations.removeValue(forKey: .glucoseRateRise)
        configurations.removeValue(forKey: .glucoseLow)
        let alertManager = DexcomAlertConfigurationManager(userMutableAlertConfigurations: configurations)
        alertManager.delegate = self
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        XCTAssertNotNil(newConfigurations)
        XCTAssertEqual(newConfigurations.count, DexcomAlertType.allCases.count-2)
        
        for alertType in DexcomAlertType.allCases {
            if alertType == .glucoseRateRise || alertType == .glucoseLow {
                XCTAssertNil(newConfigurations[alertType])
            } else {
                XCTAssertNotNil(newConfigurations[alertType])
            }
        }
    }
    
    func testAlertGlucoseUrgentLow() {
        setupAlertManager()
        generateEGV(40)
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.glucoseUrgentLow)
    }
    
    func testAlertGlucoseLow() {
        setupAlertManager()
        generateEGV(70)
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.glucoseLow)
    }
    
    func testAlertGlucoseHigh() {
        setupAlertManager()
        generateEGV(400)
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.glucoseHigh)
    }
    
    func testAlertRateRising() {
        setupAlertManager()
        cgmReading.rate = 4.0
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.glucoseRateRise)
    }
    
    func testAlertRateFalling() {
        setupAlertManager()
        cgmReading.rate = -4.0
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.glucoseRateFall)
    }
    
    func testAlertSignalLoss() {
        // configurations will be updated twice
        updatedConfigurationsExpectation.expectedFulfillmentCount = 2
        setupAlertManager()
        var signalLossConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.signalLoss, enabled: true, alwaysSoundEnabled: false, sound: .defaultSound)
        signalLossConfiguration.threshold = HKQuantity(unit: signalLossConfiguration.type.unit, doubleValue: 0)
        alertManager.updateMutableAlert(withConfiguration: signalLossConfiguration)
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.signalLoss, expectedCount: 2)
        let expected = "\(DexcomCGMManager.managerIdentifier).\(DexcomAlertType.signalLoss.rawValue)"
        XCTAssertEqual([expected, expected], deliveredAlertIdentifiersRemoved)
    }
    
    func testAlertNoReadings() {
        // configurations will be updated twice
        updatedConfigurationsExpectation.expectedFulfillmentCount = 2
        setupAlertManager()
        var noReadingsConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.noReadings, enabled: true, alwaysSoundEnabled: false, sound: .defaultSound)
        noReadingsConfiguration.alertFrequency = DexcomAlertFrequency(timeInterval: TimeInterval(seconds: -1))
        alertManager.updateMutableAlert(withConfiguration: noReadingsConfiguration)
        // set no readings alert delay value is 20 mins
        XCTAssertEqual(alertManager.noReadingsAlertDelay, TimeInterval(minutes: 20))
        // set no readings alert delay in the past to allow fasting testing
        alertManager.noReadingsAlertDelay = TimeInterval(minutes: -1)
        
        cgmReading.algorithmState = AlgorithmState.temporarySensorIssue
        cgmReading.aberrationState = AberrationState.powerAberration
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.noReadings)
        
        generateEGV()
        checkAlert(forType: DexcomAlertType.noReadings)
    }
    
    func testAlertWarmupComplete() {
        setupAlertManager()
        cgmReading.algorithmState = AlgorithmState.sensorWarmup
        generateEGV()
        cgmReading.algorithmState = AlgorithmState.inCalibration
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.warmupComplete, expectedCount: 2)
    }
    
    func testCalibrationRequestNotification() {
        setupAlertManager()
        cgmReading.algorithmState = AlgorithmState.calibrationRequest
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.calibrationRequest, expectedCount: 2)
    }
    
    func testFirstCalibrationNotification() {
        setupAlertManager()
        cgmReading.algorithmState = AlgorithmState.firstOfTwoBGsNeeded
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.firstCalibration, expectedCount: 2)
    }
    
    func testSecondCalibrationNotification() {
        setupAlertManager()
        cgmReading.algorithmState = AlgorithmState.secondOfTwoBGsNeeded
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.secondCalibration, expectedCount: 2)
    }
    
    func testCalibrationRequiredNotification() {
        setupAlertManager()
        cgmReading.algorithmState = AlgorithmState.calibrationLinearityFitFailure
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.calibrationRequired, expectedCount: 2)
    }
    
    func testCalibrationRequiredNotificationCase2() {
        setupAlertManager()
        cgmReading.algorithmState = AlgorithmState.outOfCalDueToOutlier
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.calibrationRequired, expectedCount: 2)
    }
    
    func testCalibrationRequiredLaterNotification0() {
        setupAlertManager()
        cgmReading.algorithmState = AlgorithmState.calibrationError0
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.calibrationRequiredLater, expectedCount: 2)
    }
    
    func testCalibrationRequiredLaterNotification1() {
        setupAlertManager()
        cgmReading.algorithmState = AlgorithmState.calibrationError1
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.calibrationRequiredLater, expectedCount: 2)
    }
    
    
    func testAlertSessionExpired() {
        setupAlertManager()
        let sessionPeriod = CGMSessionPeriodStruct(currentTime: Date(), sessionStartTime: Date(), sessionDuration: Int(TimeInterval(days: 10)))
        cgmReading.algorithmState = AlgorithmState.inCalibration
        generateEGV(forSession: sessionPeriod)
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.sensorExpired, expectedCount: 2)
    }
    
    func testAlertSessionExpiresSoonTimes() {
        let sessionExpiresSoonAlertTypes = [DexcomAlertType.sensorExpiresSoon24H, DexcomAlertType.sensorExpiresSoon6H, DexcomAlertType.sensorExpiresSoon2H, DexcomAlertType.sensorExpiresSoon30M]
        setupAlertManager()
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        
        sessionExpiresSoonAlertTypes.forEach { alertType in
            
            let sessionPeriod: CGMSessionPeriodModel!
            if alertType == .sensorExpiresSoon24H {
                sessionPeriod = CGMSessionPeriodStruct(currentTime: Date(), sessionStartTime: Date(), sessionDuration: Int(TimeInterval(hours: 23)))
            } else if alertType == .sensorExpiresSoon6H {
                sessionPeriod = CGMSessionPeriodStruct(currentTime: Date(), sessionStartTime: Date(), sessionDuration: Int(TimeInterval(hours: 5)))
            } else if alertType == .sensorExpiresSoon2H {
                sessionPeriod = CGMSessionPeriodStruct(currentTime: Date(), sessionStartTime: Date(), sessionDuration: Int(TimeInterval(minutes: 110)))
            } else {
                sessionPeriod = CGMSessionPeriodStruct(currentTime: Date(), sessionStartTime: Date(), sessionDuration: Int(TimeInterval(minutes: 20)))
            }
            
            generateEGV(forSession: sessionPeriod)
            checkAlert(forType: alertType, expectedCount: 2)
        }
    }
    
    func testAlertFailedSensor() {
        setupAlertManager()
        cgmReading.algorithmState = AlgorithmState.sensorFailedDueToCountsAberration
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.sensorFailed, expectedCount: 2)
    }
    
    func testAlertFailedSensorDueToRestart() {
        setupAlertManager()
        cgmReading.algorithmState = AlgorithmState.sensorFailedDueToRestart
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.sensorFailedDueToRestart, expectedCount: 2)
    }
    
    func testAlertFailedTransmitter() {
        setupAlertManager()
        cgmReading.algorithmState = AlgorithmState.sessionFailedDueToTransmitterError
        generateEGV()
        
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.transmitterFailed)
    }
    
    func testAlertScheduleRestored() {
        alertManager = DexcomAlertConfigurationManager(userMutableAlertConfigurations: configurations, alertSchedule: alertSchedule)
        alertManager.delegate = self
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        XCTAssertTrue(alertManager.hasAlertSchedule)
        XCTAssertEqual(alertManager.alertSchedule, alertSchedule)
    }
    
    func testAlertScheduleGlucoseLowEnabled() {
        
        setupAlertManager(alertSchedule: alertSchedule)
        
        // trigger glucose low
        generateEGV(85)
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.glucoseLow, alwaysSound: true)
    }
    
    func testAlertScheduleGlucoseLowDisabled() {
        // setup a glucose low scheduled alert that is disabled
        let glucoseLowConfigurationDisabled = DexcomAlertConfiguration(type: DexcomAlertType.glucoseLow, enabled: false, alwaysSoundEnabled: true, sound: .defaultSound)
        alertSchedule.configurations[.glucoseLow] = glucoseLowConfigurationDisabled
        alertManager = DexcomAlertConfigurationManager(userMutableAlertConfigurations: configurations, alertSchedule: alertSchedule)
        alertManager.delegate = self
        
        // since the scheduled glucose low alert is disabled, there will be no alert triggered
        generateEGV(85)
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.glucoseLow, wasTriggered: false)
    }
    
    func testAlertScheduleCreated() {
        alertSchedule.alwaysSoundEnabled = false
        setupAlertManager(alertSchedule: alertSchedule)
        
        // trigger glucose urgent low. Glucose urgent low will always sound
        generateEGV(40)
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.glucoseUrgentLow, alwaysSound: true)
        
        // trigger glucose low
        generateEGV(85)
        checkAlert(forType: DexcomAlertType.glucoseLow)
        
        // trigger glucose high
        generateEGV(250)
        checkAlert(forType: DexcomAlertType.glucoseHigh)
    }
    
    func testAlertScheduleActivatedDeactivated() {
        
        // scheduled alerts will only be activated for 1 min and glucose low threshold set to 70
        let startTime = alertSchedule.weeklySchedule.startTime()
        alertSchedule.weeklySchedule.endTimeComponents = Calendar.current.dateComponents(WeeklySchedule.timeComponents, from: Calendar.current.date(byAdding: .minute, value: 1, to: startTime)!)
        alertSchedule.configurations[.glucoseLow]?.threshold = HKQuantity.init(unit: DexcomAlertType.glucoseLow.unit, doubleValue: 70)
        
        setupAlertManager(alertSchedule: alertSchedule)
        
        // since the scheduled glucose low alert is threshold is 70 and it will be active, there will be no alert triggered
        generateEGV(75)
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.glucoseLow, wasTriggered: false)
        
        // since the alert schedule was only 1 minute, this reading from the future will trigger the low reading alert
        cgmReading.egvSystemTime = Date().addingTimeInterval(TimeInterval.minutes(5))
        generateEGV(75)
        
        // since the configurable glucose low alert is threshold is 80, there will be a alert triggered
        checkAlert(forType: DexcomAlertType.glucoseLow)
    }
    
    func testAlertTransmitterEOLSessionStarted() {
        let alertManager = DexcomAlertConfigurationManager(userMutableAlertConfigurations: configurations)
        alertManager.delegate = self
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        
        let alertTypes = [DexcomAlertType.sessionStartedThirdLast, DexcomAlertType.sessionStartedSecondLast, DexcomAlertType.sessionStartedLast]
        for alertType in alertTypes {
            switch alertType {
            case .sessionStartedThirdLast:
                sessionsRemaining = 3
            case .sessionStartedSecondLast:
                sessionsRemaining = 2
            case .sessionStartedLast:
                sessionsRemaining = 1
            default:
                sessionsRemaining = nil
            }
            
            cgmReading.algorithmState = AlgorithmState.sessionStopped
            alertManager.newCGMReading(cgmReading)
            cgmReading.algorithmState = AlgorithmState.sensorWarmup
            alertManager.newCGMReading(cgmReading)
            checkAlert(forType: alertType)
        }
    }
    
    func testAlertTransmitterEOLSessionEnded() {
        let alertManager = DexcomAlertConfigurationManager(userMutableAlertConfigurations: configurations)
        alertManager.delegate = self
        sessionsRemaining = 0
        cgmReading.algorithmState = AlgorithmState.inCalibration
        alertManager.newCGMReading(cgmReading)
        cgmReading.algorithmState = AlgorithmState.sessionStopped
        alertManager.newCGMReading(cgmReading)
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        checkAlert(forType: DexcomAlertType.sessionsNoMore)
    }
    
    func testCancelTransmitterNotFoundNotification() {
        let alertManager = DexcomAlertConfigurationManager(userMutableAlertConfigurations: configurations)
        alertManager.delegate = self
        alertManager.cancelTransmitterNotFoundNotification()
        wait(for: [updatedConfigurationsExpectation], timeout: 1)
        XCTAssertEqual(["\(DexcomCGMManager.managerIdentifier).\(DexcomAlertType.transmitterNotFound.rawValue)"], deliveredAlertIdentifiersRemoved)
    }
    
}

extension DexcomAlertConfigurationManagerTests: DexcomAlertConfigurationManagerDelegate {
    
    func estimatedSessionsForTransmitter() -> Int? {
        return sessionsRemaining
    }
    func alertConfigurationsUpdated(_ manager: DexcomAlertConfigurationManager, userMutableAlertConfigurations: [DexcomAlertType: DexcomAlertConfiguration], alertSchedule: DexcomAlertSchedule?) {
        newConfigurations = userMutableAlertConfigurations
        newAlertSchedule = alertSchedule
        updatedConfigurationsExpectation.fulfill()
    }
    func onTransmitterFoundAcknowledged() {
        
    }
    
    func issueAlert(_ alert: DeviceAlert) {
        alertsIssued.append(alert)
    }
    
    func removePendingAlert(identifier: DeviceAlert.Identifier) {
        pendingAlertIdentifiersRemoved.append(identifier.value)
    }
    
    func removeDeliveredAlert(identifier: DeviceAlert.Identifier) {
        deliveredAlertIdentifiersRemoved.append(identifier.value)
    }
    
    func didSuspendAlert(alertIdentifier: Int, alertType: Int, activeAlarmingCount: Int?, suspended: Bool) {
        
    }
    
    func didAcknowledgeAlert(alertIdentifier: Int, alertType: Int, activeAlarmingCount: Int?, at time: Date) {
        
    }
}

extension DexcomAlertConfigurationManagerTests {
    func generateEGV(_ value: UInt = 120, forSession session: CGMSessionPeriodModel? = nil ) {
        cgmReading.egv = value
        alertManager.newCGMReading(cgmReading, forSession: session)
    }
    
    func setupAlertManager(alertSchedule: DexcomAlertSchedule? = nil) {
        alertManager = DexcomAlertConfigurationManager(userMutableAlertConfigurations: configurations)
        if let alertSchedule = alertSchedule {
            alertManager.alertSchedule = alertSchedule
            XCTAssertTrue(alertManager.hasAlertSchedule)
        }
        alertManager.delegate = self
    }
    
    func checkAlert(forType type: DexcomAlertType, wasTriggered triggered: Bool = true, alwaysSound: Bool = false,
                    onlyImmediate: Bool = true, expectedCount: Int = 1, file: StaticString = #file, line: UInt = #line) {
        let alertsIssued = (onlyImmediate) ? self.alertsIssued.filter { $0.trigger == .immediate } : self.alertsIssued
        if triggered {
            XCTAssertEqual(expectedCount, alertsIssued.count, "More than \(expectedCount) alert(s) issued: \(alertsIssued)", file: file, line: line)
            alertsIssued.forEach { alertIssued in
                XCTAssertEqual(alertIssued.foregroundContent?.title, type.hasInApp ? type.localizedInAppTitle : nil, file: file, line: line)
                XCTAssertEqual(alertIssued.foregroundContent?.body, type.hasInApp ? type.localizedInAppMessage : nil, file: file, line: line)
                XCTAssertEqual(alertIssued.backgroundContent?.title, type.hasUserNotification ? type.localizedNotificationTitle : nil, file: file, line: line)
                XCTAssertEqual(alertIssued.backgroundContent?.body, type.hasUserNotification ? type.localizedNotificationMessage : nil, file: file, line: line)
                XCTAssertEqual(alertIssued.identifier.alertIdentifier, type.identifier, file: file, line: line)
            }
        } else {
            XCTAssertTrue(alertsIssued.isEmpty, file: file, line: line)
        }
        self.alertsIssued = []
    }
}

extension DeviceAlert.Trigger: Equatable {
    static public func ==(_ lhs: DeviceAlert.Trigger, _ rhs: DeviceAlert.Trigger) -> Bool {
        switch lhs {
        case .immediate:
            switch rhs {
            case .immediate: return true
            default: return false
            }
        case .delayed(let lhsInterval):
            switch rhs {
            case .delayed(let rhsInterval): return lhsInterval == rhsInterval
            default: return false
            }
        case .repeating(let lhsRepeatInterval):
            switch rhs {
            case .repeating(let rhsRepeatInterval): return lhsRepeatInterval == rhsRepeatInterval
            default: return false
            }

        }
    }
}
