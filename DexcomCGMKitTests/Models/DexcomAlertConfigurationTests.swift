//
//  DexcomAlertConfigurationTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-11-21.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
@testable import DexcomCGMKit

class DexcomAlertConfigurationTests: XCTestCase {
    
    override func setUp() {
        
    }
    
    func testInitializationAll() {
        for alertType in DexcomAlertType.allCases {
            let configuration = DexcomAlertConfiguration(type: alertType, enabled: true, alwaysSoundEnabled: false, sound: .defaultSound)
            XCTAssertEqual(configuration.type, alertType)
            XCTAssertTrue(configuration.enabled)
            XCTAssertFalse(configuration.alwaysSoundEnabled)
            XCTAssertEqual(configuration.hasThresholdValue, !alertType.defaultThreshold.isNaN)
            XCTAssertEqual(configuration.snoozeFrequency, alertType.defaultSnoozeFrequency)
            XCTAssertEqual(configuration.alertFrequency, alertType.defaultAlertFrequency)
            if configuration.hasThresholdValue {
                XCTAssertTrue(configuration.type.defaultThreshold.isEqual(to: alertType.defaultThreshold))
                XCTAssertEqual(configuration.threshold, HKQuantity(unit: alertType.unit, doubleValue: alertType.defaultThreshold))
                var defaultThreshold: Double = 0
                var thresholdMinMaxBounds: (min: Double, max: Double) = (0,0)
                var unit: HKUnit = HKUnit.milligramsPerDeciliter
                switch alertType {
                case .glucoseUrgentLow:
                    defaultThreshold = 55
                    thresholdMinMaxBounds = (55, 55)
                case .glucoseLow:
                    defaultThreshold = 80
                    thresholdMinMaxBounds = (60, 100)
                case .glucoseHigh:
                    defaultThreshold = 200
                    thresholdMinMaxBounds = (120, 400)
                case .glucoseRateRise:
                    defaultThreshold = 3
                    thresholdMinMaxBounds = (1, 5)
                    unit = HKUnit.milligramsPerDeciliterPerMinute
                case .glucoseRateFall:
                    defaultThreshold = -3
                    thresholdMinMaxBounds = (-5, -1)
                    unit = HKUnit.milligramsPerDeciliterPerMinute
                case .signalLoss:
                    defaultThreshold = 20
                    thresholdMinMaxBounds = (TimeInterval(minutes: 5).minutes, TimeInterval(hours: 4).minutes)
                    unit = HKUnit.minute()
                default:
                    assertionFailure("All cases need to be handled")
                }
                XCTAssertEqual(configuration.type.defaultThreshold, defaultThreshold)
                XCTAssertEqual(configuration.type.thresholdMinMaxBounds.min, thresholdMinMaxBounds.min)
                XCTAssertEqual(configuration.type.thresholdMinMaxBounds.max, thresholdMinMaxBounds.max)
                XCTAssertEqual(configuration.type.unit, unit)
            }
        }
    }
    
    func testCanDisable() {
        // based on the dexcom experience, only these alerts can be disabled by the user
        let alertTypeCanDisable: [DexcomAlertType] = [.glucoseLow, .glucoseHigh, .glucoseRateRise, .glucoseRateFall, .signalLoss, .noReadings]
        for alertType in DexcomAlertType.allCases {
            if alertTypeCanDisable.contains(alertType) {
                XCTAssertTrue(alertType.canDisable)
            } else {
                XCTAssertFalse(alertType.canDisable)
            }
        }
    }
    
    func testCanDelayRepeat() {
        // based on the dexcom experience, only these alerts display a delayed repeat to the user
        let alertTypeCanDelayRepeat: [DexcomAlertType] = [.glucoseUrgentLow, .glucoseLow, .glucoseHigh]
        for alertType in DexcomAlertType.allCases {
            if alertTypeCanDelayRepeat.contains(alertType) {
                XCTAssertTrue(alertType.canDelayRepeat)
            } else {
                XCTAssertFalse(alertType.canDelayRepeat)
            }
        }
    }
    
}
