//
//  TransmitterDataTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-09-25.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
import LoopKit
import TransmitterCore
@testable import DexcomCGMKit

class TransmitterDataTests: XCTestCase {
    
    private var cgmReading: TestTransmitterCGMReading?
    private var transmitterID: String = "8XXXXX"
    private var autoCalibration = AutoCalibrationState.auto
    private var sensorDurationDays = 10
    private var sensorWarmupDurationHours = 2
    private var calibrationBounds = TestCalibrationBounds()
    
    override func setUp() {
        super.setUp()
        cgmReading = TestTransmitterCGMReading(recordedSystemTime: Date(),
                                               transmitterId: transmitterID,
                                               egvTransmitterTime: TestTransmitterTime(),
                                               egvSystemTime: Date(),
                                               sequenceNumber: 1,
                                               algorithmState: AlgorithmState.inCalibration,
                                               egv: 80,
                                               predictedEgv: nil,
                                               rate: 1.0,
                                               isBgGeneratedOnTransmitter: false,
                                               aberrationState: AberrationState.none)
    }
    
    func testInitialization() {
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading!,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: calibrationBounds,
                                              autoCalibration: autoCalibration,
                                              sessionDurationDays: sensorDurationDays,
                                              sensorWarmupDuration: sensorWarmupDurationHours)
        XCTAssertNil(transmitterData.session)
        XCTAssertNil(transmitterData.lastCalibration)
        XCTAssertTrue(transmitterData.backfillCgmReading.isEmpty)
        XCTAssertEqual(transmitterData.transmitterId, transmitterID)
        XCTAssertEqual((transmitterData.cgmReading as! TestTransmitterCGMReading), cgmReading)
        XCTAssertEqual(transmitterData.calibrationBounds as! TestCalibrationBounds, calibrationBounds)
        XCTAssertEqual(transmitterData.autoCalibration, autoCalibration)
        XCTAssertEqual(transmitterData.sessionDurationDays, sensorDurationDays)
        XCTAssertEqual(transmitterData.sensorWarmupDuration, sensorWarmupDurationHours)
    }
    
    func testStateValid() {
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading!,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: calibrationBounds,
                                              autoCalibration: autoCalibration,
                                              sessionDurationDays: sensorDurationDays,
                                              sensorWarmupDuration: sensorWarmupDurationHours)
        XCTAssertTrue(transmitterData.isStateValid)
    }
    
    func testStateInvalid() {
        cgmReading!.algorithmState = AlgorithmState.calibrationError0
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading!,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: calibrationBounds,
                                              autoCalibration: autoCalibration,
                                              sessionDurationDays: sensorDurationDays,
                                              sensorWarmupDuration: sensorWarmupDurationHours)
        XCTAssertFalse(transmitterData.isStateValid)
    }
    
    func testTrendTypeDownDownDown() {
        cgmReading!.rate = -8.0
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading!,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: calibrationBounds,
                                              autoCalibration: autoCalibration,
                                              sessionDurationDays: sensorDurationDays,
                                              sensorWarmupDuration: sensorWarmupDurationHours)
        XCTAssertEqual(transmitterData.trendType!, GlucoseTrend.downDownDown)
    }
    
    func testTrendTypeOutOfRange() {
        cgmReading!.rate = -9.0
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading!,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: calibrationBounds,
                                              autoCalibration: autoCalibration,
                                              sessionDurationDays: sensorDurationDays,
                                              sensorWarmupDuration: sensorWarmupDurationHours)
        XCTAssertNil(transmitterData.trendType)
    }
    
    func testTrendTypeDownDown() {
        cgmReading!.rate = -2.5
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading!,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: calibrationBounds,
                                              autoCalibration: autoCalibration,
                                              sessionDurationDays: sensorDurationDays,
                                              sensorWarmupDuration: sensorWarmupDurationHours)
        XCTAssertEqual(transmitterData.trendType!, GlucoseTrend.downDown)
    }
    
    func testTrendTypeDown() {
        cgmReading!.rate = -1.5
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading!,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: calibrationBounds,
                                              autoCalibration: autoCalibration,
                                              sessionDurationDays: sensorDurationDays,
                                              sensorWarmupDuration: sensorWarmupDurationHours)
        XCTAssertEqual(transmitterData.trendType!, GlucoseTrend.down)
    }
    
    func testTrendTypeFlat() {
        cgmReading!.rate = -0.5
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading!,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: calibrationBounds,
                                              autoCalibration: autoCalibration,
                                              sessionDurationDays: sensorDurationDays,
                                              sensorWarmupDuration: sensorWarmupDurationHours)
        XCTAssertEqual(transmitterData.trendType!, GlucoseTrend.flat)
    }
    
    func testTrendTypeUp() {
        cgmReading!.rate = 1.5
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading!,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: calibrationBounds,
                                              autoCalibration: autoCalibration,
                                              sessionDurationDays: sensorDurationDays,
                                              sensorWarmupDuration: sensorWarmupDurationHours)
        XCTAssertEqual(transmitterData.trendType!, GlucoseTrend.up)
    }
    
    func testTrendTypeUpUp() {
        cgmReading!.rate = 2.5
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading!,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: calibrationBounds,
                                              autoCalibration: autoCalibration,
                                              sessionDurationDays: sensorDurationDays,
                                              sensorWarmupDuration: sensorWarmupDurationHours)
        XCTAssertEqual(transmitterData.trendType!, GlucoseTrend.upUp)
    }
    
    func testTrendTypeUpUpUp() {
        cgmReading!.rate = 5.0
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: nil,
                                              cgmReading: cgmReading!,
                                              backfillCgmReading: [],
                                              lastCalibration: nil,
                                              calibrationBounds: calibrationBounds,
                                              autoCalibration: autoCalibration,
                                              sessionDurationDays: sensorDurationDays,
                                              sensorWarmupDuration: sensorWarmupDurationHours)
        XCTAssertEqual(transmitterData.trendType!, GlucoseTrend.upUpUp)
    }
}
