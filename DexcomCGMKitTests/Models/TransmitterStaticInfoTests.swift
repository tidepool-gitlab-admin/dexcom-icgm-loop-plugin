//
//  TransmitterStaticInfoTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-09-25.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
@testable import DexcomCGMKit

class TransmitterStaticInfoTests: XCTestCase {

    private var transmitterID = "8XXXXX"
    private let deviceUUID = "deviceUUID"
    private let transmitterVersion = "1.0.1"
    private let softwareNumber: UInt32 = 1
    private let apiVersion: UInt32 = 2
    private let storageTimeDays: UInt32 = 30
    private let maxStorageTimeDays: UInt32 = 120
    private let maxRuntimeDays: UInt32 = 90
    private let sessionTimeDays: UInt32 = 10
    private let isDiagnosticDataDownloadSupported = true
    private let isEgvBackfillSupported = true
    private let isPredictedEgvSupported = true
    private let activatedOn = Date()
    private var transmitterStaticInfo: TransmitterStaticInfo!
    
    override func setUp() {
        super.setUp()
        transmitterStaticInfo = TransmitterStaticInfo(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                                      deviceUUID: deviceUUID,
                                                      transmitterVersion: transmitterVersion,
                                                      softwareNumber: softwareNumber,
                                                      apiVersion: apiVersion,
                                                      storageTimeDays: storageTimeDays,
                                                      maxStorageTimeDays: maxStorageTimeDays,
                                                      maxRuntimeDays: maxRuntimeDays,
                                                      sessionTimeDays: sessionTimeDays,
                                                      isDiagnosticDataDownloadSupported: isDiagnosticDataDownloadSupported,
                                                      isEgvBackfillSupported: isEgvBackfillSupported,
                                                      isPredictedEgvSupported: isPredictedEgvSupported,
                                                      activatedOn: activatedOn)
    }
    
    func testInitialization() {
        XCTAssertEqual(transmitterStaticInfo.transmitterId, transmitterID)
        XCTAssertEqual(transmitterStaticInfo.deviceUUID, deviceUUID)
        XCTAssertEqual(transmitterStaticInfo.transmitterVersion, transmitterVersion)
        XCTAssertEqual(transmitterStaticInfo.softwareNumber, softwareNumber)
        XCTAssertEqual(transmitterStaticInfo.apiVersion, apiVersion)
        XCTAssertEqual(transmitterStaticInfo.storageTimeDays, storageTimeDays)
        XCTAssertEqual(transmitterStaticInfo.maxStorageTimeDays, maxStorageTimeDays)
        XCTAssertEqual(transmitterStaticInfo.maxRuntimeDays, maxRuntimeDays)
        XCTAssertEqual(transmitterStaticInfo.sessionTimeDays, sessionTimeDays)
        XCTAssertEqual(transmitterStaticInfo.isDiagnosticDataDownloadSupported, isDiagnosticDataDownloadSupported)
        XCTAssertEqual(transmitterStaticInfo.isEgvBackfillSupported, isEgvBackfillSupported)
        XCTAssertEqual(transmitterStaticInfo.isPredictedEgvSupported, isPredictedEgvSupported)
        XCTAssertEqual(transmitterStaticInfo.activatedOn, activatedOn)
    }
    
    func testModelG5NotSupported() {
        transmitterID = "4XXXXX"
        let dexcomTransmitterID = DexcomTransmitterID(transmitterID)
        XCTAssertNil(dexcomTransmitterID)
    }
    
    func testModelG6() {
        transmitterID = "8XXXXX"
        transmitterStaticInfo!.dexcomTransmitterID = DexcomTransmitterID(transmitterID)!
        XCTAssertEqual(transmitterStaticInfo.dexcomTransmitterID.model, "G6")
    }
    
}
