//
//  Dexcom2DBarcodeTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-09-24.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
@testable import DexcomCGMKit

class Dexcom2DBarcodeTests: XCTestCase {
    
    func testInitializationSensor() {
        let dexcom2DBarcode = Dexcom2DBarcode(.sensor, code: nil)
        XCTAssertEqual(dexcom2DBarcode.forDeviceType, .sensor)
        XCTAssertNil(dexcom2DBarcode.code)
        XCTAssertFalse(dexcom2DBarcode.isValid())
    }
    
    func testInitializationTransmitter() {
        let dexcom2DBarcode = Dexcom2DBarcode(.transmitter, code: nil)
        XCTAssertEqual(dexcom2DBarcode.forDeviceType, .transmitter)
        XCTAssertNil(dexcom2DBarcode.code)
        XCTAssertFalse(dexcom2DBarcode.isValid())
    }
    
    func testBarcodeSensorValid() {
        let sensorCodeString = "5915"
        let dexcom2DBarcode = Dexcom2DBarcode(.sensor, code: "sensorCode"+sensorCodeString)
        XCTAssertEqual(dexcom2DBarcode.forDeviceType, .sensor)
        XCTAssertEqual(dexcom2DBarcode.code, sensorCodeString)
        XCTAssertTrue(dexcom2DBarcode.isValid())
    }
    
    func testBarcodeSensorInvalid() {
        let sensorCodeStringInvalid = "591"
        let dexcom2DBarcode = Dexcom2DBarcode(.sensor, code: sensorCodeStringInvalid)
        XCTAssertNil(dexcom2DBarcode.code)
        XCTAssertFalse(dexcom2DBarcode.isValid())
    }
    
    func testBarcodeTransmitterG5NotSupported() {
        let transmitterCodeString = "4XXXXX"
        let groupSeparator = "\u{1d}"
        let dexcom2DBarcode = Dexcom2DBarcode(.transmitter, code: "\(groupSeparator)transmitterCode"+transmitterCodeString)
        XCTAssertEqual(dexcom2DBarcode.forDeviceType, .transmitter)
        XCTAssertNil(dexcom2DBarcode.code)
    }
    
    func testBarcodeTransmitterG6Valid() {
        let transmitterCodeString = "8XXXXX"
        let groupSeparator = "\u{1d}"
        let dexcom2DBarcode = Dexcom2DBarcode(.transmitter, code: "\(groupSeparator)transmitterCode"+transmitterCodeString)
        XCTAssertEqual(dexcom2DBarcode.forDeviceType, .transmitter)
        XCTAssertEqual(dexcom2DBarcode.code, transmitterCodeString)
        XCTAssertTrue(dexcom2DBarcode.isValid())
    }
    
    func testBarcodeTransmitterInvalidLength() {
        let transmitterCodeStringInvalid = "8XXXX"
        let dexcom2DBarcode = Dexcom2DBarcode(.transmitter, code: transmitterCodeStringInvalid)
        XCTAssertNil(dexcom2DBarcode.code)
        XCTAssertFalse(dexcom2DBarcode.isValid())
    }
    
    func testBarcodeTransmitterInvalidModelCode() {
        let transmitterCodeStringInvalid = "6XXXXX"
        let dexcom2DBarcode = Dexcom2DBarcode(.transmitter, code: transmitterCodeStringInvalid)
        XCTAssertNil(dexcom2DBarcode.code)
        XCTAssertFalse(dexcom2DBarcode.isValid())
    }

}
