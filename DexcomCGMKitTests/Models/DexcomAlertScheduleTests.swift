//
//  DexcomAlertScheduleTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2020-03-09.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import XCTest
@testable import DexcomCGMKit

class DexcomAlertScheduleTests: XCTestCase {

    func testInitialization() {
        let calendar = Calendar(identifier: .gregorian)
        let today = Date()
        let originalSeconds = 0
        let originalminutes = 10
        let originalStartHour = 20
        let originalEndHour = 8
        let startTime = calendar.date(bySettingHour: originalStartHour, minute: originalminutes, second: originalSeconds, of: today)!
        let endTime = calendar.date(bySettingHour: originalEndHour, minute: originalminutes, second: originalSeconds, of: today)!
        let startTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: startTime)
        let endTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: endTime)
        
        let originalDays: Days = [.sunday]
        let weeklySchedule = WeeklySchedule(days: originalDays, startTimeComponents: startTimeComponents, endTimeComponents: endTimeComponents)
        let configurations = DexcomUserStaticAlerts.defaultConfigurations(alwaysSoundEnabled: false)
        let name = "Test"
        let enabled = false
        let alertSchedule = DexcomAlertSchedule(configurations: configurations, name: name, enabled: enabled, weeklySchedule: weeklySchedule)
        
        XCTAssertEqual(alertSchedule.weeklySchedule, weeklySchedule)
        XCTAssertEqual(alertSchedule.configurations, configurations)
        XCTAssertEqual(alertSchedule.name, name)
        XCTAssertEqual(alertSchedule.enabled, enabled)
    }
    
    func testDefaultSchedule() {
        let defaultAlertSchedule = DexcomAlertSchedule.defaultSchedule()
        XCTAssertTrue(defaultAlertSchedule.alwaysSoundEnabled)
        XCTAssertTrue(defaultAlertSchedule.enabled)
        XCTAssertEqual(defaultAlertSchedule.name, "")
        XCTAssertEqual(defaultAlertSchedule.weeklySchedule, WeeklySchedule())
        XCTAssertEqual(defaultAlertSchedule.configurations, DexcomAlertSchedule.defaultConfigurations(alwaysSoundEnabled: true))
    }
    
    func testEquality() {
        let sameAlertSchedule1 = DexcomAlertSchedule.defaultSchedule()
        let sameAlertSchedule2 = DexcomAlertSchedule.defaultSchedule()
        XCTAssertEqual(sameAlertSchedule1, sameAlertSchedule2)
        
        var differentAlertSchedule = DexcomAlertSchedule.defaultSchedule()
        
        // change weekly schedule
        let calendar = Calendar(identifier: .gregorian)
        let today = Date()
        let originalSeconds = 0
        let originalminutes = 10
        let originalStartHour = 20
        let originalEndHour = 8
        let startTime = calendar.date(bySettingHour: originalStartHour, minute: originalminutes, second: originalSeconds, of: today)!
        let endTime = calendar.date(bySettingHour: originalEndHour, minute: originalminutes, second: originalSeconds, of: today)!
        let startTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: startTime)
        let endTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: endTime)
        
        let originalDays: Days = [.sunday]
        differentAlertSchedule.weeklySchedule = WeeklySchedule(days: originalDays, startTimeComponents: startTimeComponents, endTimeComponents: endTimeComponents)
        XCTAssertNotEqual(sameAlertSchedule1, differentAlertSchedule)
        
        // change name
        differentAlertSchedule = sameAlertSchedule1
        differentAlertSchedule.name = "differentName"
        XCTAssertNotEqual(sameAlertSchedule1, differentAlertSchedule)
        
        // change enable
        differentAlertSchedule = sameAlertSchedule1
        differentAlertSchedule.enabled = !sameAlertSchedule1.enabled
        XCTAssertNotEqual(sameAlertSchedule1, differentAlertSchedule)
        
        // change configurations
        differentAlertSchedule = sameAlertSchedule1
        differentAlertSchedule.configurations = DexcomUserStaticAlerts.defaultConfigurations(alwaysSoundEnabled: false)
        XCTAssertNotEqual(sameAlertSchedule1, differentAlertSchedule)
    }

}
