//
//  DexcomTransmitterIDTests.swift
//  DexcomCGMKitTests
//
//  Created by Nathaniel Hamming on 2019-10-01.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
@testable import DexcomCGMKit

class DexcomTransmitterIDTests: XCTestCase {

    func testTransmitterIDValidG6() {
        let transmitterIDString = "8XXXXX"
        let dexcomTransmitterID = DexcomTransmitterID(transmitterIDString)
        XCTAssertNotNil(dexcomTransmitterID)
        XCTAssertEqual(dexcomTransmitterID?.transmitterID, transmitterIDString)
    }
    
    func testTransmitterIDG5NotSupported() {
        let transmitterIDString = "4XXXXX"
        let dexcomTransmitterID = DexcomTransmitterID(transmitterIDString)
        XCTAssertNil(dexcomTransmitterID)
    }
    
    func testTransmitterIDInvalidLength() {
        let transmitterIDString = "8XXXX"
        let dexcomTransmitterID = DexcomTransmitterID(transmitterIDString)
        XCTAssertNil(dexcomTransmitterID)
    }
    
    func testTransmitterIDInvalidCode() {
        let transmitterIDString = "6XXXXX"
        let dexcomTransmitterID = DexcomTransmitterID(transmitterIDString)
        XCTAssertNil(dexcomTransmitterID)
    }

}
