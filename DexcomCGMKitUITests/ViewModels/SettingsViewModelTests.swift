//
//  SettingsViewModelTests.swift
//  DexcomCGMKitUITests
//
//  Created by Nathaniel Hamming on 2019-08-20.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
import LoopKit
import DexcomCGMKit
import TransmitterCore
@testable import DexcomCGMKitUI

class SettingsViewModelTests: XCTestCase {

    private let transmitterID = "8XXXXX"
    private var mockTxComm = MockTransmitterBleCommunication()
    private var cgmManager: DexcomCGMManager!
    private var testExpectation: XCTestExpectation!
    private var viewModel: SettingsViewModel!
        
    override func setUp() {
        var cgmManagerState = DexcomCGMManagerState(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!)
        cgmManagerState.transmitterStaticInfo.maxRuntimeDays = 110 // G6 default value
        cgmManagerState.transmitterStaticInfo.sessionTimeDays = 10 // G6 default value
        cgmManagerState.transmitterStaticInfo.activatedOn = Date()
        
        // Setup CGM Manager
        cgmManager = DexcomCGMManager(state: cgmManagerState,
                                      initialTransmitterCommunication: mockTxComm,
                                      findNewTransmitterCommunication: { _,_ in self.mockTxComm })
        
        // Set Mock Comms Delegate
        mockTxComm.connectionDelegate = cgmManager
        mockTxComm.cgmDelegate = cgmManager
        
        // Set ourselves as the CGM Manager Delegate
        cgmManager.cgmManagerDelegate = self
        
        // setup settings view model
        viewModel = SettingsViewModel(cgmManager: cgmManager,
                                      glucoseUnit: HKUnit.milligramsPerDeciliter,
                                      completionHandler: { [weak self] in self?.willExitSettingsView() })
    }

    func testInitialization() {
        XCTAssertEqual(viewModel.transmitterID, transmitterID)
        XCTAssertEqual(viewModel.glucoseUnit, HKUnit.milligramsPerDeciliter)
        XCTAssertNil(viewModel.lastCalibrationDate)
        XCTAssertNil(viewModel.sensorInsertionDate)
        XCTAssertNil(viewModel.sensorExpirationDate)
        XCTAssertNil(viewModel.lastGlucoseValueFormatted)
        XCTAssertNil(viewModel.lastGlucoseDate)
        XCTAssertNil(viewModel.lastGlucoseTrendFormatted)
        XCTAssertFalse(viewModel.sessionRunning)
    }
    
    func testNewTransmitterData() {
        testExpectation = expectation(description: "New Transmitter Data")
        testExpectation.expectedFulfillmentCount = 2 // 2 calls: new EVG and new calibration
        
        // create CGM data
        let egvDate = Date()
        let cgmReading = TestTransmitterCGMReading(recordedSystemTime: egvDate,
                                                             transmitterId: transmitterID,
                                                             egvTransmitterTime: TestTransmitterTime(seconds: 30),
                                                             egvSystemTime: Date(),
                                                             sequenceNumber: 2,
                                                             algorithmState: AlgorithmState.inCalibration,
                                                             egv: 90,
                                                             predictedEgv: nil,
                                                             rate: 1.0,
                                                             isBgGeneratedOnTransmitter: false,
                                                             aberrationState: AberrationState.none)
        
        let calibrationDate = Date()
        let testTransmitterCalibration = TestTransmitterCalibration(calibrationSystemTime: calibrationDate)
        
        let sessionStartDate = Date()
        let sessionDurationDays = 10
        let sessionExpirationDate = Calendar.current.date(byAdding: .day, value: sessionDurationDays, to: sessionStartDate)
        let session = TestTransmitterSession(startDate: sessionStartDate)
        
        let transmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(transmitterID)!,
                                              session: session,
                                              cgmReading: cgmReading,
                                              backfillCgmReading: [],
                                              lastCalibration: testTransmitterCalibration,
                                              calibrationBounds: TestCalibrationBounds(),
                                              autoCalibration: AutoCalibrationState.auto,
                                              sessionDurationDays: sessionDurationDays,
                                              sensorWarmupDuration: 2)
        
        // trigger the new data callback
        mockTxComm.egvAvailable(transmitterData)
                
        wait(for: [testExpectation], timeout: 1)
        
        // check that the view model updated correctly
        XCTAssertEqual(viewModel.transmitterID, transmitterID)
        XCTAssertEqual(viewModel.lastCalibrationDate, calibrationDate)
        XCTAssertEqual(viewModel.sensorInsertionDate, sessionStartDate)
        XCTAssertEqual(viewModel.sensorExpirationDate, sessionExpirationDate)
        XCTAssertEqual(viewModel.lastGlucoseValueFormatted, "90 mg/dL")
        XCTAssertEqual(viewModel.lastGlucoseDate, egvDate)
        XCTAssertEqual(viewModel.lastGlucoseTrendFormatted, "1 mg/dL/min")
        XCTAssertTrue(viewModel.sessionRunning)
    }
    
    func testTransmitterFound() {
        let newTransmitterID = "888888"
        let transmitterStaticInfo = TransmitterStaticInfo(dexcomTransmitterID: DexcomTransmitterID(newTransmitterID)!,
                                                          deviceUUID: "",
                                                          transmitterVersion: "1.0.1",
                                                          softwareNumber: 1,
                                                          apiVersion: 2,
                                                          storageTimeDays: 30,
                                                          maxStorageTimeDays: 120,
                                                          maxRuntimeDays: 90,
                                                          sessionTimeDays: 10,
                                                          isDiagnosticDataDownloadSupported: true,
                                                          isEgvBackfillSupported: true,
                                                          isPredictedEgvSupported: true,
                                                          activatedOn: Date())
        
        mockTxComm.transmitterFound(transmitterStaticInfo)
        
        XCTAssertEqual(viewModel.transmitterID, newTransmitterID)
    }
    
    func testUpdatingTransmitterID() {        
        let newTransmitterID = "888888"
        cgmManager.replaceTransmitter(DexcomTransmitterID(newTransmitterID)!)
        XCTAssertEqual(viewModel.transmitterID, newTransmitterID)
    }
    
    func testCalibrationCompletionHandler() {
        let meterValue: Int32 = 80
        let calibrationValue = HKQuantity(unit: HKUnit.milligramsPerDeciliter, doubleValue: Double(meterValue))
        viewModel.calibrationCompletionHandler(calibrationValue)
        let calibrateCmd = CgmCommand.calibrate(uuid: UUID(),
                                                systemTime: Date(),
                                                transmitterTime: nil,
                                                meterValue: meterValue,
                                                response: nil)
        XCTAssertTrue(mockTxComm.isCurrentCommand(command: calibrateCmd))
    }
    
    func testNewSensorCompletionHandler() {
        let sensorCode = SensorCode(rawValue: "7171")
        viewModel.newSensorCompletionHandler(sensorCode)
        let startSessionCmd = CgmCommand.start(uuid: UUID(),
                                               systemTime: Date(),
                                               transmitterTime: nil,
                                               code: sensorCode,
                                               response: nil)
        XCTAssertTrue(mockTxComm.isCurrentCommand(command: startSessionCmd))
    }
    
    func testStopSensorCompletionHandler() {
        viewModel.stopSensorCompletionHandler()
        let stopSessionCmd = CgmCommand.stop(uuid: UUID(),
                                             systemTime: Date(),
                                             transmitterTime: nil,
                                             response: nil)
        XCTAssertTrue(mockTxComm.isCurrentCommand(command: stopSessionCmd))
    }
    
    func testCompletionHandler() {
        testExpectation = expectation(description: "Exiting Settings View")
        viewModel.completionHandler()
        wait(for: [testExpectation], timeout: 1)
    }
    
    func testDeleteCGMManager() {
        testExpectation = expectation(description: "Deleting CGM Manager")
        viewModel.deleteCGMManagerHandler()
        wait(for: [testExpectation], timeout: 1)
        XCTAssertTrue(mockTxComm.closeCalled)
        XCTAssertNil(cgmManager)
    }
}

extension SettingsViewModelTests: CGMManagerDelegate {
    
    func issueAlert(_ alert: DeviceAlert) {
        
    }
    
    func removePendingAlert(identifier: DeviceAlert.Identifier) {
        
    }
    
    func removeDeliveredAlert(identifier: DeviceAlert.Identifier) {
        
    }

    func deviceManager(_ manager: DeviceManager, logEventForDeviceIdentifier deviceIdentifier: String?, type: DeviceLogEntryType, message: String, completion: ((Error?) -> Void)?) { }
    
    func startDateToFilterNewData(for manager: CGMManager) -> Date? { return nil }
    
    func cgmManager(_ manager: CGMManager, didUpdateWith result: CGMResult) {
        switch result {
        case .newData(_):
            testExpectation?.fulfill()
        default:
            break
        }
    }
    
    func cgmManagerWantsDeletion(_ manager: CGMManager) {
        cgmManager = nil
    }
    
    func cgmManagerDidUpdateState(_ manager: CGMManager) { }
    
    func credentialStoragePrefix(for manager: CGMManager) -> String { return "" }
    
    func scheduleNotification(for manager: DeviceManager, identifier: String, content: UNNotificationContent, trigger: UNNotificationTrigger?) { }
    
    func clearNotification(for manager: DeviceManager, identifier: String) { }
    
    func removeNotificationRequests(for manager: DeviceManager, identifiers: [String]) { }

    func showAlert(_ manager: DeviceManager, title: String, message: String) { }
}

extension SettingsViewModelTests {
    func willExitSettingsView() {
        testExpectation.fulfill()
    }
}
