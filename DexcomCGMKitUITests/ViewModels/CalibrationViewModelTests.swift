//
//  CalibrationViewModelTests.swift
//  DexcomCGMKitUITests
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
@testable import DexcomCGMKitUI

class CalibrationViewModelTests: XCTestCase {

    private var updatedCalibration: HKQuantity!

    func testInitialization() {
        let viewModel = CalibrationViewModel(glucoseUnit: HKUnit.milligramsPerDeciliter,
                                             completionHandler: { _ in })
        XCTAssertEqual(viewModel.glucoseUnit, HKUnit.milligramsPerDeciliter)
    }
    
    func testCalibration() {
        let viewModel = CalibrationViewModel(glucoseUnit: HKUnit.milligramsPerDeciliter,
                                             completionHandler: { [weak self] in self?.updatedCalibrationValue($0) })
        var calibrationValue: Double? = 90
        let calibrationPossible = viewModel.performCalibration(calibrationValue)
        XCTAssertTrue(calibrationPossible)
        XCTAssertEqual(updatedCalibration, HKQuantity(unit: HKUnit.milligramsPerDeciliter, doubleValue: calibrationValue!))
        
        calibrationValue = nil
        let calibrationNotPossible = viewModel.performCalibration(calibrationValue)
        XCTAssertFalse(calibrationNotPossible)
    }

}

extension CalibrationViewModelTests {
    func updatedCalibrationValue(_ calibrationValue: HKQuantity) {
        updatedCalibration = calibrationValue
    }
}
