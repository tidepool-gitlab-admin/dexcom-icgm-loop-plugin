//
//  TransmitterDetailsViewModelTests.swift
//  DexcomCGMKitUITests
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import XCTest
import DexcomCGMKit
import TransmitterCore
@testable import DexcomCGMKitUI

class TransmitterDetailsViewModelTests: XCTestCase {

    private var transmitterStaticInfo: TransmitterStaticInfo!
    private var mockTxComm = MockTransmitterBleCommunication()
    private var cgmManager: DexcomCGMManager!
    private var testExpectation: XCTestExpectation!
    private var viewModel: TransmitterDetailsViewModel!
    
    override func setUp() {
        let newTransmitterID = "8XXXXX"
        transmitterStaticInfo = TransmitterStaticInfo(dexcomTransmitterID: DexcomTransmitterID(newTransmitterID)!,
                                                      deviceUUID: "12345",
                                                      transmitterVersion: "1.0.1",
                                                      softwareNumber: 1,
                                                      apiVersion: 2,
                                                      storageTimeDays: 30,
                                                      maxStorageTimeDays: 120,
                                                      maxRuntimeDays: 90,
                                                      sessionTimeDays: 10,
                                                      isDiagnosticDataDownloadSupported: true,
                                                      isEgvBackfillSupported: true,
                                                      isPredictedEgvSupported: true,
                                                      activatedOn: Date())
        var cgmManagerState = DexcomCGMManagerState(dexcomTransmitterID: DexcomTransmitterID(newTransmitterID)!)
        cgmManagerState.transmitterStaticInfo = transmitterStaticInfo
        
        // Setup CGM Manager
        cgmManager = DexcomCGMManager(state: cgmManagerState,
                                      initialTransmitterCommunication: mockTxComm,
                                      findNewTransmitterCommunication: { _,_ in self.mockTxComm })
        
        // Set Mock Comms Delegate
        mockTxComm.connectionDelegate = cgmManager
        
        // setup settings view model
        viewModel = TransmitterDetailsViewModel(cgmManager: cgmManager,
                                                sessionRunning: false)
    }

    func testInitialization() {
        XCTAssertEqual(viewModel.serialNumber, transmitterStaticInfo.transmitterId)
        XCTAssertEqual(viewModel.activatedOn, transmitterStaticInfo.activatedOn)
        XCTAssertEqual(viewModel.firmware, transmitterStaticInfo.transmitterVersion)
        XCTAssertEqual(viewModel.softwareNumber, "SW" + String(transmitterStaticInfo.softwareNumber))
        XCTAssertFalse(viewModel.detailsAvailable)
        XCTAssertFalse(viewModel.sessionRunning)
    }
    
    func testTransmitterFound() {
        testExpectation = expectation(description: "Transmitter Found")

        // set us at an observer to fulfill test expectation
        cgmManager.addObserver(self, queue: .main)

        let newTransmitterID = "888888"
        let newTransmitterStaticInfo = TransmitterStaticInfo(dexcomTransmitterID: DexcomTransmitterID(newTransmitterID)!,
                                                          deviceUUID: "6789",
                                                          transmitterVersion: "1.1.1",
                                                          softwareNumber: 2,
                                                          apiVersion: 3,
                                                          storageTimeDays: 31,
                                                          maxStorageTimeDays: 121,
                                                          maxRuntimeDays: 91,
                                                          sessionTimeDays: 11,
                                                          isDiagnosticDataDownloadSupported: true,
                                                          isEgvBackfillSupported: true,
                                                          isPredictedEgvSupported: true,
                                                          activatedOn: nil)
        
        mockTxComm.transmitterFound(newTransmitterStaticInfo)
        
        wait(for: [testExpectation], timeout: 1)
        
        XCTAssertEqual(viewModel.serialNumber, newTransmitterStaticInfo.transmitterId)
        XCTAssertEqual(viewModel.firmware, newTransmitterStaticInfo.transmitterVersion)
        XCTAssertEqual(viewModel.softwareNumber, "SW" + String(newTransmitterStaticInfo.softwareNumber))
        XCTAssertTrue(viewModel.detailsAvailable)
        XCTAssertFalse(viewModel.sessionRunning)
    }

    func testNewSensorCompletionHandler() {
        let sensorCode = SensorCode(rawValue: "7171")
        viewModel.newSensorCompletionHandler(sensorCode)
        XCTAssertEqual(cgmManager.pendingSensorCode, sensorCode)
    }
    
    func testStopSensorCompletionHandler() {
        viewModel.stopSensorCompletionHandler()
        let stopSessionCmd = CgmCommand.stop(uuid: UUID(),
                                             systemTime: Date(),
                                             transmitterTime: nil,
                                             response: nil)
        XCTAssertTrue(mockTxComm.isCurrentCommand(command: stopSessionCmd))
    }
    
    func testNewTransmitterCompletionHandler() {
        let newTransmitterID = "888888"
        viewModel.newTransmitterCompletionHandler(DexcomTransmitterID(newTransmitterID)!)
        XCTAssertEqual(cgmManager.state.transmitterStaticInfo.transmitterId, newTransmitterID)
        XCTAssertTrue(mockTxComm.closeCalled)
        XCTAssertTrue(mockTxComm.startCalled)
    }
}

extension TransmitterDetailsViewModelTests: DexcomCGMManagerObserver {
    func cgmManagerDidFindTransmitter(_ manager: DexcomCGMManager) {
        testExpectation.fulfill()
    }
}
