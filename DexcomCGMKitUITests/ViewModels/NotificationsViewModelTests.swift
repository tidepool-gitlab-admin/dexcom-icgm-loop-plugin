//
//  NotificationsViewModelTests.swift
//  DexcomCGMKitUITests
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
import DexcomCGMKit
@testable import DexcomCGMKitUI

class NotificationsViewModelTests: XCTestCase {

    private var viewModel: NotificationsViewModel!
    
    override func setUp() {
        let alertConfigurationManager = DexcomAlertConfigurationManager()
        viewModel = NotificationsViewModel(alertConfigurationManager: alertConfigurationManager,
                                           glucoseUnit: HKUnit.milligramsPerDeciliter)
    }
    
    func testInitialization() {
        let configurations = DexcomUserMutableAlerts.defaultConfigurations(alwaysSoundEnabled: true)
        XCTAssertEqual(viewModel.glucoseUnit, HKUnit.milligramsPerDeciliter)
        XCTAssertEqual(viewModel.configurations.count, configurations.count)
        for configuration in viewModel.configurations {
            XCTAssertEqual(configuration, configurations[configuration.type])
        }
    }

    func testConfigurationUpdateHandler() {
        var configuration = DexcomAlertConfiguration(type: .glucoseLow, enabled: false, alwaysSoundEnabled: false, sound: .defaultSound)
        configuration.threshold = HKQuantity(unit: HKUnit.milligramsPerDeciliter, doubleValue: 65)
        viewModel.configurationUpdateHandler(configuration)
        XCTAssertEqual(viewModel.alertConfigurationManager.mutableAlertConfiguration(forType: .glucoseLow), configuration)
        XCTAssertEqual(viewModel.configurations.first(where: { $0.type == DexcomAlertType.glucoseLow })!, configuration)
    }
    
    func testResetAlerts() {
        // insert an updated alert configuration
        var configuration = DexcomAlertConfiguration(type: .glucoseLow, enabled: false, alwaysSoundEnabled: false, sound: .defaultSound)
               configuration.threshold = HKQuantity(unit: HKUnit.milligramsPerDeciliter, doubleValue: 65)
               viewModel.configurationUpdateHandler(configuration)
        
        XCTAssertEqual(viewModel.alertConfigurationManager.mutableAlertConfiguration(forType: .glucoseLow), configuration)
        XCTAssertEqual(viewModel.configurations.first(where: { $0.type == DexcomAlertType.glucoseLow })!, configuration)
        
        viewModel.resetAlerts()
        
        let defaultAlertConfigurations = DexcomUserMutableAlerts.defaultConfigurations(alwaysSoundEnabled: true)
        XCTAssertNotEqual(viewModel.alertConfigurationManager.mutableAlertConfiguration(forType: .glucoseLow), configuration)
        XCTAssertNotEqual(viewModel.configurations.first(where: { $0.type == DexcomAlertType.glucoseLow })!, configuration)
        XCTAssertEqual(viewModel.alertConfigurationManager.mutableAlertConfiguration(forType: .glucoseLow), defaultAlertConfigurations[.glucoseLow])
        XCTAssertEqual(viewModel.configurations.first(where: { $0.type == DexcomAlertType.glucoseLow })!, defaultAlertConfigurations[.glucoseLow])
    }
}
