//
//  WeekdaySelectionViewModelTests.swift
//  DexcomCGMKitUITests
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import XCTest
import DexcomCGMKit
@testable import DexcomCGMKitUI

class WeekdaySelectionViewModelTests: XCTestCase {

    private var updatedDays: Days!

    func testInitialization() {
        let selectedDays = Days.monday
        let availableDays = Days.allCases
        let viewModel = WeekdaySelectionViewModel(selectedDays: selectedDays, updateHandler: { _ in })
        XCTAssertEqual(Days.combineOptions(viewModel.selectedDays), selectedDays)
        XCTAssertEqual(viewModel.selectableDays, availableDays)
        XCTAssertEqual(viewModel.daysAsString(selectedDays), String(describing: selectedDays))
    }

    func testDaySelection() {
        let originalDays: Days = [.sunday, .tuesday]
        let viewModel = WeekdaySelectionViewModel(selectedDays: originalDays,
                                                  updateHandler: { [weak self] in self?.didSelectNewDays($0) })
        let newDays: Days = [.tuesday, .thursday]
        viewModel.selectedDays = newDays.expandOptions()
        XCTAssertNotEqual(Days.combineOptions(viewModel.selectedDays), originalDays)
        XCTAssertEqual(Days.combineOptions(viewModel.selectedDays), newDays)
        XCTAssertEqual(updatedDays, newDays)
    }
}

extension WeekdaySelectionViewModelTests {
    func didSelectNewDays(_ days: Days) {
        updatedDays = days
    }
}
