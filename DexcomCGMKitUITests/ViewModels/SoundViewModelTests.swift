//
//  SoundViewModelTests.swift
//  DexcomCGMKitUITests
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import XCTest
import DexcomCGMKit
@testable import DexcomCGMKitUI

class SoundViewModelTests: XCTestCase {

    private var updatedSound: AlertSound!
    
    func testInitialization() {
        let selectedSound = AlertSound.defaultSound
        let availableSounds = AlertSound.allCases.map { $0.rawValue }
        let viewModel = SoundViewModel(selectedSound: selectedSound,
                                       updateHandler: { [weak self] in self?.didSelectNewSound($0) })
        XCTAssertEqual(viewModel.selectedSound, selectedSound.rawValue)
        XCTAssertEqual(viewModel.sounds, availableSounds)
    }
    
    func testSoundSelection() {
        let originalSound = AlertSound.defaultSound
        let viewModel = SoundViewModel(selectedSound: originalSound,
                                       updateHandler: { [weak self] in self?.didSelectNewSound($0) })
        let newSound = AlertSound.intense
        viewModel.selectedSound = newSound.rawValue
        XCTAssertNotEqual(viewModel.selectedSound, originalSound.rawValue)
        XCTAssertEqual(viewModel.selectedSound, newSound.rawValue)
        XCTAssertEqual(updatedSound, newSound)
    }

}

extension SoundViewModelTests {
    func didSelectNewSound(_ sound: AlertSound) {
        updatedSound = sound
    }
}
