//
//  AlertScheduleDetailsViewModelTests.swift
//  DexcomCGMKitUITests
//
//  Created by Nathaniel Hamming on 2020-03-12.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
import DexcomCGMKit
@testable import DexcomCGMKitUI

class AlertScheduleDetailsViewModelTests: XCTestCase {

    private var viewModel: AlertScheduleDetailsViewModel!
    private var glucoseUnit: HKUnit = HKUnit.milligramsPerDeciliter
    private var alertSchedule: DexcomAlertSchedule!
    private var alertConfigurationManager: DexcomAlertConfigurationManager!
    
    override func setUp() {
        let calendar = Calendar(identifier: .gregorian)
        let today = Date()
        let originalSeconds = 0
        let originalminutes = 10
        let originalStartHour = 20
        let originalEndHour = 8
        let startTime = calendar.date(bySettingHour: originalStartHour, minute: originalminutes, second: originalSeconds, of: today)!
        let endTime = calendar.date(bySettingHour: originalEndHour, minute: originalminutes, second: originalSeconds, of: today)!
        let startTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: startTime)
        let endTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: endTime)
        let days: Days = [.sunday]
        
        alertSchedule = DexcomAlertSchedule.defaultSchedule()
        alertSchedule.weeklySchedule = WeeklySchedule(days: days,
                                                             startTimeComponents: startTimeComponents,
                                                             endTimeComponents: endTimeComponents)
        alertSchedule.name = "Test Schedule"
        
        alertConfigurationManager = DexcomAlertConfigurationManager(alertSchedule: alertSchedule)
        viewModel = AlertScheduleDetailsViewModel(alertConfigurationManager: alertConfigurationManager,
                                                  glucoseUnit: glucoseUnit)
    }

    func testInitialization() {
        XCTAssertTrue(viewModel.hasAlertSchedule)
        for configuration in viewModel.configurations {
            XCTAssertEqual(configuration, alertSchedule.configurations[configuration.type])
        }
        XCTAssertEqual(viewModel.alertSchedule.enabled, alertSchedule.enabled)
        XCTAssertEqual(viewModel.alertSchedule.alwaysSoundEnabled, alertSchedule.alwaysSoundEnabled)
        XCTAssertEqual(viewModel.scheduleName, alertSchedule.name)
        XCTAssertEqual(viewModel.scheduleStartTime, alertSchedule.weeklySchedule.startTime())
        XCTAssertEqual(viewModel.scheduleEndTime, alertSchedule.weeklySchedule.endTime())
        XCTAssertEqual(viewModel.alertSchedule.weeklySchedule.days, alertSchedule.weeklySchedule.days)
    }

    func testDaysUpdateHandler() {
        let expectedDays: Days = [.monday, .thursday]
        viewModel.daysUpdatedHandler(expectedDays)
        XCTAssertEqual(viewModel.alertSchedule.weeklySchedule.days, expectedDays)
    }
    
    func testSaveAlertSchedule() {
        var expectedAlertSchedule = DexcomAlertSchedule.defaultSchedule()
        expectedAlertSchedule.name = "Expected Schedule"
        expectedAlertSchedule.enabled = false
        expectedAlertSchedule.alwaysSoundEnabled = false
        viewModel.alertSchedule = expectedAlertSchedule
        viewModel.saveAlertSchedule()
        XCTAssertEqual(alertConfigurationManager.alertSchedule, expectedAlertSchedule)
        XCTAssertTrue(alertConfigurationManager.hasAlertSchedule)
        XCTAssertEqual(alertConfigurationManager.alertSchedule!.enabled, expectedAlertSchedule.enabled)
        XCTAssertEqual(alertConfigurationManager.alertSchedule!.alwaysSoundEnabled, expectedAlertSchedule.alwaysSoundEnabled)
        XCTAssertEqual(alertConfigurationManager.alertSchedule!.configurations, expectedAlertSchedule.configurations)
        XCTAssertEqual(alertConfigurationManager.alertSchedule!.name, expectedAlertSchedule.name)
        XCTAssertEqual(alertConfigurationManager.alertSchedule!.weeklySchedule.startTimeComponents, expectedAlertSchedule.weeklySchedule.startTimeComponents)
        XCTAssertEqual(alertConfigurationManager.alertSchedule!.weeklySchedule.endTimeComponents, expectedAlertSchedule.weeklySchedule.endTimeComponents)
        XCTAssertEqual(alertConfigurationManager.alertSchedule!.weeklySchedule.days, expectedAlertSchedule.weeklySchedule.days)
    }
    
    func testConfigurationUpdateHandler() {
        let expectedConfiguration = DexcomAlertConfiguration(type: .glucoseRateFall,
                                                             enabled: false,
                                                             alwaysSoundEnabled: false,
                                                             sound: .defaultSound)
        viewModel.configurationUpdateHandler(expectedConfiguration)
        for configuration in viewModel.configurations {
            if configuration.type == expectedConfiguration.type {
                XCTAssertEqual(configuration, expectedConfiguration)
            }
        }
        XCTAssertEqual(alertConfigurationManager.alertSchedule?.configurations[.glucoseRateFall], expectedConfiguration)
    }
}
