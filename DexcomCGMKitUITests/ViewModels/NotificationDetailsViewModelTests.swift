//
//  NotificationDetailsViewModelTests.swift
//  DexcomCGMKitUITests
//
//  Created by Nathaniel Hamming on 2020-03-12.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
import DexcomCGMKit
@testable import DexcomCGMKitUI

class NotificationDetailsViewModelTests: XCTestCase {

    private var viewModel: NotificationDetailsViewModel!
    private var glucoseUnit = HKUnit.milligramsPerDeciliter
    private var originalConfiguration: DexcomAlertConfiguration!
    private var updatedConfiguration: DexcomAlertConfiguration!

    override func setUp() {
        originalConfiguration = DexcomAlertConfiguration(type: .glucoseLow, enabled: true, alwaysSoundEnabled: true, sound: .defaultSound)
        viewModel = NotificationDetailsViewModel(configuration: originalConfiguration,
                                                 glucoseUnit: glucoseUnit,
                                                 updateHandler: { [weak self] in self?.didUpdateConfiguration($0) })
    }
    
    func testInitialization() {
        XCTAssertEqual(viewModel.configuration, originalConfiguration)
        XCTAssertEqual(viewModel.glucoseUnit, HKUnit.milligramsPerDeciliter)
        XCTAssertEqual(viewModel.configurationDescription, originalConfiguration.type.localizedDescription)
        XCTAssertEqual(viewModel.threshold, originalConfiguration.threshold.doubleValue(for: glucoseUnit))
        XCTAssertEqual(viewModel.repeatValue, originalConfiguration.snoozeFrequency.timeInterval)
        XCTAssertTrue(viewModel.displaysDetails)
        XCTAssertFalse(viewModel.isUrgentLow)
        XCTAssertEqual(viewModel.configurationUnit, glucoseUnit)
        
        let expectedThresholdValues = Array(stride(from: originalConfiguration.thresholdMinValue.doubleValue(for: glucoseUnit),
                                                   through: originalConfiguration.thresholdMaxValue.doubleValue(for: glucoseUnit),
                                                   by: 1).map { $0.roundedToTenths })
        XCTAssertEqual(viewModel.thresholdValues(), expectedThresholdValues)
     
        var expectedRepeatValues = Array(stride(from: TimeInterval.minutes(15), through: TimeInterval.hours(4), by: TimeInterval.minutes(5)))
        expectedRepeatValues.insert(TimeInterval.minutes(0), at: 0)
        XCTAssertEqual(viewModel.repeatValues(), expectedRepeatValues)
        
        XCTAssertEqual(viewModel.formattedThreshold(80), "80 mg/dL")
        XCTAssertEqual(viewModel.formattedRepeat(TimeInterval.minutes(65)), "1 hour, 5 minutes")
        XCTAssertEqual(viewModel.formattedSelectedRepeat(TimeInterval.minutes(0)), "Every time")
    }
    
    func testSoundUpdateHandler() {
        XCTAssertEqual(viewModel.configuration.sound, AlertSound.defaultSound)
        viewModel.soundUpdateHandler(AlertSound.intense)
        XCTAssertEqual(viewModel.configuration.sound, AlertSound.intense)
    }

    
    func testConfigurationUpdateHandler() {
        var expectedConfiguration = DexcomAlertConfiguration(type: .glucoseLow, enabled: true, alwaysSoundEnabled: true, sound: .defaultSound)
        expectedConfiguration.threshold = HKQuantity(unit: glucoseUnit, doubleValue: 75)
        expectedConfiguration.snoozeFrequency = DexcomSnoozeFrequency(timeInterval: TimeInterval.minutes(45))
        viewModel.configuration = expectedConfiguration
        XCTAssertEqual(viewModel.configuration, expectedConfiguration)
        XCTAssertEqual(updatedConfiguration, expectedConfiguration)
    }
    
}

extension NotificationDetailsViewModelTests {
    func didUpdateConfiguration(_ configuration: DexcomAlertConfiguration) {
        updatedConfiguration = configuration
    }
}
