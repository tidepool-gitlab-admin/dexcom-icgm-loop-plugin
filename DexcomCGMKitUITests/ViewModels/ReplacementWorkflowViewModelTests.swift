//
//  ReplacementWorkflowViewModelTests.swift
//  DexcomCGMKitUITests
//
//  Created by Nathaniel Hamming on 2020-03-12.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import XCTest
//import HealthKit
import DexcomCGMKit
import TransmitterCore
@testable import DexcomCGMKitUI

class ReplacementWorkflowViewModelTests: XCTestCase {

    private var viewModel: ReplacementWorkflowViewModel!
    private var didStopSensor: Bool = false
    private var newSensorCode: SensorCode?
    private var newTransmitterID: DexcomTransmitterID!
    
    override func setUp() {
        viewModel = ReplacementWorkflowViewModel(replacementWorkflow: .replaceTransmitterAndSensor,
                                                 replaceSensorCompletionHandler: { [weak self] in self?.replacementSensorCode($0) }, stopSessionHandler: { [weak self] in self?.stopSensor() }, replaceTransmitterCompletionHandler: { [weak self] in self?.replacementTransmitterID($0) })
    }

    func testInitialization() {
        XCTAssertEqual(viewModel.replacementWorkflow, ReplacementWorkflow.replaceTransmitterAndSensor)
    }
    
    func testValidSensorCode() {
        let validSensorCode = "7171"
        let invalidSensorCode = "1111"
        XCTAssertTrue(viewModel.validSensorCode(validSensorCode))
        XCTAssertFalse(viewModel.validSensorCode(invalidSensorCode))
        
        let validSensorBarcode = Dexcom2DBarcode(.sensor, code: validSensorCode)
        let invalidSensorBarcode = Dexcom2DBarcode(.sensor, code: invalidSensorCode)
        XCTAssertTrue(viewModel.validSensorCode(validSensorBarcode))
        XCTAssertFalse(viewModel.validSensorCode(invalidSensorBarcode))
    }
    
    func testValidTransmitterID() {
        let validTransmitterID = "888888"
        let invalidTransmitterID = "444444"
        XCTAssertTrue(viewModel.validTransmitterID(validTransmitterID))
        XCTAssertFalse(viewModel.validTransmitterID(invalidTransmitterID))
        
        let barcodeGroupSeparator = "\u{1d}"
        let validTransmitterBarcode = Dexcom2DBarcode(.transmitter, code: barcodeGroupSeparator+validTransmitterID)
        let invalidTransmitterBarcode = Dexcom2DBarcode(.transmitter, code: barcodeGroupSeparator+invalidTransmitterID)
        XCTAssertTrue(viewModel.validTransmitterID(validTransmitterBarcode))
        XCTAssertFalse(viewModel.validTransmitterID(invalidTransmitterBarcode))
    }

    func testReplaceSensorCompletionHandler() {
        var expectedSensorCode: SensorCode? = SensorCode(rawValue: "7171")
        viewModel.replaceSensorCompletionHandler(expectedSensorCode)
        XCTAssertEqual(newSensorCode, expectedSensorCode)
        
        expectedSensorCode = SensorCode(rawValue: "")
        viewModel.replaceSensorCompletionHandler(expectedSensorCode)
        XCTAssertEqual(newSensorCode, expectedSensorCode)
        XCTAssertNil(newSensorCode)
    }
    
    func testReplaceTransmitterCompletionHandler() {
        let expectedTransmitterID = DexcomTransmitterID("888888")!
        viewModel.replaceTransmitterCompletionHandler?(expectedTransmitterID)
        XCTAssertEqual(newTransmitterID, expectedTransmitterID)
    }
    
    func testStopSessionHandler() {
        viewModel.stopSessionHandler?()
        XCTAssertTrue(didStopSensor)
    }
    
    func testWorkflowCompletionHandler() {
        let validTransmitterID = "888888"
        let expectedTransmitterID = DexcomTransmitterID(validTransmitterID)!
        XCTAssertTrue(viewModel.validTransmitterID(validTransmitterID))
        viewModel.workflowCompletionHandler?()
        XCTAssertEqual(newTransmitterID, expectedTransmitterID)


        // need to make a new view model for sensor replacement workflow
        viewModel = ReplacementWorkflowViewModel(replacementWorkflow: .replaceSensor,
                                                 replaceSensorCompletionHandler: { [weak self] in self?.replacementSensorCode($0) }, stopSessionHandler: { [weak self] in self?.stopSensor() }, replaceTransmitterCompletionHandler: { [weak self] in self?.replacementTransmitterID($0) })
        let validSensorCode = "7171"
        let expectedSensorCode: SensorCode? = SensorCode(rawValue: validSensorCode)
        XCTAssertTrue(viewModel.validSensorCode(validSensorCode))
        viewModel.workflowCompletionHandler?()
        XCTAssertEqual(newSensorCode, expectedSensorCode)
    }
}


extension ReplacementWorkflowViewModelTests {
    func stopSensor() {
        didStopSensor = true
    }
    
    func replacementSensorCode(_ sensorCode: SensorCode?) {
        newSensorCode = sensorCode
    }
    
    func replacementTransmitterID(_ transmitterID: DexcomTransmitterID) {
        newTransmitterID = transmitterID
    }
}
