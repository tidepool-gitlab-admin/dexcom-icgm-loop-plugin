//
//  NotificationListViewModelTests.swift
//  DexcomCGMKitUITests
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import XCTest
import HealthKit
import DexcomCGMKit
@testable import DexcomCGMKitUI

class NotificationListViewModelTests: XCTestCase {

    private var updatedConfiguration: DexcomAlertConfiguration!
    private var viewModel: NotificationListViewModel!
    private var glucoseUnit = HKUnit.milligramsPerDeciliter
    
    override func setUp() {
        let configurations: [DexcomAlertConfiguration] = DexcomUserMutableAlerts.defaultConfigurations(alwaysSoundEnabled: true).compactMap { $0.value }
        viewModel = NotificationListViewModel(configurations: configurations,
                                              glucoseUnit: glucoseUnit,
                                              configurationUpdateHandler: { [weak self] in self?.configurationUpdateHandler($0) })
    }
    
    func testInitialization() {
        let configurations: [DexcomAlertConfiguration] = DexcomUserMutableAlerts.defaultConfigurations(alwaysSoundEnabled: true).compactMap { $0.value }
        let viewModel = NotificationListViewModel(configurations: configurations,
                                                  glucoseUnit: HKUnit.milligramsPerDeciliter,
                                                  configurationUpdateHandler: { _ in })
        XCTAssertEqual(viewModel.glucoseUnit, HKUnit.milligramsPerDeciliter)
        XCTAssertEqual(viewModel.configurations, configurations)
    }
    
    func testDisplayValue() {
        var configurations = DexcomUserMutableAlerts.defaultConfigurations(alwaysSoundEnabled: true)
        configurations[.signalLoss] = DexcomAlertConfiguration(type: .signalLoss, enabled: true, alwaysSoundEnabled: true, sound: .defaultSound)
        for configuration in configurations {
            let displayValue = viewModel.value(forConfiguration: configuration.value)
            if !configuration.value.enabled {
                XCTAssertEqual(displayValue, "Off")
            } else {
                switch configuration.key {
                case .glucoseUrgentLow, .glucoseLow, .glucoseHigh:
                    XCTAssertEqual(displayValue, String(format: "%.0f mg/dL", configuration.key.defaultThreshold))
                case .signalLoss:
                    XCTAssertEqual(displayValue, "On")
                default:
                    XCTAssert(true) // this point should never be reached
                }
            }
        }
    }

    func testUpdateHandler() {
        let configuration = DexcomAlertConfiguration(type: .glucoseLow, enabled: false, alwaysSoundEnabled: false, sound: .defaultSound)
        viewModel.configurationUpdateHandler(configuration)
        XCTAssertEqual(updatedConfiguration, configuration)
    }
}

extension NotificationListViewModelTests {
    func configurationUpdateHandler(_ configuration: DexcomAlertConfiguration) {
        updatedConfiguration = configuration
    }
}
