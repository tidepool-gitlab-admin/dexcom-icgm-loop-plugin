//
//  DexcomCGMKitPlugin.swift
//  DexcomCGMKitPlugin
//
//  Created by Nathaniel Hamming on 2019-08-20.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation
import LoopKitUI
import DexcomCGMKit
import DexcomCGMKitUI

class DexcomCGMKitPlugin: NSObject, LoopUIPlugin {
    public var pumpManagerType: PumpManagerUI.Type? {
        return nil
    }
    
    public var cgmManagerType: CGMManagerUI.Type? {
        return DexcomCGMManager.self
    }
    
    override init() {
        super.init()
        
        print("Loaded DexcomCGMKitPlugin class")
    }
}
