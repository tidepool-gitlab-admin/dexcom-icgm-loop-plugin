//
//  Days.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-11-26.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

public struct Days: OptionSet, Codable, Hashable {
    public let rawValue: Int
    
    public static let sunday = Days(rawValue: 1 << 0)
    public static let monday = Days(rawValue: 1 << 1)
    public static let tuesday = Days(rawValue: 1 << 2)
    public static let wednesday = Days(rawValue: 1 << 3)
    public static let thursday = Days(rawValue: 1 << 4)
    public static let friday = Days(rawValue: 1 << 5)
    public static let saturday = Days(rawValue: 1 << 6)
    
    public static let none: Days = []
    public static let everyday: Days = [.sunday, .monday, .tuesday, .wednesday, .thursday, .friday, .saturday]
    public static let weekdays: Days = [.monday, .tuesday, .wednesday, .thursday, .friday]
    public static let weekend: Days = [.sunday, .saturday]
    
    public static let allCases: [Days] = [.sunday, .monday, .tuesday, .wednesday, .thursday, .friday, .saturday]
        
    static var localizedCalendar: Calendar {
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale.autoupdatingCurrent
        return calendar
    }

    public static var localizedWeekdaySymbols: [String] {
        return Days.localizedCalendar.standaloneWeekdaySymbols
    }
    
    static var localizedShortWeekdaySymbols: [String] {
        return Days.localizedCalendar.shortStandaloneWeekdaySymbols
    }
        
    public var localizedValue: String {
        switch self {
        case .none  :
            return LocalizedString("No days selected", comment: "The displayed value when no days of the week were selected.")
        case .everyday:
            return LocalizedString("Every day", comment: "The displayed value when all days of the week are selected.")
        case .weekdays:
            return LocalizedString("Weekdays", comment: "The displayed value when only Monday, Tuesday, Wednesday, Thursday and Friday are selected.")
        case .weekend:
            return LocalizedString("Weekend", comment: "The displayed value when only Saturday and Sunday are selected.")
        default:
            var value = ""
            let localizedShortWeekdaySymbols = Days.localizedShortWeekdaySymbols
            
            for (index, day) in Days.allCases.enumerated() {
                if self.contains(day) {
                    if value.count == 0 {
                        value = localizedShortWeekdaySymbols[index]
                    } else {
                        value = value + " " + localizedShortWeekdaySymbols[index]
                    }
                }
            }
            return value
        }
    }
    
    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
    
    public func contains(yesterdayOfDate date: Date) -> Bool {
        let weekdayIndex = Days.localizedCalendar.component(.weekday, from: date)
        let yesterdayIndex = weekdayIndex - 1 == 0 ? 7 : weekdayIndex - 1
        return self.contains(gregorianWeekdayIndex: yesterdayIndex)
    }
    
    public func contains(dayOfDate date: Date) -> Bool {
        let weekdayIndex = Days.localizedCalendar.component(.weekday, from: date)
        return self.contains(gregorianWeekdayIndex: weekdayIndex)
    }
    
    public func contains(gregorianWeekdayIndex: Int) -> Bool {
        // Weekday units are the numbers 1 through n, where n is the number of days in the week. For example, in the Gregorian calendar, n is 7 and Sunday is represented by 1.
        guard gregorianWeekdayIndex >= 1 && gregorianWeekdayIndex <= 7 else {
            return false
        }
        return self.contains(Days.allCases[gregorianWeekdayIndex-1])
    }
}
    
extension Days {
    static public func combineOptions(_ expandedDays: Set<Days>) -> Days {
        return expandedDays.reduce(Days.none) {
            return $1.union($0)
        }
    }
    
    public func expandOptions() -> Set<Days> {
        return Set<Days>(Days.allCases.filter { self.contains($0) })
    }
}

extension Days: CustomStringConvertible {
    public var description: String {
        var description = ""
        for (index, day) in Days.allCases.enumerated() {
            if self.contains(day) {
                if !description.isEmpty {
                    description.append(" ")
                }
                description.append(Days.localizedWeekdaySymbols[index])
            }
        }
        return description
    }
}
