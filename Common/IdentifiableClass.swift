//
//  IdentifiableClass.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-09.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation

protocol IdentifiableClass: class {
    static var className: String { get }
}

extension IdentifiableClass {
    static var className: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}
