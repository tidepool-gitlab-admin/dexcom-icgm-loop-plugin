//
//  WeeklySchedule.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-11-27.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation

public struct WeeklySchedule: Codable, Equatable {
    static public var timeComponents: Set<Calendar.Component> {
        return [.hour, .minute]
    }
    
    public var days: Days

    // wall clock value of the start time
    public var startTimeComponents: DateComponents
    
    // wall clock value of the end time
    public var endTimeComponents: DateComponents
    
    public init() {
        days = [.none]
        startTimeComponents = DateComponents()
        endTimeComponents = DateComponents()
    }
    
    public init(days: Days, startTimeComponents: DateComponents, endTimeComponents: DateComponents) {
        self.days = days
        self.startTimeComponents = startTimeComponents
        self.endTimeComponents = endTimeComponents
    }
    
    public func contains(_ date: Date, usingCalendar calendar: Calendar = Calendar.current) -> Bool {
        // determine if the schedule runs overnight
        let startTimeOffset = TimeInterval.hours(startTimeComponents.hour ?? 0) + TimeInterval.minutes(startTimeComponents.minute ?? 0)
        let endTimeOffset = TimeInterval.hours(endTimeComponents.hour ?? 0) + TimeInterval.minutes(endTimeComponents.minute ?? 0)
        let overnightSchedule = endTimeOffset < startTimeOffset
        
        // check the day
        let isStartDay = days.contains(dayOfDate: date)
        let isEndDay = days.contains(yesterdayOfDate: date)
        guard isStartDay || (overnightSchedule && isEndDay) else {
            return false
        }
        
        // check the time
        // use the user's current calendar/timezone to interpret the incoming date and convert to wall clock value with that context
        let timeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: date)
        let time = TimeInterval.hours(timeComponents.hour ?? 0) + TimeInterval.minutes(timeComponents.minute ?? 0)
        if !overnightSchedule,
            isStartDay,
            time >= startTimeOffset && time <= endTimeOffset {
            return true
        } else if overnightSchedule,
            isStartDay,
            time >= startTimeOffset {
            return true
        } else if overnightSchedule,
            isEndDay,
            time <= endTimeOffset {
            return true
        } else {
            return false
        }
    }
    
    public func startTime(usingCalendar calendar: Calendar = Calendar.current) -> Date {
        return time(forTimeComponents: startTimeComponents, usingCalendar: calendar) ?? Date()
    }
    
    public func endTime(usingCalendar calendar: Calendar = Calendar.current) -> Date {
        return time(forTimeComponents: endTimeComponents, usingCalendar: calendar) ?? Date()
    }
    
    private func time(forTimeComponents timeComponents: DateComponents, usingCalendar calendar: Calendar) -> Date? {
        let time = calendar.date(bySettingHour: timeComponents.hour ?? 0,
                             minute: timeComponents.minute ?? 0,
                             second: timeComponents.second ?? 0,
                             of: Date())
        return time
    }
}
