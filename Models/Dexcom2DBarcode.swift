//
//  Dexcom2DBarcode.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-09-18.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation
import AVFoundation
import TransmitterCore

public struct Dexcom2DBarcode {
    static let type : AVMetadataObject.ObjectType = .dataMatrix
    
    public enum DeviceType: Int {
        case sensor
        case transmitter
        
        var suffixLength: Int {
            switch self {
            case .sensor:
                return SensorCode.codeLength
            case .transmitter:
                return DexcomTransmitterID.length
            }
        }
    }
    
    let forDeviceType: DeviceType
    private var codeString: String?
    private(set) public var code: String? {
        get {
            if isValid(),
                let codeString = codeString {
                return String(codeString.suffix(forDeviceType.suffixLength))
            }
            return nil
        }
        set {
            codeString = newValue
        }
    }
    
    public init(_ forDeviceType: DeviceType, code: String?) {
        self.forDeviceType = forDeviceType
        self.code = code
    }
    
    public func isValid() -> Bool {
        // check for correct suffix length
        guard let codeString = codeString,
            codeString.count >= forDeviceType.suffixLength else {
                return false
        }
        
        // check for regular express of code
        let range = NSRange(codeString.startIndex..<codeString.endIndex, in: codeString)
        
        switch forDeviceType {
        case .sensor:
            let pattern = #".*([0-9]{4})$"#
            let regex = try! NSRegularExpression(pattern: pattern)
            guard let firstMatch = regex.firstMatch(in: codeString, options: [], range: range),
                let firstMatchRange = Range(firstMatch.range(at: 1), in: codeString),
                SensorCode(rawValue: String(codeString[firstMatchRange])) != nil else {
                return false
            }
        case .transmitter:
            let pattern = #"^\u001d.*"# + DexcomTransmitterID.pattern
            let regex = try! NSRegularExpression(pattern: pattern)
            guard let firstMatch = regex.firstMatch(in: codeString, options: [], range: range),
                let firstMatchRange = Range(firstMatch.range(at: 1), in: codeString),
                DexcomTransmitterID(String(codeString[firstMatchRange])) != nil else {
                    return false
            }
        }
        
        return true
    }
}
