//
//  DexcomCGMReading.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-12-09.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import TransmitterCore

struct DexcomCGMReading: TransmitterCgmReading, Codable {
    
    let recordedSystemTime: Date
    
    let transmitterId: String
    
    var transmitterTime: CodableTransmitterTime
    
    // TransmitterTime isn't codable, but required by the Dexcom SDK TransmitterCgmReading protocol
    var egvTransmitterTime: TransmitterTime {
        get {
            return transmitterTime
        }
        set {
            transmitterTime = CodableTransmitterTime(newValue)
        }
    }
        
    let egvSystemTime: Date
    
    let sequenceNumber: UInt
    
    let algorithmState: AlgorithmState
    
    let egv: UInt
    
    let predictedEgv: UInt?
    
    // NOTE: according to Dexcom, a `rate` of 12.7 is invalid.  However, we still want to store that value here, and
    // only "reflect" it to loop as invalid when it needs it, or else Dexcom gets very confused when reading back its
    // backfill cache.  See LOOP-996.
    let rate: Double
    
    let isBgGeneratedOnTransmitter: Bool
    
    let aberrationState: AberrationState
        
    init(_ cgmReading: TransmitterCgmReading) {
        self.recordedSystemTime = cgmReading.recordedSystemTime
        self.transmitterId = cgmReading.transmitterId
        self.transmitterTime = CodableTransmitterTime(cgmReading.egvTransmitterTime)
        self.egvSystemTime = cgmReading.egvSystemTime
        self.sequenceNumber = cgmReading.sequenceNumber
        self.algorithmState = cgmReading.algorithmState
        self.egv = cgmReading.egv
        self.predictedEgv = cgmReading.predictedEgv
        self.rate = cgmReading.rate
        self.isBgGeneratedOnTransmitter = cgmReading.isBgGeneratedOnTransmitter
        self.aberrationState = cgmReading.aberrationState
    }
}

extension DexcomCGMReading: Equatable {
    public static func == (lhs: DexcomCGMReading, rhs: DexcomCGMReading) -> Bool {
        return lhs.transmitterId == rhs.transmitterId &&
            lhs.transmitterTime == rhs.transmitterTime &&
            lhs.sequenceNumber == rhs.sequenceNumber &&
            lhs.egv == rhs.egv
    }
}

struct CodableTransmitterTime: TransmitterTime, Codable, Equatable {
    let seconds: UInt
    
    init(_ transmitterTime: TransmitterTime) {
        seconds = transmitterTime.seconds
    }
}
