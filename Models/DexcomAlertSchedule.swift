//
//  DexcomAlertSchedule.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-11-27.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation

public struct DexcomAlertSchedule: DexcomAlertDetails {
    public static func defaultConfigurations(alwaysSoundEnabled: Bool) -> [DexcomAlertType: DexcomAlertConfiguration] {
        // urgent low always sounds
        let urgentLowAlertConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.glucoseUrgentLow,
                                                             enabled: true,
                                                             alwaysSoundEnabled: true,
                                                             sound: .defaultSound)
        
        let lowAlertConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.glucoseLow,
                                                             enabled: true,
                                                             alwaysSoundEnabled: alwaysSoundEnabled,
                                                             sound: .defaultSound)
        
        let highAlertConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.glucoseHigh,
                                                              enabled: true,
                                                              alwaysSoundEnabled: alwaysSoundEnabled,
                                                              sound: .defaultSound)
        
        let risingAlertConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.glucoseRateRise,
                                                                enabled: false,
                                                                alwaysSoundEnabled: alwaysSoundEnabled,
                                                                sound: .defaultSound)
        
        let fallingAlertConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.glucoseRateFall,
                                                                 enabled: false,
                                                                 alwaysSoundEnabled: alwaysSoundEnabled,
                                                                 sound: .defaultSound)
        
        let configurations: [DexcomAlertType: DexcomAlertConfiguration] = [
            DexcomAlertType.glucoseUrgentLow: urgentLowAlertConfiguration,
            DexcomAlertType.glucoseLow: lowAlertConfiguration,
            DexcomAlertType.glucoseHigh: highAlertConfiguration,
            DexcomAlertType.glucoseRateRise: risingAlertConfiguration,
            DexcomAlertType.glucoseRateFall: fallingAlertConfiguration
        ]
        
        return configurations
    }
    
    public static func defaultSchedule() -> DexcomAlertSchedule {
        let weeklySchedule = WeeklySchedule()
        let configurations = DexcomAlertSchedule.defaultConfigurations(alwaysSoundEnabled: true)
        return DexcomAlertSchedule(configurations: configurations,
                                   name: "",
                                   enabled: true,
                                   weeklySchedule: weeklySchedule)
    }
    
    public var configurations: [DexcomAlertType: DexcomAlertConfiguration]
    
    public var name: String
    
    public var enabled: Bool
    
    public var weeklySchedule: WeeklySchedule

    public init(configurations: [DexcomAlertType: DexcomAlertConfiguration] = [:], name: String, enabled: Bool, weeklySchedule: WeeklySchedule) {
        self.configurations = configurations
        self.name = name
        self.enabled = enabled
        self.weeklySchedule = weeklySchedule
    }
}

extension DexcomAlertSchedule: Equatable {
    public static func == (lhs: DexcomAlertSchedule, rhs: DexcomAlertSchedule) -> Bool {
        return lhs.configurations == rhs.configurations &&
            lhs.name == rhs.name &&
            lhs.enabled == rhs.enabled &&
            lhs.weeklySchedule == rhs.weeklySchedule
    }
}
