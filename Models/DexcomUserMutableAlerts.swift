//
//  DexcomMutableAlerts.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-12-05.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation

public struct DexcomUserMutableAlerts: DexcomAlertDetails {
    public static func defaultConfigurations(alwaysSoundEnabled: Bool) -> [DexcomAlertType: DexcomAlertConfiguration] {
        // urgent low always sounds
        let urgentLowAlertConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.glucoseUrgentLow,
                                                                   enabled: true,
                                                                   alwaysSoundEnabled: true,
                                                                   sound: .defaultSound)

        let lowAlertConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.glucoseLow,
                                                             enabled: true,
                                                             alwaysSoundEnabled: alwaysSoundEnabled,
                                                             sound: .defaultSound)
        
        let highAlertConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.glucoseHigh,
                                                              enabled: true,
                                                              alwaysSoundEnabled: alwaysSoundEnabled,
                                                              sound: .defaultSound)
                
        let risingAlertConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.glucoseRateRise,
                                                                enabled: false,
                                                                alwaysSoundEnabled: alwaysSoundEnabled,
                                                                sound: .defaultSound)
        
        let fallingAlertConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.glucoseRateFall,
                                                                 enabled: false,
                                                                 alwaysSoundEnabled: alwaysSoundEnabled,
                                                                 sound: .defaultSound)
        
        let signalLossAlertConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.signalLoss,
                                                                    enabled: false,
                                                                    alwaysSoundEnabled: alwaysSoundEnabled,
                                                                    sound: .defaultSound)
            
        let noReadingsAlertConfiguration = DexcomAlertConfiguration(type: DexcomAlertType.noReadings,
                                                                    enabled: false,
                                                                    alwaysSoundEnabled: alwaysSoundEnabled,
                                                                    sound: .defaultSound)
        
        let configurations: [DexcomAlertType: DexcomAlertConfiguration] = [
            .glucoseUrgentLow: urgentLowAlertConfiguration,
            .glucoseLow: lowAlertConfiguration,
            .glucoseHigh: highAlertConfiguration,
            .glucoseRateRise: risingAlertConfiguration,
            .glucoseRateFall: fallingAlertConfiguration,
            .signalLoss: signalLossAlertConfiguration,
            .noReadings: noReadingsAlertConfiguration
        ]
        return configurations
    }
    
    public static func defaultAlerts() -> DexcomUserMutableAlerts {
        let configurations = DexcomUserMutableAlerts.defaultConfigurations(alwaysSoundEnabled: true)
        return DexcomUserMutableAlerts(configurations: configurations)
    }
    
    public var configurations: [DexcomAlertType: DexcomAlertConfiguration]
        
    public init(configurations: [DexcomAlertType: DexcomAlertConfiguration] = [:]) {
        self.configurations = configurations
    }
}
