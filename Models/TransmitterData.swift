//
//  TransmitterData.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-09-12.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import LoopKit
import TransmitterCore
import GlucoseCore
import CgmSystem

public struct TransmitterData : TransmitterCgmData {
    public var transmitterId: String {
        return dexcomTransmitterID.transmitterID
    }
    public var dexcomTransmitterID: DexcomTransmitterID
    public var session: TransmitterSession?
    public var cgmReading: TransmitterCgmReading
    public var backfillCgmReading: [TransmitterCgmReading]
    public var lastCalibration: TransmitterCalibration?
    public var calibrationBounds: CalibrationBounds
    public var autoCalibration: AutoCalibrationState
    public var sessionDurationDays: Int
    public var sensorWarmupDuration: Int
    
    public init(dexcomTransmitterID: DexcomTransmitterID,
                session: TransmitterSession?,
                cgmReading: TransmitterCgmReading,
                backfillCgmReading: [TransmitterCgmReading],
                lastCalibration: TransmitterCalibration?,
                calibrationBounds: CalibrationBounds,
                autoCalibration: AutoCalibrationState,
                sessionDurationDays: Int,
                sensorWarmupDuration: Int) {
        self.dexcomTransmitterID = dexcomTransmitterID
        self.session = session
        self.cgmReading = cgmReading
        self.backfillCgmReading = backfillCgmReading
        self.lastCalibration = lastCalibration
        self.calibrationBounds = calibrationBounds
        self.autoCalibration = autoCalibration
        self.sessionDurationDays = sessionDurationDays
        self.sensorWarmupDuration = sensorWarmupDuration
    }
}

extension TransmitterData: SensorDisplayable {
    public var isStateValid: Bool {
        return cgmReading.hasReliableGlucose
    }
    
    public var stateDescription: String {
        return cgmReading.localizedDescription
    }
    
    public var isLocal: Bool {
        return true
    }
    
    public var trendType: GlucoseTrend? {
        switch cgmReading.trend {
        case .outOfRange:
            return nil
        case .fallingRapidly:
            return .downDownDown
        case .falling:
            return .downDown
        case .fallingSlowly:
            return .down
        case .flat, .none:
            return .flat
        case .risingSlowly:
            return .up
        case .rising:
            return .upUp
        case .risingRapidly:
            return .upUpUp
        }
    }
    
    public var sessionPeriod: CGMSessionPeriodModel? {
        guard let session = self.session else {
            return nil
        }
        var sessionDuration = TimeInterval(days: Double(self.sessionDurationDays))
        if sessionDuration == 0 {
            // the session duration is less than a day, which the transmitter reports as 0 days
            // Since a session is running, the duration should be at least as long as the time since the session start time
            sessionDuration = self.cgmReading.egvSystemTime.timeIntervalSince(session.sessionStartSystemTime)
        }
        return CGMSessionMapper.getSessionPeriod(transmitterSession: session,
                                                 sessionDuration: Int(sessionDuration),
                                                 sensorWarmupDuration: self.sensorWarmupDuration)
    }
}
