//
//  TransmitterStaticInfo.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-09-12.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import TransmitterCore

public struct TransmitterStaticInfo: TransmitterInfo, Equatable, Codable {
    public var transmitterId: String {
        return dexcomTransmitterID.transmitterID
    }
    public var dexcomTransmitterID: DexcomTransmitterID
    public var deviceUUID: String
    public var transmitterVersion: String
    public var softwareNumber: UInt32
    public var apiVersion: UInt32
    public var storageTimeDays: UInt32
    public var maxStorageTimeDays: UInt32
    public var maxRuntimeDays: UInt32
    public var sessionTimeDays: UInt32
    public var isDiagnosticDataDownloadSupported: Bool
    public var isEgvBackfillSupported: Bool
    public var isPredictedEgvSupported: Bool
    public var activatedOn: Date?
    
    public init(dexcomTransmitterID: DexcomTransmitterID,
                deviceUUID: String,
                transmitterVersion: String,
                softwareNumber: UInt32,
                apiVersion: UInt32,
                storageTimeDays: UInt32,
                maxStorageTimeDays: UInt32,
                maxRuntimeDays: UInt32,
                sessionTimeDays: UInt32,
                isDiagnosticDataDownloadSupported: Bool,
                isEgvBackfillSupported: Bool,
                isPredictedEgvSupported: Bool,
                activatedOn: Date?) {
        self.dexcomTransmitterID = dexcomTransmitterID
        self.deviceUUID = deviceUUID
        self.transmitterVersion = transmitterVersion
        self.softwareNumber = softwareNumber
        self.apiVersion = apiVersion
        self.storageTimeDays = storageTimeDays
        self.maxStorageTimeDays = maxStorageTimeDays
        self.maxRuntimeDays = maxRuntimeDays
        self.sessionTimeDays = sessionTimeDays
        self.isDiagnosticDataDownloadSupported = isDiagnosticDataDownloadSupported
        self.isEgvBackfillSupported = isEgvBackfillSupported
        self.isPredictedEgvSupported = isPredictedEgvSupported
        self.activatedOn = activatedOn
    }
}

extension TransmitterStaticInfo {
    public var transmitterEOL: Date? {
        return activatedOn?.addingTimeInterval(TimeInterval(days: Double(maxRuntimeDays)))
    }
    
    public var sessionDuration: TimeInterval {
        return TimeInterval(days: Double(sessionTimeDays))
    }
}
