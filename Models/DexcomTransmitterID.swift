//
//  DexcomTransmitterID.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-09-26.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

public struct DexcomTransmitterID: Equatable, Codable {
    
    public static let length = 6
    
    public static let pattern = "([\(ModelCode.g6.rawValue)][^iIoOvVzZ]{5})$"
    
    private(set) public var transmitterID: String
    
    public init?(_ transmitterID: String) {
        self.transmitterID = transmitterID
        guard validTransmitterID() else {
            return nil
        }
    }
    
    private func hasValidModelCode() -> Bool {
        let range = NSRange(transmitterID.startIndex..<transmitterID.endIndex, in: transmitterID)
        let regex = try! NSRegularExpression(pattern: DexcomTransmitterID.pattern)
        guard regex.firstMatch(in: transmitterID, options: [], range: range) != nil else {
            return false
        }
        return true
    }
    
    private func validTransmitterID() -> Bool {
        return transmitterID.count == DexcomTransmitterID.length && hasValidModelCode()
    }
    
    public var model: String? {
        if transmitterID.first == ModelCode.g5.rawValue {
            return LocalizedString("G5", comment: "Dexcom G5 model")
        } else if transmitterID.first == ModelCode.g6.rawValue {
            return LocalizedString("G6", comment: "Dexcom G6 model")
        }
        return nil
    }
    
    private enum ModelCode: String.Element {
        case g5 = "4"
        case g6 = "8"
    }
}
