//
//  DexcomAlertConfiguration.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-10-03.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import HealthKit
import LoopKit
import AlertsCore
import GlucoseCore

public protocol AlertConfiguration {
    
    var type: DexcomAlertType { get }
    
    var threshold: HKQuantity { get set }
    
    var thresholdMinValue: HKQuantity { get }
    
    var thresholdMaxValue: HKQuantity { get }
    
    var enabled: Bool { get set }
    
    var alwaysSoundEnabled: Bool { get set }
    
    var alertFrequency: DexcomAlertFrequency { get set }
    
    var snoozeFrequency: DexcomSnoozeFrequency { get set }
    
    var sound: AlertSound { get set }
    
}

public struct DexcomAlertConfiguration: AlertConfiguration, Codable, Hashable, Identifiable {
    
    public var id = UUID()
    
    public let type: DexcomAlertType
    
    private var thresholdValue: Double
    
    // HKQuantity isn't codable
    public var threshold: HKQuantity {
        get {
            return HKQuantity.init(unit: self.type.unit, doubleValue: thresholdValue)
        }
        set {
            thresholdValue = newValue.doubleValue(for: self.type.unit)
        }
    }
    
    public var thresholdMinValue: HKQuantity {
        return HKQuantity.init(unit: self.type.unit, doubleValue: self.type.thresholdMinMaxBounds.min)
    }

    public var thresholdMaxValue: HKQuantity {
        return HKQuantity.init(unit: self.type.unit, doubleValue: self.type.thresholdMinMaxBounds.max)
    }
    
    public var hasThresholdValue: Bool {
        return !thresholdValue.isNaN
    }

    public var enabled: Bool
        
    public var alwaysSoundEnabled: Bool
    
    public var alertFrequency: DexcomAlertFrequency
    
    public var snoozeFrequency: DexcomSnoozeFrequency
    
    public var sound: AlertSound
    
    public init(type: DexcomAlertType, enabled: Bool, alwaysSoundEnabled: Bool, sound: AlertSound) {
        self.type = type
        self.thresholdValue = type.defaultThreshold
        self.enabled = enabled
        self.alwaysSoundEnabled = alwaysSoundEnabled
        self.alertFrequency = type.defaultAlertFrequency
        self.snoozeFrequency = type.defaultSnoozeFrequency
        self.sound = sound
    }
        
}

extension DexcomAlertConfiguration: Equatable {
    public static func == (lhs: DexcomAlertConfiguration, rhs: DexcomAlertConfiguration) -> Bool {
        return lhs.type == rhs.type &&
            (lhs.thresholdValue.isNaN ? rhs.thresholdValue.isNaN : lhs.thresholdValue == rhs.thresholdValue) &&
            lhs.enabled == rhs.enabled &&
            lhs.alwaysSoundEnabled == rhs.alwaysSoundEnabled &&
            lhs.alertFrequency == rhs.alertFrequency &&
            lhs.snoozeFrequency == rhs.snoozeFrequency &&
            lhs.sound == rhs.sound
    }
}

public enum DexcomAlertType: Int, Codable, CaseIterable {
    // The Dexcom API AlertProtocol expects an alert ID that is unique and an alert type value (see AlertType)
    // DexcomAlertType.id is the unique alert ID
    case glucoseUrgentLow
    case glucoseLow
    case glucoseHigh
    case glucoseRateRise
    case glucoseRateFall
    case signalLoss
    case noReadings
    case warmupComplete
    case sensorExpired
    case sensorExpiresSoon24H
    case sensorExpiresSoon6H
    case sensorExpiresSoon2H
    case sensorExpiresSoon30M
    case sensorFailed
    case transmitterFailed
    case transmitterNotFound
    case transmitterFound
    case sessionStartedThirdLast
    case sessionStartedSecondLast
    case sessionStartedLast
    case sessionsNoMore
    case sensorFailedDueToRestart
    case calibrationRequest
    case firstCalibration
    case secondCalibration
    case calibrationRequired
    case calibrationRequiredLater
    
    public static func type(forAlert alert: AlertProtocol) -> DexcomAlertType? {
        return DexcomAlertType.allCases.first(where: { $0.id == alert.identifier })
    }
    
    public var id: Int {
        return rawValue
    }
    
    public var identifier: DeviceAlert.AlertIdentifier {
        return "\(id)"
    }
    
    public static func type(forIdentifier alertIdentifier: DeviceAlert.AlertIdentifier) -> DexcomAlertType? {
        return DexcomAlertType.allCases.first(where: { $0.id == Int(alertIdentifier) })
    }

    // maps DexcomAlertType to Dexcom defined AlertType
    public var value: Int {
        switch self {
        case .glucoseUrgentLow:
            return AlertType.glucoseUrgentLow
        case .glucoseLow:
            return AlertType.glucoseLow
        case .glucoseHigh:
            return AlertType.glucoseHigh
        case .glucoseRateRise:
            return AlertType.glucoseRisingRate
        case .glucoseRateFall:
            return AlertType.glucoseRisingRate
        case .signalLoss:
            return AlertType.signalLoss
        case .noReadings:
            return AlertType.noReadings
        case .warmupComplete:
            return AlertType.warmupComplete
        case .sensorExpired:
            return AlertType.sensorExpiration
        case .sensorExpiresSoon24H:
            return AlertType.sensorExpireSoon
        case .sensorExpiresSoon6H:
            return AlertType.sensorExpireSoon
        case .sensorExpiresSoon2H:
            return AlertType.sensorExpireSoon
        case .sensorExpiresSoon30M:
            return AlertType.sensorExpireSoon
        case .sensorFailed:
            return AlertType.sensorFailed
        case .sensorFailedDueToRestart:
            return AlertType.sensorFailedDueToRestart
        case .calibrationRequest:
            return AlertType.calibrationRequest
        case .firstCalibration:
            return AlertType.firstCalibration
        case .secondCalibration:
            return AlertType.secondCalibration
        case .calibrationRequired:
            return AlertType.calibrationRequired
        case .calibrationRequiredLater:
            return AlertType.calibrationRequiredLater
        // These are "extra" alerts outside of Dexcom's alerts. Here we ensure their numerical range is outside their reserved values.
        case .transmitterFailed,
             .transmitterNotFound,
             .transmitterFound,
             .sessionStartedThirdLast,
             .sessionStartedSecondLast,
             .sessionStartedLast,
             .sessionsNoMore:
            return AlertType.reservedRange.upperBound.advanced(by: 1) + self.rawValue
        }
    }
    
    public var canDisable: Bool {
        switch self {
        case .glucoseLow, .glucoseHigh, .glucoseRateFall, .glucoseRateRise, .signalLoss, .noReadings:
            return true
        default:
            return false
        }
    }
    
    public var canDelayRepeat: Bool {
        switch self {
        case .glucoseUrgentLow, .glucoseLow, .glucoseHigh:
            return true
        default:
            return false
        }
    }
    
    // these default threshold values match the Dexcom G6 app defaults. See unit definition below for corresponding units
    public var defaultThreshold: Double {
        switch self {
        case .glucoseUrgentLow:
            return 55
        case .glucoseLow:
            return 80
        case .glucoseHigh:
            return 200
        case .glucoseRateRise:
            return 3
        case .glucoseRateFall:
            return -3
        case .signalLoss:
            return 20
        default:
            return Double.nan
        }
    }
    
    // these threshold min/max bound values match the Dexcom G6 app defaults. See unit definition below for corresponding units
    public var thresholdMinMaxBounds: (min: Double, max: Double) {
        switch self {
        case .glucoseUrgentLow:
            return (55, 55)
        case .glucoseLow:
            return (60, 100)
        case .glucoseHigh:
            return (120, 400)
        case .glucoseRateRise:
            return (1, 5)
        case .glucoseRateFall:
            return (-5, -1)
        case .signalLoss:
            return (TimeInterval(minutes: 5).minutes, TimeInterval(hours: 4).minutes)
        default:
            return (Double.nan, Double.nan)
        }
    }
    
    public var unit: HKUnit {
        switch self {
        case .glucoseUrgentLow, .glucoseLow, .glucoseHigh:
            return HKUnit.milligramsPerDeciliter
        case .glucoseRateRise, .glucoseRateFall:
            return HKUnit.milligramsPerDeciliterPerMinute
        case .signalLoss:
            return HKUnit.minute()
        default:
            return HKUnit.init(from: "no units")
        }
    }
    
    // General note for alert and snooze frequencies:
    //  - Putting the alert at 30 seconds before the expected connection interval (5 min interval for G6) helps time the alert to fire in an expected manner. Putting the alert at the minute mark may require another connection interval before it fires.
    public var defaultAlertFrequency: DexcomAlertFrequency {
        switch self {
        case .warmupComplete, .sensorExpired, .sensorExpiresSoon24H, .sensorExpiresSoon6H, .sensorExpiresSoon2H, .sensorExpiresSoon30M, .sensorFailed, .sensorFailedDueToRestart, .transmitterFailed, .sessionStartedThirdLast, .sessionStartedSecondLast, .sessionStartedLast, .sessionsNoMore, .transmitterNotFound:
            return DexcomAlertFrequency.noRepeats // the alert will fire once
        case .calibrationRequest, .firstCalibration, .secondCalibration:
            return DexcomAlertFrequency(timeInterval: TimeInterval.minutes(5)).adjustedForConnectionInterval() // Page 119 in G6-transmitter-API-1.2.pdf
        case .calibrationRequired:
            return DexcomAlertFrequency(maxCount: 2, timeInterval: TimeInterval.minutes(5)).adjustedForConnectionInterval() // Page 119 in G6-transmitter-API-1.2.pdf
        case .calibrationRequiredLater:
            return DexcomAlertFrequency(timeInterval: TimeInterval.minutes(5)).adjustedForConnectionInterval() // Page 119 in G6-transmitter-API-1.2.pdf
        default:
            return DexcomAlertFrequency(timeInterval: TimeInterval.minutes(5)).adjustedForConnectionInterval()
        }
    }
    
    public var defaultSnoozeFrequency: DexcomSnoozeFrequency {
        switch self {
        case .glucoseUrgentLow, .glucoseLow, .glucoseHigh:
            return DexcomSnoozeFrequency(timeInterval: TimeInterval.minutes(30))
        case .noReadings:
            return DexcomSnoozeFrequency(timeInterval: TimeInterval.minutes(20))
        case .calibrationRequest, .firstCalibration, .secondCalibration:
            return DexcomSnoozeFrequency(timeInterval: TimeInterval.minutes(15)) // Page 119 in G6-transmitter-API-1.2.pdf
        case .calibrationRequired, .calibrationRequiredLater:
            return DexcomSnoozeFrequency.noRepeats // Page 119 in G6-transmitter-API-1.2.pdf
        default:
            return DexcomSnoozeFrequency.noDelay
        }
    }
    
    public var hasUserNotification: Bool {
        switch self {
        // a few alerts do not have user notification incarnations
        case .sessionStartedLast:
            return false
        default:
            return true
        }
    }
    
    public var hasInApp: Bool {
        switch self {
        // a few alerts do not have in app incarnations
        case .calibrationRequest:
            return false
        default:
            return true
        }
    }
    
    // same as in the G6 app
    public var localizedNotificationTitle: String {
        switch self {
        case .glucoseUrgentLow:
            return String(format: LocalizedString("%@ Glucose Alarm", comment: "Title of glucose alarm (1: alert type)"), self.localizedTitle)
        case .glucoseLow, .glucoseHigh, .glucoseRateRise, .glucoseRateFall:
            return String(format: LocalizedString("%@ Glucose Alert", comment: "Title of glucose alert (1: alert type)"), self.localizedTitle)
        case .signalLoss, .noReadings:
            return String(format: LocalizedString("%@ Alert", comment: "Title of transmitter alert (1: alert type)"), self.localizedTitle)
        default:
            return self.localizedTitle
        }
    }
    
    // same as in the G6 app
    public var localizedViewTitle: String {
        switch self {
        case .glucoseUrgentLow:
            return String(format: LocalizedString("%@ Alarm", comment: "Title of alarm details view (1: alarm type)"), self.localizedTitle)
        case .glucoseLow, .glucoseHigh, .glucoseRateRise, .glucoseRateFall, .signalLoss, .noReadings:
            return String(format: LocalizedString("%@ Alert", comment: "Title of alert details view (1: alert type)"), self.localizedTitle)
        default:
            return self.localizedTitle
        }
    }
    
    // same as in the G6 app
    public var localizedInAppTitle: String {
        switch self {
        default:
            return self.localizedNotificationTitle
        }
    }

    // same as in the G6 app
    public var localizedNotificationMessage: String {
        switch self {
        case .glucoseUrgentLow:
            return LocalizedString("Your sensor glucose reading is urgently low.", comment: "Message for urgent low glucose alarm")
        case .glucoseLow:
            return LocalizedString("Your sensor glucose reading is low.", comment: "Message for low glucose alert")
        case .glucoseHigh:
            return LocalizedString("Your sensor glucose reading is high.", comment: "Message for high glucose alert")
        case .glucoseRateRise:
            return LocalizedString("Your sensor glucose reading is rising quickly.", comment: "Message for glucose rising alert")
        case .glucoseRateFall:
            return LocalizedString("Your sensor glucose reading is falling quickly.", comment: "Message for glucose falling alert")
        case .signalLoss:
            return LocalizedString("You will not receive alerts, alarms, or sensor glucose readings.", comment: "Message for signal loss alert")
        case .noReadings:
            return LocalizedString("You will not receive alerts, alarms, or sensor glucose readings.", comment: "Message for no readings alert")
        case .warmupComplete:
            return LocalizedString("The sensor has completed warmup.", comment: "Message for warmup completed)") // could not generate this alert in the G6 app
        case .sensorExpired:
            return LocalizedString("Your sensor session has ended. Replace your sensor.", comment: "Message for sensor expired")
        case .sensorExpiresSoon24H:
            return LocalizedString("Your sensor session will end in less than 24 hours. You will not receive alerts, alarms, or sensor glucose readings after this time unless you replace your sensor.", comment: "Message for sensor expires soon alert (24 hour)")
        case .sensorExpiresSoon6H:
            return LocalizedString("Your sensor session will end in less than 6 hours. You will not receive alerts, alarms, or sensor glucose readings after this time unless you replace your sensor.", comment: "Message for sensor expires soon alert (8 hour)")
        case .sensorExpiresSoon2H:
            return LocalizedString("Your sensor session will end in less than 2 hours. You will not receive alerts, alarms, or sensor glucose readings after this time unless you replace your sensor.", comment: "Message for sensor expires soon alert (2 hour)")
        case .sensorExpiresSoon30M:
            return LocalizedString("Your sensor session will end in less than 30 minutes. You will not receive alerts, alarms, or sensor glucose readings after this time unless you replace your sensor.", comment: "Message for sensor expires soon alert (30 minute)")
        case .sensorFailed:
            return LocalizedString("Replace your sensor now. You will not receive alerts, alarms, or sensor glucose readings.", comment: "Message for sensor failed alert.")
        case .sensorFailedDueToRestart:
            return LocalizedString("Replace your sensor now. You will not receive alerts, alarms, or sensor glucose readings.", comment: "Message for sensor failed alert due to restart.")
        case .transmitterFailed:
            return LocalizedString("Your transmitter is not working. Pair a new transmitter.", comment: "Message for transmitter failed alert.")
        case .transmitterNotFound:
            return LocalizedString("Check transmitter insertion and SN. Try pairing again.", comment: "Transmitter not found alert")
        case .transmitterFound:
            return LocalizedString("You successfully paired your Dexcom G6 Transmitter. Open app to start your session.", comment: "Message for transmitter found alert.")
        case .sessionStartedThirdLast:
            return LocalizedString("Your transmitter will expire in less than a month.\n\nIf you haven't already, please order a new transmitter.", comment: "Message for third last sessions started alert")
        case .sessionStartedSecondLast:
            return LocalizedString("Your transmitter will expire in less than 3 weeks.\n\nIf you haven't already, please order a new transmitter.", comment: "Message for second last sessions started alert")
        case .sessionStartedLast:
            return LocalizedString("This is the last session for your transmitter.\n\nIf you haven't already, please order a new transmitter.", comment: "Message for last session started alert")
        case .sessionsNoMore:
            return LocalizedString("No more sessions can run on this transmitter.\n\nPlease pair a new transmitter.", comment: "Message for no more sessions alert")
        case .calibrationRequest:
            return LocalizedString("Enter new blood glucose reading to maintain your Dexcom G6 sensor accuracy.", comment: "Message for calibration request alert")
        case .firstCalibration:
            return LocalizedString("Enter your 1st blood glucose reading.", comment: "Message for first calibration request alert")
        case .secondCalibration:
            return LocalizedString("Enter your 2nd blood glucose reading.", comment: "Message for second calibration request alert")
        case .calibrationRequired:
            return LocalizedString("Enter new blood glucose reading to recalibrate your Dexcom G6 sensor.", comment: "Message for calibration required alert")
        case .calibrationRequiredLater:
            return LocalizedString("After 15 minutes, enter a new blood glucose reading to recalibrate your sensor.", comment: "Message for Calibration required (wedge high/low) error alert")
        }
    }
    
    // For those alerts with different InApp body content from the UserNotifications.  Defaults to localizedNotificationMessage.
    public var localizedInAppMessage: String {
        switch self {
        case .transmitterFound:
            return LocalizedString("You successfully paired your Dexcom G6 Transmitter.", comment: "Message for in-app transmitter found alert.")
        case .transmitterFailed:
            return LocalizedString("You will not receive Dexcom G6 alerts, alarms, or sensor glucose readings until you replace your sensor..", comment: "Message for in-app transmitter failed alert.")
        default:
            return localizedNotificationMessage
        }
    }
    
    // same as in the G6 app
    public var localizedTitle: String {
        switch self {
        case .glucoseUrgentLow:
            return LocalizedString("Urgent Low", comment: "Urgent low alarm title")
        case .glucoseLow:
            return LocalizedString("Low", comment: "Low alert title")
        case .glucoseHigh:
            return LocalizedString("High", comment: "High alert title")
        case .glucoseRateRise:
            return LocalizedString("Rise Rate", comment: "Rise rate alert title")
        case .glucoseRateFall:
            return LocalizedString("Fall Rate", comment: "Fall rate alert title")
        case .signalLoss:
            return LocalizedString("Signal Loss", comment: "Signal loss alert title")
        case .noReadings:
            return LocalizedString("No Readings", comment: "No readings alert title")
        case .warmupComplete:
            return LocalizedString("Sensor Warmup Complete", comment: "Warmup complete alert title")
        case .sensorExpired:
            return LocalizedString("Sensor Expired", comment: "Sensor expired alert title")
        case .sensorExpiresSoon24H, .sensorExpiresSoon6H, .sensorExpiresSoon2H, .sensorExpiresSoon30M:
            return LocalizedString("Sensor Expiring", comment: "Sensor expires soon alert title")
        case .sensorFailed:
            return LocalizedString("Sensor Failed", comment: "Sensor failed alert title")
        case .sensorFailedDueToRestart:
            return LocalizedString("No Restarts", comment: "Sensor failed due to restart alert title")
        case .transmitterFailed:
            return LocalizedString("Transmitter Failed", comment: "Transmitter failed alert title")
        case .transmitterNotFound:
            return LocalizedString("Transmitter Not Found.", comment: "Transmitter not found alert title")
        case .transmitterFound:
            return LocalizedString("Dexcom Transmitter Paired", comment: "Transmitter found alert title")
        case .sessionStartedThirdLast, .sessionStartedSecondLast:
            return LocalizedString("Transmitter Battery Low", comment: "Session remaining alert title")
        case .sessionStartedLast:
            return LocalizedString("Last Session Alert", comment: "Last session started alert title")
        case .sessionsNoMore:
            return LocalizedString("Pair New Transmitter", comment: "Last session ended alert title")
        case .calibrationRequest:
            return LocalizedString("Dexcom G6 Calibration Alert", comment: "Calibration request alert title")
        case .firstCalibration:
            return LocalizedString("Dexcom G6 Calibration Required", comment: "First calibration required alert title")
        case .secondCalibration:
            return LocalizedString("Dexcom G6 Calibration Required", comment: "Second calibration required alert title")
        case .calibrationRequired:
            return LocalizedString("Dexcom G6 Calibration Error", comment: "Calibration required alert title")
        case .calibrationRequiredLater:
            return LocalizedString("Dexcom G6 Calibration Error", comment: "Calibration required (wedge high/low) error alert title")
        }
    }
    
    // same as in the G6 app
    public var localizedDescription: String {
        switch self {
        case .glucoseUrgentLow:
            return LocalizedString("The Urgent Low Alarm will alarm you when your glucose level falls below %@.", comment: "Description for Urgent Low Alarm (1: threshold value with units)")
        case .glucoseLow:
            return LocalizedString("The Low Alert will alert you when your glucose level falls below the set level.", comment: "Description for Low Alert")
        case .glucoseHigh:
            return LocalizedString("The High Alert will alert you when your glucose level rises above the set level.", comment: "Description for High Alert")
        case .glucoseRateRise:
            return LocalizedString("The Rise Rate Alert will alert you when your glucose levels are rising at or above the set rate.", comment: "Description for Rise Rate Alert")
        case .glucoseRateFall:
            return LocalizedString("The Fall Rate Alert will alert you when your glucose levels are falling at or below the set rate.", comment: "Description for Fall Rate Alert")
        case .signalLoss:
            return LocalizedString("The Signal Loss Alert will alert you when the app stops receiving glucose readings from your transmitter.", comment: "Description for Signal Loss Alert")
        case .noReadings:
            return LocalizedString("The No Readings Alert will alert you 20 minutes after your smart device stops receiving glucose readings from your sensor. You will not receive alerts, alarms, or sensor glucose readings.", comment: "Description for No Readings Alert")
        default:
            return self.localizedNotificationMessage
        }
    }
    
    // same as in the G6 app
    public var localizedThresholdTitle: String? {
        switch self {
        case .glucoseUrgentLow:
            return LocalizedString("Notify Me Below", comment: "Title describing the alarm threshold notification for values below")
        case .glucoseLow, .glucoseRateFall:
            return LocalizedString("Notify Me Below", comment: "Title describing the alert threshold notification for values below")
        case .glucoseHigh, .glucoseRateRise:
            return LocalizedString("Notify Me Above", comment: "Title describing the alert threshold notification for values above")
        case .signalLoss:
            return LocalizedString("For More Than", comment: "Title describing the time in minutes that a signal can be lost without alert")
        default:
            return nil
        }
    }
    
    public var localizedDismissAction: String {
        switch self {
        case .transmitterFound:
            return LocalizedString("Start Session", comment: "Alert acknowledgement for Start Session button")
        default:
            return LocalizedString("OK", comment: "Alert acknowledgment OK button")
        }
    }
}

public enum AlertSound: String, Codable, CaseIterable {
    case noSound, defaultSound, intense, calm
}

public typealias DexcomSnoozeFrequency = DexcomAlertFrequency

public struct DexcomAlertFrequency: AlertFrequency, Equatable, Codable, Hashable {
    
    public static let largeCount = Int.max
    
    public static let noRepeats = DexcomAlertFrequency(maxCount: 1, timeInterval: 0)

    public static let noDelay = DexcomAlertFrequency(timeInterval: 0)

    public var maxCount: Int

    public var timeInterval: TimeInterval
    
    public init(maxCount: Int = largeCount, timeInterval: TimeInterval) {
        self.maxCount = maxCount
        self.timeInterval = timeInterval
    }
    
    public func adjustedForConnectionInterval() -> DexcomAlertFrequency {
        if timeInterval != 0 {
            // Remove 30 seconds so that the alert fires on the expected connection interval
            return DexcomAlertFrequency(timeInterval: timeInterval - TimeInterval.seconds(30))
        }
        return self
    }
}
