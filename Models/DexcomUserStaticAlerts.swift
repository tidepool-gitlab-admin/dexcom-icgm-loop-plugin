//
//  DexcomUserStaticAlerts.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-12-05.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation

struct DexcomUserStaticAlerts: DexcomAlertDetails {
    public static func defaultConfigurations(alwaysSoundEnabled: Bool) -> [DexcomAlertType: DexcomAlertConfiguration] {
        return [
            DexcomAlertConfiguration(type: DexcomAlertType.warmupComplete,
                                     enabled: true,
                                     alwaysSoundEnabled: alwaysSoundEnabled,
                                     sound: .defaultSound),
            
            DexcomAlertConfiguration(type: DexcomAlertType.calibrationRequest,
                                     enabled: true,
                                     alwaysSoundEnabled: alwaysSoundEnabled,
                                     sound: .defaultSound),
            
            DexcomAlertConfiguration(type: DexcomAlertType.firstCalibration,
                                     enabled: true,
                                     alwaysSoundEnabled: alwaysSoundEnabled,
                                     sound: .defaultSound),
            
            DexcomAlertConfiguration(type: DexcomAlertType.secondCalibration,
                                     enabled: true,
                                     alwaysSoundEnabled: alwaysSoundEnabled,
                                     sound: .defaultSound),
            
            DexcomAlertConfiguration(type: DexcomAlertType.calibrationRequired,
                                     enabled: true,
                                     alwaysSoundEnabled: alwaysSoundEnabled,
                                     sound: .defaultSound),
            
            DexcomAlertConfiguration(type: DexcomAlertType.calibrationRequiredLater,
                                     enabled: true,
                                     alwaysSoundEnabled: alwaysSoundEnabled,
                                     sound: .defaultSound),
            
            DexcomAlertConfiguration(type: DexcomAlertType.sensorExpired,
                                     enabled: true,
                                     alwaysSoundEnabled: alwaysSoundEnabled,
                                     sound: .defaultSound),
            
            DexcomAlertConfiguration(type: DexcomAlertType.sensorExpiresSoon24H,
                                     enabled: true,
                                     alwaysSoundEnabled: alwaysSoundEnabled,
                                     sound: .defaultSound),
            
            DexcomAlertConfiguration(type: DexcomAlertType.sensorExpiresSoon6H,
                                     enabled: true,
                                     alwaysSoundEnabled: alwaysSoundEnabled,
                                     sound: .defaultSound),
            
            DexcomAlertConfiguration(type: DexcomAlertType.sensorExpiresSoon2H,
                                     enabled: true,
                                     alwaysSoundEnabled: alwaysSoundEnabled,
                                     sound: .defaultSound),
            
            DexcomAlertConfiguration(type: DexcomAlertType.sensorExpiresSoon30M,
                                     enabled: true,
                                     alwaysSoundEnabled: alwaysSoundEnabled,
                                     sound: .defaultSound),
            
            // sensor failed always sounds
            DexcomAlertConfiguration(type: DexcomAlertType.sensorFailed,
                                     enabled: true,
                                     alwaysSoundEnabled: true,
                                     sound: .defaultSound),
            
            // sensor failed due to restart always sounds
            DexcomAlertConfiguration(type: DexcomAlertType.sensorFailedDueToRestart,
                                     enabled: true,
                                     alwaysSoundEnabled: true,
                                     sound: .defaultSound),
            
            // transmitter failed always sounds
            DexcomAlertConfiguration(type: DexcomAlertType.transmitterFailed,
                                     enabled: true,
                                     alwaysSoundEnabled: true,
                                     sound: .defaultSound),
            ]
            .reduce([:]) { (dict: [DexcomAlertType: DexcomAlertConfiguration], config: DexcomAlertConfiguration) in
                dict.merging([config.type : config], uniquingKeysWith: { _,_ in
                    fatalError("Duplicate Alerts found.  Invalid alert configuration.")
                })
        }
    }
    
    public static func defaultAlerts() -> DexcomUserStaticAlerts {
        let configurations = DexcomUserStaticAlerts.defaultConfigurations(alwaysSoundEnabled: true)
        return DexcomUserStaticAlerts(configurations: configurations)
    }
    
    public var configurations: [DexcomAlertType: DexcomAlertConfiguration]
    
    public init(configurations: [DexcomAlertType: DexcomAlertConfiguration] = [:]) {
        self.configurations = configurations
    }
}
