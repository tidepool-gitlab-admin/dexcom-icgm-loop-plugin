# DexcomiCGMKit
This is a Loop CGMManager implemention to provide support for the Dexcom iCGM.

While this implementation is open source, it depends on a private framework that is not openly available. Tidepool is providing the implementation as open source in the interests of transparency, even though this CGMManager cannot be built without the private framework.
