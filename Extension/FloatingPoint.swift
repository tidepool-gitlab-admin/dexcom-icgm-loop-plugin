//
//  FloatingPoint.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2020-03-10.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

extension FloatingPoint {
    var roundedToTenths: Self {
        return (self * 10).rounded() / 10
    }
}
