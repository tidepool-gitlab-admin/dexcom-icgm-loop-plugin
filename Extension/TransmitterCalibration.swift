//
//  TransmitterCalibration.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-09-09.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation
import HealthKit
import LoopKit
import TransmitterCore

public extension TransmitterCalibration {
    var glucoseValue: HKQuantity {
        // all meter values from a Dexcom transmitter are reported in mg/dL
        return HKQuantity(unit: HKUnit.milligramsPerDeciliter, doubleValue: Double(meterValue))
    }
    
    var isDisplayOnly: Bool {
        // isDisplayOnly means the value was used for calibration
        return true
    }
    
    var isValid: Bool {
        // At times when a transmitter is paired, it reports a last calibration value of 0 mg/dL. It happens at the start of the transmitter (time is 0). This is an invalid value and should not be reported.
        guard meterValue != 0 && calibrationTransmitterTime.seconds != 0 else {
            return false
        }
        return true
    }
    
    // An identifier for this reading thatʼs consistent between backfill/live data
    func syncIdentifier(forTransmitter transmitterID: String) -> String {
        // Dexcom doc: The calibrationTransmitterTime acts as the identify for this entity.
        return "\(transmitterID) \(calibrationTransmitterTime.seconds)"
    }
    
    func glucoseSample(fromDevice device: HKDevice? = nil, withTransmitterID transmitterID: String) -> NewGlucoseSample {
        return NewGlucoseSample(date: calibrationSystemTime, quantity: glucoseValue, isDisplayOnly: isDisplayOnly, syncIdentifier: syncIdentifier(forTransmitter: transmitterID), device: device)
    }
}
