//
//  HKUnit.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-09-09.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import HealthKit

public extension HKUnit {
    static let milligramsPerDeciliter: HKUnit = {
        return HKUnit.gramUnit(with: .milli).unitDivided(by: .literUnit(with: .deci))
    }()
    
    static let milligramsPerDeciliterPerMinute: HKUnit = {
        return HKUnit.milligramsPerDeciliter.unitDivided(by: .minute())
    }()
}
