//
//  SensorCode.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-09-18.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation
import TransmitterCore

extension SensorCode {
    public static let codeLength = 4
}
