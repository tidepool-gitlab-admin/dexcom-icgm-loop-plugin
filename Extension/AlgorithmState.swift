//
//  AlgorithmState.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-12-10.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import TransmitterCore

extension AlgorithmState: Codable { }
