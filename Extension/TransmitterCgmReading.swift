//
//  TransmitterCgmReading.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-09-09.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation
import HealthKit
import LoopKit
import TransmitterCore
import GlucoseCore
import CgmSystem

// Dexcom docs: The trend rate if valid. NaN is used for invalid, but sometimes also the "special" value of
// 12.7.  See LOOP-996.
fileprivate let dexcomInvalidTrendRate = 12.7
fileprivate let trendEpsilon = 0.001 // Picked out of thin air?

public extension TransmitterCgmReading {
    var glucoseValue: HKQuantity? {
        guard hasReliableGlucose else {
            return nil
        }
        // all glucose values from a Dexcom transmitter are reported in mg/dL
        return HKQuantity(unit: HKUnit.milligramsPerDeciliter, doubleValue: Double(egv))
    }
    
    var trendRate: HKQuantity? {
        guard !rate.isNaN && !rate.isClose(to: dexcomInvalidTrendRate, within: trendEpsilon) else {
            return nil
        }
        // rate is in mg/dL/min.
        let unit = HKUnit.milligramsPerDeciliterPerMinute
        return HKQuantity(unit: unit, doubleValue: rate)
    }
    
    var isDisplayOnly: Bool {
        // Dexcom documentation states that a TransmitterCgmReading "Represents a CGM reading from the Transmitter". Thus it is not a glucose measurement used for calibration
        return false
    }
    
    // An identifier for this reading thatʼs consistent between backfill/live data
    var syncIdentifier: String {
        // Dexcom doc: The egvTransmitterTime acts as the identify for this entity.
        return "\(transmitterId) \(egvTransmitterTime.seconds)"
    }
    
    var trend: Trend {
        return CGMReadingMapper.getTrend(rate: self.rate)
    }
    
    var mappedToCGMReadingModel: CGMReadingModel {
        return CGMReadingMapper.getCgmReading(txData: self)
    }
        
    var hasReliableGlucose: Bool {
        // Dexcom docs: This value is only valid in the algorithm states of InCalibration, OutlierCalibrationRequest, and CalibrationRequest. This value is zero in all other algorithm states
        switch algorithmState {
        case .inCalibration,
             .outlierCalibrationRequest,
             .calibrationRequest:
            return true
        case .calibrationError0, .calibrationError1, .firstOfTwoBGsNeeded, .secondOfTwoBGsNeeded, .calibrationLinearityFitFailure, .outOfCalDueToOutlier, .sessionStopped, .sessionFailedDueToTransmitterError, .sessionFailedDueToUnrecoverableError, .sensorFailedDueToCountsAberration, .sensorFailedDueToResidualAberration, .temporarySensorIssue, .sensorFailedDueToProgressiveSensorDecline, .sensorFailedDueToHighCountsAberration, .sensorFailedDueToLowCountsAberration, .sensorFailedDueToRestart, .sensorWarmup:
            return false
        }
    }
    
    var localizedDescription: String {
        //TODO these were taken from CGMBLEKit and need to be confirmed
        switch algorithmState {
        case .calibrationRequest, .outlierCalibrationRequest, .firstOfTwoBGsNeeded, .secondOfTwoBGsNeeded, .calibrationError1, .calibrationError0, .calibrationLinearityFitFailure, .outOfCalDueToOutlier:
            return LocalizedString("Sensor needs calibration", comment: "The description of sensor calibration state when sensor needs calibration.")
        case .inCalibration:
            return LocalizedString("Sensor calibration is OK", comment: "The description of sensor calibration state when sensor calibration is ok.")
        case .sessionStopped, .sensorFailedDueToCountsAberration, .sensorFailedDueToResidualAberration, .sessionFailedDueToUnrecoverableError, .sessionFailedDueToTransmitterError, .temporarySensorIssue, .sensorFailedDueToProgressiveSensorDecline, .sensorFailedDueToHighCountsAberration, .sensorFailedDueToLowCountsAberration, .sensorFailedDueToRestart:
            return LocalizedString("Sensor is stopped", comment: "The description of sensor calibration state when sensor sensor is stopped.")
        case .sensorWarmup:
            return LocalizedString("Sensor is warming up", comment: "The description of sensor calibration state when sensor sensor is warming up.")
        }
    }
    
    func validGlucoseSample(fromDevice device: HKDevice? = nil) -> NewGlucoseSample? {
        if let glucoseValue = glucoseValue {
            return NewGlucoseSample(date: egvSystemTime, quantity: glucoseValue, isDisplayOnly: isDisplayOnly, syncIdentifier: syncIdentifier, device: device)
        }
        return nil
    }
}

extension FloatingPoint {
    func isClose(to other: Self, within epsilon: Self) -> Bool {
        return abs(self - other) <= epsilon
    }
}
