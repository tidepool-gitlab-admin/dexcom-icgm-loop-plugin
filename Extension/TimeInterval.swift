//
//  TimeInterval.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-09-19.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    static func days(_ days: Double) -> TimeInterval {
        return self.init(days: days)
    }
    
    static func hours(_ hours: Int) -> TimeInterval {
        return self.init(hours: Double(hours))
    }
    
    static func hours(_ hours: Double) -> TimeInterval {
        return self.init(hours: hours)
    }
    
    static func minutes(_ minutes: Int) -> TimeInterval {
        return self.init(minutes: Double(minutes))
    }
    
    static func minutes(_ minutes: Double) -> TimeInterval {
        return self.init(minutes: minutes)
    }
    
    static func seconds(_ seconds: Double) -> TimeInterval {
        return self.init(seconds)
    }
    
    static func milliseconds(_ milliseconds: Double) -> TimeInterval {
        return self.init(milliseconds / 1000)
    }
    
    init(days: Int) {
        self.init(days: Double(days))
    }
    
    init(days: Double) {
        self.init(hours: days * 24)
    }
    
    init(hours: Int) {
        self.init(hours: Double(hours))
    }
    
    init(hours: Double) {
        self.init(minutes: hours * 60)
    }
    
    init(minutes: Int) {
        self.init(minutes: Double(minutes))
    }
    
    init(minutes: Double) {
        self.init(minutes * 60)
    }
    
    init(seconds: Int) {
        self.init(seconds: Double(seconds))
    }
    
    init(seconds: Double) {
        self.init(seconds)
    }
    
    init(milliseconds: Int) {
        self.init(milliseconds: Double(milliseconds))
    }
    
    init(milliseconds: Double) {
        self.init(milliseconds / 1000)
    }
    
    var milliseconds: Double {
        return self * 1000
    }
    
    init(hundredthsOfMilliseconds: Int) {
        self.init(hundredthsOfMilliseconds: Double(hundredthsOfMilliseconds))
    }
    
    init(hundredthsOfMilliseconds: Double) {
        self.init(hundredthsOfMilliseconds / 100000)
    }
    
    var hundredthsOfMilliseconds: Double {
        return self * 100000
    }
    
    var minutes: Double {
        return self / 60.0
    }
    
    var hours: Double {
        return minutes / 60.0
    }
    
    var hoursAndMinutes: (hours:Int, minutes: Int) {
        let hours = Int(self.hours.rounded(.towardZero))
        let minutes = Int(self.minutes.truncatingRemainder(dividingBy: 60))
        return (hours: hours, minutes: minutes)
    }
}
