//
//  Date.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-11-28.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

extension Date {
    enum Weekday: String, CaseIterable {
        case sunday, monday, tuesday, wednesday, thursday, friday, saturday
    }
        
    func next(_ weekday: Weekday) -> Date {
        return get(.forward, weekday)
    }
    
    func previous(_ weekday: Weekday) -> Date {
        return get(.backward, weekday)
    }
    
    func get(_ direction: Calendar.SearchDirection, _ weekDay: Weekday) -> Date {
        let dayName = weekDay.rawValue
        let weekdayNames = Weekday.allCases.map { $0.rawValue }
        // Weekday units are the numbers 1 through n, where n is the number of days in the week. For example, in the Gregorian calendar, n is 7 and Sunday is represented by 1.
        let weekdayIndex = weekdayNames.firstIndex(of: dayName)! + 1
        
        let calendar = Calendar(identifier: .gregorian)
        var nextDateComponent = calendar.dateComponents([.hour, .minute, .second], from: self)
        nextDateComponent.weekday = weekdayIndex
        
        // there will always be another day
        let date = calendar.nextDate(after: self,
                                     matching: nextDateComponent,
                                     matchingPolicy: .nextTime,
                                     direction: direction)!
        return date
    }
    
}
