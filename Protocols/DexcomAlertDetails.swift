//
//  DexcomAlertDetails.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-12-05.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation

public protocol DexcomAlertDetails: Codable, Equatable {
    
    static func defaultConfigurations(alwaysSoundEnabled: Bool) -> [DexcomAlertType: DexcomAlertConfiguration]
    
    var configurations: [DexcomAlertType: DexcomAlertConfiguration] { get set }
    
    var alwaysSoundEnabled: Bool { get set }
}

extension DexcomAlertDetails {
    public static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.configurations == rhs.configurations
    }
}

extension DexcomAlertDetails {
    public var alwaysSoundEnabled: Bool {
        get {
            var alwaysSoundEnabled = false
            configurations.forEach { (type, configuration) in
                // sound is always on for urgent low, sensor failed, and transmitter failed
                if type != .glucoseUrgentLow,
                    type != .sensorFailed,
                    type != .transmitterFailed,
                    configuration.alwaysSoundEnabled {
                    alwaysSoundEnabled = true
                    return
                }
            }
            return alwaysSoundEnabled
        }
        set {
            configurations.forEach { (type, _) in
                // sound is always on for urgent low, sensor failed, and transmitter failed
                if type != .glucoseUrgentLow,
                   type != .sensorFailed,
                   type != .transmitterFailed {
                    configurations[type]?.alwaysSoundEnabled = newValue
                }
            }
        }
    }
}
