//
//  NotificationDetailsViewModel.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-18.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import HealthKit
import LoopKit
import DexcomCGMKit

class NotificationDetailsViewModel: NotificationDetailsViewModelProtocol {
    
    let updateHandler: (DexcomAlertConfiguration) -> Void
    
    let glucoseUnit: HKUnit
    
    @Published var configuration: DexcomAlertConfiguration {
        didSet {
            if configuration != oldValue {
                updateHandler(configuration)
            }
        }
    }
        
    var configurationDescription: String {
        switch configuration.type {
        case .glucoseUrgentLow:
            let threholdString = glucoseFormatter.string(from: configuration.threshold, for: glucoseUnit)!
            return String(format: configuration.type.localizedDescription, threholdString)
        default:
            return configuration.type.localizedDescription
        }
    }
        
    @Published var threshold: Double = 0 {
        didSet {
            configuration.threshold = HKQuantity(unit: configurationUnit, doubleValue: threshold)
        }
    }
    
    @Published var repeatValue: TimeInterval = 0 {
        didSet {
            configuration.snoozeFrequency = DexcomSnoozeFrequency(timeInterval: repeatValue)
        }
    }

    var displaysDetails: Bool {
        return configuration.hasThresholdValue || configuration.type.canDelayRepeat
    }

    let isUrgentLow: Bool
    
    var soundUpdateHandler: (AlertSound) -> Void {
        return { alertSound in
            if self.configuration.sound != alertSound {
                self.configuration.sound = alertSound
            }
        }
    }
    
    private lazy var glucoseFormatter: QuantityFormatter = {
        let formatter = QuantityFormatter()
        formatter.setPreferredNumberFormatter(for: glucoseUnit)
        return formatter
    }()
    
    private lazy var timeFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.allowedUnits = [.hour, .minute]
        return formatter
    }()
        
    init(configuration: DexcomAlertConfiguration, glucoseUnit: HKUnit, updateHandler: @escaping (DexcomAlertConfiguration) -> Void) {
        self.updateHandler = updateHandler
        self.glucoseUnit = glucoseUnit
        self.configuration = configuration
        isUrgentLow = configuration.type == .glucoseUrgentLow
        if configuration.hasThresholdValue {
            threshold = configuration.threshold.doubleValue(for: configurationUnit).roundedToTenths // precision is limited to tenths
        }
        if configuration.type.canDelayRepeat {
            repeatValue = configuration.snoozeFrequency.timeInterval
        }
    }
    
    var configurationUnit: HKUnit {
        switch configuration.type {
        case .glucoseRateRise, .glucoseRateFall:
            return glucoseUnit.unitDivided(by: .minute())
        case .signalLoss:
            return configuration.type.unit
        default:
            return glucoseUnit
        }
    }
    
    func thresholdValues() -> [Double] {
        var stepSize: Double
        if configuration.type == .signalLoss {
            stepSize = 5
        } else if glucoseUnit == HKUnit.milligramsPerDeciliter {
            stepSize = 1
        } else {
            stepSize = 0.1
        }
        let thresholdMin = configuration.thresholdMinValue.doubleValue(for: configurationUnit)
        let thresholdMax = configuration.thresholdMaxValue.doubleValue(for: configurationUnit)
        return Array(stride(from: thresholdMin, through: thresholdMax, by: stepSize).map { $0.roundedToTenths }) // precision is limited to tenths
    }
    
    func repeatValues() -> [TimeInterval] {
        let stepSize = TimeInterval.minutes(5)
        let frequencyMin = TimeInterval.minutes(15)
        let frequencyMax = TimeInterval.hours(4)
        var repeatValues = Array(stride(from:frequencyMin, through: frequencyMax, by: stepSize))
        repeatValues.insert(TimeInterval.minutes(0), at: 0)
        return repeatValues
    }

    func formattedThreshold(_ threshold: Double) -> String? {
        switch configuration.type {
        case .glucoseRateRise, .glucoseRateFall:
           return glucoseFormatter.string(from: HKQuantity(unit: glucoseUnit, doubleValue: threshold), for: glucoseUnit)
        case .signalLoss:
            return timeFormatter.string(from: TimeInterval(minutes: threshold))
        default:
            return glucoseFormatter.string(from: HKQuantity(unit: glucoseUnit, doubleValue: threshold), for: glucoseUnit)
        }
    }
    
    func formattedRepeat(_ repeatValue: TimeInterval) -> String {
        if repeatValue == 0 {
             return LocalizedString("Every time", comment: "The title text for the repeat frequency cell when hours and minutes = 0")
        }
        return timeFormatter.string(from: repeatValue)!
    }

    func formattedSelectedRepeat(_ selectedRepeat: TimeInterval) -> String {
        if selectedRepeat == 0 {
           return formattedRepeat(selectedRepeat)
        } else {
            return String(format: LocalizedString("Every %@", comment: "Appending 'Every' as the prefix to the repeat freuqency (1: X hour(s), Y minute(s))"), formattedRepeat(selectedRepeat))
        }
    }
}
