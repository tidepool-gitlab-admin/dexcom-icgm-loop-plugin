//
//  SoundViewModel.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-19.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import DexcomCGMKit

class SoundViewModel: SoundViewModelProtocol {
    let updateHandler: (AlertSound) -> Void
    
    let sounds: [String] = AlertSound.allCases.map { $0.rawValue }
    
    @Published var selectedSound: String? {
        didSet {
            if selectedSound != nil,
                selectedSound != oldValue,
                let selectedSound = selectedSound,
                let alertSound = AlertSound(rawValue: selectedSound)
            {
                updateHandler(alertSound)
            }
        }
    }
    
    init(selectedSound: AlertSound, updateHandler: @escaping (AlertSound) -> Void) {
        self.updateHandler = updateHandler
        self.selectedSound = selectedSound.rawValue
    }
}
