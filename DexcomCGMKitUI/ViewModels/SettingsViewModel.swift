//
//  SettingsViewModel.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-13.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import HealthKit
import LoopKit
import DexcomCGMKit
import TransmitterCore

class SettingsViewModel: SettingsViewModelProtocol {
    let cgmManager: DexcomCGMManager
    
    let glucoseUnit: HKUnit
        
    var transmitterID: String {
        return cgmManager.state.transmitterStaticInfo.transmitterId
    }
    
    @Published private(set) var lastCalibrationDate: Date?
    
    @Published private(set) var sensorInsertionDate: Date?
    
    @Published private(set) var sensorExpirationDate: Date?
        
    @Published private(set) var lastGlucoseValueFormatted: String?
    
    @Published private(set) var lastGlucoseDate: Date?
    
    @Published private(set) var lastGlucoseTrendFormatted: String?
    
    @Published private(set) var sessionRunning: Bool = false
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter
    }()
        
    let completionHandler: () -> Void
    
    let calibrationCompletionHandler: (HKQuantity) -> Void
    
    let newSensorCompletionHandler: (SensorCode?) -> Void
    
    let stopSensorCompletionHandler: () -> Void
    
    let deleteCGMManagerHandler: () -> Void
        
    private lazy var glucoseFormatter: QuantityFormatter = {
        let formatter = QuantityFormatter()
        formatter.setPreferredNumberFormatter(for: glucoseUnit)
        return formatter
    }()
    
    init(cgmManager: DexcomCGMManager, glucoseUnit: HKUnit, completionHandler: @escaping () -> Void) {
        self.cgmManager = cgmManager
        self.glucoseUnit = glucoseUnit
        self.completionHandler = completionHandler
        calibrationCompletionHandler = { [weak cgmManager] in cgmManager?.calibrate(value: $0) }
        newSensorCompletionHandler = { [weak cgmManager] in cgmManager?.startSession($0) }
        stopSensorCompletionHandler = { [weak cgmManager] in cgmManager?.stopSession() }
        deleteCGMManagerHandler = { [weak cgmManager] in
            cgmManager?.closeCommunication()
            cgmManager?.notifyDelegateOfDeletion {
                DispatchQueue.main.async {
                    completionHandler()
                }
            }
        }
        updateCurrentData(cgmManager.latestTransmitterData)
        cgmManager.addObserver(self, queue: .main)
    }
    
    private func updateCurrentData(_ latestTransmitterData: TransmitterData?) {
        if let latestTransmitterData = cgmManager.latestTransmitterData {
            // session
            self.sensorInsertionDate = latestTransmitterData.session?.sessionStartSystemTime
            if let sessionStartDate = latestTransmitterData.session?.sessionStartSystemTime,
                let sessionExpirationDate = Calendar.current.date(byAdding: .day, value: latestTransmitterData.sessionDurationDays, to: sessionStartDate) {
                self.sensorExpirationDate = sessionExpirationDate
            }
            self.sessionRunning = latestTransmitterData.session != nil
            
            // glucose
            if let quantity = latestTransmitterData.cgmReading.glucoseValue, let formattedGlucoseValue = glucoseFormatter.string(from: quantity, for: glucoseUnit) {
                lastGlucoseValueFormatted = formattedGlucoseValue
                lastGlucoseDate = latestTransmitterData.cgmReading.recordedSystemTime
                if let trendRate = latestTransmitterData.cgmReading.trendRate {
                    let glucoseUnitPerMinute = glucoseUnit.unitDivided(by: .minute())
                    // This seemingly strange replacement of glucose units is only to display the unit string correctly
                    let trendPerMinute = HKQuantity(unit: glucoseUnit, doubleValue: trendRate.doubleValue(for: glucoseUnitPerMinute))
                    if let formatted = glucoseFormatter.string(from: trendPerMinute, for: glucoseUnit) {
                        lastGlucoseTrendFormatted = String(format: LocalizedString("%@/min", comment: "Format string for glucose trend per minute. (1: glucose value and unit)"), formatted)
                    }
                }
            }
            
            // calibration
            self.lastCalibrationDate = latestTransmitterData.lastCalibration?.calibrationSystemTime
        }
    }
}

extension SettingsViewModel: DexcomCGMManagerObserver {
    func cgmManagerDidUpdateLatestTransmitterData(_ manager: DexcomCGMManager) {
        updateCurrentData(cgmManager.latestTransmitterData)
    }
}
