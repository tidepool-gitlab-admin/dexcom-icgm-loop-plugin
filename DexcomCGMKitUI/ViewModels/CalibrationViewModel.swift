//
//  CalibrationViewModel.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-21.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import HealthKit

class CalibrationViewModel: CalibrationViewModelProtocol {
    let glucoseUnit: HKUnit
    let completionHandler: (HKQuantity) -> Void
    
    init(glucoseUnit: HKUnit, completionHandler: @escaping (HKQuantity) -> Void) {
        self.glucoseUnit = glucoseUnit
        self.completionHandler = completionHandler
    }
    
    func performCalibration(_ calibrationDouble: Double?) -> Bool {
        guard let calibrationDouble = calibrationDouble else {
            return false
        }
        
        let calibrationValue = HKQuantity(unit: glucoseUnit, doubleValue: calibrationDouble)
        completionHandler(calibrationValue)
        return true
    }
}
