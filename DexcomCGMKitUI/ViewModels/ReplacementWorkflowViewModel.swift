//
//  ReplacementWorkflowViewModel.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-03.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import DexcomCGMKit
import TransmitterCore

class ReplacementWorkflowViewModel {
    let replacementWorkflow: ReplacementWorkflow
    let replaceSensorCompletionHandler: (SensorCode?) -> Void
    let stopSessionHandler: (() -> Void)?
    let replaceTransmitterCompletionHandler: ((DexcomTransmitterID) -> Void)?
    var workflowCompletionHandler: (() -> Void)? = nil

    init(replacementWorkflow: ReplacementWorkflow = .replaceSensor,
        replaceSensorCompletionHandler: @escaping (SensorCode?) -> Void,
        stopSessionHandler: (() -> Void)? = nil,
        replaceTransmitterCompletionHandler: ((DexcomTransmitterID) -> Void)? = nil)
    {
        self.replacementWorkflow = replacementWorkflow
        self.replaceSensorCompletionHandler = replaceSensorCompletionHandler
        self.stopSessionHandler = stopSessionHandler
        self.replaceTransmitterCompletionHandler = replaceTransmitterCompletionHandler
    }
    
    func validSensorCode(_ rawSensorCode: String) -> Bool {
        let sensorCode = SensorCode(rawValue: rawSensorCode)
        return validSensorCode(sensorCode)
    }
    
    func validSensorCode(_ sensor2DBarcode: Dexcom2DBarcode?) -> Bool {
        var sensorCode: SensorCode?
        if let sensor2DBarcode = sensor2DBarcode,
            sensor2DBarcode.isValid(),
            let rawSensorCode = sensor2DBarcode.code
        {
            sensorCode = SensorCode(rawValue: rawSensorCode)
        }
        return validSensorCode(sensorCode)
    }
    
    func validSensorCode(_ sensorCode: SensorCode?) -> Bool {
        guard sensorCode != nil else {
            return false
        }
        
        switch replacementWorkflow {
        case .replaceSensor:
            workflowCompletionHandler = { [weak self] in
                self?.replaceSensorCompletionHandler(sensorCode)
            }
        case .replaceTransmitterAndSensor:
            replaceSensorCompletionHandler(sensorCode)
        }
        return true
    }
    
    func validTransmitterID(_ rawTransmitterID: String) -> Bool {
        let transmitterID = DexcomTransmitterID(rawTransmitterID)
        return validTransmitterID(transmitterID)
    }
    
    func validTransmitterID(_ transmitter2DBarcode: Dexcom2DBarcode?) -> Bool {
        var transmitterID: DexcomTransmitterID?
        if let transmitter2DBarcode = transmitter2DBarcode,
            transmitter2DBarcode.isValid(),
            let transmitterIDString = transmitter2DBarcode.code
        {
            transmitterID = DexcomTransmitterID(transmitterIDString)
        }
        return validTransmitterID(transmitterID)
    }
    
    func validTransmitterID(_ transmitterID: DexcomTransmitterID?) -> Bool {
        guard let transmitterID = transmitterID else {
            return false
        }
        
        workflowCompletionHandler = { [weak self] in
            self?.replaceTransmitterCompletionHandler?(transmitterID)
        }
        return true
    }
}
