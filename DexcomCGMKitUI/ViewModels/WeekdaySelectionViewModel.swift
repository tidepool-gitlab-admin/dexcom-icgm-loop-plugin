
//
//  WeekdaySelectionViewModel.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-24.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import DexcomCGMKit

class WeekdaySelectionViewModel: WeekdaySelectionViewModelProtocol {
    @Published var selectedDays: Set<Days> {
        didSet {
            updateHandler(Days.combineOptions(selectedDays))
        }
    }
    
    let selectableDays: [Days] = Days.allCases
    
    let updateHandler: (Days) -> Void
    
    var daysAsString: (Days) -> String {
        return { String(describing: $0) }
    }
    
    init(selectedDays: Days, updateHandler: @escaping (Days) -> Void) {
        // the view expects an set of selected days. So expanding Days to match
        self.selectedDays = selectedDays.expandOptions()
        self.updateHandler = updateHandler
    }
}
