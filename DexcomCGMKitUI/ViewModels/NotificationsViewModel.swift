//
//  NotificationsViewModel.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-14.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import LoopKit
import HealthKit
import DexcomCGMKit

class NotificationsViewModel: ObservableObject {
    
    var alertConfigurationManager: DexcomAlertConfigurationManager
    
    @Published var configurations: [DexcomAlertConfiguration]
    
    let glucoseUnit: HKUnit
    
    var hasAlertSchedule: Bool {
        return alertConfigurationManager.hasAlertSchedule
    }
    
    @Published var scheduleName: String?
            
    var configurationUpdateHandler: (DexcomAlertConfiguration) -> Void {
        return { updatedConfiguration in
            self.alertConfigurationManager.updateMutableAlert(withConfiguration: updatedConfiguration)
            if let index = self.configurations.firstIndex(where: { $0.type == updatedConfiguration.type }) {
                self.configurations[index] = updatedConfiguration
            }
        }
    }
    
    private let displayedTypes: [DexcomAlertType] = [.glucoseUrgentLow, .glucoseLow, .glucoseHigh, .glucoseRateRise, .glucoseRateFall, .signalLoss, .noReadings]

    init(alertConfigurationManager: DexcomAlertConfigurationManager, glucoseUnit: HKUnit) {
        self.alertConfigurationManager = alertConfigurationManager
        self.glucoseUnit = glucoseUnit
        // the displayedTypes configurations always exist
        self.configurations = self.displayedTypes.map { alertConfigurationManager.mutableAlertConfiguration(forType: $0)! }
        updateScheduleName()
    }
    
    func resetAlerts() {
        alertConfigurationManager.resetAlerts()
        self.configurations = self.displayedTypes.map { alertConfigurationManager.mutableAlertConfiguration(forType: $0)! }
        updateScheduleName()
    }
    
    func updateScheduleName() {
        scheduleName = alertConfigurationManager.alertSchedule?.name
    }
}
