//
//  EntryOptionsState.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-05.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

enum EntryOptionsState {
    case normal
    case entryError
}
