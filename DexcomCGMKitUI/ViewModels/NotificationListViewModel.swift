//
//  NotificationListViewModel.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-02.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import HealthKit
import LoopKit
import DexcomCGMKit

class NotificationListViewModel: NotificationListViewModelProtocol {
    @Published var configurations: [DexcomAlertConfiguration]
    
    let glucoseUnit: HKUnit
    
    var configurationUpdateHandler: (DexcomAlertConfiguration) -> Void
    
    init(configurations: [DexcomAlertConfiguration], glucoseUnit: HKUnit, configurationUpdateHandler: @escaping (DexcomAlertConfiguration) -> Void) {
        self.configurations = configurations
        self.glucoseUnit = glucoseUnit
        self.configurationUpdateHandler = configurationUpdateHandler
    }
    
    private lazy var glucoseFormatter: QuantityFormatter = {
        let formatter = QuantityFormatter()
        formatter.setPreferredNumberFormatter(for: glucoseUnit)
        return formatter
    }()
    
    func value(forConfiguration configuration: DexcomAlertConfiguration) -> String? {
        if !configuration.enabled {
            return LocalizedString("Off", comment: "The description if the alert is disabled")
        }

        switch configuration.type {
        case .glucoseUrgentLow, .glucoseLow, .glucoseHigh:
            return glucoseFormatter.string(from: configuration.threshold, for: glucoseUnit) ?? LocalizedString("Off", comment: "The description if the alert is disabled")
        case .glucoseRateRise, .glucoseRateFall, .signalLoss, .noReadings:
            return configuration.enabled ? LocalizedString("On", comment: "The description if the alert is enabled") : LocalizedString("Off", comment: "The description if the alert is disabled")
        default:
            return nil
        }
    }
}
