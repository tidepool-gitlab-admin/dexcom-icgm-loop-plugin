//
//  TransmitterDetailsViewModel.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-14.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import DexcomCGMKit
import TransmitterCore

class TransmitterDetailsViewModel: TransmitterDetailsViewModelProtocol {
    
    let cgmManager: DexcomCGMManager
    
    @Published private(set) var serialNumber: String?
    
    @Published private(set) var activatedOn: Date?
    
    @Published private(set) var firmware: String?
    
    @Published private(set) var softwareNumber: String?
    
    @Published private(set) var detailsAvailable: Bool = false
    
    @Published var sessionRunning: Bool
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        return formatter
    }()
    
    let newSensorCompletionHandler: (SensorCode?) -> Void
    
    let newTransmitterCompletionHandler: (DexcomTransmitterID) -> Void
    
    let stopSensorCompletionHandler: () -> Void

    init(cgmManager: DexcomCGMManager, sessionRunning: Bool) {
        self.cgmManager = cgmManager
        newSensorCompletionHandler = { [weak cgmManager] in cgmManager?.pendingSensorCode = $0 }
        newTransmitterCompletionHandler = { [weak cgmManager] in cgmManager?.replaceTransmitter($0) }
        stopSensorCompletionHandler = { [weak cgmManager] in cgmManager?.stopSession() }
        //TODO test that this is updated correctly
        self.sessionRunning = sessionRunning
        updateTransmitterDetails(state: cgmManager.state)
        cgmManager.addObserver(self, queue: .main)
    }
    
    private func updateTransmitterDetails(state: DexcomCGMManagerState) {
        self.serialNumber = state.transmitterStaticInfo.transmitterId
        self.activatedOn = state.transmitterStaticInfo.activatedOn
        self.firmware = state.transmitterStaticInfo.transmitterVersion
        self.softwareNumber = "SW" + String(state.transmitterStaticInfo.softwareNumber)
        self.detailsAvailable = state.connectionState == .transmitterFound
    }
}

extension TransmitterDetailsViewModel: DexcomCGMManagerObserver {
    func cgmManagerDidFindTransmitter(_ manager: DexcomCGMManager) {
        updateTransmitterDetails(state: manager.state)
    }
}
