//
//  AlertScheduleDetailsViewModel.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-24.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import HealthKit
import DexcomCGMKit

class AlertScheduleDetailsViewModel: ObservableObject {
    private let alertConfigurationManager: DexcomAlertConfigurationManager
    
    @Published var alertSchedule: DexcomAlertSchedule
    
    @Published var configurations: [DexcomAlertConfiguration] = []
    
    let glucoseUnit: HKUnit
    
    @Published var hasAlertSchedule: Bool
    
    var scheduleName: String {
        get {
            return alertSchedule.name
        }
        set {
            if newValue != alertSchedule.name {
                alertSchedule.name = newValue
            }
        }
    }
    
    @Published var scheduleStartTime: Date = Date() {
        didSet {
            if scheduleStartTime != oldValue {
                var calendar = Calendar.current
                calendar.timeZone = TimeZone.currentFixed
                alertSchedule.weeklySchedule.startTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: scheduleStartTime)
            }
        }
    }
    
    @Published var scheduleEndTime: Date = Date() {
        didSet {
            if scheduleEndTime != oldValue {
                var calendar = Calendar.current
                calendar.timeZone = TimeZone.currentFixed
                alertSchedule.weeklySchedule.endTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: scheduleEndTime)
            }
        }
    }
    
    var daysUpdatedHandler: (Days) -> Void {
        return { [weak self] days in
            self?.alertSchedule.weeklySchedule.days = days
        }
    }
    
    private let displayedTypes: [DexcomAlertType] = [.glucoseUrgentLow, .glucoseLow, .glucoseHigh, .glucoseRateRise, .glucoseRateFall]
    
    var configurationUpdateHandler: (DexcomAlertConfiguration) -> Void {
        return { [weak self] updatedConfiguration in
            self?.alertConfigurationManager.updateAlertSchedule(withConfiguration: updatedConfiguration)
            if let index = self?.configurations.firstIndex(where: { $0.type == updatedConfiguration.type }) {
                self?.configurations[index] = updatedConfiguration
            }
        }
    }
    
    init(alertConfigurationManager: DexcomAlertConfigurationManager, glucoseUnit: HKUnit) {
        self.alertConfigurationManager = alertConfigurationManager
        self.glucoseUnit = glucoseUnit
        hasAlertSchedule = alertConfigurationManager.hasAlertSchedule
        alertSchedule = alertConfigurationManager.alertSchedule ?? DexcomAlertSchedule.defaultSchedule()
        restoreAlertSchedule()
    }
    
    func saveAlertSchedule() {
        alertConfigurationManager.alertSchedule = alertSchedule
        hasAlertSchedule = true
    }
    
    func deleteAlertSchedule() {
        alertConfigurationManager.alertSchedule = nil
        hasAlertSchedule = false
        restoreAlertSchedule()
    }
    
    func restoreAlertSchedule() {
        alertSchedule = alertConfigurationManager.alertSchedule ?? DexcomAlertSchedule.defaultSchedule()
        scheduleName = alertSchedule.name
        if hasAlertSchedule {
            scheduleStartTime = alertSchedule.weeklySchedule.startTime()
            scheduleEndTime = alertSchedule.weeklySchedule.endTime()
        } else {
            scheduleStartTime = Date()
            scheduleEndTime = Date()
        }
        
        // the displayedTypes configurations always exist
        configurations = displayedTypes.map { alertSchedule.configurations[$0]! }
    }
}
