//
//  AlertFrequencyPickerTableViewCell.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-10-10.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit

protocol AlertFrequencyPickerDelegate: class {
    func didSelectFrequency(_ timeInterval: TimeInterval, sender: AlertFrequencyPickerTableViewCell)
}

public final class AlertFrequencyPickerTableViewCell: ValuePickerTableViewCell {

    private weak var delegate: AlertFrequencyPickerDelegate?
    
    private var minTimeInterval: TimeInterval = TimeInterval(minutes: 0)
    
    private var maxTimeInterval: TimeInterval = TimeInterval(hours: 4)
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override public func setup() {
        super.setup()
        valuePicker.delegate = self
        valuePicker.dataSource = self
    }
    
    func set(delegate: AlertFrequencyPickerDelegate, initialValue: TimeInterval, minValue: TimeInterval = TimeInterval(minutes: 0), maxValue: TimeInterval = TimeInterval(hours: 4)) {
        self.delegate = delegate
        self.minTimeInterval = minValue
        self.maxTimeInterval = maxValue
        setPickerValue(duration: initialValue, animated: false)
    }

    private func setPickerValue(duration: TimeInterval, animated: Bool) {
        let (hours, mins) = duration.hoursAndMinutes
        setPickerValue(hours: hours, minutes: mins, animated: animated)
    }
    
    private func setPickerValue(hours: Int, minutes: Int, animated: Bool) {
        let hourRow = FrequencyComponent.hour.values.firstIndex(of: hours) ?? 0
        let minuteRow = FrequencyComponent.minute.values.firstIndex(of: minutes) ?? 0
        valuePicker.selectRow(hourRow, inComponent: FrequencyComponent.hour.rawValue, animated: animated)
        valuePicker.selectRow(minuteRow, inComponent: FrequencyComponent.minute.rawValue, animated: animated)
    }
}

//MARK: - Picker Data Source and Delegate

extension AlertFrequencyPickerTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource {
    private enum FrequencyComponent: Int, CaseIterable {
        case hour
        case hourUnit
        case minute
        case minuteUnit
        
        var values: Array<Int> {
            switch self {
            case .hour:
                return Array(stride(from: 0, through: 4, by: 1))
            case .minute:
                return Array(stride(from: 0, to: 60, by: 5))
            default:
                return []
            }
        }
        
        var stringValues: Array<String> {
            switch self {
            case .hour:
                return values.map { String(format:"%d", $0) }
            case .hourUnit:
                return [LocalizedString("hr", comment: "Units for hour")]
            case .minute:
                return values.map { String(format:"%d",$0) }
            case .minuteUnit:
                return [LocalizedString("min", comment: "Units for minute")]
            }
        }
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return FrequencyComponent.allCases.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return FrequencyComponent(rawValue: component)?.stringValues.count ?? 0
    }

    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return FrequencyComponent(rawValue: component)?.stringValues[row]
    }

    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let hours = TimeInterval(hours: Double(FrequencyComponent.hour.values[pickerView.selectedRow(inComponent: FrequencyComponent.hour.rawValue)]))
        let minutes = TimeInterval(minutes: Double(FrequencyComponent.minute.values[pickerView.selectedRow(inComponent: FrequencyComponent.minute.rawValue)]))
        var duration = hours+minutes
        if duration < minTimeInterval {
            duration = minTimeInterval
            setPickerValue(duration: duration, animated: true)
        } else if duration > maxTimeInterval {
            duration = maxTimeInterval
            setPickerValue(duration: duration, animated: true)
        }
        delegate?.didSelectFrequency(duration, sender: self)
    }

}
