//
//  Barcode2DScannerView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-18.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import AVFoundation

protocol Barcode2DScannerViewDelegate: class {
    func scanningSucceededWithCode(_ str: String?)
    func scanningDidFail()
    func scanningDidStop()
}

extension Barcode2DScannerViewDelegate {
    func scanningDidFail() {
        //optional
    }
    
    func scanningDidStop() {
        //optional
    }
}

class Barcode2DScannerView: UIView {
    
    weak var delegate: Barcode2DScannerViewDelegate?
    
    var captureSession: AVCaptureSession?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    // MARK: - AVFoundation
    
    override class var layerClass: AnyClass  {
        return AVCaptureVideoPreviewLayer.self
    }
    
    override var layer: AVCaptureVideoPreviewLayer {
        return super.layer as! AVCaptureVideoPreviewLayer
    }
}

// MARK: -

extension Barcode2DScannerView {
    
    var isRunning: Bool {
        return captureSession?.isRunning ?? false
    }
    
    func startScanning() {
        captureSession?.startRunning()
    }
    
    func stopScanning() {
        captureSession?.stopRunning()
        delegate?.scanningDidStop()
    }
    
    private func setup() {
        clipsToBounds = true
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            scanningDidFail()
            return
        }
        
        let videoInput: AVCaptureDeviceInput
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            scanningDidFail()
            return
        }
        
        guard let captureSession = captureSession,
            captureSession.canAddInput(videoInput) else {
            scanningDidFail()
            return
        }
        
        captureSession.addInput(videoInput)
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        guard captureSession.canAddOutput(metadataOutput) else {
            scanningDidFail()
            return
        }

        captureSession.addOutput(metadataOutput)
        metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        metadataOutput.metadataObjectTypes = [.dataMatrix]
        
        layer.session = captureSession
        layer.videoGravity = .resizeAspectFill
        
        captureSession.startRunning()
    }
    
    func scanningDidFail() {
        delegate?.scanningDidFail()
        captureSession = nil
    }
    
    func found(code: String) {
        delegate?.scanningSucceededWithCode(code)
    }
    
}

extension Barcode2DScannerView: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput,
                        didOutput metadataObjects: [AVMetadataObject],
                        from connection: AVCaptureConnection) {
        stopScanning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject,
                let stringValue = readableObject.stringValue else {
                    scanningDidFail()
                    return
            }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
    }
}
