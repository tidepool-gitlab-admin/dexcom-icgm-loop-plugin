//
//  SetupGuideView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-05.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI

struct SetupGuideView: View {
    var viewModel: ReplacementWorkflowViewModel
    @State private var rootLinkIsActive: Bool = false
    let dismissAction: () -> Void
    
    var cancelButton: some View {
        Button(LocalizedString("Cancel", comment: "Cancel button title"), action: {
            self.dismissAction()
        })
    }
    
    var body: some View {
        NavigationView {
            SensorCodeLocationView(viewModel: viewModel, rootLinkIsActive: $rootLinkIsActive)
                .navigationBarItems(trailing: cancelButton)
        }
    }
}

struct SetupGuideView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = ReplacementWorkflowViewModel(replaceSensorCompletionHandler: { _ in })
        return Group {
            SetupGuideView(viewModel: viewModel, dismissAction: { })
                .colorScheme(.light)
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("SE light")

            SetupGuideView(viewModel: viewModel, dismissAction: { })
                .colorScheme(.dark)
                .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
                .previewDisplayName("XS Max dark")
        }
    }
}
