//
//  CalibrationView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-11.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import HealthKit
import LoopKitUI
import DexcomCGMKit

struct CalibrationView<Model>: View, HorizontalSizeClassOverride where Model: CalibrationViewModelProtocol {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var viewModel: Model
    @State private var calibrationDouble: Double?
    
    var saveButton: some View {
        Button(LocalizedString("Save", comment: "Save button title"), action: {
            if self.viewModel.performCalibration(self.calibrationDouble) {
                self.done()
            }
        })
            .disabled(calibrationDouble == nil)
    }
    
    var cancelButton: some View {
        Button(LocalizedString("Cancel", comment: "Cancel button title"), action: {
            self.done()
        })
    }
    
    func done() {
        self.presentationMode.wrappedValue.dismiss()
    }
    
    var body: some View {
        List {
            Section {
                CalibrationInstructions()
                    .padding(.top, 8)
                    .padding(.bottom, 8)
                LabeledNumberInput(value: $calibrationDouble,
                                   label: viewModel.glucoseUnit.shortLocalizedUnitString(),
                                   allowFractions: (viewModel.glucoseUnit.preferredFractionDigits > 0))
                    .padding(.top, 15)
                    .padding(.bottom, 16)
            }
        }
        .navigationBarItems(leading: cancelButton, trailing: saveButton)
        .listStyle(GroupedListStyle())
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(Text(LocalizedString("Calibrate", comment: "Navigation view title")))
    }
}

struct Calibration_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = CalibrationViewModel(glucoseUnit: HKUnit.milligramsPerDeciliter, completionHandler: {_ in })
        return Group {
            CalibrationView(viewModel: viewModel)
                .colorScheme(.light)
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("SE light")
            CalibrationView(viewModel: viewModel)
                .colorScheme(.dark)
                .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
                .previewDisplayName("XS Max dark")
        }
    }
}
