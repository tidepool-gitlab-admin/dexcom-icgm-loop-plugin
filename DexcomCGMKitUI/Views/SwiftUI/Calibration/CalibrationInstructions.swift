//
//  CalibrationInstructions.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-11.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI

struct CalibrationInstructions: View {
    private let instructions = [
        LocalizedString("Wash and dry your hands.", comment: "Step 1 for calibration"),
        LocalizedString("Take a fingerstick with your meter.", comment: "Step 2 for calibration"),
        LocalizedString("Promptly enter the exact value from your meter.", comment: "Step 3 for calibration")
    ]
    @State private var displayAdditionalInfo = false
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            HStack(alignment: .firstTextBaseline) {
                Text(LocalizedString("Steps:", comment: "The title of the calibration steps."))
                    .font(.headline)
                Spacer()
                Button(action: { self.displayAdditionalInfo = true }) {
                    Image(systemName: "info.circle")
                        .font(.headline)
                        .foregroundColor(.accentColor)
                        .accessibility(label: Text(LocalizedString("Additional calibration info", comment: "Calibration additional info button accessibility title")))
                }
            }
            InstructionList(instructions: instructions)
                .accessibility(label: Text(LocalizedString("Calibration steps. First, wash and dry your hands. Then take a finger stick with your meter. Finally, promptly enter the exact value from your meter.", comment: "Accessibility description for calibration steps")))
                .padding(.top, 5)
        }
        .actionSheet(isPresented: $displayAdditionalInfo) {
            ActionSheet(
                title: Text(""),
                message: Text(LocalizedString("Calibrating aligns your CGM to your meter value. Unless prompted, it is optional.\n\nEnter your blood glucose meter value to calibrate your CGM.", comment: "Description of why to calibrate")),
                buttons: [.default(Text(LocalizedString("OK", comment: "OK button title")))]
            )
        }
    }
}

struct CalibrationInstructions_Previews: PreviewProvider {
    static var previews: some View {
        CalibrationInstructions()
    }
}
