//
//  TransmitterIDEntryPhotoView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-05.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI
import DexcomCGMKit
import TransmitterCore

struct TransmitterIDEntryPhotoView: View {
    var viewModel: ReplacementWorkflowViewModel
    @Binding var entryOptionsState: EntryOptionsState
    @Binding var returnLinkIsActive: Bool
    @Binding var rootLinkIsActive: Bool
    @State private var workflowCompleted: Bool = false
    
    var body: some View {
        VStack(spacing: 0) {
            ZStack(alignment: .center) {
                ScannerView(deviceType: .transmitter,
                            startScanning: !workflowCompleted && returnLinkIsActive,
                            completionHandler:
                    { transmitter2DBarcode in
                        if self.viewModel.validTransmitterID(transmitter2DBarcode) {
                            self.workflowCompleted = true
                        } else {
                            self.entryOptionsState = .entryError
                            self.returnLinkIsActive = false
                        }
                })
                
                Text("[   ]")
                    .font(.system(size: 80))
                    .foregroundColor(.purple)
                
                NavigationLink(destination: SensorInsertView(viewModel: self.viewModel,
                                                             rootLinkIsActive: self.$rootLinkIsActive),
                               isActive: self.$workflowCompleted)
                {
                    EmptyView()
                }
                .isDetailLink(false)
                .disabled(true)
            }
            .aspectRatio(4/3, contentMode: ContentMode.fill)
            .padding(.bottom)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
                        
            ScrollView {
                VStack (alignment: .leading, spacing: 10) {
                    Text(LocalizedString("Find transmitter SN on outside of transmitter box.", comment:
                        "Description on where to find the transmitter SN"))
                    
                    HStack(alignment: .top, spacing: 10) {
                        Image(frameworkImage: "2d-barcode-thumb")
                            .resizable()
                            .aspectRatio(contentMode: ContentMode.fit)
                            .frame(height: 50)
                        
                        InstructionList(instructions:[
                            LocalizedString("Position transmitter SN inside purple guides.", comment: "Descrition of how to take photo of transmitter SN"),
                            LocalizedString("Tap to focus.", comment: "Description of how to focus on the transmitter SN"),
                            LocalizedString("Photo is taken automatically.", comment: "Description of how the photo is taken")
                        ])
                    }
                }
                .padding()
                .background(Color(UIColor.tertiarySystemBackground).cornerRadius(6))
            }
        }
        .navigationBarTitle(Text(LocalizedString("Take Photo", comment: "Navigation view title")), displayMode: .inline)
    }
}

struct TransmitterIDEntryPhotoView_Previews: PreviewProvider {
    static var previews: some View {
        return Group {
            NavigationView {
                PreviewWrapper()
            }
            .colorScheme(.light)
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("SE light")
            NavigationView {
                PreviewWrapper()
            }
            .colorScheme(.dark)
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("XS Max dark")
        }
    }
    
    struct PreviewWrapper: View {
        let viewModel = ReplacementWorkflowViewModel(replaceSensorCompletionHandler: { _ in })
        let entryOptionsState = EntryOptionsState.normal
        var body: some View {
            TransmitterIDEntryPhotoView(viewModel: viewModel,
                                        entryOptionsState: .constant(entryOptionsState),
                                        returnLinkIsActive: .constant(true),
                                        rootLinkIsActive: .constant(true))
        }
    }
}
