//
//  TransmitterIDEntryOptionsView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-05.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI

struct TransmitterIDEntryOptionsView: View, HorizontalSizeClassOverride {
    var viewModel: ReplacementWorkflowViewModel
    @Binding var rootLinkIsActive: Bool
    @State private var takePhotoSelected: Bool = false
    @State private var enterManuallySelected: Bool = false
    @State private var entryOptionsState: EntryOptionsState = .normal
    
    var body: some View {
        GuidePage(content: {
            VStack(alignment: .leading, spacing: 10) {
                Image(frameworkImage: "transmitter-id")
                    .resizable()
                    .aspectRatio(contentMode: ContentMode.fit)
                    .frame(height: 150)
                
                if self.entryOptionsState == .normal {
                    Text(LocalizedString("Take photo of Transmitter SN on outside of transmitter box, or manually enter SN.", comment: "Description of where to take a photo of the transmitter SN"))
                    Text(LocalizedString("Keep box for future use.", comment: "Reminder to keep the transmitter box"))
                } else {
                    Text(LocalizedString("Transmitter SN entered is not correct.", comment: "Description that the enterted transmitter SN is incorrect"))
                    Text(LocalizedString("Take photo of Transmitter SN on outside of transmitter box again, or manually enter SN.", comment: "Request to re-enter the transmitter SN"))
                }
            }
            .padding()
        }) {
            VStack(alignment: .leading, spacing: 15) {
                GuideNavigationButton(navigationLinkIsActive: self.$takePhotoSelected,
                                      label: LocalizedString("Take Photo", comment: "Button title to select the take a photo option"))
                {
                    TransmitterIDEntryPhotoView(viewModel: self.viewModel,
                                                entryOptionsState: self.$entryOptionsState,
                                                returnLinkIsActive: self.$takePhotoSelected,
                                                rootLinkIsActive: self.$rootLinkIsActive)
                }
                
                GuideNavigationButton(navigationLinkIsActive: self.$enterManuallySelected,
                                      label: LocalizedString("Enter Manually", comment: "Button title to select the enter manually option"),
                                      buttonStyle: .secondary)
                {
                    TransmitterIDEntryManualView(viewModel: self.viewModel,
                                                 entryOptionsState: self.$entryOptionsState,
                                                 returnLinkIsActive: self.$enterManuallySelected,
                                                 rootLinkIsActive: self.$rootLinkIsActive)
                }
            }
            .padding()
        }
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(entryOptionsState == .normal ? LocalizedString("Transmitter SN", comment: "Navigation view title") : LocalizedString("Incorrect SN", comment: "Navigation view title"))
    }
}

struct TransmitterIDEntryOptionsView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = ReplacementWorkflowViewModel(replaceSensorCompletionHandler: { _ in })
        return Group {
            NavigationView {
                TransmitterIDEntryOptionsView(viewModel: viewModel, rootLinkIsActive: .constant(true))
            }
            .colorScheme(.light)
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("SE light")
            NavigationView {
                TransmitterIDEntryOptionsView(viewModel: viewModel, rootLinkIsActive: .constant(true))
            }
            .colorScheme(.dark)
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("XS Max dark")
        }
    }
}

