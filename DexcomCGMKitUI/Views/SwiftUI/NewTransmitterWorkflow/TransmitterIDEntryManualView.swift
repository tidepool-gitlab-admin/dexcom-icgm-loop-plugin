//
//  TransmitterIDEntryManualView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-05.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI
import DexcomCGMKit

struct TransmitterIDEntryManualView: View, HorizontalSizeClassOverride {
    var viewModel: ReplacementWorkflowViewModel
    @Binding var entryOptionsState: EntryOptionsState
    @Binding var returnLinkIsActive: Bool
    @Binding var rootLinkIsActive: Bool
    @State var rawTransmitterID: String = ""
    @State private var displayConfirmation: Bool = false
    @State private var workflowCompleted: Bool = false
    
    private var saveButton: some View {
        Button(LocalizedString("Save", comment: "Save button title"), action: {
            self.displayConfirmation = true
        })
            .disabled(rawTransmitterID.count < DexcomTransmitterID.length)
            .alert(isPresented: self.$displayConfirmation) {
                Alert(title: Text(LocalizedString("Confirm Entry", comment: "Confirm entered transmitter SN title")),
                      message: Text(self.rawTransmitterID),
                      primaryButton: .default(Text(LocalizedString("Confirm", comment: "Confirm button title")),
                                              action: {
                                                if self.viewModel.validTransmitterID(self.rawTransmitterID) {
                                                    self.workflowCompleted = true
                                                } else {
                                                    self.entryOptionsState = .entryError
                                                    self.returnLinkIsActive = false
                                                }
                      }),
                      secondaryButton: .cancel()
                )
        }
    }
    
    var body: some View {
        GuidePage(content: {
            VStack (alignment: .leading, spacing: 10) {
                TextField(LocalizedString("Enter Transmitter SN", comment: "Placeholder text until the transmitter SN is entered"), text: self.$rawTransmitterID)
                    .keyboardType(.asciiCapable)
                    .autocapitalization(.allCharacters)
                    .font(.largeTitle)
                    .padding(.top)
                    .multilineTextAlignment(.center)
                
                Divider()
                
                Text(LocalizedString("The transmitter SN is printed on the bottom of your transmitter and on the outside of the transmitter box.", comment: "Description of where to find the transmitter SN"))
                    .padding(.bottom, 10)
            }
        }) {
            NavigationLink(destination: SensorInsertView(viewModel: self.viewModel,
                                                         rootLinkIsActive: self.$rootLinkIsActive),
                           isActive: self.$workflowCompleted)
            {
                EmptyView()
            }
            .isDetailLink(false)
            .disabled(true)
        }
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(LocalizedString("Enter Transmitter SN", comment: "Navigation view title"))
        .navigationBarItems(trailing: self.saveButton)
    }
}

struct TransmitterIDEntryManualView_Previews: PreviewProvider {
    static var previews: some View {
        return Group {
            NavigationView {
                PreviewWrapper()
            }
            .colorScheme(.light)
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("SE light")
            NavigationView {
                PreviewWrapper()
            }
            .colorScheme(.dark)
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("XS Max dark")
        }
    }
    
    struct PreviewWrapper: View {
        let viewModel = ReplacementWorkflowViewModel(replaceSensorCompletionHandler: { _ in })
        let entryOptionsState = EntryOptionsState.normal
        var body: some View {
            TransmitterIDEntryManualView(viewModel: viewModel,
                                         entryOptionsState: .constant(entryOptionsState),
                                         returnLinkIsActive: .constant(true),
                                         rootLinkIsActive: .constant(true))
        }
    }
}
