//
//  SensorCodeEntryManualView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-28.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI
import DexcomCGMKit
import TransmitterCore

struct SensorCodeEntryManualView: View, HorizontalSizeClassOverride {
    var viewModel: ReplacementWorkflowViewModel
    @Binding var entryOptionsState: EntryOptionsState
    @Binding var returnLinkIsActive: Bool
    @Binding var rootLinkIsActive: Bool
    @State var rawSensorCode: String = ""
    @State private var displayConfirmation: Bool = false
    @State private var workflowCompleted: Bool = false
    @State private var startTransmitterWorkflow: Bool = false
    @State private var displayLearnMoreInfo: Bool = false
    
    private var saveButton: some View {
        Button(LocalizedString("Save", comment: "Save button title"), action: {
            self.displayConfirmation = true
        })
            .disabled(rawSensorCode.count < SensorCode.codeLength)
            .alert(isPresented: self.$displayConfirmation) {
                Alert(title: Text(LocalizedString("Confirm Entry", comment: "Alert title to confirm the sensor code entry")),
                      message: Text(self.rawSensorCode),
                      primaryButton: .default(Text(LocalizedString("Confirm", comment: "Confirm button title")),
                                              action:
                        {
                            if self.viewModel.validSensorCode(self.rawSensorCode) {
                                // check the replacement workflow type and navigate to the next view
                                switch self.viewModel.replacementWorkflow {
                                case .replaceSensor:
                                    self.workflowCompleted = true
                                case .replaceTransmitterAndSensor:
                                    self.startTransmitterWorkflow = true
                                }
                            } else {
                                self.entryOptionsState = .entryError
                                self.returnLinkIsActive = false
                            }
                      }),
                      secondaryButton: .cancel()
                )
        }
    }
    
    var body: some View {
        GuidePage(content: {
            VStack (alignment: .leading, spacing: 10) {
                TextField(LocalizedString("Enter Sensor Code", comment: "Placeholder text before entering sensor code in text field"), text: self.$rawSensorCode)
                    .keyboardType(.numberPad)
                    .font(.largeTitle)
                    .padding(.top)
                    .multilineTextAlignment(.center)
                
                Divider()
                
                VStack(alignment: .leading, spacing: 10) {
                    Text(LocalizedString("Find the sensor code on the sensor applicator peel-off liner.", comment: "Description of where to find the sensor code"))
                    
                    Button(action:{
                        self.displayLearnMoreInfo = true
                    }) {
                        Text(LocalizedString("Learn more", comment: "Button title to learn more about the sensor code"))
                    }
                    .padding(.bottom, 10)
                    .sheet(isPresented: self.$displayLearnMoreInfo) {
                        AdditionalDescriptionView(title: LocalizedString("Sensor Code?", comment: "Title of additional description that explains why a sensor code is needed"),
                                                  boldedMessage: LocalizedString("What is a sensor code and why do I need one?", comment: "body text for explaining the sensor code"),
                                                  additionalDescription: LocalizedString("When you start a new sensor, you must enter a code into your display device to use the G6 without fingerstick calibrations. Each sensor has its own code printed on the back of the adhesive patch. Do not use a code from a different sensor or make up a code. If you do not enter the correct code, your sensor will not work as well and could be inaccurate. If you have lost the sensor code, you may calibrate the G6 using fingersticks.", comment: "body text for explaining the sensor code"))
                    }
                }
            }
        }) {
            NavigationLink(destination: SensorInsertView(viewModel: self.viewModel,
                                                         rootLinkIsActive: self.$rootLinkIsActive),
                           isActive: self.$workflowCompleted)
            {
                EmptyView()
            }
            .isDetailLink(false)
            .disabled(true)
            
            NavigationLink(destination: TransmitterIDEntryOptionsView(viewModel: self.viewModel,
                                                                      rootLinkIsActive: self.$rootLinkIsActive),
                           isActive: self.$startTransmitterWorkflow)
            {
                EmptyView()
            }
            .isDetailLink(false)
            .disabled(true)
        }
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(LocalizedString("Enter Sensor Code", comment: "Navigation view title"))
        .navigationBarItems(trailing: self.saveButton)
    }
}

struct SensorCodeEntryManualView_Previews: PreviewProvider {
    static var previews: some View {
        return Group {
            NavigationView {
                PreviewWrapper()
            }
            .colorScheme(.light)
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("SE light")
            NavigationView {
                PreviewWrapper()
            }
            .colorScheme(.dark)
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("XS Max dark")
        }
    }
    
    struct PreviewWrapper: View {
        let viewModel = ReplacementWorkflowViewModel(replaceSensorCompletionHandler: { _ in })
        let entryOptionsState = EntryOptionsState.normal
        var body: some View {
            SensorCodeEntryManualView(viewModel: viewModel,
                                      entryOptionsState: .constant(entryOptionsState),
                                      returnLinkIsActive: .constant(true),
                                      rootLinkIsActive: .constant(true))
        }
    }
}
