//
//  SensorInsertView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-28.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI
import GlucoseCore

struct SensorInsertView: View, HorizontalSizeClassOverride {
    var viewModel: ReplacementWorkflowViewModel
    @Binding var rootLinkIsActive: Bool
    @State private var displayConfirmInsertSensor: Bool = false
    @State private var presentInsertVideo = false
    
    var body: some View {
        GuidePage(content: {
            VStack(alignment: .leading, spacing: 10) {
                ZStack {
                    Image(frameworkImage: "insert-sensor-now-video")
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: ContentMode.fit)
                        .frame(height: 150)
                    NavigationLink(destination: PlayerViewController(urlString: CGMHelpText.startSensor, linkIsActive: self.$presentInsertVideo),
                                   isActive: self.$presentInsertVideo)
                    {
                        EmptyView()
                    }
                    .isDetailLink(false)
                }
                
                Text(LocalizedString("Insert and Attach Video", comment: "Title for video"))
                    .bold()
                
                InstructionList(instructions: [
                    LocalizedString("Insert your sensor.", comment: "Step 1 for inserting sensor"),
                    LocalizedString("Attach your transmitter.", comment: "Step 2 for inserting sensor")
                ])
            }
            .padding()
        }) {
            Button(action: {
                self.displayConfirmInsertSensor = true
            }) {
                Text(LocalizedString("Next", comment: "Next button title"))
                    .actionButtonStyle()
            }
            .padding()
            .alert(isPresented: self.$displayConfirmInsertSensor) {
                Alert(title: Text(LocalizedString("Insert Sensor Now", comment: "Title of insert sensor confirmation")),
                      message: Text(LocalizedString("You must insert your sensor and attach your transmitter before continuing with setup. Have you inserted your sensor and attached your transmitter?", comment: "Message for insert sensor confirmation")),
                      primaryButton: .default(Text(LocalizedString("Yes", comment: "Yes button title")), action: {
                        self.viewModel.workflowCompletionHandler?()
                        self.rootLinkIsActive = false
                      }),
                      secondaryButton: .cancel(Text(LocalizedString("No", comment: "No button title")))
                )
            }
        }
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(LocalizedString("Insert Sensor Now", comment: "Navigation view title"))
    }
}

struct SensorInsertView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = ReplacementWorkflowViewModel(replaceSensorCompletionHandler: { _ in })
        return Group {
            NavigationView {
                SensorInsertView(viewModel: viewModel, rootLinkIsActive: .constant(true))
            }
            .colorScheme(.light)
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("SE light")
            NavigationView {
                SensorInsertView(viewModel: viewModel, rootLinkIsActive: .constant(true))
            }
            .colorScheme(.dark)
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("XS Max dark")
        }
    }
}
