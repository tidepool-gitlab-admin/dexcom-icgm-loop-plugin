//
//  SensorCodeEntryOptionsView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-28.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI

struct SensorCodeEntryOptionsView: View, HorizontalSizeClassOverride {
    var viewModel: ReplacementWorkflowViewModel
    @Binding var rootLinkIsActive: Bool
    @State private var takePhotoSelected: Bool = false
    @State private var enterManuallySelected: Bool = false
    @State private var entryOptionsState: EntryOptionsState = .normal
    
    var body: some View {
        GuidePage(content: {
            VStack(alignment: .leading, spacing: 10) {
                Image(frameworkImage: "enter-sensor-code")
                    .resizable()
                    .aspectRatio(contentMode: ContentMode.fit)
                    .frame(height: 150)
                
                if self.entryOptionsState == .normal {
                    Text(LocalizedString("Only enter the sensor code from the sensor you insert.", comment: "Warning about what sensor code to enter"))
                } else {
                    Text(LocalizedString("Code entered is not correct.", comment: "Message for incorrect sensor code"))
                    Text(LocalizedString("Take photo of sensor code on sensor applicator again, or enter code manually.", comment: "Instruction to re-enter the sensor code because it was incorrect"))
                }
            }
            .padding()
        }) {
            VStack(alignment: .leading, spacing: 15) {
                GuideNavigationButton(navigationLinkIsActive: self.$takePhotoSelected,
                                      label: LocalizedString("Take Photo", comment: "Button title to select the take photo option"))
                {
                    SensorCodeEntryPhotoView(viewModel: self.viewModel,
                                             entryOptionsState: self.$entryOptionsState,
                                             returnLinkIsActive: self.$takePhotoSelected,
                                             rootLinkIsActive: self.$rootLinkIsActive)
                }
                
                GuideNavigationButton(navigationLinkIsActive: self.$enterManuallySelected,
                                      label: LocalizedString("Enter Manually", comment: "Button title to select the enter manually option"),
                                      buttonStyle: .secondary)
                {
                    SensorCodeEntryManualView(viewModel: self.viewModel,
                                              entryOptionsState: self.$entryOptionsState,
                                              returnLinkIsActive: self.$enterManuallySelected,
                                              rootLinkIsActive: self.$rootLinkIsActive)
                }
            }
            .padding()
        }
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(entryOptionsState == .normal ? LocalizedString("Sensor Code", comment: "Navigation view title") : LocalizedString("Incorrect Code", comment: "Navigation view title"))
    }
}

struct SensorCodeEntryOptionsView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = ReplacementWorkflowViewModel(replaceSensorCompletionHandler: { _ in })
        return Group {
            NavigationView {
                SensorCodeEntryOptionsView(viewModel: viewModel, rootLinkIsActive: .constant(true))
            }
            .colorScheme(.light)
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("SE light")
            NavigationView {
                SensorCodeEntryOptionsView(viewModel: viewModel, rootLinkIsActive: .constant(true))
            }
            .colorScheme(.dark)
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("XS Max dark")
        }
    }
}
