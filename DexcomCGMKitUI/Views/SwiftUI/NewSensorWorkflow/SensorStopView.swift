//
//  SensorStopView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-27.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI

struct SensorStopView: View, HorizontalSizeClassOverride {
    var viewModel: ReplacementWorkflowViewModel
    @Binding var rootLinkIsActive: Bool
    @State private var stopSensorConfirmed: Bool = false
    
    var body: some View {
        GuidePage(content: {
            VStack(alignment: .leading, spacing: 10) {
                Text(LocalizedString("Are you sure you want to end your sensor session early?", comment: "Warning about stopping the sensor before the session is completed."))
                Text(LocalizedString("Once it is stopped, your sensor cannot be restarted.", comment: "Warning about stopping the sensor before the session is completed."))
                    .bold()
                Text(LocalizedString("You will not recieve alerts or sensor glucose readings until you replace your sensor.", comment: "Warning about stopping the sensor before the session is completed."))
            }
        }) {
            GuideNavigationButton(navigationLinkIsActive: self.$stopSensorConfirmed,
                                  label: LocalizedString("End Sensor Session", comment: "Button title to end sensor session"),
                                  buttonPressedAction: { self.viewModel.stopSessionHandler?() },
                                  buttonStyle: .destructive)
            {
                SensorCodeLocationView(viewModel: self.viewModel,
                                       rootLinkIsActive: self.$rootLinkIsActive)
            }
            .padding()
        }
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(LocalizedString("Stop Sensor", comment: "Navigation view title"))
    }
}

struct SensorStopView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = ReplacementWorkflowViewModel(replaceSensorCompletionHandler: { _ in })
        return Group {
            NavigationView {
                SensorStopView(viewModel: viewModel, rootLinkIsActive: .constant(true))
            }
            .colorScheme(.light)
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("SE light")
            NavigationView {
                SensorStopView(viewModel: viewModel, rootLinkIsActive: .constant(true))
            }
            .colorScheme(.dark)
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("XS Max dark")
        }
    }
}
