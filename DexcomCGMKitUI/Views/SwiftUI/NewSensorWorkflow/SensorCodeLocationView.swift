//
//  SensorCodeLocationView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-27.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI

struct SensorCodeLocationView: View, HorizontalSizeClassOverride {
    var viewModel: ReplacementWorkflowViewModel
    @Binding var rootLinkIsActive: Bool
    @State private var noCodeSelected: Bool = false
    @State private var enterCodeSelected: Bool = false
    
    var body: some View {
        GuidePage(content: {
            VStack(alignment: .leading, spacing: 10) {
                Image(frameworkImage: "sensor-code")
                    .resizable()
                    .aspectRatio(contentMode: ContentMode.fit)
                    .frame(height: 150)
                
                Text(LocalizedString("Sensor code is on sensor applicator.", comment: "Description of where to find the sensor code."))
            }
            .padding()
        }) {
            VStack(alignment: .leading, spacing: 15) {
                GuideNavigationButton(navigationLinkIsActive: self.$enterCodeSelected,
                                      label: LocalizedString("Enter Code", comment: "Button title for navigating to the enter code view"))
                {
                    SensorCodeEntryOptionsView(viewModel: self.viewModel,
                                               rootLinkIsActive: self.$rootLinkIsActive)
                }
                
                GuideNavigationButton(navigationLinkIsActive: self.$noCodeSelected,
                                      label: LocalizedString("No Code", comment:"Button title for navigating to the no code view"),
                                      buttonStyle: .secondary)
                {
                    SensorNoCodeView(viewModel: self.viewModel,
                                     rootLinkIsActive: self.$rootLinkIsActive)
                }
            }
            .padding()
        }
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(LocalizedString("Sensor Code", comment: "Navigation view title"))
    }
}

struct SensorCodeLocationView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = ReplacementWorkflowViewModel(replaceSensorCompletionHandler: { _ in })
        return Group {
            NavigationView {
                SensorCodeLocationView(viewModel: viewModel, rootLinkIsActive: .constant(true))
            }
            .colorScheme(.light)
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("SE light")
            NavigationView {
                SensorCodeLocationView(viewModel: viewModel, rootLinkIsActive: .constant(true))
            }
            .colorScheme(.dark)
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("XS Max dark")
        }
    }
}
