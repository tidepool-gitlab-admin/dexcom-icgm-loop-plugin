//
//  SensorNoCodeView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-06.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI

struct SensorNoCodeView: View, HorizontalSizeClassOverride {
    var viewModel: ReplacementWorkflowViewModel
    @Binding var rootLinkIsActive: Bool
    @State private var confirmSelected: Bool = false

    var body: some View {
        GuidePage(content: {
            VStack(alignment: .leading, spacing: 10) {
                Image(frameworkImage: "no-sensor-code")
                    .resizable()
                    .aspectRatio(contentMode: ContentMode.fit)
                    .frame(height: 150)
                
                Text(LocalizedString("If you continue without a sensor code, daily fingerstick calibrations are required.", comment: "Warning about not using a sensor code to start a session"))
            }
            .padding()
        }) {
            GuideNavigationButton(navigationLinkIsActive: self.$confirmSelected,
                                  label: LocalizedString("I Understand", comment: "Button title for agreeing with the presented warning"),
                                  buttonPressedAction:
                {
                    if self.viewModel.replacementWorkflow == .replaceSensor {
                        self.viewModel.workflowCompletionHandler = { self.viewModel.replaceSensorCompletionHandler(nil) }
                    } else {
                        self.viewModel.replaceSensorCompletionHandler(nil)
                    }},
                                  buttonStyle: .primary)
            {
                if self.viewModel.replacementWorkflow == .replaceSensor {
                    SensorInsertView(viewModel: self.viewModel,
                                     rootLinkIsActive: self.$rootLinkIsActive)
                } else {
                    TransmitterIDEntryOptionsView(viewModel: self.viewModel,
                                                  rootLinkIsActive: self.$rootLinkIsActive)
                }
            }
            .padding()
        }
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(LocalizedString("No Sensor Code", comment: "Navigation view title"))
    }
}

struct SensorNoCodeView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = ReplacementWorkflowViewModel(replaceSensorCompletionHandler: { _ in })
        return Group {
            NavigationView {
                SensorNoCodeView(viewModel: viewModel, rootLinkIsActive: .constant(true))
            }
            .colorScheme(.light)
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("SE light")
            NavigationView {
                SensorNoCodeView(viewModel: viewModel, rootLinkIsActive: .constant(true))
            }
            .colorScheme(.dark)
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("XS Max dark")
        }
    }
}
