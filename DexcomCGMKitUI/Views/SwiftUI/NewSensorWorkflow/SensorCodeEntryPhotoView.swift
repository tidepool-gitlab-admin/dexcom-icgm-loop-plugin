//
//  SensorCodeEntryPhotoView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-28.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI
import DexcomCGMKit
import TransmitterCore

struct SensorCodeEntryPhotoView: View {
    var viewModel: ReplacementWorkflowViewModel
    @Binding var entryOptionsState: EntryOptionsState
    @Binding var returnLinkIsActive: Bool
    @Binding var rootLinkIsActive: Bool
    @State private var workflowCompleted: Bool = false
    @State private var startTransmitterWorkflow: Bool = false
    @State private var noCodeSelected: Bool = false
    
    var body: some View {
        VStack(spacing: 0) {
            ZStack(alignment: .center) {
                ScannerView(deviceType: .sensor,
                            startScanning: (!workflowCompleted && !startTransmitterWorkflow) && returnLinkIsActive,
                            completionHandler:
                    { sensor2DBarcode in
                        if self.viewModel.validSensorCode(sensor2DBarcode) {
                            switch self.viewModel.replacementWorkflow {
                            case .replaceSensor:
                                self.workflowCompleted = true
                            case .replaceTransmitterAndSensor:
                                self.startTransmitterWorkflow = true
                            }
                        } else {
                            self.entryOptionsState = .entryError
                            self.returnLinkIsActive = false
                        }
                })
                
                Text("[   ]")
                    .font(.system(size: 80))
                    .foregroundColor(.purple)
                
                NavigationLink(destination: SensorInsertView(viewModel: self.viewModel,
                                                             rootLinkIsActive: self.$rootLinkIsActive),
                               isActive: self.$workflowCompleted)
                {
                    EmptyView()
                }
                .isDetailLink(false)
                .disabled(true)
                
                NavigationLink(destination: TransmitterIDEntryOptionsView(viewModel: self.viewModel,
                                                                          rootLinkIsActive: self.$rootLinkIsActive),
                               isActive: self.$startTransmitterWorkflow)
                {
                    EmptyView()
                }
                .isDetailLink(false)
                .disabled(true)
            }
            .aspectRatio(4/3, contentMode: ContentMode.fill)
            .padding(.bottom)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            
            ScrollView {
                VStack (alignment: .leading, spacing: 10) {
                    Text(LocalizedString("Find sensor code on sensor applicator.", comment: "Description of where to find the sensor code"))
                    
                    HStack(alignment: .top, spacing: 10) {
                        Image(frameworkImage: "2d-barcode-thumb")
                            .resizable()
                            .aspectRatio(contentMode: ContentMode.fit)
                            .frame(height: 50)
                        
                        InstructionList(instructions:[
                            LocalizedString("Position sensor code inside purple guides.", comment: "Description on how to take a photo of the sensor code"),
                            LocalizedString("Tap to focus.", comment: "Description on how to focus on the sensor code"),
                            LocalizedString("Photo is taken automatically.", comment: "Description of how the photo of the sensort code is taken")
                        ])
                    }
                }
                .padding()
                .background(Color(UIColor.tertiarySystemBackground).cornerRadius(6))
            }
                
            VStack {
                GuideNavigationButton(navigationLinkIsActive: self.$noCodeSelected,
                                      label: LocalizedString("No Code", comment: "Button title to select no code"),
                                      buttonStyle: .secondary)
                {
                    SensorNoCodeView(viewModel: self.viewModel,
                                     rootLinkIsActive: self.$rootLinkIsActive)
                }
            }
            .padding()
            .background(Color(UIColor.systemBackground).shadow(radius: 5))
        }
        .navigationBarTitle(Text(LocalizedString("Take Photo", comment: "Navigation view title")), displayMode: .inline)
    }
}

struct SensorCodeEntryPhotoView_Previews: PreviewProvider {
    static var previews: some View {
        return Group {
            NavigationView {
                PreviewWrapper()
            }
            .colorScheme(.light)
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("SE light")
            NavigationView {
                PreviewWrapper()
            }
            .colorScheme(.dark)
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("XS Max dark")
        }
    }
    
    struct PreviewWrapper: View {
        let viewModel = ReplacementWorkflowViewModel(replaceSensorCompletionHandler: { _ in })
        let entryOptionsState = EntryOptionsState.normal
        var body: some View {
            SensorCodeEntryPhotoView(viewModel: viewModel,
                                     entryOptionsState: .constant(entryOptionsState),
                                     returnLinkIsActive: .constant(true),
                                     rootLinkIsActive: .constant(true))
        }
    }
}
