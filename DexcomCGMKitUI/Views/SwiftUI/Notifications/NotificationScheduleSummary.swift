//
//  NotificationScheduleSummary.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-24.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI
import DexcomCGMKit

struct NotificationScheduleSummary: View {
    var alertSchedule: DexcomAlertSchedule
    
    private var dateFormatter = DateFormatter()

    init(alertSchedule: DexcomAlertSchedule = DexcomAlertSchedule.defaultSchedule()) {
        self.alertSchedule = alertSchedule
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        dateFormatter.timeZone = TimeZone.currentFixed
    }
    
    var body: some View {
        return
            VStack(alignment: .leading, spacing: 10) {
                LabeledValueView(label: LocalizedString("Enabled", comment: "Notification schedule enabled cell title"),
                                 value: alertSchedule.enabled ? LocalizedString("On", comment: "The item is enabled") : LocalizedString("Off", comment: "The item is disabled"))
                
                Divider()
                
                LabeledValueView(label: LocalizedString("Start Time", comment: "Start Time cell title"),
                                 value: dateFormatter.string(from: alertSchedule.weeklySchedule.startTime()))
                
                Divider()
                
                LabeledValueView(label: LocalizedString("End Time", comment: "End Time cell title"),
                                 value: dateFormatter.string(from: alertSchedule.weeklySchedule.endTime()))
                
                Divider()
                
                LabeledValueView(label: LocalizedString("Days", comment: "Days cell title"),
                                 value: alertSchedule.weeklySchedule.days.localizedValue)
                
                Divider()
                
                LabeledValueView(label: LocalizedString("Allow Critical Alerts", comment: "Title of alert schedule always sound toggle"),
                                 value: alertSchedule.alwaysSoundEnabled ? LocalizedString("On", comment: "The item is enabled") : LocalizedString("Off", comment: "The item is disabled"))
        }
    }
}

struct NotificationScheduleSummary_Previews: PreviewProvider {
    static var previews: some View {
        NotificationScheduleSummary()
    }
}
