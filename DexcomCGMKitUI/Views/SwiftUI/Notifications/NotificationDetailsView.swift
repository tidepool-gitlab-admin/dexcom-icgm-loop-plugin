//
//  NotificationDetailsView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-18.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import HealthKit
import LoopKit
import LoopKitUI
import DexcomCGMKit

struct NotificationDetailsView<Model>: View, HorizontalSizeClassOverride where Model: NotificationDetailsViewModelProtocol {
    @ObservedObject var viewModel: Model
    @State private var editingThreshold: Bool = false
    @State private var editingRepeat: Bool = false
        
    var body: some View {
        List {
            if viewModel.configuration.type.canDisable {
                Section {
                    Toggle(isOn: $viewModel.configuration.enabled) {
                        Text(viewModel.configuration.type.localizedViewTitle)
                    }
                }
            }
            
            if viewModel.displaysDetails {
                Section {
                    if viewModel.configuration.hasThresholdValue {
                        Button(action: {
                            self.editingThreshold.toggle()
                            if self.editingThreshold {
                                self.editingRepeat = false
                            }
                        }) {
                            LabeledValueView(label: viewModel.configuration.type.localizedThresholdTitle ?? "",
                                             value: viewModel.formattedThreshold(viewModel.threshold),
                                             highlightValue: editingThreshold)
                        }
                        .disabled(self.viewModel.isUrgentLow)
                    }
                    if editingThreshold {
                        Picker("", selection: $viewModel.threshold) {
                            ForEach(viewModel.thresholdValues(), id: \.self) {
                                Text(self.viewModel.formattedThreshold($0)!)
                            }
                        }
                        .pickerStyle(WheelPickerStyle())
                    }
                    if viewModel.configuration.type.canDelayRepeat {
                        Button(action: {
                            self.editingRepeat.toggle()
                            if self.editingRepeat {
                                self.editingThreshold = false
                            }
                        }) {
                            LabeledValueView(label: LocalizedString("Repeat", comment: "Time Picker label"),
                                             value: viewModel.formattedSelectedRepeat(viewModel.repeatValue),
                                             highlightValue: editingRepeat)
                        }
                        .disabled(self.viewModel.isUrgentLow)
                    }
                    if editingRepeat {
                        Picker("", selection: $viewModel.repeatValue) {
                            ForEach(viewModel.repeatValues(), id: \.self) {
                                Text(self.viewModel.formattedRepeat($0))
                            }
                        }
                        .pickerStyle(WheelPickerStyle())
                    }
                    if viewModel.isUrgentLow {
                        DescriptiveText(label: String(format: LocalizedString("The %@ notification level and repeat setting cannot be changed or turned off. Only the sound setting can be changed.", comment: "Footer in urgent low alert notify me below and repeat section (1: urgent low localized title)"), viewModel.configuration.type.localizedViewTitle))
                    }
                }
            }
            Section {
                NavigationLink(destination:
                    SoundView(viewModel:
                        SoundViewModel(selectedSound: viewModel.configuration.sound,
                                       updateHandler: viewModel.soundUpdateHandler))) {
                                        LabeledValueView(label: LocalizedString("Sound", comment: "Sound button title"), value: String(describing: viewModel.configuration.sound))
                }
                .isDetailLink(false)
                DescriptiveText(label: viewModel.configurationDescription)
            }
        }
        .listStyle(GroupedListStyle())
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(Text(viewModel.configuration.type.localizedTitle))
    }
}

struct NotificationDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModelUrgentLow = NotificationDetailsViewModel(configuration: DexcomAlertConfiguration(type: .glucoseUrgentLow,
                                                                                                      enabled: true,
                                                                                                      alwaysSoundEnabled: true,
                                                                                                      sound: .defaultSound),
                                                              glucoseUnit: HKUnit.milligramsPerDeciliter, updateHandler: { _ in })
        let viewModelOther = NotificationDetailsViewModel(configuration: DexcomAlertConfiguration(type: .glucoseLow,
                                                                                                  enabled: false,
                                                                                                  alwaysSoundEnabled: true,
                                                                                                  sound: .intense),
                                                          glucoseUnit: HKUnit.milligramsPerDeciliter, updateHandler: { _ in })
        return Group {
            NotificationDetailsView(viewModel: viewModelUrgentLow)
                .colorScheme(.light)
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("SE light")
            NotificationDetailsView(viewModel: viewModelOther)
                .colorScheme(.dark)
                .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
                .previewDisplayName("XS Max dark")
        }
    }
}
