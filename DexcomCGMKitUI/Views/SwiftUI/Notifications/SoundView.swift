//
//  SoundView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-19.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI

struct SoundView<Model>: View, HorizontalSizeClassOverride where Model: SoundViewModelProtocol {
    @ObservedObject var viewModel: Model
    
    var body: some View {
        List(viewModel.sounds, id: \.self) { sound in
            SelectableLabel(label: sound, selectedLabel: self.$viewModel.selectedSound)
        }
        .listStyle(GroupedListStyle())
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(LocalizedString("Sound", comment: "Navigation view title"))
    }
}

struct SoundView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = SoundViewModel(selectedSound: .defaultSound, updateHandler: { _ in })
        return Group {
            SoundView(viewModel: viewModel)
                .colorScheme(.light)
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("SE light")
            SoundView(viewModel: viewModel)
                .colorScheme(.dark)
                .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
                .previewDisplayName("XS Max dark")
        }
    }
}
