//
//  EditAlertScheduleView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-16.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import HealthKit
import LoopKitUI
import DexcomCGMKit

struct EditAlertScheduleView: View, HorizontalSizeClassOverride {
    @Environment(\.presentationMode) private var presentationMode
    @ObservedObject var viewModel: AlertScheduleDetailsViewModel
    
    var saveButton: some View {
        Button(LocalizedString("Save", comment: "Save button title"), action: {
            self.viewModel.saveAlertSchedule()
            self.done()
        })
            .disabled(viewModel.scheduleName.isEmpty ||
                viewModel.alertSchedule.weeklySchedule.days == .none)
    }
    
    var cancelButton: some View {
        Button(LocalizedString("Cancel", comment: "Cancel button title"), action: {
            self.viewModel.restoreAlertSchedule()
            self.done()
        })
    }
    
    func done() {
        self.presentationMode.wrappedValue.dismiss()
    }
    
    var body: some View {
        List {
            Section {
                LabeledTextField(label:LocalizedString("Name", comment: "Text Field label"),
                                 placeholder: LocalizedString("Schedule Name", comment: "Placeholder text when to schedule name is entered"),
                                 value: $viewModel.scheduleName)
                Toggle(isOn: $viewModel.alertSchedule.enabled) {
                    Text(LocalizedString("Enabled", comment: "Toogle for notification schedule on/off"))
                }
            }
            
            Section {
                Toggle(isOn: $viewModel.alertSchedule.alwaysSoundEnabled) {
                    Text(LocalizedString("Allow Critical Alerts", comment: "Toogle for notifications to use critical alert"))
                }
                DescriptiveText(label: LocalizedString("Critical alerts appear on the lock screen and play a sound if Do Not Distsurb is on or the iPhone is muted.", comment: "Description of the allow critical alerts toggle"))
            }
            
            Section {
                DatePicker(selection: $viewModel.scheduleStartTime, displayedComponents: .hourAndMinute) {
                    Text(LocalizedString("Start Time", comment: "Field label"))
                }
                .transition(.inAndOut)
                
                DatePicker(selection: $viewModel.scheduleEndTime, displayedComponents: .hourAndMinute) {
                    Text(LocalizedString("End Time", comment: "Field label"))
                }
                .transition(.inAndOut)
                
                NavigationLink(destination: WeekdaySelectionView(viewModel: WeekdaySelectionViewModel(selectedDays: viewModel.alertSchedule.weeklySchedule.days, updateHandler: viewModel.daysUpdatedHandler)))
                {
                    LabeledValueView(label: LocalizedString("Days", comment: "Days button title"),
                                     value: viewModel.alertSchedule.weeklySchedule.days.localizedValue)
                }
                .isDetailLink(false)
            }
            .environment(\.timeZone, TimeZone.currentFixed)
            
            Section(header: SectionHeader(label: LocalizedString("Dexcom G6 Notifications", comment: "Section title for Dexcom G6 notifications"))) {
                NotificationListView(viewModel: NotificationListViewModel(configurations: viewModel.configurations,
                                                                          glucoseUnit: viewModel.glucoseUnit,
                                                                          configurationUpdateHandler: viewModel.configurationUpdateHandler))
            }

        }
        .navigationBarItems(leading: cancelButton, trailing: saveButton)
        .listStyle(GroupedListStyle())
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(viewModel.hasAlertSchedule ? Text(LocalizedString("Edit Schedule", comment: "Navigation view title")) : Text(LocalizedString("Create New Schedule", comment: "Navigation view title")))
    }
}

extension AnyTransition {
    static var inAndOut: AnyTransition {
        let insertion = AnyTransition.move(edge: .top)
            .combined(with: .opacity)
        let removal = AnyTransition.move(edge: .bottom)
            .combined(with: .opacity)
        return .asymmetric(insertion: insertion, removal: removal)
    }
}

struct EditAlertScheduleView_Previews: PreviewProvider {
    static var previews: some View {
        let configurations = DexcomUserMutableAlerts.defaultAlerts().configurations
        let weeklySchedule = WeeklySchedule(days: [.monday, .tuesday, .wednesday, .thursday, .friday],
                                            startTimeComponents: DateComponents(hour:20, minute:30),
                                            endTimeComponents: DateComponents(hour:7, minute:00))
        let alertSchedule = DexcomAlertSchedule(name: "Test",
                                                enabled: true,
                                                weeklySchedule: weeklySchedule)
        let alertConfigurationManager = DexcomAlertConfigurationManager(userMutableAlertConfigurations: configurations,
                                                                        alertSchedule: alertSchedule)
        let glucoseUnit: HKUnit = HKUnit.milligramsPerDeciliter
        let viewModel = AlertScheduleDetailsViewModel(alertConfigurationManager: alertConfigurationManager, glucoseUnit: glucoseUnit)
        return EditAlertScheduleView(viewModel: viewModel)
    }
}
