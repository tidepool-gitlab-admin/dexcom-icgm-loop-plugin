//
//  NotificationsView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-14.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import HealthKit
import LoopKitUI
import DexcomCGMKit

struct NotificationsView: View, HorizontalSizeClassOverride {
    @ObservedObject var viewModel: NotificationsViewModel
    @State private var displayResetWarning: Bool = false

    var body: some View {
        List {
            Section {
                Toggle(isOn: $viewModel.alertConfigurationManager.alwaysSoundEnabled) {
                    Text(LocalizedString("Allow Critical Alerts", comment: "Toogle for notifications to use critical alert"))
                }
                DescriptiveText(label: LocalizedString("Critical alerts appear on the lock screen and play a sound if Do Not Distsurb is on or the iPhone is muted.", comment: "Description of the allow critical alerts toggle"))
            }
            
            Section {
                NavigationLink(destination: AlertScheduleDetailsView(viewModel:
                    AlertScheduleDetailsViewModel(alertConfigurationManager: viewModel.alertConfigurationManager,
                                                  glucoseUnit: viewModel.glucoseUnit)))
                {
                    LabeledValueView(label: LocalizedString("Notifications Schedule", comment: "Label for notification schedule"),
                                     value: self.viewModel.alertConfigurationManager.hasAlertSchedule ? self.viewModel.alertConfigurationManager.alertSchedule?.name : LocalizedString("No schedule", comment: "Label if no notification schedule exists."))
                        .onAppear(perform: { self.viewModel.updateScheduleName() })
                }
                .isDetailLink(false)
            }

            Section(header: SectionHeader(label: LocalizedString("Dexcom G6 Notifications", comment: "Section title for Dexcom G6 notifications")))
            {
                NotificationListView(viewModel: NotificationListViewModel(configurations: viewModel.configurations,
                                                                          glucoseUnit: viewModel.glucoseUnit,
                                                                          configurationUpdateHandler: viewModel.configurationUpdateHandler))
            }
            
            Section {
                Button(action: {
                    self.displayResetWarning = true
                }) {
                    Text(LocalizedString("Reset Notification Settings", comment: "Reset notification settings button title"))
                }
                .foregroundColor(.destructive)
                .actionSheet(isPresented: $displayResetWarning) {
                    ActionSheet(title: Text(""),
                                message: Text(LocalizedString("Do you want to reset notification settings? This will reset all notification settings to factory defaults.", comment: "Warning for resetting notification settings")),
                                buttons: [.cancel(),
                                          .destructive(Text(LocalizedString("Reset Notification Settings", comment: "Confirm notification reset button title")), action: {
                                            self.viewModel.resetAlerts()
                                          })]
                    )
                }
            }
        }
        .listStyle(GroupedListStyle())
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(Text(LocalizedString("Notification Settings", comment: "Navigation view title")))
    }
}

struct NotificationsView_Previews: PreviewProvider {
    static var previews: some View {
        let configurations = DexcomUserMutableAlerts.defaultAlerts().configurations
        let weeklySchedule = WeeklySchedule(days: Days.everyday,
                                            startTimeComponents: DateComponents(hour:20, minute:30),
                                            endTimeComponents: DateComponents(hour:7, minute:00))
        let alertSchedule = DexcomAlertSchedule(name: "Test",
                                                enabled: true,
                                                weeklySchedule: weeklySchedule)
        let alertConfigurationManager = DexcomAlertConfigurationManager(userMutableAlertConfigurations: configurations,
                                                                        alertSchedule: alertSchedule)
        let viewModel = NotificationsViewModel(alertConfigurationManager: alertConfigurationManager,
                                               glucoseUnit: HKUnit.milligramsPerDeciliter)
        return Group {
            NotificationsView(viewModel: viewModel)
                .colorScheme(.light)
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("SE light")
            NotificationsView(viewModel: viewModel)
                .colorScheme(.dark)
                .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
                .previewDisplayName("XS Max dark")
        }
    }
}
