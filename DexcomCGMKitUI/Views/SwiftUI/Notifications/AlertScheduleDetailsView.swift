//
//  AlertScheduleDetailsView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-21.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import HealthKit
import LoopKitUI
import DexcomCGMKit

struct AlertScheduleDetailsView: View, HorizontalSizeClassOverride {
    @ObservedObject var viewModel: AlertScheduleDetailsViewModel
    @State private var editMode = EditMode.inactive
    
    private func onDelete(offsets: IndexSet) {
        viewModel.deleteAlertSchedule()
    }
    
    var body: some View {
        return Group {
            List {
                if viewModel.hasAlertSchedule {
                    Section(header: SectionHeader(label: viewModel.scheduleName)) {
                        ForEach(1..<2, id:\.self) { index in
                            NavigationLink(destination: EditAlertScheduleView(viewModel: self.viewModel))
                            {
                                NotificationScheduleSummary(alertSchedule: self.viewModel.alertSchedule)
                            }
                            .isDetailLink(false)
                            .disabled(self.editMode == EditMode.active)
                        }
                        .onDelete(perform: onDelete)
                    }
                } else {
                    Section {
                        NavigationLink(destination: EditAlertScheduleView(viewModel: viewModel))
                        {
                            Text(LocalizedString("Create new schedule", comment: "Create new notification schedule button title"))
                                .foregroundColor(.accentColor)
                        }
                        .isDetailLink(false)
                    }
                }
            }
            .animation(.easeInOut)
        }
        .listStyle(GroupedListStyle())
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarItems(trailing: EditButton().disabled(!viewModel.hasAlertSchedule))
        .environment(\.editMode, $editMode)
        .navigationBarTitle(Text(LocalizedString("Notification Schedule", comment: "Navigation Schedule title")))
    }
}

struct AlertScheduleView_Previews: PreviewProvider {
    static var previews: some View {
        let configurations = DexcomUserMutableAlerts.defaultAlerts().configurations
        let weeklySchedule = WeeklySchedule(days: [.monday, .tuesday, .wednesday, .thursday, .friday],
                                            startTimeComponents: DateComponents(hour:20, minute:30),
                                            endTimeComponents: DateComponents(hour:7, minute:00))
        let alertSchedule = DexcomAlertSchedule(name: "Test",
                                                enabled: true,
                                                weeklySchedule: weeklySchedule)
        let alertConfigurationManager = DexcomAlertConfigurationManager(userMutableAlertConfigurations: configurations,
                                                                        alertSchedule: alertSchedule)
        let glucoseUnit: HKUnit = HKUnit.milligramsPerDeciliter
        let viewModel = AlertScheduleDetailsViewModel(alertConfigurationManager: alertConfigurationManager, glucoseUnit: glucoseUnit)
        return Group {
            AlertScheduleDetailsView(viewModel: viewModel)
                .listStyle(GroupedListStyle())
                .colorScheme(.light)
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("SE light")
            
            AlertScheduleDetailsView(viewModel: viewModel)
                .listStyle(GroupedListStyle())
                .environment(\.horizontalSizeClass, .regular)
                .colorScheme(.dark)
                .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
                .previewDisplayName("XS Max dark")
        }
    }
}
