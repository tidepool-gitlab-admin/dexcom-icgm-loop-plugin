//
//  NotificationListView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-02.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import HealthKit
import LoopKitUI
import DexcomCGMKit
struct NotificationListView<Model>: View where Model: NotificationListViewModelProtocol {
    let viewModel: Model
    
    var body: some View {
        ForEach(viewModel.configurations) { configuration in
            NavigationLink(destination:
                NotificationDetailsView(viewModel:
                    NotificationDetailsViewModel(configuration: configuration,
                                                 glucoseUnit: self.viewModel.glucoseUnit,
                                                 updateHandler: self.viewModel.configurationUpdateHandler))) {
                                                    LabeledValueView(label: configuration.type.localizedTitle, value: self.viewModel.value(forConfiguration: configuration))
            }
            .isDetailLink(false)
        }
    }
}

struct NotificationListView_Previews: PreviewProvider {
    static var previews: some View {
        let mutableAlertConfigs = DexcomUserMutableAlerts.defaultConfigurations(alwaysSoundEnabled: true)
        let configurations: [DexcomAlertConfiguration] = [mutableAlertConfigs[.glucoseUrgentLow]!, mutableAlertConfigs[.glucoseLow]!, mutableAlertConfigs[.glucoseHigh]!, mutableAlertConfigs[.glucoseRateRise]!, mutableAlertConfigs[.glucoseRateFall]!, mutableAlertConfigs[.signalLoss]!, mutableAlertConfigs[.noReadings]!]
        let viewModel = NotificationListViewModel(configurations: configurations,
                                                  glucoseUnit: HKUnit.milligramsPerDeciliter,
                                                  configurationUpdateHandler: { _ in })
        return Group {
            List {
                Section {
                    NotificationListView(viewModel: viewModel)
                }
            }
            .listStyle(GroupedListStyle())
            .colorScheme(.light)
            .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
            .previewDisplayName("SE light")
            
            List {
                Section {
                    NotificationListView(viewModel: viewModel)
                }
            }
            .listStyle(GroupedListStyle())
            .environment(\.horizontalSizeClass, .regular)
            .colorScheme(.dark)
            .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
            .previewDisplayName("XS Max dark")
        }
    }
}
