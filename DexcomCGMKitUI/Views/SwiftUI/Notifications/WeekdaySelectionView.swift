//
//  WeekdaySelectionView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-24.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI
import DexcomCGMKit

struct WeekdaySelectionView<Model>: View, HorizontalSizeClassOverride where Model: WeekdaySelectionViewModelProtocol {
    @ObservedObject var viewModel: Model
    
    var body: some View {
        MultipleSelectionList<Days>(items: viewModel.selectableDays,
                                    selectedItems: $viewModel.selectedDays,
                                    itemToDisplayableString: viewModel.daysAsString)
            .environment(\.horizontalSizeClass, horizontalOverride)
            .navigationBarTitle(LocalizedString("Schedule Days", comment: "Navigation view title"))
    }
}

struct WeekdaySelectionView_Previews: PreviewProvider {
    static var previews: some View {
        return Group {
            PreviewWrapper()
                .colorScheme(.light)
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("SE light")
            PreviewWrapper()
                .colorScheme(.dark)
                .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
                .previewDisplayName("XS Max dark")
        }
    }
    
    struct PreviewWrapper: View {
        let viewModel = WeekdaySelectionViewModel(selectedDays: [.monday, .wednesday], updateHandler: { _ in })
        
        var body: some View {
            WeekdaySelectionView(viewModel: viewModel)
        }
    }
}
