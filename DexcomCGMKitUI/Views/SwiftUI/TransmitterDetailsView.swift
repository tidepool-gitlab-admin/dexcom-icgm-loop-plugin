//
//  TransmitterDetailsView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-14.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import LoopKitUI
import DexcomCGMKit

struct TransmitterDetailsView<Model>: View, HorizontalSizeClassOverride where Model: TransmitterDetailsViewModelProtocol {
    @ObservedObject var viewModel: Model
    @Binding var rootLinkIsActive: Bool
    @State private var displayStopSearchingWarning: Bool = false
    @State private var stopSearchingConfirmed: Bool = false
    
    private var newSensorWorkflowView: some View {
        return SensorCodeLocationView(viewModel:
            ReplacementWorkflowViewModel(replacementWorkflow: .replaceTransmitterAndSensor,
                                         replaceSensorCompletionHandler: viewModel.newSensorCompletionHandler,
                                         replaceTransmitterCompletionHandler: viewModel.newTransmitterCompletionHandler),
                                      rootLinkIsActive: $rootLinkIsActive)
    }
    
    private var stopSensorWorkflowView: some View {
        return SensorStopView(viewModel:
            ReplacementWorkflowViewModel(replacementWorkflow: .replaceTransmitterAndSensor,
                                         replaceSensorCompletionHandler: viewModel.newSensorCompletionHandler,
                                         stopSessionHandler: viewModel.stopSensorCompletionHandler,
                                         replaceTransmitterCompletionHandler: viewModel.newTransmitterCompletionHandler),
                              rootLinkIsActive: $rootLinkIsActive)
    }
    
    var body: some View {
        List {
            Section {
                if viewModel.sessionRunning {
                    NavigationLink(destination: stopSensorWorkflowView) {
                        Text(LocalizedString("Pair New Transmitter", comment: "Pair new transmitter button title"))
                            .foregroundColor(.accentColor)
                    }
                    .isDetailLink(false)
                } else {
                    ZStack(alignment: .leading) {
                         // this navigation link is used to queue off the replace transmitter workflow
                        NavigationLink(destination: newSensorWorkflowView, isActive: $stopSearchingConfirmed) {
                            EmptyView()
                        }
                        .isDetailLink(false)
                        .disabled(true)
                        Button(action: {
                            self.displayStopSearchingWarning = true
                        }) {
                            Text(LocalizedString("Pair New Transmitter", comment: "Pair new transmitter button title"))
                                .foregroundColor(.accentColor)
                        }
                        .actionSheet(isPresented: $displayStopSearchingWarning) {
                            ActionSheet(title: Text(""),
                                        message: Text(LocalizedString("Are you sure you want to stop searching for this transmitter?", comment: "Warning for stop searching for transmitter")),
                                        buttons: [.cancel(),
                                                  .destructive(Text(LocalizedString("Stop Searching", comment: "Stop searching button title")), action: {
                                                    self.viewModel.cgmManager.closeCommunication()
                                                    self.stopSearchingConfirmed = true
                                                  })]
                            )
                        }
                    }
                }
            }
            
            if viewModel.detailsAvailable {
                Section {
                    LabeledValueView(label: LocalizedString("Serial Number", comment: "Field label"), value: viewModel.serialNumber)
                    LabeledDateView(label: LocalizedString("Activated On", comment: "Field label"), date: viewModel.activatedOn, dateFormatter: viewModel.dateFormatter)
                }
                Section {
                    LabeledValueView(label: LocalizedString("Firmware", comment: "Field label"), value: viewModel.firmware)
                    LabeledValueView(label: LocalizedString("Software Number", comment: "Field label"), value: viewModel.softwareNumber)
                }
            } else {
                Section {
                    LabeledValueView(label: LocalizedString("Searching for transmitter", comment: "Field label"), value: viewModel.serialNumber)
                    DescriptiveText(label: LocalizedString("Keep smart device within 20 feet of transmitter. Pairing may take up to 30 minutes.", comment: "Description on how to improve connectivity with transmtiter"))
                }
                
            }
        }
        .listStyle(GroupedListStyle())
        .environment(\.horizontalSizeClass, horizontalOverride)
        .navigationBarTitle(LocalizedString("Transmitter", comment: "Navigation view title"))
    }
}

struct TransmitterDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        let cgmManager = DexcomCGMManager(state: DexcomCGMManagerState(dexcomTransmitterID: DexcomTransmitterID("8YYYY8")!))
        let viewModelDetailsNotAvailable = TransmitterDetailsViewModel(cgmManager: cgmManager, sessionRunning: false)
        let viewModelDetailsAvailable = MockTransmitterDetailsViewModel()
        return Group {
            TransmitterDetailsView(viewModel: viewModelDetailsNotAvailable, rootLinkIsActive: .constant(true))
                .colorScheme(.light)
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("SE light")
            TransmitterDetailsView(viewModel: viewModelDetailsAvailable, rootLinkIsActive: .constant(true))
                .colorScheme(.dark)
                .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
                .previewDisplayName("XS Max dark")
        }
    }
}
