//
//  SettingsView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-12.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import HealthKit
import DexcomCGMKit
import LoopKitUI

struct SettingsView<Model>: View, HorizontalSizeClassOverride where Model: SettingsViewModelProtocol {
    @ObservedObject var viewModel: Model
    @State private var stopSensorLinkIsActive: Bool = false
    @State private var newSensorLinkIsActive: Bool = false
    @State private var transmitterLinkIsActive: Bool = false
    @State private var displayStopSensorWarning: Bool = false
    @State private var displayDeleteWarning: Bool = false
    
    var doneButton: some View {
        Button("Done", action: {
            self.done()
        })
    }
    
    func done() {
        viewModel.completionHandler()
    }
    
    var body: some View {
        NavigationView {
            List {
                Section(header: SectionHeader(label: LocalizedString("Notifications", comment: "Notification section title"))) {
                    NavigationLink(destination:
                        NotificationsView(viewModel:
                            NotificationsViewModel(alertConfigurationManager: viewModel.cgmManager.alertConfigurationManager,
                                                   glucoseUnit: viewModel.glucoseUnit)))
                    {
                        Text(LocalizedString("Notifications", comment: "Notification button title"))
                    }
                    .isDetailLink(false)
                }
                
                if viewModel.sessionRunning {
                    Section(header: SectionHeader(label: LocalizedString("Sensor", comment: "Sensor section title"))) {
                        NavigationLink(destination:
                            CalibrationView(viewModel:
                                CalibrationViewModel(glucoseUnit: viewModel.glucoseUnit,
                                                     completionHandler: viewModel.calibrationCompletionHandler))) 
                        {
                            Text(LocalizedString("Calibrate", comment: "Calibrate button title"))
                                .foregroundColor(.accentColor)
                        }
                        .isDetailLink(false)
                    }
                    
                    Section {
                        LabeledDateView(label: LocalizedString("Last Calibration", comment: "Field label"),
                                        date: viewModel.lastCalibrationDate,
                                        dateFormatter: viewModel.dateFormatter)
                        LabeledDateView(label: LocalizedString("Insertion Time", comment: "Field label"),
                                        date: viewModel.sensorInsertionDate,
                                        dateFormatter: viewModel.dateFormatter)
                        LabeledDateView(label: LocalizedString("Sensor Expiration", comment: "Field label"),
                                        date: viewModel.sensorExpirationDate,
                                        dateFormatter: viewModel.dateFormatter)
                    }
                    
                    Section {
                        NavigationLink(destination:
                            SensorStopView(viewModel:
                                ReplacementWorkflowViewModel(replaceSensorCompletionHandler: viewModel.newSensorCompletionHandler,
                                                             stopSessionHandler: viewModel.stopSensorCompletionHandler),
                                           rootLinkIsActive: $stopSensorLinkIsActive),
                                       isActive: $stopSensorLinkIsActive)
                        {
                            Text("Stop Sensor")
                                .foregroundColor(.destructive)
                        }
                        .isDetailLink(false)
                    }
                } else {
                    Section(header: SectionHeader(label: LocalizedString("Sensor", comment: "Sensor section title"))) {
                        NavigationLink(destination:
                            SensorCodeLocationView(viewModel: ReplacementWorkflowViewModel(replaceSensorCompletionHandler: viewModel.newSensorCompletionHandler),
                                                   rootLinkIsActive: $newSensorLinkIsActive),
                                       isActive: $newSensorLinkIsActive)
                        {
                            Text(LocalizedString("New Sensor", comment: "New Sensor Button title"))
                                .foregroundColor(.accentColor)
                        }
                        .isDetailLink(false)
                    }
                }
                
                Section(header: SectionHeader(label: LocalizedString("Transmitter", comment: "Transmitter section title"))) {
                    NavigationLink(destination:
                        TransmitterDetailsView(viewModel:
                            TransmitterDetailsViewModel(cgmManager: viewModel.cgmManager,
                                                        sessionRunning: viewModel.sessionRunning),
                                               rootLinkIsActive: $transmitterLinkIsActive),
                                   isActive: $transmitterLinkIsActive)
                    {
                        LabeledValueView(label: LocalizedString("Transmitter", comment: "Transmitter button title"),
                                         value: viewModel.transmitterID)
                    }
                    .isDetailLink(false)
                }
                
                Section(header:SectionHeader(label: LocalizedString("Device Information", comment: "Device information section title"))) {
                    LabeledValueView(label: LocalizedString("Glucose", comment: "Field label"),
                                     value: viewModel.lastGlucoseValueFormatted)
                    LabeledDateView(label: LocalizedString("Last reading", comment: "Field label"),
                                    date: viewModel.lastGlucoseDate,
                                    dateFormatter: viewModel.dateFormatter)
                    LabeledValueView(label: LocalizedString("Trend", comment: "Field label"),
                                     value: viewModel.lastGlucoseTrendFormatted)
                }
                
                Section(header: SectionHeader(label: LocalizedString("Support", comment: "Support section title")),
                        footer: Text(LocalizedString("Opens the Support section.", comment: "Support Section footer")))
                {
                    NavigationLink(destination: EmptyView()) {
                        Text(LocalizedString("Get Help with Dexcom G6", comment: "Support button title"))
                    }
                    .isDetailLink(false)
                }
                
                Section {
                    Button(action:{
                        self.displayDeleteWarning = true
                    }) {
                        Text(LocalizedString("Delete/Swap/Replace CGM", comment: "Delete CGM Manager button title"))
                            .foregroundColor(.destructive)
                    }
                    .sheet(isPresented: $displayDeleteWarning) {
                        AdditionalDescriptionView(title: LocalizedString("Delete/Swap/Replace CGM", comment: "Title of additional description for deleting the CGM manager"),
                                                  boldedMessage: LocalizedString("This will disconnect from your existing CGM and delete all the CGM settings (for example, the alert configurations). In order to use a CGM, you will need to complete the setup process again.", comment: "Warning for deleting the CGM manager"),
                                                  additionalDescription: LocalizedString("Only use this to switching between CGMs from different manufacturers. Do not use this to replace a CGM of the same manufacturer/model (replace a Dexcom G6 with another Dexcom G6). If you are looking to replace a CGM of the same manufacturer/model, go back to settings and tap on the Transmitter ID.", comment: "Description for deleting the CGM manager"),
                                                  confirmButtonTitle: LocalizedString("Delete/Swap/Replace CGM", comment: "Confirmation button title"),
                                                  confirmButtonType: .destructive,
                                                  confirmAction: { self.viewModel.deleteCGMManagerHandler() },
                                                  displayCancelButton: true)
                    }
                }
            }
            .listStyle(GroupedListStyle())
            .environment(\.horizontalSizeClass, horizontalOverride)
            .navigationBarItems(trailing: doneButton)
            .navigationBarTitle(viewModel.cgmManager.localizedTitle)
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        let cgmManager = DexcomCGMManager(state: DexcomCGMManagerState(dexcomTransmitterID: DexcomTransmitterID("8YYYY8")!))
        let viewModelWithoutSession = SettingsViewModel(cgmManager: cgmManager, glucoseUnit: HKUnit.milligramsPerDeciliter, completionHandler: { })
        let viewModelWithSession = MockSettingsViewModel()
        viewModelWithSession.sessionRunning = true
        return Group {
            SettingsView(viewModel: viewModelWithoutSession)
                .colorScheme(.light)
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("SE light")
            SettingsView(viewModel: viewModelWithSession)
                .colorScheme(.dark)
                .previewDevice(PreviewDevice(rawValue: "iPhone XS Max"))
                .previewDisplayName("XS Max dark")
        }
    }
}
