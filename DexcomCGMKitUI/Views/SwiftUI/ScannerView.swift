//
//  ScannerView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-28.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import DexcomCGMKit

struct ScannerView: UIViewRepresentable {
    let deviceType: Dexcom2DBarcode.DeviceType
    var startScanning: Bool
    let completionHandler: (Dexcom2DBarcode?) -> Void
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func makeUIView(context: Context) -> Barcode2DScannerView {
        let scannerView = Barcode2DScannerView()
        scannerView.stopScanning()
        scannerView.delegate = context.coordinator
        return scannerView
    }
    
    func updateUIView(_ scannerView: Barcode2DScannerView, context: Context) {
        if !scannerView.isRunning && startScanning {
            scannerView.startScanning()
        }
    }
    
    class Coordinator: NSObject, Barcode2DScannerViewDelegate {
        var view: ScannerView

        init(_ view: ScannerView) {
            self.view = view
        }
        
        func scanningSucceededWithCode(_ code: String?) {
            view.completionHandler(Dexcom2DBarcode(view.deviceType, code: code))
        }
    }
}
