//
//  ValuePickerTableViewCell.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-10-09.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit

public class ValuePickerTableViewCell: UITableViewCell {

    public static let height: CGFloat = 216
    
    public var valuePicker: UIPickerView!
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    public func setup() {
        valuePicker = UIPickerView()
        self.addSubview(valuePicker)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        valuePicker.center = self.contentView.center
    }
    
}

extension ValuePickerTableViewCell: NibLoadable { }
