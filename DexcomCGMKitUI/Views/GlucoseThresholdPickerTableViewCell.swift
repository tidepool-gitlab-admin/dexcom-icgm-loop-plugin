//
//  GlucoseThresholdPickerTableViewCell.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-10-10.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import HealthKit
import LoopKit

protocol GlucoseThresholdPickerDelegate: class {
    func didSelectGlucoseThreshold(_ threshold: HKQuantity)
}

final class GlucoseThresholdPickerTableViewCell: ValuePickerTableViewCell {
    
    private static let maxGlucoseValue: Double = 600
    
    private var preferredUnit: HKUnit!
    
    private var thresholdType: ThresholdType!
    
    private var minValue: HKQuantity!
    
    private var maxValue: HKQuantity!
    
    enum ThresholdType {
        case low, high

        // these bounds match the bounds in the Dexcom G6 app
        func bounds(forUnit unit: HKUnit) -> (lower: Int, upper: Int) {
            switch self {
            case .low:
                return (unit == HKUnit.milligramsPerDeciliter) ? (60, 100) : (3, 5)
            case .high:
                return (unit == HKUnit.milligramsPerDeciliter) ? (120, 400) : (6, 22)
            }
        }
    }
    
    private weak var delegate: GlucoseThresholdPickerDelegate?
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override public func setup() {
        super.setup()
        valuePicker.delegate = self
        valuePicker.dataSource = self
    }
    
    func set(delegate: GlucoseThresholdPickerDelegate,
             thresholdType: ThresholdType,
             initialValue: HKQuantity,
             preferredUnit: HKUnit,
             minValue: HKQuantity = HKQuantity(unit: HKUnit.milligramsPerDeciliter, doubleValue: 0),
             maxValue: HKQuantity = HKQuantity(unit: HKUnit.milligramsPerDeciliter, doubleValue: maxGlucoseValue)) {
        self.delegate = delegate
        self.thresholdType = thresholdType
        self.preferredUnit = preferredUnit
        self.minValue = minValue
        self.maxValue = maxValue
        setPickerValue(initialValue, animated: false)
    }
    
    private func setPickerValue(_ initialValue: HKQuantity, animated: Bool) {
        let integerRow = GlucoseComponent.integer.values(forThresholdType: thresholdType, andUnit: preferredUnit).firstIndex(of: Int(initialValue.doubleValue(for: preferredUnit))) ?? 0
        valuePicker.selectRow(integerRow, inComponent: GlucoseComponent.integer.rawValue, animated: animated)
        if GlucoseComponent.providesMantissa(forUnit: preferredUnit) {
            let mantissaRow = GlucoseComponent.mantissa.values(forThresholdType: thresholdType, andUnit: preferredUnit).firstIndex(of: Int((initialValue.doubleValue(for: preferredUnit).truncatingRemainder(dividingBy: 1)*10).rounded(.toNearestOrAwayFromZero))) ?? 0
            valuePicker.selectRow(mantissaRow, inComponent: GlucoseComponent.mantissa.rawValue, animated: animated)
        }
    }
}

//MARK: - Picker Data Source and Delegate

extension GlucoseThresholdPickerTableViewCell: UIPickerViewDataSource, UIPickerViewDelegate {
    private enum GlucoseComponent: Int, CaseIterable {
        case integer
        case mantissa
        case unit
        
        static func providesMantissa(forUnit unit: HKUnit) -> Bool {
            return unit != HKUnit.milligramsPerDeciliter
        }
        
        func values(forThresholdType thresholdType: ThresholdType, andUnit unit: HKUnit) -> Array<Int> {
            switch self {
            case .integer:
                let bounds = thresholdType.bounds(forUnit: unit)
                return Array(stride(from: bounds.lower, through: bounds.upper, by: 1))
            case .mantissa:
                return Array(stride(from: 0, through: 9, by: 1))
            default:
                return []
            }
        }
        
        func valuesAsStrings(forThresholdType thresholdType: ThresholdType, andUnit unit: HKUnit) -> Array<String> {
            switch self {
            case .integer:
                let strings = values(forThresholdType: thresholdType, andUnit: unit).map { String(format:"%d", $0) }
                return GlucoseComponent.providesMantissa(forUnit: unit) ? strings.map { $0 + LocalizedString(".", comment: "decimal point symbol") } : strings
            case .mantissa:
                return values(forThresholdType: thresholdType, andUnit: unit).map { String(format:"%d", $0) }
            default:
                return []
            }
        }
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return GlucoseComponent.providesMantissa(forUnit: preferredUnit) ? 3 : 2
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let currentComponentIndex = GlucoseComponent.providesMantissa(forUnit: preferredUnit) ? component : component == GlucoseComponent.mantissa.rawValue ? GlucoseComponent.unit.rawValue : component
        let glucoseComponent = GlucoseComponent(rawValue: currentComponentIndex)!
        switch glucoseComponent {
        case .integer, .mantissa:
            return glucoseComponent.valuesAsStrings(forThresholdType: thresholdType, andUnit: preferredUnit).count
        case .unit:
            return 1
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let currentComponentIndex = GlucoseComponent.providesMantissa(forUnit: preferredUnit) ? component : component == GlucoseComponent.mantissa.rawValue ? GlucoseComponent.unit.rawValue : component
        let glucoseComponent = GlucoseComponent(rawValue: currentComponentIndex)!
        switch glucoseComponent {
        case .integer, .mantissa:
            return glucoseComponent.valuesAsStrings(forThresholdType: thresholdType, andUnit: preferredUnit)[row]
        case .unit:
            return preferredUnit.shortLocalizedUnitString()
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let integer = Double(GlucoseComponent.integer.values(forThresholdType: thresholdType, andUnit: preferredUnit)[pickerView.selectedRow(inComponent: GlucoseComponent.integer.rawValue)])
        var mantissa: Double = 0
        if GlucoseComponent.providesMantissa(forUnit: preferredUnit) {
            mantissa = Double(GlucoseComponent.mantissa.values(forThresholdType: thresholdType, andUnit: preferredUnit)[pickerView.selectedRow(inComponent: GlucoseComponent.mantissa.rawValue)])/10
        }
        var value = HKQuantity.init(unit: preferredUnit, doubleValue: integer+mantissa)
        if value.doubleValue(for: HKUnit.milligramsPerDeciliter) < minValue.doubleValue(for: HKUnit.milligramsPerDeciliter) {
            value = minValue
            setPickerValue(value, animated: true)
        } else if value.doubleValue(for: HKUnit.milligramsPerDeciliter) > maxValue.doubleValue(for: HKUnit.milligramsPerDeciliter) {
            value = maxValue
            setPickerValue(value, animated: true)
        }
        delegate?.didSelectGlucoseThreshold(value)
    }
}
