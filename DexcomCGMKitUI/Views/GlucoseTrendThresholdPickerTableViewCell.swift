//
//  GlucoseTrendThresholdPickerTableViewCell.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-10-11.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import HealthKit

protocol GlucoseTrendThresholdPickerDelegate: class {
    func didSelectGlucoseTrendThreshold(_ threshold: HKQuantity)
}

class GlucoseTrendThresholdPickerTableViewCell: ValuePickerTableViewCell {

    private var preferredUnit: HKUnit!
    
    private var preferredUnitPerMinute: HKUnit!
        
    private weak var delegate: GlucoseTrendThresholdPickerDelegate?
    
    private var trendType: TrendType!
    
    enum TrendType {
        case rising, falling
        
        // these bounds match the bounds in the Dexcom G6 app
        static func bounds(forUnit unit: HKUnit) -> (lower: Int, upper: Int) {
            return (unit == HKUnit.milligramsPerDeciliterPerMinute) ? (1, 5) : (0, 0)
        }
    }
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override public func setup() {
        super.setup()
        valuePicker.delegate = self
        valuePicker.dataSource = self
    }
    
    func set(delegate: GlucoseTrendThresholdPickerDelegate, trendType: TrendType, initialValue: HKQuantity, preferredUnit: HKUnit) {
        self.delegate = delegate
        self.trendType = trendType
        self.preferredUnit = preferredUnit
        self.preferredUnitPerMinute = preferredUnit.unitDivided(by: .minute())
        setPickerInitialValue(initialValue)
    }
    
    private func setPickerInitialValue(_ initialValue: HKQuantity) {
        let integerRow = GlucoseTrendComponent.integer.values(forUnit: preferredUnitPerMinute).firstIndex(of: abs(Int(initialValue.doubleValue(for: preferredUnitPerMinute)))) ?? 0
        valuePicker.selectRow(integerRow, inComponent: GlucoseTrendComponent.integer.rawValue, animated: false)
        if GlucoseTrendComponent.providesMantissa(forUnit: preferredUnitPerMinute) {
            let mantissaRow = GlucoseTrendComponent.mantissa.values(forUnit: preferredUnitPerMinute).firstIndex(of: abs(Int((initialValue.doubleValue(for: preferredUnitPerMinute).truncatingRemainder(dividingBy: 1)*10).rounded(.toNearestOrAwayFromZero)))) ?? 0
            valuePicker.selectRow(mantissaRow, inComponent: GlucoseTrendComponent.mantissa.rawValue, animated: false)
        }
    }
}

//MARK: - Picker Data Source and Delegate

extension GlucoseTrendThresholdPickerTableViewCell: UIPickerViewDataSource, UIPickerViewDelegate {
    private enum GlucoseTrendComponent: Int, CaseIterable {
        case integer
        case mantissa
        case unit
        
        static func providesMantissa(forUnit unit: HKUnit) -> Bool {
            return unit != HKUnit.milligramsPerDeciliterPerMinute
        }
        
        func values(forUnit unit: HKUnit) -> Array<Int> {
            switch self {
            case .integer:
                let bounds = TrendType.bounds(forUnit: unit)
                return Array(stride(from: bounds.lower, through: bounds.upper, by: 1))
            case .mantissa:
                return Array(stride(from: 1, through: 2, by: 1))
            default:
                return []
            }
        }
        
        func valuesAsStrings(forUnit unit: HKUnit) -> Array<String> {
            switch self {
            case .integer:
                let strings = values(forUnit: unit).map { String(format:"%d", $0) }
                return GlucoseTrendComponent.providesMantissa(forUnit: unit) ? strings.map { $0 + "." } : strings
            case .mantissa:
                return values(forUnit: unit).map { String(format:"%d", $0) }
            default:
                return []
            }
        }
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return GlucoseTrendComponent.providesMantissa(forUnit: preferredUnitPerMinute) ? 3 : 2
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let currentComponentIndex = GlucoseTrendComponent.providesMantissa(forUnit: preferredUnitPerMinute) ? component : component == GlucoseTrendComponent.mantissa.rawValue ? GlucoseTrendComponent.unit.rawValue : component
        let glucoseTrendComponent = GlucoseTrendComponent(rawValue: currentComponentIndex)!
        switch glucoseTrendComponent {
        case .integer, .mantissa:
            return glucoseTrendComponent.valuesAsStrings(forUnit: preferredUnitPerMinute).count
        case .unit:
            return 1
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let currentComponentIndex = GlucoseTrendComponent.providesMantissa(forUnit: preferredUnitPerMinute) ? component : component == GlucoseTrendComponent.mantissa.rawValue ? GlucoseTrendComponent.unit.rawValue : component
        let glucoseTrendComponent = GlucoseTrendComponent(rawValue: currentComponentIndex)!
        switch glucoseTrendComponent {
        case .integer, .mantissa:
            return glucoseTrendComponent.valuesAsStrings(forUnit: preferredUnitPerMinute)[row]
        case .unit:
            // mg/dL/min and mmol/L/min does not display correctly.
            return String(format: LocalizedString("%@/min", comment: "Format alert threshold unit per minute. (1: preferred unit)"), preferredUnit.shortLocalizedUnitString())
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let integer = Double(GlucoseTrendComponent.integer.values(forUnit: preferredUnitPerMinute)[pickerView.selectedRow(inComponent: GlucoseTrendComponent.integer.rawValue)])
        var mantissa: Double = 0
        if GlucoseTrendComponent.providesMantissa(forUnit: preferredUnitPerMinute) {
            mantissa = Double(GlucoseTrendComponent.mantissa.values(forUnit: preferredUnitPerMinute)[pickerView.selectedRow(inComponent: GlucoseTrendComponent.mantissa.rawValue)])/10
        }
        let value = trendType == .falling ? -1*(integer+mantissa) : integer+mantissa
        delegate?.didSelectGlucoseTrendThreshold(HKQuantity.init(unit: preferredUnitPerMinute, doubleValue: value))
    }
}
