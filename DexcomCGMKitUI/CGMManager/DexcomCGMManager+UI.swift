//
//  DexcomCGMManager+UI.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-08-30.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import SwiftUI
import HealthKit
import LoopKitUI
import DexcomCGMKit
import TransmitterCore

extension DexcomCGMManager: CGMManagerUI {
    
    public static func setupViewController() -> (UIViewController & CGMManagerSetupViewController & CompletionNotifying)? {
        let setupVC = SetupHostingController(rootView: nil)
        let setupWorkflowVM = ReplacementWorkflowViewModel(replacementWorkflow: .replaceTransmitterAndSensor,
                                                           replaceSensorCompletionHandler:
            { [weak setupVC] (sensorCode: SensorCode?) in
                setupVC?.pendingSensorCode = sensorCode
            },
                                                           replaceTransmitterCompletionHandler:
            { [weak setupVC] (dexcomTransmitterID: DexcomTransmitterID) in
                setupVC?.completeSetup(state: DexcomCGMManagerState(dexcomTransmitterID: dexcomTransmitterID))
            }
        )
        let setupGuideView = SetupGuideView(viewModel: setupWorkflowVM, dismissAction: { [weak setupVC] in setupVC?.dismiss(animated: true) })
        setupVC.rootView = setupGuideView
        return setupVC
    }
    
    public func settingsViewController(for glucoseUnit: HKUnit) -> (UIViewController & CompletionNotifying) {
        let settingsVC = SettingsHostingController(rootView: nil)
        let settingsViewModel = SettingsViewModel(cgmManager: self,
                                                  glucoseUnit: glucoseUnit,
                                                  completionHandler: { [weak settingsVC] in settingsVC?.notifyComplete() })
        let settingsView = SettingsView(viewModel: settingsViewModel)
        settingsVC.rootView = settingsView
        return settingsVC
    }
    
    public var smallImage: UIImage? {
        return nil
    }
    
}

private class SetupHostingController: UIHostingController<SetupGuideView?>, CGMManagerSetupViewController, CompletionNotifying {
    
    weak var setupDelegate: CGMManagerSetupViewControllerDelegate?
    
    weak var completionDelegate: CompletionDelegate?
    
    var pendingSensorCode: SensorCode?
    
    func completeSetup(state: DexcomCGMManagerState) {
        let manager = DexcomCGMManager(state: state)
        setupDelegate?.cgmManagerSetupViewController(self, didSetUpCGMManager: manager)
        manager.pendingSensorCode = pendingSensorCode
        manager.startCommunication()
        completionDelegate?.completionNotifyingDidComplete(self)
    }
}

private class SettingsHostingController: UIHostingController<SettingsView<SettingsViewModel>?>, CompletionNotifying {

    weak var completionDelegate: CompletionDelegate?

    func notifyComplete() {
        completionDelegate?.completionNotifyingDidComplete(self)
    }
}
