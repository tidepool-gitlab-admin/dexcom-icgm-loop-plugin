//
//  DateFormatter.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-04.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

extension DateFormatter {
    static func localizedString(from date: Date, dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style, timeZone: TimeZone) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = dateStyle
        dateFormatter.timeStyle = timeStyle
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = timeZone
        return dateFormatter.string(from: date)
    }
}
