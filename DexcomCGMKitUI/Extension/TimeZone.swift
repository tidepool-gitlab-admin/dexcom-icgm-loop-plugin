//
//  TimeZone.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2020-02-03.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

extension TimeZone {
    static var currentFixed: TimeZone {
        return TimeZone(secondsFromGMT: TimeZone.current.secondsFromGMT())!
    }
}
