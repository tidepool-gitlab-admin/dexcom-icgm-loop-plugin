//
//  UITableViewCell.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-09.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation

extension UITableViewCell: IdentifiableClass { }
