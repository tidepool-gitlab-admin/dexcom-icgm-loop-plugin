//
//  DexcomCGMKitUI.h
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-08-20.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DexcomCGMKitUI.
FOUNDATION_EXPORT double DexcomCGMKitUIVersionNumber;

//! Project version string for DexcomCGMKitUI.
FOUNDATION_EXPORT const unsigned char DexcomCGMKitUIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DexcomCGMKitUI/PublicHeader.h>


