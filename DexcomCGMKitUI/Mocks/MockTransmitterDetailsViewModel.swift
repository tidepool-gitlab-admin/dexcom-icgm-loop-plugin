//
//  MockTransmitterDetailsViewModel.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-14.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import DexcomCGMKit
import TransmitterCore

class MockTransmitterDetailsViewModel: TransmitterDetailsViewModelProtocol {
    let cgmManager: DexcomCGMManager
    
    let serialNumber: String? = "XYZ789"
    
    let activatedOn: Date? = Date().addingTimeInterval(.days(-5))
    
    let firmware: String? = "1.6.5.27"
    
    let softwareNumber: String? = "SW1163"
    
    var detailsAvailable: Bool = true
    
    var sessionRunning: Bool = true
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        return formatter
    }()
    
    let newSensorCompletionHandler: (SensorCode?) -> Void = { _ in }
    
    let newTransmitterCompletionHandler: (DexcomTransmitterID) -> Void = { _ in }
    
    let stopSensorCompletionHandler: () -> Void = { }
    
    init() {
        let cgmManager = DexcomCGMManager(state: DexcomCGMManagerState(dexcomTransmitterID: DexcomTransmitterID(self.serialNumber!)!))
        self.cgmManager = cgmManager
    }
}
