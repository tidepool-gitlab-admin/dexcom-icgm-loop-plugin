//
//  MockSettingsViewModel.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-14.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import HealthKit
import DexcomCGMKit
import TransmitterCore

class MockSettingsViewModel: SettingsViewModelProtocol {
    
    let cgmManager: DexcomCGMManager
    
    let glucoseUnit: HKUnit = HKUnit.milligramsPerDeciliter
        
    let transmitterID: String = "8YYYY1"

    let lastCalibrationDate: Date? = Date().addingTimeInterval(.days(-2))
    
    let sensorInsertionDate: Date? = Date().addingTimeInterval(.days(-4))
    
    let sensorExpirationDate: Date? = Date().addingTimeInterval(.days(-6))
    
    let lastGlucoseValueFormatted: String? = "90 mg/dL"
    
    let lastGlucoseDate: Date? = Date()
    
    let lastGlucoseTrendFormatted: String? = "-1 mg/dL/min"

    var sessionRunning: Bool = false
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter
    }()
    
    let completionHandler: () -> Void = { }
    
    let calibrationCompletionHandler: (HKQuantity) -> Void = { _ in }
    
    let newSensorCompletionHandler: (SensorCode?) -> Void = { _ in }
    
    let stopSensorCompletionHandler: () -> Void = { }
    
    let deleteCGMManagerHandler: () -> Void = { }
        
    init() {
        let cgmManager = DexcomCGMManager(state: DexcomCGMManagerState(dexcomTransmitterID: DexcomTransmitterID(self.transmitterID)!))
        self.cgmManager = cgmManager
    }
}
