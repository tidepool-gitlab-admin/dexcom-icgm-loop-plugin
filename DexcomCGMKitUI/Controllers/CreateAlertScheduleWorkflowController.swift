//
//  CreateAlertScheduleWorkflowController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-25.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import UIKit
import HealthKit
import DexcomCGMKit

struct CreateAlertScheduleWorkflowController: UIViewControllerRepresentable {
    let glucoseUnit: HKUnit
    let completionHandler: (DexcomAlertSchedule) -> Void
    @Binding var rootLinkIsActive : Bool
        
    func makeUIViewController(context: Context) -> DexcomCGMAlertScheduleNameViewController {
        let alertScheduleNameVC = DexcomCGMAlertScheduleNameViewController.instantiateFromStoryboard()
        alertScheduleNameVC.alertSchedule = DexcomAlertSchedule.defaultSchedule()
        alertScheduleNameVC.glucoseUnit = glucoseUnit
        alertScheduleNameVC.completionHandler = { (alertSchedule: DexcomAlertSchedule) in
            self.completionHandler(alertSchedule)
            self.popToRoot()
        }
        return alertScheduleNameVC
    }

    func updateUIViewController(_ viewController: DexcomCGMAlertScheduleNameViewController, context: Context) {
        
    }

    func popToRoot() {
        // this deactivates the root link, which effectively pops to root
        self.rootLinkIsActive = false
    }
}
