//
//  PlayerView.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-28.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import AVKit

struct PlayerViewController: UIViewControllerRepresentable {
    let urlString: String
    @Binding var linkIsActive: Bool
    
    func makeUIViewController(context: Context) -> AVPlayerViewController {
        let playerViewController = AVPlayerViewController()
        if let url = URL(string: urlString) {
            let player = AVPlayer(url: url)
            playerViewController.player = player
        } else {
            linkIsActive = false
        }
        return playerViewController
    }
    
    func updateUIViewController(_ viewController: AVPlayerViewController, context: Context) {
        if let player = viewController.player {
            if linkIsActive,
                player.timeControlStatus == .paused
            {
                player.play()
            } else if !linkIsActive,
                player.timeControlStatus == .playing
            {
                player.pause()
                viewController.player = nil
            }
        }
    }
}
