//
//  DexcomCGMInsertSensorViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-16.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit

class DexcomCGMInsertSensorViewController: UIViewController {

    var completionHandler: (() -> Void)?
    
    var replacementWorkflow = ReplacementWorkflow.replaceSensor
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if let completionHandler = completionHandler {
            completionHandler()
        }
    }
    
}
