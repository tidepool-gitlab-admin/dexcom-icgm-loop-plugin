//
//  DexcomCGMEnterTransmitterIDViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-26.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import DexcomCGMKit

class DexcomCGMEnterTransmitterIDViewController: UIViewController {

    var completionHandler: ((DexcomTransmitterID) -> Void)?
    
    enum State {
        case normal
        case transmitterIDError
    }
    
    var state : State = .normal
    
    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        switch state {
        case .normal:
            title = LocalizedString("Transmitter SN", comment: "Title of enter transmitter ID view")
            messageLabel.text = LocalizedString("Take photo of Transmitter SN on outside of transmitter box, or manually enter SN. \nKeep box for future use. \n\nTransmitter SN is also on bottom of transmitter and in receiver Settings menu.", comment:"Instructions on enter transmitter ID view")
        case .transmitterIDError:
            title = LocalizedString("Incorrect Transmitter SN", comment: "Title of enter transmitter ID view after incorrect transmitter ID has been entered")
            messageLabel.text = LocalizedString("Transmitter SN entered is not correct. \n\nTake photo of Transmitter SN on outside of transmitter box again, or manually enter SN", comment:"Instructions on enter transmitter ID view after incorrect transmitter ID has been entered")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMTransmitterIDManualViewController:
            vc.completionHandler = completionHandler
        case let vc as DexcomCGMTransmitterIDCameraViewController:
            vc.completionHandler = completionHandler
        default:
            break
        }
    }
    
    @IBAction func unwindToEnterTransmitterIDViewController(segue: UIStoryboardSegue) { }

}
