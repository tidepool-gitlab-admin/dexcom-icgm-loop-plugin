//
//  DexcomCGMSensorCodeViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-16.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import DexcomCGMKit
import TransmitterCore

class DexcomCGMSensorCodeViewController: UIViewController {
    
    var replaceSensorCompletionHandler: ((SensorCode?) -> Void)?

    var replaceTransmitterCompletionHandler: ((DexcomTransmitterID) -> Void)?

    var replacementWorkflow = ReplacementWorkflow.replaceSensor

    static public func instantiateFromStoryboard() -> DexcomCGMSensorCodeViewController {
        let storyboard = UIStoryboard(name: "DexcomCGMManager", bundle: Bundle(for: DexcomCGMSensorCodeViewController.self))
        let sensorCodeVC = storyboard.instantiateViewController(withIdentifier: "DexcomCGMSensorCode") as! DexcomCGMSensorCodeViewController
        return sensorCodeVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMNoSensorCodeViewController:
            if let completionHandler = replaceSensorCompletionHandler {
                vc.replaceSensorCompletionHandler = { completionHandler(nil) }
            }
            vc.replaceTransmitterCompletionHandler = replaceTransmitterCompletionHandler
            vc.replacementWorkflow = replacementWorkflow
        case let vc as DexcomCGMEnterSensorCodeViewController:
            vc.replaceSensorCompletionHandler = replaceSensorCompletionHandler
            vc.replaceTransmitterCompletionHandler = replaceTransmitterCompletionHandler
            vc.replacementWorkflow = replacementWorkflow
        default:
            break
        }
    }
}
