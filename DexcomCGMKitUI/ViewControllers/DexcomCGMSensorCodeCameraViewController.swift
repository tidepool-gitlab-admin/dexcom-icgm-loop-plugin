//
//  DexcomCGMSensorCodeCameraViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-18.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import DexcomCGMKit
import TransmitterCore

class DexcomCGMSensorCodeCameraViewController: UIViewController {

    var replaceSensorCompletionHandler: ((SensorCode?) -> Void)?
    
    var replaceTransmitterCompletionHandler: ((DexcomTransmitterID) -> Void)?

    var replacementWorkflow = ReplacementWorkflow.replaceSensor
    
    private(set) var sensor2DBarcode: Dexcom2DBarcode?
    
    @IBOutlet weak var scannerView: Barcode2DScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !scannerView.isRunning {
            scannerView.startScanning()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMNoSensorCodeViewController:
            if let completionHandler = replaceSensorCompletionHandler {
                vc.replaceSensorCompletionHandler = { completionHandler(nil) }
            }
            vc.replacementWorkflow = replacementWorkflow
        case let vc as DexcomCGMInsertSensorViewController:
            if let completionHandler = replaceSensorCompletionHandler,
                let sensor2DBarcode = sensor2DBarcode,
                let sensorCode = sensor2DBarcode.code {
                vc.completionHandler = { completionHandler(SensorCode(rawValue: sensorCode)) }
            }
        case let vc as DexcomCGMEnterSensorCodeViewController:
            vc.state = .sensorCodeError
        case let vc as DexcomCGMEnterTransmitterIDViewController:
            if let completionHandler = replaceSensorCompletionHandler,
                let sensor2DBarcode = sensor2DBarcode,
                let sensorCode = sensor2DBarcode.code {
                completionHandler(SensorCode(rawValue: sensorCode))
            }
            vc.completionHandler = replaceTransmitterCompletionHandler
        default:
            break
        }
    }
    
    private func done() {
        guard let sensor2DBarcode = sensor2DBarcode, sensor2DBarcode.isValid() else {
            performSegue(withIdentifier: "unwindToEnterSensorCode", sender: self)
            return
        }
        
        switch replacementWorkflow {
        case .replaceSensor:
            performSegue(withIdentifier: "SensorCodeCameraToInsertSensor", sender: self)
        case .replaceTransmitterAndSensor:
            
            performSegue(withIdentifier: "SensorCodeCameraToTransmitterID", sender: self)
        }
    }
}

// MARK: - 2D Barcode Scanner Delegate

extension DexcomCGMSensorCodeCameraViewController: Barcode2DScannerViewDelegate {
    func scanningDidFail() {
        //TODO display message to user that scanning failed
    }
    
    func scanningSucceededWithCode(_ code: String?) {
        sensor2DBarcode = Dexcom2DBarcode(.sensor, code: code)
        done()
    }
    
    func scanningDidStop() {
        // NOP
    }
    
}
