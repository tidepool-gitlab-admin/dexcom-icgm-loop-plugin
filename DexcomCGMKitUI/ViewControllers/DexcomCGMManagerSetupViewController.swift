//
//  DexcomCGMManagerSetupViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-08-30.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation
import LoopKit
import LoopKitUI
import DexcomCGMKit
import TransmitterCore

public class DexcomCGMManagerSetupViewController: UINavigationController, CGMManagerSetupViewController, UINavigationControllerDelegate, CompletionNotifying {
    
    public var setupDelegate: CGMManagerSetupViewControllerDelegate?
    
    private var pendingSensorCode: SensorCode?
    
    public var completionDelegate: CompletionDelegate?
    
    class func instantiateFromStoryboard() -> DexcomCGMManagerSetupViewController {
        let storyboard = UIStoryboard(name: "DexcomCGMManager", bundle: Bundle(for: DexcomCGMManagerSetupViewController.self))
        return storyboard.instantiateViewController(withIdentifier: "DexcomCGMTransmitterSetup") as! DexcomCGMManagerSetupViewController
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
        if let sensorCodeVC = self.viewControllers[0] as? DexcomCGMSensorCodeViewController {
            sensorCodeVC.replaceSensorCompletionHandler = { [weak self] (sensorCode: SensorCode?) in
                self?.pendingSensorCode = sensorCode
            }
            sensorCodeVC.replaceTransmitterCompletionHandler = { [weak self] (dexcomTransmitterID: DexcomTransmitterID) in
                self?.completeSetup(state: DexcomCGMManagerState(dexcomTransmitterID: dexcomTransmitterID))
            }
            sensorCodeVC.replacementWorkflow = .replaceTransmitterAndSensor
        }
    }
    
    func completeSetup(state: DexcomCGMManagerState) {
        let manager = DexcomCGMManager(state: state)
        setupDelegate?.cgmManagerSetupViewController(self, didSetUpCGMManager: manager)
        manager.pendingSensorCode = pendingSensorCode
        manager.startCommunication()
        completionDelegate?.completionNotifyingDidComplete(self)
    }
    
    public func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        if let setupViewController = viewController as? SetupTableViewController {
            setupViewController.delegate = self
        }
    }
}

extension DexcomCGMManagerSetupViewController: SetupTableViewControllerDelegate {
    public func setupTableViewControllerCancelButtonPressed(_ viewController: SetupTableViewController) {
        completionDelegate?.completionNotifyingDidComplete(self)
    }
}
