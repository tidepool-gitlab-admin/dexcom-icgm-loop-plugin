//
//  DexcomCGMSensorCodeManualViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-16.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import DexcomCGMKit
import TransmitterCore

class DexcomCGMSensorCodeManualViewController: UIViewController {

    var replaceSensorCompletionHandler: ((SensorCode?) -> Void)?
    
    var replaceTransmitterCompletionHandler: ((DexcomTransmitterID) -> Void)?

    var replacementWorkflow = ReplacementWorkflow.replaceSensor
    
    @IBOutlet private var sensorCodeTextField: UITextField!
    
    private var sensorCode: SensorCode? {
        get {
            guard let rawSensorCode = sensorCodeTextField.text else {
                return nil
            }
            return SensorCode(rawValue: rawSensorCode)
        }
    }
    
    private var saveButton: UIBarButtonItem!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveTapped(_:)))
        saveButton.isEnabled = false
        navigationItem.setRightBarButton(saveButton, animated: false)

        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTapped(_:)))
        navigationItem.setLeftBarButton(cancelButton, animated: false)
        
        sensorCodeTextField.becomeFirstResponder()
    }
    
    @objc func saveTapped(_ sender: Any) {
        confirmSaveSensorCode()
    }
    
    @objc func cancelTapped(_ sender: Any) {
        done()
    }

    private func confirmSaveSensorCode() {
        let dialog = UIAlertController(title: LocalizedString("Confirm Entry", comment: "Title for confirm entry notification"), message: sensorCodeTextField.text, preferredStyle: .alert)
        
        dialog.addAction(UIAlertAction(title: LocalizedString("Confirm", comment: "Title for confirm button"), style: .default, handler: { (action: UIAlertAction!) in
            self.saveSensorCode()
        }))
        
        dialog.addAction(UIAlertAction(title: LocalizedString("Cancel", comment: "Title for cancel button"), style: .cancel))
        
        present(dialog, animated: true, completion: nil)
    }
    
    private func saveSensorCode() {
        guard sensorCode != nil else {
            performSegue(withIdentifier: "unwindToEnterSensorCode", sender: self)
            return
        }
        
        switch replacementWorkflow {
        case .replaceSensor:
            performSegue(withIdentifier: "SensorCodeManualToInsertSensor", sender: self)
        case .replaceTransmitterAndSensor:
            performSegue(withIdentifier: "SensorCodeManualToTransmitterID", sender: self)
        }
    }

    private func done() {
        if let nav = navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func textFieldDidUpate(_ sender:UITextField) {
        if let sensorCodeString = sensorCodeTextField.text,
            sensorCodeString.count == SensorCode.codeLength {
            saveButton.isEnabled = true
            sensorCodeTextField.resignFirstResponder()
        } else {
            saveButton.isEnabled = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMNoSensorCodeViewController:
            if let completionHandler = replaceSensorCompletionHandler {
                vc.replaceSensorCompletionHandler = { completionHandler(nil) }
            }
            vc.replacementWorkflow = replacementWorkflow
        case let vc as DexcomCGMInsertSensorViewController:
            if let completionHandler = replaceSensorCompletionHandler {
                vc.completionHandler = { completionHandler(self.sensorCode) }
            }
        case let vc as DexcomCGMEnterSensorCodeViewController:
            vc.state = .sensorCodeError
        case let vc as DexcomCGMEnterTransmitterIDViewController:
            if let completionHandler = replaceSensorCompletionHandler {
                completionHandler(self.sensorCode)
            }
            vc.completionHandler = replaceTransmitterCompletionHandler
        default:
            break
        }
    }
}
