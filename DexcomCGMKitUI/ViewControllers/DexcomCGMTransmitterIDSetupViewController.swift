//
//  DexcomCGMTransmitterIDSetupViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-08-30.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import LoopKit
import LoopKitUI
import DexcomCGMKit

class DexcomCGMTransmitterIDSetupViewController: SetupTableViewController {
    
    let transmitterIDLength = DexcomTransmitterID.length
    
    private(set) var transmitterID: String? {
        get {
            return transmitterIDTextField.text
        }
        set {
            transmitterIDTextField.text = newValue
        }
    }
    
    private func updateStateForSettings() {
        if let transmitterID = transmitterID,
            DexcomTransmitterID(transmitterID) != nil {
            state = .completed
        } else {
            state = .inputSettings
        }
    }
    
    private enum State {
        case loadingView
        case inputSettings
        case completed
    }
    
    private var state: State = .loadingView {
        didSet {
            switch state {
            case .loadingView:
                updateStateForSettings()
            case .inputSettings:
                footerView.primaryButton.isEnabled = false
            case .completed:
                footerView.primaryButton.isEnabled = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        state = .inputSettings
    }
    
    override func continueButtonPressed(_ sender: Any) {
        if state == .completed,
            let setupViewController = navigationController as? DexcomCGMManagerSetupViewController,
            let transmitterID = transmitterID,
            let dexcomTransmitterID = DexcomTransmitterID(transmitterID)
        {
            setupViewController.completeSetup(state: DexcomCGMManagerState(dexcomTransmitterID: dexcomTransmitterID))
        }
    }
    
    override func cancelButtonPressed(_ sender: Any) {
        if transmitterIDTextField.isFirstResponder {
            transmitterIDTextField.resignFirstResponder()
        } else {
            super.cancelButtonPressed(sender)
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return state == .completed
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet private var transmitterIDTextField: UITextField!
    
    // MARK: - UITableViewDelegate
    
    private enum Section: Int {
        case transmitterID
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch Section(rawValue: indexPath.section)! {
        case .transmitterID:
            tableView.deselectRow(at: indexPath, animated: false)
        }
    }
}

extension DexcomCGMTransmitterIDSetupViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text, let stringRange = Range(range, in: text) else {
            updateStateForSettings()
            return true
        }
        
        let newText = text.replacingCharacters(in: stringRange, with: string)
        
        if newText.count <= transmitterIDLength {
            textField.text = newText
        }
        
        if newText.count == transmitterIDLength {
            textField.resignFirstResponder()
        }
        
        updateStateForSettings()
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
