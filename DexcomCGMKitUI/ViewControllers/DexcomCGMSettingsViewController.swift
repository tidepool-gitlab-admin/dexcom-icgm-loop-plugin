//
//  DexcomCGMSettingsViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-08-30.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import SwiftUI
import HealthKit
import LoopKit
import LoopKitUI
import DexcomCGMKit
import TransmitterCore

class DexcomCGMSettingsViewController: UITableViewController {
    
    private(set) var cgmManager: DexcomCGMManager!
    
    private(set) var glucoseUnit: HKUnit!
    
    var sensorCodeString: String?
    
    static public func instantiateFromStoryboard(cgmManager: DexcomCGMManager, glucoseUnit: HKUnit) -> DexcomCGMSettingsViewController {
        let storyboard = UIStoryboard(name: "DexcomCGMManager", bundle: Bundle(for: DexcomCGMSettingsViewController.self))
        let settingsVC = storyboard.instantiateViewController(withIdentifier: "DexcomCGMTransmitterSettings") as! DexcomCGMSettingsViewController
        settingsVC.cgmManager = cgmManager
        settingsVC.glucoseUnit = glucoseUnit
        cgmManager.addObserver(settingsVC, queue: .main)
        return settingsVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = cgmManager.localizedTitle
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 55
        
        tableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: SettingsTableViewCell.className)
        tableView.register(TextButtonTableViewCell.self, forCellReuseIdentifier: TextButtonTableViewCell.className)
        tableView.register(LabeledTextFieldTableViewCell.nib(), forCellReuseIdentifier: LabeledTextFieldTableViewCell.className)
        
        let button = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped(_:)))
        navigationItem.setRightBarButton(button, animated: false)
    
    }
    
    @IBAction func unwindToDexcomSettingsViewController(segue: UIStoryboardSegue) { }
    
    @objc func doneTapped(_ sender: Any) {
        complete()
    }
    
    private func complete() {
        if let nav = navigationController as? SettingsNavigationViewController {
            nav.notifyComplete()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMSensorCodeViewController:
            vc.replaceSensorCompletionHandler = { [weak self] (sensorCode: SensorCode?) in
                self?.cgmManager.startSession(sensorCode)
            }
        case let vc as DexcomCGMTransmitterDetailsViewController:
            vc.cgmManager = cgmManager
        case let vc as DexcomCGMAlertsViewController:
            vc.alertConfigurationManager = cgmManager.alertConfigurationManager
            vc.glucoseUnit = glucoseUnit
        default:
            break
        }
    }
    
    // MARK: - Tableview Data Source
    
    private var sections: [Section] {
        var sections: [Section] = [.alerts, .lastReading, .sensor, .transmitter, .support]
#if DEBUG
        sections.append(.dev)
#endif
        return sections
    }
    
    private enum Section: Int, CaseIterable {
        case alerts
        case lastReading
        case sensor
        case transmitter
        case support
        case dev
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    private enum LatestReadingRow: Int, CaseIterable {
        case glucose
        case date
        case trend
        case status
    }
    
    private var sensorRows: [SensorRow] {
        if cgmManager.latestTransmitterData?.session == nil {
            // only allow the user to start a new sensor
            return [.stopOrNewSensor]
        } else {
            return SensorRow.allCases
        }
    }
    
    private enum SensorRow: Int, CaseIterable {
        case calibrate
        case lastCalibration
        case insertionTime
        case sensorExpires
        case stopOrNewSensor
    }
    
    private enum DevRow: Int, CaseIterable {
        case sensorCode
        case startSession
        case stopSession
        case delete
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .alerts:
            return 1
        case .lastReading:
            return LatestReadingRow.allCases.count
        case .sensor:
            return sensorRows.count
        case .transmitter:
            return 1
        case .support:
            return 1
        case .dev:
            return DevRow.allCases.count
        }
    }
    
    private lazy var glucoseFormatter: QuantityFormatter = {
        let formatter = QuantityFormatter()
        formatter.setPreferredNumberFormatter(for: glucoseUnit)
        return formatter
    }()
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .long
        formatter.doesRelativeDateFormatting = true
        return formatter
    }()
    
    private lazy var sensorDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        formatter.doesRelativeDateFormatting = true
        return formatter
    }()
    
    private lazy var sessionLengthFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.day, .hour]
        formatter.unitsStyle = .full
        return formatter
    }()
    
    private lazy var transmitterLengthFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.day]
        formatter.unitsStyle = .full
        return formatter
    }()
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .alerts:
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className, for: indexPath) as! SettingsTableViewCell
            cell.textLabel?.text = LocalizedString("Alerts", comment: "The title text for the dexcom settings alerts cell")
            cell.detailTextLabel?.text = nil
            cell.accessoryType = .disclosureIndicator
            return cell
        case .lastReading:
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className, for: indexPath) as! SettingsTableViewCell
            let glucose = cgmManager.latestTransmitterData?.cgmReading
            
            switch LatestReadingRow(rawValue: indexPath.row)! {
            case .glucose:
                cell.setGlucose(glucose?.glucoseValue, unit: glucoseUnit, formatter: glucoseFormatter)
            case .date:
                cell.setGlucoseDate(glucose?.recordedSystemTime, formatter: dateFormatter)
                if glucose?.hasReliableGlucose != true {
                    cell.detailTextLabel?.text = SettingsTableViewCell.NoValueString
                }
            case .trend:
                cell.textLabel?.text = LocalizedString("Trend", comment: "Title describing glucose trend")
                if glucose?.hasReliableGlucose == true, let trendRate = glucose?.trendRate {
                    let glucoseUnitPerMinute = glucoseUnit.unitDivided(by: .minute())
                    // This seemingly strange replacement of glucose units is only to display the unit string correctly
                    let trendPerMinute = HKQuantity(unit: glucoseUnit, doubleValue: trendRate.doubleValue(for: glucoseUnitPerMinute))
                    if let formatted = glucoseFormatter.string(from: trendPerMinute, for: glucoseUnit) {
                        cell.detailTextLabel?.text = String(format: LocalizedString("%@/min", comment: "Format string for glucose trend per minute. (1: glucose value and unit)"), formatted)
                    } else {
                        cell.detailTextLabel?.text = SettingsTableViewCell.NoValueString
                    }
                } else {
                    cell.detailTextLabel?.text = SettingsTableViewCell.NoValueString
                }
            case .status:
                cell.textLabel?.text = LocalizedString("Status", comment: "Title describing CGM calibration and battery state")
                //TODO need proper conversion from enum values to human facing strings
                if let stateDescription = glucose?.algorithmState.rawValue, !stateDescription.isEmpty {
                    cell.detailTextLabel?.text = stateDescription
                } else {
                    cell.detailTextLabel?.text = SettingsTableViewCell.NoValueString
                }
            }
            return cell
        case .sensor:
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className, for: indexPath) as! SettingsTableViewCell
            var textColor = UIColor.black
            if #available(iOSApplicationExtension 13.0, iOS 13.0, *) {
                textColor = UIColor.label
            }
            cell.textLabel?.textColor = textColor
            switch sensorRows[indexPath.row] {
            case .calibrate:
                cell.textLabel?.text = LocalizedString("Calibrate", comment: "The title text for the calibrate cell")
                cell.textLabel?.textColor = UIColor.systemBlue
                cell.detailTextLabel?.text = nil
                cell.accessoryType = .disclosureIndicator
            case .lastCalibration:
                let calibration = cgmManager.latestTransmitterData?.lastCalibration
                cell.setGlucoseDate(calibration?.calibrationSystemTime, formatter: sensorDateFormatter)
                cell.textLabel?.text = LocalizedString("Last Calibration", comment: "Title describing last calibration time")
            case .insertionTime:
                cell.textLabel?.text = LocalizedString("Insertion Time", comment: "Title describing sensor insertion time")
                //the session start date and the insert date are the same, since sensors are single use
                if let sessionStartDate = cgmManager.latestTransmitterData?.session?.sessionStartSystemTime {
                    cell.detailTextLabel?.text = sensorDateFormatter.string(from: sessionStartDate)
                } else {
                    cell.detailTextLabel?.text = SettingsTableViewCell.NoValueString
                }
            case .sensorExpires:
                cell.textLabel?.text = LocalizedString("Session Expires", comment: "Title describing sensor session expires")
                if let sessionStartDate = cgmManager.latestTransmitterData?.session?.sessionStartSystemTime,
                    let sessionDurationInDays = cgmManager.latestTransmitterData?.sessionDurationDays,
                    let sessionExpirationDate = Calendar.current.date(byAdding: .day, value: sessionDurationInDays, to: sessionStartDate) {
                    cell.detailTextLabel?.text = sensorDateFormatter.string(from: sessionExpirationDate)
                } else {
                    cell.detailTextLabel?.text = SettingsTableViewCell.NoValueString
                }
            case .stopOrNewSensor:
                let cell = tableView.dequeueReusableCell(withIdentifier: TextButtonTableViewCell.className, for: indexPath) as! TextButtonTableViewCell
                if cgmManager.latestTransmitterData?.session == nil {
                    cell.textLabel?.text = LocalizedString("New sensor", comment: "Title text for the button to start a new sensor")
                    if !cgmManager.transmitterHasMoreSessions {
                        cell.isEnabled = false
                        cell.isUserInteractionEnabled = false
                    } else {
                        cell.isEnabled = true
                        cell.isUserInteractionEnabled = true
                    }
                } else {
                    cell.textLabel?.text = LocalizedString("Stop session", comment: "Title text for the button to stop the current session")
                }
                return cell
            }
            return cell
        case .transmitter:
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className, for: indexPath) as! SettingsTableViewCell
            cell.textLabel?.text = LocalizedString("Transmitter", comment: "The title text for the Dexcom settings transmitter cell")
            cell.detailTextLabel?.text = cgmManager.state.transmitterStaticInfo.transmitterId
            cell.accessoryType = .disclosureIndicator
            return cell
        case .support:
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className, for: indexPath) as! SettingsTableViewCell
            cell.textLabel?.text = String(format: LocalizedString("Get Help with Dexcom %1$@", comment: "The title text for the Dexcom settings help cell. (1: transmitter model)"), cgmManager.state.transmitterStaticInfo.dexcomTransmitterID.model ?? LocalizedString("CGM", comment: "Short form of Continuous Glucose Monitor"))
            cell.detailTextLabel?.text = nil
            cell.accessoryType = .disclosureIndicator
            return cell
        case .dev:
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className, for: indexPath) as! SettingsTableViewCell
            switch DevRow(rawValue: indexPath.row)! {
            case .sensorCode:
                let sensorCodeCell = tableView.dequeueReusableCell(withIdentifier: LabeledTextFieldTableViewCell.className, for: indexPath) as! LabeledTextFieldTableViewCell
                sensorCodeCell.titleLabel.text = LocalizedString("Sensor Code", comment: "The title text for the sensor code cell")
                sensorCodeCell.textField.placeholder = SettingsTableViewCell.NoValueString
                sensorCodeCell.maximumTextLength = 4
                sensorCodeCell.textField.keyboardType = UIKeyboardType.numberPad
                sensorCodeCell.delegate = self
                return sensorCodeCell
            case .startSession:
                cell.textLabel?.text = LocalizedString("Start Session", comment: "The title text for the start session cell")
                cell.detailTextLabel?.text = nil
            case .stopSession:
                cell.textLabel?.text = LocalizedString("Stop Session", comment: "The title text for the start session cell")
                cell.detailTextLabel?.text = nil
            case .delete:
                let cell = tableView.dequeueReusableCell(withIdentifier: TextButtonTableViewCell.className, for: indexPath) as! TextButtonTableViewCell
                
                cell.textLabel?.text = LocalizedString("Delete CGM", comment: "Title text for the button to remove a CGM from Loop")
                cell.textLabel?.textAlignment = .center
                cell.tintColor = UIColor.systemRed
                cell.isEnabled = true
                return cell
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch sections[section] {
        case .lastReading:
            return LocalizedString("Last Reading", comment: "Section title for dexcom setting last glucose reading")
        case .sensor:
            return LocalizedString("Sensor", comment: "Section title for sensor")
        case .transmitter:
            return LocalizedString("Transmitter", comment: "Section title for transmitter")
        case .support:
            return LocalizedString("Support", comment: "Section title for dexcom setting support")
        case .dev:
            return "Testing support while UI is built"
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch sections[section] {
        case .support:
            return LocalizedString("Opens the Support section", comment: "Section footer for dexcom setting support")
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        switch sections[indexPath.section] {
        case .alerts:
            return true
        case .lastReading:
            return false
        case .sensor:
            switch sensorRows[indexPath.row] {
            case .calibrate, .stopOrNewSensor:
                return true
            default:
                return false
            }
        case .transmitter:
            return true
        case .support:
            return true
        case .dev:
            return true
        }
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if self.tableView(tableView, shouldHighlightRowAt: indexPath) {
            return indexPath
        } else {
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch sections[indexPath.section] {
        case .alerts:
            performSegue(withIdentifier: "DexcomSettingsToAlerts", sender: self)
            break
        case .lastReading:
            break
        case .sensor:
            switch sensorRows[indexPath.row] {
            case .calibrate:
                let calibrationView = CalibrationView(viewModel: CalibrationViewModel(glucoseUnit: glucoseUnit,
                                                                                      completionHandler: { self.cgmManager.calibrate(value: $0) }))
                let calibrationController = UIHostingController(rootView: calibrationView)
                present(calibrationController, animated: true)
            case .stopOrNewSensor:
                if cgmManager.latestTransmitterData?.session == nil {
                    displayNewSensorVC()
                } else {
                    let confirmVC = UIAlertController(sessionStopHandler: {
                        self.cgmManager.stopSession()
                        self.displayNewSensorVC()
                    })
                    
                    present(confirmVC, animated: true) {
                        tableView.deselectRow(at: indexPath, animated: true)
                    }
                }
            default:
                break
            }
        case .transmitter:
            performSegue(withIdentifier: "DexcomSettingsToTransmitterDetails", sender: self)
        case .support:
            //TODO add UI and logic for dexcom support
            break
        case .dev:
            switch DevRow(rawValue: indexPath.row)! {
            case .startSession:
                cgmManager.startSession(SensorCode(rawValue: sensorCodeString ?? ""))
            case .stopSession:
                cgmManager.stopSession()
            case .delete:
                let confirmVC = UIAlertController(cgmDeletionHandler: {
                    self.cgmManager.closeCommunication()
                    self.cgmManager.notifyDelegateOfDeletion {
                        DispatchQueue.main.async {
                            self.complete()
                        }
                    }
                })
                
                present(confirmVC, animated: true) {
                    tableView.deselectRow(at: indexPath, animated: true)
                }
            default:
                break
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func displayNewSensorVC() {
        performSegue(withIdentifier: "DexcomSettingsToNewSensor", sender: self)
    }
}

// MARK: - Dexcom CGM Manager Observer

extension DexcomCGMSettingsViewController: DexcomCGMManagerObserver {
    func cgmManagerDidFindTransmitter(_ manager: DexcomCGMManager) {
        // NOP
    }
    
    func cgmManagerDidUpdateLatestTransmitterData(_ manager: DexcomCGMManager) {
        tableView.reloadData()
    }
}

// MARK: - UIAlertController

private extension UIAlertController {
    convenience init(cgmDeletionHandler handler: @escaping () -> Void) {
        self.init(
            title: nil,
            message: LocalizedString("Are you sure you want to delete this CGM?", comment: "Confirmation message for deleting a CGM"),
            preferredStyle: .actionSheet
        )
        
        addAction(UIAlertAction(
            title: LocalizedString("Delete CGM", comment: "Button title to delete CGM"),
            style: .destructive,
            handler: { (_) in
                handler()
        }
        ))
        
        let cancel = LocalizedString("Cancel", comment: "The title of the cancel action in an action sheet")
        addAction(UIAlertAction(title: cancel, style: .cancel, handler: nil))
    }
    
    convenience init(sessionStopHandler handler: @escaping () -> Void) {
        self.init(
            title: nil,
            message: LocalizedString("Are you sure you want to stop this session?", comment: "Confirmation message for stopping an existing session"),
            preferredStyle: .actionSheet
        )
        
        addAction(UIAlertAction(
            title: LocalizedString("Stop current session", comment: "Button title to stop current session"),
            style: .destructive,
            handler: { (_) in
                handler()
        }
        ))
        
        let cancel = LocalizedString("Cancel", comment: "The title of the cancel action in an action sheet")
        addAction(UIAlertAction(title: cancel, style: .cancel, handler: nil))
    }
}


private extension SettingsTableViewCell {
    func setGlucose(_ glucose: HKQuantity?, unit: HKUnit, formatter: QuantityFormatter) {
        textLabel?.text = LocalizedString("Glucose", comment: "Title describing glucose value")
        
        if let quantity = glucose, let formatted = formatter.string(from: quantity, for: unit) {
            detailTextLabel?.text = formatted
        } else {
            detailTextLabel?.text = SettingsTableViewCell.NoValueString
        }
    }
    
    func setGlucoseDate(_ date: Date?, formatter: DateFormatter) {
        textLabel?.text = LocalizedString("Date", comment: "Title describing glucose date")
        
        if let date = date {
            detailTextLabel?.text = formatter.string(from: date)
        } else {
            detailTextLabel?.text = SettingsTableViewCell.NoValueString
        }
    }
}

// MARK: - Text Field Tableview Cell Delegate

extension DexcomCGMSettingsViewController: TextFieldTableViewCellDelegate {
    public func textFieldTableViewCellDidBeginEditing(_ cell: TextFieldTableViewCell) {
        //NOP
    }
    
    public func textFieldTableViewCellDidEndEditing(_ cell: TextFieldTableViewCell) {
        print("textFieldTableViewCellDidEndEditing")
    }
    
    public func textFieldTableViewCellDidChangeEditing(_ cell: TextFieldTableViewCell) {
        if let sensorCode = cell.textField.text, sensorCode != "" {
            self.sensorCodeString = sensorCode
            if sensorCode.count == cell.maximumTextLength {
                cell.textField.resignFirstResponder()
            }
        } else {
            sensorCodeString = nil
        }
    }
}

// MARK: - Replacement Workflow Protocol

protocol ReplacementWorkflowProtocol {
    var replacementWorkflow: ReplacementWorkflow {get set}
}

enum ReplacementWorkflow {
    case replaceSensor
    case replaceTransmitterAndSensor
}
