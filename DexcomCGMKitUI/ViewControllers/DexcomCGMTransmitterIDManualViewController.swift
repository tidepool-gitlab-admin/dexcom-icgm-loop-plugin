//
//  DexcomCGMTransmitterIDManualViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-26.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import DexcomCGMKit

class DexcomCGMTransmitterIDManualViewController: UIViewController {
    
    var completionHandler: ((DexcomTransmitterID) -> Void)?
    
    @IBOutlet private var transmitterIDTextField: UITextField!
    
    private var saveButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveTapped(_:)))
        saveButton.isEnabled = false
        navigationItem.setRightBarButton(saveButton, animated: false)
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTapped(_:)))
        navigationItem.setLeftBarButton(cancelButton, animated: false)
        
        transmitterIDTextField.becomeFirstResponder()
    }
    
    @objc func saveTapped(_ sender: Any) {
        confirmSaveTransmitterID()
    }
    
    @objc func cancelTapped(_ sender: Any) {
        done()
    }
    
    private func confirmSaveTransmitterID() {
        let dialog = UIAlertController(title: LocalizedString("Confirm Entry", comment: "Title for confirm entry notification"), message: transmitterIDTextField.text, preferredStyle: .alert)
        
        dialog.addAction(UIAlertAction(title: LocalizedString("Confirm", comment: "Title for confirm button"), style: .default, handler: { (action: UIAlertAction!) in
            self.saveTransmitterID()
        }))
        
        dialog.addAction(UIAlertAction(title: LocalizedString("Cancel", comment: "Title for cancel button"), style: .cancel))
        
        present(dialog, animated: true, completion: nil)
    }
    
    private func saveTransmitterID() {
        if let transmitterID = transmitterIDTextField.text,
            DexcomTransmitterID(transmitterID) != nil {
            performSegue(withIdentifier: "TransmitterIDManualToInsertSensor", sender: self)
        } else {
            performSegue(withIdentifier: "unwindToEnterTransmitterID", sender: self)
        }
    }
    
    private func done() {
        if let nav = navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func textFieldDidUpate(_ sender:UITextField) {
        if let transmitterIDString = transmitterIDTextField.text,
            transmitterIDString.count == DexcomTransmitterID.length {
            saveButton.isEnabled = true
            transmitterIDTextField.resignFirstResponder()
        } else {
            saveButton.isEnabled = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMInsertSensorViewController:
            if let completionHandler = completionHandler,
                let transmitterIDString = transmitterIDTextField.text,
                let transmitterID = DexcomTransmitterID(transmitterIDString) {
                vc.completionHandler = {() -> Void in completionHandler(transmitterID)}
            }
        case let vc as DexcomCGMEnterTransmitterIDViewController:
            vc.state = .transmitterIDError
        default:
            break
        }
    }
}
