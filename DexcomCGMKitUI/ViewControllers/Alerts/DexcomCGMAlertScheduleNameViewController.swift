//
//  DexcomCGMAlertScheduleNameViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-11-29.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import HealthKit
import LoopKitUI
import DexcomCGMKit

class DexcomCGMAlertScheduleNameViewController: UITableViewController {
    
    @IBOutlet weak var scheduleName: UITextField!
    
    private var nextButton: UIBarButtonItem!
    
    var alertSchedule: DexcomAlertSchedule!
    
    var glucoseUnit: HKUnit!
    
    var completionHandler: ((DexcomAlertSchedule) -> Void)?
        
    static public func instantiateFromStoryboard() -> DexcomCGMAlertScheduleNameViewController {
        let storyboard = UIStoryboard(name: "DexcomAlerts", bundle: Bundle(for: DexcomCGMAlertScheduleNameViewController.self))
        let alertScheduleNameVC = storyboard.instantiateViewController(withIdentifier: "DexcomAlertScheduleName") as! DexcomCGMAlertScheduleNameViewController
        return alertScheduleNameVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scheduleName.addTarget(self, action: #selector(scheduleNameUpdated(_:)), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // moved this code here from viewDidLoad(), otherwise it will not work with SwiftUI
        nextButton = UIBarButtonItem(title: LocalizedString("Next", comment: "The title of a navigation bar button to process to the next step of the alert schedule workflow."), style: UIBarButtonItem.Style.plain, target: self, action: #selector(nextTapped(_:)))
        nextButton.isEnabled = false
        self.parent?.navigationItem.setRightBarButton(nextButton, animated: false)
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTapped(_:)))
        self.parent?.navigationItem.setLeftBarButton(cancelButton, animated: false)
        
        self.parent?.navigationItem.title = LocalizedString("Schedule Name", comment: "Title of the view for setting the alert schedule name")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scheduleName.becomeFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMAlertScheduleStartEndTimeViewController:
            vc.alertSchedule = alertSchedule
            vc.glucoseUnit = glucoseUnit
            vc.completionHandler = completionHandler
        default:
            break
        }
    }

    @objc func nextTapped(_ sender: Any) {
        saveScheduleName()
    }
    
    private func saveScheduleName() {
        alertSchedule.name = scheduleName.text!
        performSegue(withIdentifier: "AlertScheduleNameToStartEndTime", sender: self)
    }
    
    @objc func cancelTapped(_ sender: Any) {
        cancel()
    }
    
    @objc func scheduleNameUpdated(_ sender: Any) {
        if let scheduleName = scheduleName.text,
            scheduleName.count > 0 {
            nextButton.isEnabled = true
        } else {
            nextButton.isEnabled = false
        }
    }
    
    private func cancel() {
        if let nav = parent?.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return LocalizedString("Enter a name for your alert schedule.", comment: "The header of section for the alert schedule name.")
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return LocalizedString("For example, night or weekend.", comment: "The footer of section for the alert schedule name.")
    }

}
