//
//  DexcomCGMAlertSettingsViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-10-03.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import HealthKit
import LoopKit
import LoopKitUI
import DexcomCGMKit
import AlertsCore

class DexcomCGMAlertSettingsViewController: UITableViewController {

    private let defaultCellHeight: CGFloat = 44
    
    var completionHandler: ((DexcomAlertConfiguration) -> Void)?
    
    var alertConfiguration: DexcomAlertConfiguration!
    
    var glucoseUnit: HKUnit!
    
    private var displayEnableSection: Bool {
        return alertConfiguration.type.canDisable
    }
    
    private var displayDetailsSection: Bool {
        return displayRepeatRow || displayThresholdRow
    }
    
    private var displayRepeatRow: Bool {
        return alertConfiguration.type.canDelayRepeat
    }
    
    private var displayThresholdRow: Bool {
        return alertConfiguration.hasThresholdValue
    }
    
    private var isUrgentLowAlert: Bool {
        return alertConfiguration.type == DexcomAlertType.glucoseUrgentLow
    }
    
    private var valuePickerIndexPath: IndexPath?
    
    private var isThresholdPicker: Bool {
        return valuePickerIndexPath?.row == DetailsRow.threshold.rawValue + 1
    }
    
    private var isPickerVisable: Bool {
        return valuePickerIndexPath != nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = alertConfiguration.type.localizedViewTitle
        
        tableView.register(SwitchTableViewCell.self, forCellReuseIdentifier: SwitchTableViewCell.className)
        tableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: SettingsTableViewCell.className)
        tableView.register(AlertFrequencyPickerTableViewCell.self, forCellReuseIdentifier: AlertFrequencyPickerTableViewCell.className)
        tableView.register(GlucoseThresholdPickerTableViewCell.self, forCellReuseIdentifier: GlucoseThresholdPickerTableViewCell.className)
        tableView.register(GlucoseTrendThresholdPickerTableViewCell.self, forCellReuseIdentifier: GlucoseTrendThresholdPickerTableViewCell.className)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let completionHandler = completionHandler {
            completionHandler(alertConfiguration)
        }
        super.viewWillDisappear(animated)
    }

    @objc private func enabledChanged(_ sender: UISwitch) {
        alertConfiguration.enabled = sender.isOn
    }
    
    private lazy var glucoseFormatter: QuantityFormatter = {
        let formatter = QuantityFormatter()
        formatter.setPreferredNumberFormatter(for: glucoseUnit)
        return formatter
    }()
    
    // MARK: - Table view data source

    private var sections: [Section] {
        var sections: [Section] = [.sound]
        if displayDetailsSection {
            sections.insert(.details, at: sections.startIndex)
        }
        if displayEnableSection {
            sections.insert(.enable, at: sections.startIndex)
        }
        return sections
    }
    
    private enum Section: Int, CaseIterable {
        case enable
        case details
        case sound
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    private var detailsRows: [DetailsRow] {
        var detailsRows: [DetailsRow] = []
        if displayThresholdRow {
            detailsRows.append(.threshold)
        }
        if displayRepeatRow {
            detailsRows.append(.frequency)
        }
        if isPickerVisable {
            if isThresholdPicker {
                detailsRows.insert(.thresholdPicker, at: DetailsRow.thresholdPicker.rawValue)
            } else {
                detailsRows.append(.frequencyPicker)
            }
        }
        return detailsRows
    }
    
    private enum DetailsRow: Int, CaseIterable {
        case threshold
        case thresholdPicker
        case frequency
        case frequencyPicker
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .enable:
            return 1
        case .details:
            return detailsRows.count
        case .sound:
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch sections[section] {
        case .details:
            if isUrgentLowAlert {
                return LocalizedString("The Urgent Low Alarm notification level and repeat setting cannot be changed or turned off. Only the sound setting can be changed.", comment: "Footer in urgent low alert notify me below and repeat section")
            }
            return nil
        case .sound:
            switch alertConfiguration.type {
            case .glucoseUrgentLow:
                let threholdString = glucoseFormatter.string(from: alertConfiguration.threshold, for: glucoseUnit)!
                return String(format: alertConfiguration.type.localizedDescription, threholdString)
            default:
                return alertConfiguration.type.localizedDescription
            }
            
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let valuePickerIndexPath = valuePickerIndexPath,
            valuePickerIndexPath == indexPath {
            return ValuePickerTableViewCell.height
        } else {
            return defaultCellHeight
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .enable:
            let switchCell = tableView.dequeueReusableCell(withIdentifier: SwitchTableViewCell.className) as! SwitchTableViewCell
            switchCell.switch?.isOn = alertConfiguration.enabled
            switchCell.textLabel?.text = String(format: LocalizedString("%@ Alert", comment: "The title text for the alert switch cell (1: localized alert type)"), alertConfiguration.type.localizedTitle)
            switchCell.switch?.addTarget(self, action: #selector(enabledChanged(_:)), for: .valueChanged)
            return switchCell
        case .details:
            switch detailsRows[indexPath.row] {
            case .threshold:
                let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className) as! SettingsTableViewCell
                cell.textLabel?.text = alertConfiguration.type.localizedThresholdTitle
                switch alertConfiguration.type {
                case .glucoseRateRise, .glucoseRateFall:
                    // This seemingly strange replacement of glucose units is only to display the unit string correctly
                    let glucoseUnitPerMinute = glucoseUnit.unitDivided(by: .minute())
                    let thresholdPerMinute = HKQuantity(unit: glucoseUnit, doubleValue: alertConfiguration.threshold.doubleValue(for: glucoseUnitPerMinute))
                    if let formatted = glucoseFormatter.string(from: thresholdPerMinute, for: glucoseUnit) {
                        cell.detailTextLabel?.text = String(format: LocalizedString("%@/min", comment: "Format string for alert threshold per minute. (1: glucose value and unit)"), formatted)
                    }
                case .signalLoss:
                    let (hours, minutes) = TimeInterval(minutes: alertConfiguration.threshold.doubleValue(for: HKUnit.minute())).hoursAndMinutes
                    cell.detailTextLabel?.text = String(format: LocalizedString("%d hrs %d mins", comment: "The title text for the signal loss period cell (1: hours) (2: minutes)"), hours, minutes)
                default:
                    cell.detailTextLabel?.text = glucoseFormatter.string(from: alertConfiguration.threshold, for: glucoseUnit)
                }
                if isUrgentLowAlert {
                    cell.selectionStyle = .none
                    cell.isUserInteractionEnabled = false
                } else {
                    cell.selectionStyle = .default
                    cell.isUserInteractionEnabled = true
                }
                return cell
            case .frequency:
                let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className) as! SettingsTableViewCell
                let (hours, minutes) = alertConfiguration.snoozeFrequency.timeInterval.hoursAndMinutes
                cell.textLabel?.text = LocalizedString("Repeat", comment: "Alert repeat frequency descriptive text")
                cell.detailTextLabel?.text = String(format: LocalizedString("Every %d hrs %d mins", comment: "The title text for the repeat frequency cell (1: hours) (2: minutes)"), hours, minutes)
                if isUrgentLowAlert {
                    cell.selectionStyle = .none
                    cell.isUserInteractionEnabled = false
                } else {
                    cell.selectionStyle = .default
                    cell.isUserInteractionEnabled = true
                }
                return cell
            case .thresholdPicker:
                switch alertConfiguration.type {
                case .glucoseLow, .glucoseHigh, .glucoseUrgentLow:
                    let glucoseThresholdCell = tableView.dequeueReusableCell(withIdentifier: GlucoseThresholdPickerTableViewCell.className) as! GlucoseThresholdPickerTableViewCell
                    glucoseThresholdCell.set(delegate: self,
                                             thresholdType: (alertConfiguration.type == .glucoseHigh ? .high : .low),
                                             initialValue: alertConfiguration.threshold,
                                             preferredUnit: glucoseUnit,
                                             minValue: alertConfiguration.thresholdMinValue,
                                             maxValue: alertConfiguration.thresholdMaxValue)
                    return glucoseThresholdCell
                case .glucoseRateFall, .glucoseRateRise:
                    let glucoseTrendThresholdCell = tableView.dequeueReusableCell(withIdentifier: GlucoseTrendThresholdPickerTableViewCell.className) as! GlucoseTrendThresholdPickerTableViewCell
                    glucoseTrendThresholdCell.set(delegate: self,
                                                  trendType: (alertConfiguration.type == .glucoseRateRise ? .rising : .falling),
                                                  initialValue: alertConfiguration.threshold,
                                                  preferredUnit: glucoseUnit)
                    return glucoseTrendThresholdCell
                case .signalLoss:
                    let alertFrequencyCell = tableView.dequeueReusableCell(withIdentifier: AlertFrequencyPickerTableViewCell.className) as! AlertFrequencyPickerTableViewCell
                    let initialPickerValue = TimeInterval(minutes: alertConfiguration.threshold.doubleValue(for: HKUnit.minute()))
                    alertFrequencyCell.set(delegate: self,
                                           initialValue: initialPickerValue,
                                           minValue: TimeInterval(minutes: alertConfiguration.thresholdMinValue.doubleValue(for: HKUnit.minute())))
                    return alertFrequencyCell
                default:
                    fatalError("All user configurable alerts need to be handled: \(String(describing: alertConfiguration))")
                }
            case .frequencyPicker:
                let alertFrequencyCell = tableView.dequeueReusableCell(withIdentifier: AlertFrequencyPickerTableViewCell.className) as! AlertFrequencyPickerTableViewCell
                alertFrequencyCell.set(delegate: self,
                                       initialValue: alertConfiguration.snoozeFrequency.timeInterval)
                return alertFrequencyCell
            }
        case .sound:
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className) as! SettingsTableViewCell
            cell.textLabel?.text = LocalizedString("Sound", comment: "The title text for the alert sound cell")
            cell.detailTextLabel?.text = String(describing: alertConfiguration.sound)
            cell.accessoryType = .disclosureIndicator
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch sections[indexPath.section] {
        case .details:
            switch detailsRows[indexPath.row] {
            case .threshold, .frequency:
                tableView.beginUpdates()
                if let valuePickerIndexPath = valuePickerIndexPath,
                    valuePickerIndexPath.row - 1 == indexPath.row {
                    // dismiss value picker
                    tableView.deleteRows(at: [valuePickerIndexPath], with: .fade)
                    self.valuePickerIndexPath = nil
                } else {
                    if let valuePickerIndexPath = valuePickerIndexPath {
                        // dismiss the currently visable value picker
                        tableView.deleteRows(at: [valuePickerIndexPath], with: .fade)
                    }
                    // display the new value picker
                    let valuePickerIndexPath = indexPathToInsertPicker(indexPath)
                    tableView.insertRows(at: [valuePickerIndexPath], with: .fade)
                    tableView.deselectRow(at: valuePickerIndexPath, animated: true)
                    self.valuePickerIndexPath = valuePickerIndexPath
                }
                tableView.endUpdates()
            default:
                break
            }
        case .sound:
            performSegue(withIdentifier: "AlertSettingsToAlertSound", sender: nil)
        default:
            break
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func indexPathToInsertPicker(_ indexPath: IndexPath) -> IndexPath {
        if let valuePickerIndexPath = valuePickerIndexPath, valuePickerIndexPath.row < indexPath.row {
            return indexPath
        } else {
            return IndexPath(row: indexPath.row + 1, section: indexPath.section)
        }
    }

    func updateRows(at indexPath: IndexPath) {
        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
    }
}

//MARK: - Alert Frequency Picker Delegate

extension DexcomCGMAlertSettingsViewController: AlertFrequencyPickerDelegate {
    func didSelectFrequency(_ timeInterval: TimeInterval, sender: AlertFrequencyPickerTableViewCell) {
        switch alertConfiguration.type {
        case .signalLoss:
            alertConfiguration.threshold = HKQuantity(unit: HKUnit.minute(), doubleValue: timeInterval.minutes)
            let section = sections.firstIndex(of: .details)!
            let row = detailsRows.firstIndex(of: .threshold)!
            updateRows(at: IndexPath(row: row, section: section))
        default:
            alertConfiguration.snoozeFrequency = DexcomSnoozeFrequency(timeInterval: timeInterval)
            let section = sections.firstIndex(of: .details)!
            let row = detailsRows.firstIndex(of: .frequency)!
            updateRows(at: IndexPath(row: row, section: section))
        }
    }
}

//MARK: - Glucose Threshold Picker Delegate

extension DexcomCGMAlertSettingsViewController: GlucoseThresholdPickerDelegate {
    func didSelectGlucoseThreshold(_ threshold: HKQuantity) {
        alertConfiguration.threshold = threshold
        let section = sections.firstIndex(of: .details)!
        let row = detailsRows.firstIndex(of: .threshold)!
        updateRows(at: IndexPath(row: row, section: section))
    }
}

//MARK: - Glucose Trend Threshold Picker Delegate

extension DexcomCGMAlertSettingsViewController: GlucoseTrendThresholdPickerDelegate {
    func didSelectGlucoseTrendThreshold(_ threshold: HKQuantity) {
        alertConfiguration.threshold = threshold
        let section = sections.firstIndex(of: .details)!
        let row = detailsRows.firstIndex(of: .threshold)!
        updateRows(at: IndexPath(row: row, section: section))
    }
}
