//
//  DexcomCGMAlertScheduleStartEndTimeViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-11-29.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import HealthKit
import LoopKitUI
import DexcomCGMKit

class DexcomCGMAlertScheduleStartEndTimeViewController: UITableViewController {
        
    private var startTime: Date!
    
    private var endTime: Date!
    
    private let timePickerTimeZone = TimeZone.currentFixed
        
    var alertSchedule: DexcomAlertSchedule!
    
    var glucoseUnit: HKUnit!
    
    var completionHandler: ((DexcomAlertSchedule) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        tableView.register(DateAndDurationTableViewCell.nib(), forCellReuseIdentifier: DateAndDurationTableViewCell.className)
        
        let nextButton = UIBarButtonItem(title: LocalizedString("Next", comment: "The title of a navigation bar button to process to the next step of the alert schedule workflow."), style: UIBarButtonItem.Style.plain, target: self, action: #selector(nextTapped(_:)))
        navigationItem.setRightBarButton(nextButton, animated: false)
        
        startTime = Date()
        endTime = Date()
        
        navigationItem.title = LocalizedString("Schedule Time", comment: "Title of the view for setting the alert schedule start and end times")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMAlertScheduleDaysViewController:
            vc.alertSchedule = alertSchedule
            vc.glucoseUnit = glucoseUnit
            vc.completionHandler = completionHandler
        default:
            break
        }
    }

    @objc func nextTapped(_ sender: Any) {
        saveStartEndTime()
    }
    
    private func saveStartEndTime() {
        var calendar = Calendar.current
        calendar.timeZone = timePickerTimeZone
        alertSchedule.weeklySchedule.startTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: startTime)
        alertSchedule.weeklySchedule.endTimeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: endTime)
        performSegue(withIdentifier: "AlertScheduleStartEndTimeToDays", sender: self)
    }
    
    private func localizedFormattedDate(_ date: Date) -> String {
        return DateFormatter.localizedString(from: date, dateStyle: .none, timeStyle: .short, timeZone: timePickerTimeZone)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    private enum Rows: Int, CaseIterable {
        case startTime
        case endTime
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return LocalizedString("Set the start and end time for your alert schedule.", comment: "The header of section for the alert schedule start and end time.")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Rows.allCases.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DateAndDurationTableViewCell.className) as! DateAndDurationTableViewCell
        switch Rows(rawValue: indexPath.row)! {
        case .startTime:
            cell.titleLabel.text = LocalizedString("Start Time", comment: "Title of the scheduled alerts start time picker cell")
            cell.datePicker.isEnabled = true
            cell.datePicker.datePickerMode = .time
            cell.datePicker.minuteInterval = 1
            cell.datePicker.timeZone = timePickerTimeZone
            cell.date = startTime
            cell.dateLabel.text = localizedFormattedDate(startTime)
            cell.delegate = self
        case .endTime:
            cell.titleLabel.text = LocalizedString("End Time", comment: "Title of the scheduled alerts end time picker cell")
            cell.datePicker.isEnabled = true
            cell.datePicker.datePickerMode = .time
            cell.datePicker.minuteInterval = 1
            cell.datePicker.timeZone = timePickerTimeZone
            cell.date = endTime
            cell.dateLabel.text = localizedFormattedDate(endTime)
            cell.delegate = self
        }
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.endEditing(false)
        tableView.beginUpdates()
        hideDatePickerCells(excluding: indexPath)
        return indexPath
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.endUpdates()
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension DexcomCGMAlertScheduleStartEndTimeViewController: DatePickerTableViewCellDelegate {
    func datePickerTableViewCellDidUpdateDate(_ cell: DatePickerTableViewCell) {
        guard let durationPickerCell = cell as? DateAndDurationTableViewCell,
            let row = tableView.indexPath(for: cell)?.row else {
            return
        }
        
        durationPickerCell.dateLabel.text = localizedFormattedDate(durationPickerCell.date)
        
        switch Rows(rawValue: row)! {
        case .startTime:
            startTime = durationPickerCell.date
        case .endTime:
            endTime = durationPickerCell.date
        }
    }
}
