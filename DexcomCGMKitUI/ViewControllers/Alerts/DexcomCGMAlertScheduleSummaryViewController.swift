//
//  DexcomCGMAlertScheduleSummaryViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-12-02.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import HealthKit
import LoopKit
import LoopKitUI
import DexcomCGMKit

class DexcomCGMAlertScheduleSummaryViewController: UITableViewController {

    var alertSchedule: DexcomAlertSchedule!
    
    var glucoseUnit: HKUnit!
    
    private let timePickerTimeZone = TimeZone.currentFixed
    
    var completionHandler: ((DexcomAlertSchedule) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped(_:)))
        navigationItem.setRightBarButton(doneButton, animated: false)
        navigationItem.title = LocalizedString("Schedule Summary", comment: "Title of the view for setting the alert schedule summary")
        
        tableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: SettingsTableViewCell.className)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMAlertsViewController:
            vc.alertConfigurationManager.alertSchedule = alertSchedule
            break
        default:
            break
        }
    }
    
    @objc func doneTapped(_ sender: Any) {
        performSegue(withIdentifier: "UnwindToDexcomAlertSettings", sender: self)
        completionHandler?(alertSchedule)
    }
    
    private lazy var glucoseFormatter: QuantityFormatter = {
        let formatter = QuantityFormatter()
        formatter.setPreferredNumberFormatter(for: glucoseUnit)
        return formatter
    }()
    
    private func localizedFormattedDate(_ date: Date) -> String {
        return DateFormatter.localizedString(from: date, dateStyle: .none, timeStyle: .short, timeZone: timePickerTimeZone)
    }

    // MARK: - Table view data source

    private enum Sections: Int, CaseIterable {
        case name
        case alwaysSound
        case schedule
        case glucoseThresholds
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allCases.count
    }

    private enum ScheduleRows: Int, CaseIterable {
        case time
        case days
    }
    
    private enum GlucoseThresholdRows: Int, CaseIterable {
        case low
        case high
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Sections(rawValue: section)! {
        case .schedule:
            return ScheduleRows.allCases.count
        case .glucoseThresholds:
            return GlucoseThresholdRows.allCases.count
        default:
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className, for: indexPath) as! SettingsTableViewCell
        switch Sections(rawValue: indexPath.section)! {
        case .name:
            cell.textLabel?.text = LocalizedString("Name", comment: "The title text for the alert schedule name cell")
            cell.detailTextLabel?.text = alertSchedule.name
        case .alwaysSound:
            cell.textLabel?.text = LocalizedString("Always Sound", comment: "The title text for the always sound cell")
            cell.detailTextLabel?.text = alertSchedule.alwaysSoundEnabled ?
                LocalizedString("ON", comment: "Table cell value for alert schedule always sound") :
                LocalizedString("OFF", comment: "Table cell value for alert schedule always sound")
        case .schedule:
            switch ScheduleRows(rawValue: indexPath.row)! {
            case .time:
                cell.textLabel?.text = LocalizedString("Time", comment: "The title text for the alert schedule time cell")
                let startTime = alertSchedule.weeklySchedule.startTime()
                let endTime = alertSchedule.weeklySchedule.endTime()
                cell.detailTextLabel?.text = String(format: "%@ - %@",
                                                    localizedFormattedDate(startTime),
                                                    localizedFormattedDate(endTime))
            case .days:
                cell.textLabel?.text = LocalizedString("Days", comment: "The title text for the alert schedule days cell")
                cell.detailTextLabel?.text = alertSchedule.weeklySchedule.days.localizedValue
            }
        case .glucoseThresholds:
            switch GlucoseThresholdRows(rawValue: indexPath.row)! {
            case .low:
                cell.textLabel?.text = LocalizedString("Low", comment: "The title text for the alert schedule low glucose threshold cell")
                if let alertConfigurationLow = alertSchedule.configurations[.glucoseLow] {
                    cell.detailTextLabel?.text = glucoseFormatter.string(from: alertConfigurationLow.threshold, for: glucoseUnit)
                }
            case .high:
                cell.textLabel?.text = LocalizedString("High", comment: "The title text for the alert schedule high glucose threshold cell")
                if let alertConfigurationLow = alertSchedule.configurations[.glucoseHigh] {
                    cell.detailTextLabel?.text = glucoseFormatter.string(from: alertConfigurationLow.threshold, for: glucoseUnit)
                }
            }
        }
        cell.selectionStyle = .none
        return cell
    }

}
