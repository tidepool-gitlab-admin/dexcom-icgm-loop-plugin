//
//  DexcomCGMAlertScheduleAlwaysSoundViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-12-02.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import HealthKit
import LoopKitUI
import DexcomCGMKit

class DexcomCGMAlertScheduleAlwaysSoundViewController: UITableViewController {

    var alertSchedule: DexcomAlertSchedule!
    
    var glucoseUnit: HKUnit!
    
    var completionHandler: ((DexcomAlertSchedule) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nextButton = UIBarButtonItem(title: LocalizedString("Next", comment: "The title of a navigation bar button to process to the next step of the alert schedule workflow."), style: UIBarButtonItem.Style.plain, target: self, action: #selector(nextTapped(_:)))
        navigationItem.setRightBarButton(nextButton, animated: false)
        navigationItem.title = LocalizedString("Always Sound", comment: "Title of the view for setting the alert schedule always sound")
        
        tableView.register(SwitchTableViewCell.self, forCellReuseIdentifier: SwitchTableViewCell.className)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMAlertScheduleSummaryViewController:
            vc.alertSchedule = alertSchedule
            vc.glucoseUnit = glucoseUnit
            vc.completionHandler = completionHandler
        default:
            break
        }
    }
    
    @objc private func nextTapped(_ sender: Any) {
        performSegue(withIdentifier: "AlertScheduleAlwaysSoundToSummary", sender: self)
    }
    
    @objc private func alwaysSoundChanged(_ sender: UISwitch) {
        alertSchedule.alwaysSoundEnabled = sender.isOn
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return LocalizedString("Allow scheduled alerts to sound when silent or Do Not Disturb are on.", comment: "The header of section for the alert schedule always sound.")
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return LocalizedString("These can't be silenced \n*  Urgent Low\n*  Transmitter Failure\n*  Sensor Failure.", comment: "The footer of section for the alert schedule always sound.")
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SwitchTableViewCell.className, for: indexPath) as! SwitchTableViewCell
        cell.switch?.isOn = alertSchedule.alwaysSoundEnabled
        cell.textLabel?.text = LocalizedString("Always Sound", comment: "The title text for the always sound switch cell")
        cell.switch?.addTarget(self, action: #selector(alwaysSoundChanged(_:)), for: .valueChanged)
        return cell
    }

}
