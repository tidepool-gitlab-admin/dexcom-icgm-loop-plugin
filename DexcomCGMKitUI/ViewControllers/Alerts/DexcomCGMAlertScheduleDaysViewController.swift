//
//  DexcomCGMAlertScheduleDaysViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-12-02.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import HealthKit
import DexcomCGMKit

protocol DexcomCGMAlertScheduleDaysViewControllerDelegate: AnyObject {
    func didUpdateDays(_ days: Days)
}

class DexcomCGMAlertScheduleDaysViewController: UITableViewController {

    private var localizedWeekdaySymbols: [String]!
    
    private var nextButton: UIBarButtonItem!
    
    var alertSchedule: DexcomAlertSchedule!
    
    var glucoseUnit: HKUnit!
    
    var displayNextButton: Bool = true
    
    weak var delegate: DexcomCGMAlertScheduleDaysViewControllerDelegate?
    
    var completionHandler: ((DexcomAlertSchedule) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateNextButton()
        navigationItem.title = LocalizedString("Schedule Days", comment: "Title of the view for setting the alert schedule days")
        
        var calendar = Calendar.autoupdatingCurrent
        calendar.locale = Locale.autoupdatingCurrent
        localizedWeekdaySymbols = Days.localizedWeekdaySymbols
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMAlertScheduleGlucoseThresholdViewController:
            vc.thresholdType = .low
            vc.alertSchedule = alertSchedule
            vc.glucoseUnit = glucoseUnit
            vc.completionHandler = completionHandler
        default:
            break
        }
    }
    
    private func updateNextButton() {
        if displayNextButton {
            if nextButton == nil {
                nextButton = UIBarButtonItem(title: LocalizedString("Next", comment: "The title of a navigation bar button to process to the next step of the alert schedule workflow."), style: UIBarButtonItem.Style.plain, target: self, action: #selector(nextTapped(_:)))
                navigationItem.setRightBarButton(nextButton, animated: false)
            }
            nextButton.isEnabled = alertSchedule.weeklySchedule.days != .none
        }
    }
    
    @objc func nextTapped(_ sender: Any) {
        saveScheduleDays()
    }
    
    private func saveScheduleDays() {
        performSegue(withIdentifier: "AlertScheduleDaysToGlucoseLow", sender: self)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return LocalizedString("Set the days for your alert schedule.", comment: "The header of section for the alert schedule days.")
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localizedWeekdaySymbols.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlertScheduleWeekdayCell", for: indexPath) as UITableViewCell
        cell.textLabel?.text = String(format: LocalizedString("Every %@", comment: "The title text for the alert schedule day cell (1: weekday symbol)"), localizedWeekdaySymbols[indexPath.row])
        let day = Days.allCases[indexPath.row]
        if alertSchedule.weeklySchedule.days.contains(day) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let day = Days.allCases[indexPath.row]
        if alertSchedule.weeklySchedule.days.contains(day) {
            alertSchedule.weeklySchedule.days.remove(day)
        } else {
            alertSchedule.weeklySchedule.days.insert(day)
        }
        delegate?.didUpdateDays(alertSchedule.weeklySchedule.days)
        
        updateNextButton()
        tableView.beginUpdates()
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.reloadRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
    }
    
}
