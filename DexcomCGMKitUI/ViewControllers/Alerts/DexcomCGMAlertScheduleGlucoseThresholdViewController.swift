//
//  DexcomCGMAlertScheduleGlucoseThresholdViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-12-02.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import HealthKit
import HealthKit
import DexcomCGMKit

class DexcomCGMAlertScheduleGlucoseThresholdViewController: UITableViewController {
    
    private var alertConfiguration: DexcomAlertConfiguration!
    
    var thresholdType: GlucoseThresholdPickerTableViewCell.ThresholdType!
    
    var alertSchedule: DexcomAlertSchedule!
    
    var glucoseUnit: HKUnit!
    
    var completionHandler: ((DexcomAlertSchedule) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nextButton = UIBarButtonItem(title: LocalizedString("Next", comment: "The title of a navigation bar button to process to the next step of the alert schedule workflow."), style: UIBarButtonItem.Style.plain, target: self, action: #selector(nextTapped(_:)))
        navigationItem.setRightBarButton(nextButton, animated: false)
        
        switch thresholdType! {
        case .low:
            alertConfiguration = alertSchedule.configurations[.glucoseLow]
            navigationItem.title = LocalizedString("Low Alert", comment: "Title of the view for setting the alert schedule glucose low alert")
        case .high:
            alertConfiguration = alertSchedule.configurations[.glucoseHigh]
            navigationItem.title = LocalizedString("High Alert", comment: "Title of the view for setting the alert schedule glucose high alert")
        }
        
        tableView.register(GlucoseThresholdPickerTableViewCell.self, forCellReuseIdentifier: GlucoseThresholdPickerTableViewCell.className)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMAlertScheduleGlucoseThresholdViewController:
            vc.thresholdType = .high
            vc.alertSchedule = alertSchedule
            vc.glucoseUnit = glucoseUnit
            vc.completionHandler = completionHandler
        case let vc as DexcomCGMAlertScheduleAlwaysSoundViewController:
            vc.alertSchedule = alertSchedule
            vc.glucoseUnit = glucoseUnit
            vc.completionHandler = completionHandler
        default:
            break
        }
    }
    
    @objc func nextTapped(_ sender: Any) {
       saveGlucoseThreshold()
    }
    
    private func saveGlucoseThreshold() {
        switch thresholdType! {
        case .low:
            alertSchedule.configurations[.glucoseLow] = alertConfiguration
            performSegue(withIdentifier: "AlertScheduleGlucoseLowToGlucoseHigh", sender: self)
        case .high:
            alertSchedule.configurations[.glucoseHigh] = alertConfiguration
            performSegue(withIdentifier: "AlertScheduleGlucoseHighToAlwaysSound", sender: self)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch thresholdType! {
        case .low:
            return LocalizedString("During your schedule, you will be alerted when your sensor glucose reading falls below:", comment: "The title of the section for setting othe alert schedule low glucose alert threshold.")
        case .high:
            return LocalizedString("During your schedule, you will be alerted when your sensor glucose reading rises above:", comment: "The title of the section for setting othe alert schedule high glucose alert threshold.")
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ValuePickerTableViewCell.height
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GlucoseThresholdPickerTableViewCell.className) as! GlucoseThresholdPickerTableViewCell
        cell.set(delegate: self,
                 thresholdType: thresholdType,
                 initialValue: alertConfiguration.threshold,
                 preferredUnit: glucoseUnit,
                 minValue: alertConfiguration.thresholdMinValue,
                 maxValue: alertConfiguration.thresholdMaxValue)
        return cell
    }

}

//MARK: - Glucose Threshold Picker Delegate

extension DexcomCGMAlertScheduleGlucoseThresholdViewController: GlucoseThresholdPickerDelegate {
    func didSelectGlucoseThreshold(_ threshold: HKQuantity) {
        alertConfiguration.threshold = threshold
    }
}
