//
//  DexcomCGMAlertsViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-10-03.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import HealthKit
import LoopKit
import LoopKitUI
import DexcomCGMKit
import AlertsCore

class DexcomCGMAlertsViewController: UITableViewController {

    var alertConfigurationManager: DexcomAlertConfigurationManager!
    
    var glucoseUnit: HKUnit!
    
    private let timePickerTimeZone = TimeZone.currentFixed
        
    private var valuePickerIndexPath: IndexPath?
    
    private var isPickerVisable: Bool {
        return valuePickerIndexPath != nil
    }
    
    private var isStartTimePicker: Bool {
        return valuePickerIndexPath?.row == ScheduleDetailsRow.startTime.rawValue + 1
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        tableView.register(SwitchTableViewCell.self, forCellReuseIdentifier: SwitchTableViewCell.className)
        tableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: SettingsTableViewCell.className)
        tableView.register(LabeledTextFieldTableViewCell.nib(), forCellReuseIdentifier: LabeledTextFieldTableViewCell.className)
        tableView.register(DateAndDurationTableViewCell.nib(), forCellReuseIdentifier: DateAndDurationTableViewCell.className)
        tableView.register(TextButtonTableViewCell.self, forCellReuseIdentifier: TextButtonTableViewCell.className)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    @objc private func alwaysSoundChanged(_ sender: UISwitch) {
        alertConfigurationManager.alwaysSoundEnabled = sender.isOn
    }
    
    @objc private func scheduleEnabledChanged(_ sender: UISwitch) {
        if alertConfigurationManager.hasAlertSchedule {
            alertConfigurationManager.alertSchedule?.enabled = sender.isOn
            tableView.reloadData()
        } else {
            performSegue(withIdentifier: "AlertsToAlertScheduleName", sender: self)
        }
    }
    
    @objc private func scheduleAlwaysSoundChanged(_ sender: UISwitch) {
        alertConfigurationManager.alertSchedule?.alwaysSoundEnabled = sender.isOn
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMAlertSettingsViewController:
            let (alertConfiguration, isScheduledAlert) = sender as! (DexcomAlertConfiguration, Bool)
            vc.alertConfiguration = alertConfiguration
            vc.glucoseUnit = glucoseUnit
            vc.completionHandler = { [weak self] (configuration: DexcomAlertConfiguration) in
                if isScheduledAlert {
                    self?.alertConfigurationManager.updateAlertSchedule(withConfiguration: configuration)
                } else {
                    self?.alertConfigurationManager.updateMutableAlert(withConfiguration: configuration)
                }
                self?.tableView.reloadData()
            }
        case let vc as DexcomCGMAlertScheduleNameViewController:
            // create alert schedule to be populated in this workflow
            vc.alertSchedule = DexcomAlertSchedule.defaultSchedule()
            vc.glucoseUnit = glucoseUnit
        case let vc as DexcomCGMAlertScheduleDaysViewController:
            vc.alertSchedule = alertConfigurationManager.alertSchedule
            vc.glucoseUnit = glucoseUnit
            vc.displayNextButton = false
            vc.delegate = self
        default:
            break
        }
    }
    
    @IBAction func unwindToDexcomAlertSettingsViewController(segue: UIStoryboardSegue) { }

    private lazy var glucoseFormatter: QuantityFormatter = {
        let formatter = QuantityFormatter()
        formatter.setPreferredNumberFormatter(for: glucoseUnit)
        return formatter
    }()
    
    private func localizedFormattedDate(_ date: Date) -> String {
        return DateFormatter.localizedString(from: date, dateStyle: .none, timeStyle: .short, timeZone: timePickerTimeZone)
    }
    
    // MARK: - Table view data source

    private var sections: [Sections] {
        if let alertSchedule = alertConfigurationManager.alertSchedule,
            alertSchedule.enabled {
            return Sections.allCases
        } else {
            var sections = Sections.allCases
            sections.remove(at: sections.firstIndex(of: .scheduledAlerts)!)
            return sections
        }
    }
    
    private enum Sections: Int, CaseIterable {
        case alwaysSound
        case alerts
        case scheduleDetails
        case scheduledAlerts
        case resetAlerts
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    private enum AlertsRow: Int, CaseIterable {
        case urgentLow
        case low
        case high
        case rise
        case fall
        case signalLoss
        case noReadings
        
        var alertType: DexcomAlertType {
            switch self {
            case .urgentLow:
                return .glucoseUrgentLow
            case .low:
                return .glucoseLow
            case .high:
                return .glucoseHigh
            case .rise:
                return .glucoseRateRise
            case .fall:
                return .glucoseRateFall
            case .signalLoss:
                return .signalLoss
            case .noReadings:
                return .noReadings
            }
        }
    }
    
    private var scheduleDetailsRows: [ScheduleDetailsRow] {
        if let alertSchedule = alertConfigurationManager.alertSchedule,
            alertSchedule.enabled {
           return ScheduleDetailsRow.allCases
        } else {
            return [.enabled]
        }
    }
    
    private enum ScheduleDetailsRow: Int, CaseIterable {
        case enabled
        case alwaysSound
        case name
        case startTime
        case endTime
        case days
    }
    
    private var scheduledAlertsRows: [AlertsRow] {
        if let alertSchedule = alertConfigurationManager.alertSchedule,
            alertSchedule.enabled {
            var scheduledAlerts = AlertsRow.allCases
            scheduledAlerts.remove(at: scheduledAlerts.firstIndex(of: .signalLoss)!)
            scheduledAlerts.remove(at: scheduledAlerts.firstIndex(of: .noReadings)!)
            return scheduledAlerts
        } else {
            return []
        }
    }
        
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .alwaysSound:
            return 1
        case .alerts:
            return AlertsRow.allCases.count
        case .scheduleDetails:
            return scheduleDetailsRows.count
        case .scheduledAlerts:
            return scheduledAlertsRows.count
        case .resetAlerts:
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch sections[section] {
        case .scheduleDetails:
            return LocalizedString("Scheduled", comment: "The header of section for the alert schedule details.")
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch sections[section] {
        case .alwaysSound:
            return LocalizedString("Allow alerts to sound even when Silent or Do Not Distsurb are no. These can't be silenced: Urgent Low, Transmitter Failure, and Sensor Failure.", comment: "The footer of section for the always sound.")
        case .scheduleDetails:
            if !(alertConfigurationManager.alertSchedule?.enabled ?? false) {
                return LocalizedString("Allows you to schedule and customize a second group of alerts. For example, set different alert settings at night.", comment: "The footer of section for the alert schedule details.")
            }
            return nil
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .alwaysSound:
            let switchCell = tableView.dequeueReusableCell(withIdentifier: SwitchTableViewCell.className) as! SwitchTableViewCell
            switchCell.switch?.isOn = alertConfigurationManager.alwaysSoundEnabled
            switchCell.textLabel?.text = LocalizedString("Always Sound", comment: "The title text for the always sound switch cell")
            switchCell.switch?.addTarget(self, action: #selector(alwaysSoundChanged(_:)), for: .valueChanged)
            return switchCell
        case .alerts, .scheduledAlerts:
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className) as! SettingsTableViewCell
            var alertConfiguration: DexcomAlertConfiguration
            // the user will always selects a known alert
            let alertType = AlertsRow(rawValue: indexPath.row)!.alertType
            if sections[indexPath.section] == .alerts {
                alertConfiguration = alertConfigurationManager.mutableAlertConfiguration(forType: alertType)!
            } else {
                alertConfiguration = alertConfigurationManager.alertScheduleConfiguration(forType: alertType)!
            }
            
            cell.textLabel?.text = alertConfiguration.type.localizedTitle
            if !alertConfiguration.enabled {
                cell.detailTextLabel?.text = LocalizedString("Off", comment: "The description if the alert is disabled")
            } else if alertConfiguration.type == .glucoseRateRise ||
                alertConfiguration.type == .glucoseRateFall ||
                alertConfiguration.type == .signalLoss ||
                alertConfiguration.type == .noReadings {
                cell.detailTextLabel?.text = LocalizedString("On", comment: "The description if the alert is enabled")
            } else {
                cell.detailTextLabel?.text = glucoseFormatter.string(from: alertConfiguration.threshold, for: glucoseUnit)
            }
            cell.accessoryType = .disclosureIndicator
            return cell
        case .scheduleDetails:
            switch scheduleDetailsRows[indexPath.row] {
            case .enabled:
                let switchCell = tableView.dequeueReusableCell(withIdentifier: SwitchTableViewCell.className) as! SwitchTableViewCell
                switchCell.switch?.isOn = alertConfigurationManager.alertSchedule?.enabled ?? false
                switchCell.textLabel?.text = LocalizedString("Alert Schedule", comment: "The title text for the alert schedule switch cell")
                switchCell.switch?.addTarget(self, action: #selector(scheduleEnabledChanged(_:)), for: .valueChanged)
                switchCell.selectionStyle = .none
                return switchCell
            case .alwaysSound:
                let switchCell = tableView.dequeueReusableCell(withIdentifier: SwitchTableViewCell.className, for: indexPath) as! SwitchTableViewCell
                switchCell.switch?.isOn = alertConfigurationManager.alertSchedule!.alwaysSoundEnabled
                switchCell.textLabel?.text = LocalizedString("Always Sound", comment: "The title text for the always sound switch cell")
                switchCell.switch?.addTarget(self, action: #selector(scheduleAlwaysSoundChanged(_:)), for: .valueChanged)
                switchCell.selectionStyle = .none
                return switchCell
            case .name:
                let cell = tableView.dequeueReusableCell(withIdentifier: LabeledTextFieldTableViewCell.className, for: indexPath) as! LabeledTextFieldTableViewCell
                cell.titleLabel.text = LocalizedString("Name", comment: "The title text for the sensor code cell")
                cell.textField.text = alertConfigurationManager.alertSchedule!.name
                cell.textField.placeholder = alertConfigurationManager.alertSchedule!.name
                cell.textField.keyboardType = UIKeyboardType.alphabet
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
            case .startTime:
                let cell = tableView.dequeueReusableCell(withIdentifier: DateAndDurationTableViewCell.className) as! DateAndDurationTableViewCell
                cell.titleLabel.text = LocalizedString("Start Time", comment: "Title of the scheduled alerts start time picker cell")
                cell.datePicker.isEnabled = true
                cell.datePicker.datePickerMode = .time
                cell.datePicker.minuteInterval = 1
                cell.datePicker.timeZone = timePickerTimeZone
                cell.date = alertConfigurationManager.alertSchedule!.weeklySchedule.startTime()
                cell.dateLabel.text = localizedFormattedDate(cell.date)
                cell.delegate = self
                return cell
            case .endTime:
                let cell = tableView.dequeueReusableCell(withIdentifier: DateAndDurationTableViewCell.className) as! DateAndDurationTableViewCell
                cell.titleLabel.text = LocalizedString("End Time", comment: "Title of the scheduled alerts end time picker cell")
                cell.datePicker.isEnabled = true
                cell.datePicker.datePickerMode = .time
                cell.datePicker.minuteInterval = 1
                cell.datePicker.timeZone = timePickerTimeZone
                cell.date = alertConfigurationManager.alertSchedule!.weeklySchedule.endTime()
                cell.dateLabel.text = localizedFormattedDate(cell.date)
                cell.delegate = self
                return cell
            case .days:
                let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className) as! SettingsTableViewCell
                cell.textLabel?.text = LocalizedString("Days", comment: "Title of the scheduled alerts days cell")
                cell.detailTextLabel?.text = alertConfigurationManager.alertSchedule!.weeklySchedule.days.localizedValue
                cell.accessoryType = .disclosureIndicator
                return cell
            }
        case .resetAlerts:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextButtonTableViewCell.className, for: indexPath) as! TextButtonTableViewCell
            cell.textLabel?.text = LocalizedString("Reset Alert Settings", comment: "Title of button to reset the dexcom alerts")
            cell.isEnabled = true
            return cell
        }
    }
    
    public override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.endEditing(false)
        tableView.beginUpdates()
        hideDatePickerCells(excluding: indexPath)
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch sections[indexPath.section] {
        case .alerts, .scheduledAlerts:
            let alertType = AlertsRow(rawValue: indexPath.row)!.alertType
            if sections[indexPath.section] == .alerts {
                let alertConfiguration = alertConfigurationManager.mutableAlertConfiguration(forType: alertType)!
                performSegue(withIdentifier: "AlertsToAlertSettings", sender: (alertConfiguration: alertConfiguration, isScheduledAlert: false))
            } else {
                let alertConfiguration = alertConfigurationManager.alertScheduleConfiguration(forType: alertType)!
                performSegue(withIdentifier: "AlertsToAlertSettings", sender: (alertConfiguration: alertConfiguration, isScheduledAlert: true))
            }
        case .scheduleDetails:
            switch scheduleDetailsRows[indexPath.row] {
            case .days:
                performSegue(withIdentifier: "AlertsToDays", sender: self)
            default:
                break
            }
        case .resetAlerts:
            let confirmVC = UIAlertController(alertResetHandler: {
                self.alertConfigurationManager.resetAlerts()
                self.tableView.reloadData()
            })
            
            present(confirmVC, animated: true) {
                tableView.deselectRow(at: indexPath, animated: true)
            }
        default:
            break
        }
        
        tableView.endUpdates()
        tableView.deselectRow(at: indexPath, animated: true)
    }

}

// MARK: - Text Field Tableview Cell Delegate

extension DexcomCGMAlertsViewController: TextFieldTableViewCellDelegate {
    public func textFieldTableViewCellDidBeginEditing(_ cell: TextFieldTableViewCell) {
        //NOP
    }
    
    public func textFieldTableViewCellDidEndEditing(_ cell: TextFieldTableViewCell) {
        if let scheduleName = cell.textField.text,
            scheduleName != "" {
            alertConfigurationManager.alertSchedule!.name = scheduleName
        } else {
            cell.textField.text = alertConfigurationManager.alertSchedule!.name
        }
    }
    
    public func textFieldTableViewCellDidChangeEditing(_ cell: TextFieldTableViewCell) {
        //NOP
    }

}

// MARK: - Date Picker Tableview Cell Delegate

extension DexcomCGMAlertsViewController: DatePickerTableViewCellDelegate {
    func datePickerTableViewCellDidUpdateDate(_ cell: DatePickerTableViewCell) {
        guard let durationPickerCell = cell as? DateAndDurationTableViewCell,
            let row = tableView.indexPath(for: cell)?.row else {
            return
        }

        var calendar = Calendar.current
        calendar.timeZone = timePickerTimeZone
        let timeComponents = calendar.dateComponents(WeeklySchedule.timeComponents, from: durationPickerCell.date)
        
        switch scheduleDetailsRows[row] {
        case .startTime:
            alertConfigurationManager.alertSchedule!.weeklySchedule.startTimeComponents = timeComponents
        case .endTime:
            alertConfigurationManager.alertSchedule!.weeklySchedule.endTimeComponents = timeComponents
        default:
            break
        }
        durationPickerCell.dateLabel.text = localizedFormattedDate(durationPickerCell.date)
    }
}

// MARK: - Dexcom Alert Schedule Days Delegate

extension DexcomCGMAlertsViewController: DexcomCGMAlertScheduleDaysViewControllerDelegate {
    func didUpdateDays(_ days: Days) {
        alertConfigurationManager.alertSchedule?.weeklySchedule.days = days
        tableView.reloadRows(at: [IndexPath(row: ScheduleDetailsRow.days.rawValue, section: Sections.scheduleDetails.rawValue)], with: .none)
    }
}

// MARK: - UIAlertController

private extension UIAlertController {
    convenience init(alertResetHandler handler: @escaping () -> Void) {
        self.init(
            title: nil,
            message: LocalizedString("Do you want to reset alert settings? This will reset all alert settings to factory defaults", comment: "Confirmation message for resetting CGM alert settings"),
            preferredStyle: .actionSheet
        )
        
        addAction(UIAlertAction(
            title: LocalizedString("Reset Alert Settings", comment: "Button title to reset CGM alert settings"),
            style: .destructive,
            handler: { (_) in
                handler()
        }
        ))
        
        let cancel = LocalizedString("Cancel", comment: "The title of the cancel action in an action sheet")
        addAction(UIAlertAction(title: cancel, style: .cancel, handler: nil))
    }
}
