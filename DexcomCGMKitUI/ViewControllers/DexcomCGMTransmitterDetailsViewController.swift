//
//  DexcomCGMTransmitterDetailsViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-20.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import LoopKitUI
import DexcomCGMKit
import TransmitterCore

class DexcomCGMTransmitterDetailsViewController: UITableViewController {

    var cgmManager: DexcomCGMManager!
    
    static public func instantiateFromStoryboard(cgmManager: DexcomCGMManager) -> DexcomCGMTransmitterDetailsViewController {
        let storyboard = UIStoryboard(name: "DexcomCGMManager", bundle: Bundle(for: DexcomCGMTransmitterDetailsViewController.self))
        let transmitterDetailsVC = storyboard.instantiateViewController(withIdentifier: "DexcomCGMTransmitterDetailsViewController") as! DexcomCGMTransmitterDetailsViewController
        transmitterDetailsVC.cgmManager = cgmManager
        
        return transmitterDetailsVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: SettingsTableViewCell.className)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cgmManager.addObserver(self, queue: .main)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cgmManager.removeObserver(self)
    }

    @IBAction func unwindToDexcomTransmitterDetailsViewController(segue: UIStoryboardSegue) { }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMStopSensorViewController:
            vc.cgmManager = cgmManager
        case let vc as DexcomCGMSensorCodeViewController:
            vc.replaceSensorCompletionHandler = { [weak self] (sensorCode: SensorCode?) in
                self?.cgmManager.pendingSensorCode = sensorCode
            }
            vc.replaceTransmitterCompletionHandler = { [weak self] (transmitterID: DexcomTransmitterID) in
                self?.cgmManager.replaceTransmitter(transmitterID)
            }
            vc.replacementWorkflow = .replaceTransmitterAndSensor
        default:
            break
        }
    }
    
    private lazy var transmitterDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        formatter.doesRelativeDateFormatting = true
        return formatter
    }()
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    private var transmitterDetailRows: [TransmitterDetailRow] {
        if cgmManager.state.connectionState == .transmitterFound {
            return TransmitterDetailRow.allCases
        } else {
            return [.pair, .transmitterID]
        }
    }
    
    private enum TransmitterDetailRow: Int, CaseIterable {
        case pair
        case transmitterID
        case activatedOn
        case firmware
        case softwareNumber
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transmitterDetailRows.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.className, for: indexPath) as! SettingsTableViewCell
        let transmitterInfo = cgmManager.state.transmitterStaticInfo
        switch transmitterDetailRows[indexPath.row] {
        case .pair:
            cell.textLabel?.text = LocalizedString("Pair New", comment: "The title text for the pair new cell")
            cell.detailTextLabel?.text = nil
            cell.accessoryType = .disclosureIndicator
        case .transmitterID:
            if cgmManager.state.connectionState == .transmitterFound {
                cell.selectionStyle = .none
                cell.textLabel?.text = LocalizedString("SN", comment: "The title text for the serial number cell")
                cell.detailTextLabel?.text = transmitterInfo.transmitterId
            } else {
                cell.textLabel?.text = LocalizedString("Scanning for transmitter", comment: "The title text for the scanning for transmitter cell")
                cell.detailTextLabel?.text = transmitterInfo.transmitterId
                cell.selectionStyle = .none
            }
        case .activatedOn:
            cell.selectionStyle = .none
            cell.textLabel?.text = LocalizedString("Activated On", comment: "The title text for the activated on cell")
            if let activatedOn = transmitterInfo.activatedOn {
                cell.detailTextLabel?.text = transmitterDateFormatter.string(from: activatedOn)
            } else {
                cell.detailTextLabel?.text = SettingsTableViewCell.NoValueString
            }
        case .firmware:
            cell.selectionStyle = .none
            cell.textLabel?.text = LocalizedString("Firmware", comment: "The title text for the firmware cell")
            cell.detailTextLabel?.text = transmitterInfo.transmitterVersion
        case .softwareNumber:
            cell.selectionStyle = .none
            cell.textLabel?.text = LocalizedString("Software Number", comment: "The title text for the software number cell")
            cell.detailTextLabel?.text = "SW" + String(transmitterInfo.softwareNumber)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch transmitterDetailRows[indexPath.section] {
        case .pair:
            // replace existing transmitter
            if cgmManager.latestTransmitterData?.session != nil {
                // stop session before replacing transmitter
                performSegue(withIdentifier: "TransmitterDetailsToStopSensor", sender: self)
            } else {
                performSegue(withIdentifier: "TransmitterDetailsToSensorCode", sender: self)
            }
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Dexcom CGM Manager Observer

extension DexcomCGMTransmitterDetailsViewController: DexcomCGMManagerObserver {
    func cgmManagerDidFindTransmitter(_ manager: DexcomCGMManager) {
        // NOP
    }
    
    func cgmManagerDidUpdateLatestTransmitterData(_ manager: DexcomCGMManager) {
        tableView.reloadData()
    }
}
