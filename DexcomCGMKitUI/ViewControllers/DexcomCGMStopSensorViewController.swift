//
//  DexcomCGMStopSensorViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-26.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import DexcomCGMKit
import TransmitterCore

class DexcomCGMStopSensorViewController: UIViewController {

    var replacementWorkflow = ReplacementWorkflow.replaceTransmitterAndSensor
    
    var replaceSensorCompletionHandler: ((SensorCode?) -> Void)?

    var replaceTransmitterCompletionHandler: ((DexcomTransmitterID) -> Void)?
    
    var cgmManager: DexcomCGMManager!
    
    static public func instantiateFromStoryboard() -> DexcomCGMStopSensorViewController {
        let storyboard = UIStoryboard(name: "DexcomCGMManager", bundle: Bundle(for: DexcomCGMStopSensorViewController.self))
        let stopSensorVC = storyboard.instantiateViewController(withIdentifier: "DexcomCGMStopSensor") as! DexcomCGMStopSensorViewController
        return stopSensorVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMSensorCodeViewController:
            cgmManager.stopSession()
            vc.replacementWorkflow = replacementWorkflow
            vc.replaceSensorCompletionHandler = replaceSensorCompletionHandler
            vc.replaceTransmitterCompletionHandler = replaceTransmitterCompletionHandler
        default:
            break
        }
    }
}
