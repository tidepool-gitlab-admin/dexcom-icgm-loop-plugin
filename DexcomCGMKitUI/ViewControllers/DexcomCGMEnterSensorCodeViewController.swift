//
//  DexcomCGMEnterSensorCodeViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-16.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import DexcomCGMKit
import TransmitterCore

class DexcomCGMEnterSensorCodeViewController: UIViewController {

    var replaceSensorCompletionHandler: ((SensorCode?) -> Void)?
    
    var replaceTransmitterCompletionHandler: ((DexcomTransmitterID) -> Void)?

    var replacementWorkflow = ReplacementWorkflow.replaceSensor
    
    enum State {
        case normal
        case sensorCodeError
    }
    
    var state: State = .normal
    
    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        switch state {
        case .normal:
            title = LocalizedString("Sensor Code", comment: "Title of enter sensor code view")
            messageLabel.text = LocalizedString("ONLY enter Sensor Code from sensor you insert", comment:"Instructions on enter sensor code view")
        case .sensorCodeError:
            title = LocalizedString("Incorrect Sensor Code", comment: "Title of enter sensor code view after incorrect sensor code has been entered")
            messageLabel.text = LocalizedString("Code entered is not correct. \n\nTake photo of Sensor Code on sensor applicator again, or enter code manually.", comment:"Instructions on enter sensor code view after incorrect sensor code has been entered")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMSensorCodeManualViewController:
            vc.replaceSensorCompletionHandler = replaceSensorCompletionHandler
            vc.replaceTransmitterCompletionHandler = replaceTransmitterCompletionHandler
            vc.replacementWorkflow = replacementWorkflow
        case let vc as DexcomCGMSensorCodeCameraViewController:
            vc.replaceSensorCompletionHandler = replaceSensorCompletionHandler
            vc.replaceTransmitterCompletionHandler = replaceTransmitterCompletionHandler
            vc.replacementWorkflow = replacementWorkflow
        default:
            break
        }
    }
    
    @IBAction func unwindToEnterSensorCodeViewController(segue: UIStoryboardSegue) { }
}
