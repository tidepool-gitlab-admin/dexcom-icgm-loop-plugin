//
//  DexcomCGMTransmitterIDCameraViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-26.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import DexcomCGMKit

class DexcomCGMTransmitterIDCameraViewController: UIViewController {

    var completionHandler: ((DexcomTransmitterID) -> Void)?
    
    private(set) var transmitter2DBarcode: Dexcom2DBarcode?
    
    @IBOutlet weak var scannerView: Barcode2DScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !scannerView.isRunning {
            scannerView.startScanning()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMInsertSensorViewController:
            if let completionHandler = completionHandler,
                let transmitter2DBarcode = transmitter2DBarcode,
                let transmitterIDString = transmitter2DBarcode.code,
                let transmitterID = DexcomTransmitterID(transmitterIDString) {
                vc.completionHandler = { completionHandler(transmitterID) }
            }
        case let vc as DexcomCGMEnterTransmitterIDViewController:
            vc.state = .transmitterIDError
        default:
            break
        }
    }
    
    private func done() {
        if let transmitter2DBarcode = transmitter2DBarcode, transmitter2DBarcode.isValid() {
            performSegue(withIdentifier: "TransmitterIDCameraToInsertSensor", sender: self)
        } else {
            performSegue(withIdentifier: "unwindToEnterTransmitterID", sender: self)
        }
    }

}

// MARK: - 2D Barcode Scanner Delegate

extension DexcomCGMTransmitterIDCameraViewController: Barcode2DScannerViewDelegate {
    func scanningDidFail() {
        //TODO display message to user that scanning failed
    }
    
    func scanningSucceededWithCode(_ code: String?) {
        transmitter2DBarcode = Dexcom2DBarcode(.transmitter, code: code)
        done()
    }
    
    func scanningDidStop() {
        // NOP
    }
    
}

