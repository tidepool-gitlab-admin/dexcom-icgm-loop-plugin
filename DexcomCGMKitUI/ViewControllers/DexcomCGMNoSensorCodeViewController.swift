//
//  DexcomCGMNoSensorCodeViewController.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2019-09-16.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import UIKit
import DexcomCGMKit

class DexcomCGMNoSensorCodeViewController: UIViewController {

    var replaceSensorCompletionHandler: (() -> Void)?
    
    var replaceTransmitterCompletionHandler: ((DexcomTransmitterID) -> Void)?
    
    var replacementWorkflow = ReplacementWorkflow.replaceSensor
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as DexcomCGMInsertSensorViewController:
            vc.completionHandler = replaceSensorCompletionHandler
        case let vc as DexcomCGMEnterTransmitterIDViewController:
            vc.completionHandler = replaceTransmitterCompletionHandler
        default:
            break
        }
    }
    
    @IBAction func iUnderstandButtonPressed(_ sender: UIButton) {
        switch replacementWorkflow {
        case .replaceSensor:
            performSegue(withIdentifier: "NoSensorCodeToInsertSensor", sender: self)
        case .replaceTransmitterAndSensor:
            if let completionHandler = replaceSensorCompletionHandler {
                completionHandler()
            }
            performSegue(withIdentifier: "NoSensorCodeToTransmitterID", sender: self)
        }
    }
}
