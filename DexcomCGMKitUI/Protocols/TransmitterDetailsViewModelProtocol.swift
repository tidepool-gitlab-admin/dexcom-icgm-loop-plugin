//
//  TransmitterDetailsViewModelProtocol.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-14.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI
import DexcomCGMKit
import TransmitterCore

protocol TransmitterDetailsViewModelProtocol: ObservableObject {
    //TODO this is just a placeholder until workflows are moved to SwiftUI
    var cgmManager: DexcomCGMManager { get }
    var serialNumber: String? { get }
    var activatedOn: Date? { get }
    var firmware: String? { get }
    var softwareNumber: String? { get }
    var detailsAvailable: Bool { get }
    var sessionRunning: Bool { get set }
    var dateFormatter: DateFormatter { get }
    var newSensorCompletionHandler: (SensorCode?) -> Void { get }
    var newTransmitterCompletionHandler: (DexcomTransmitterID) -> Void { get }
    var stopSensorCompletionHandler: () -> Void { get }
}
