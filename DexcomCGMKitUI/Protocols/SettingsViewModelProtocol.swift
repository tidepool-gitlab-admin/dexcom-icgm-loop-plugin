//
//  SettingsViewModelProtocol.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-14.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import HealthKit
import DexcomCGMKit
import TransmitterCore

protocol SettingsViewModelProtocol: ObservableObject {
    var cgmManager: DexcomCGMManager { get }
    var glucoseUnit: HKUnit { get }
    var transmitterID: String { get }
    var lastCalibrationDate: Date? { get }
    var sensorInsertionDate: Date? { get }
    var sensorExpirationDate: Date? { get }
    var lastGlucoseValueFormatted: String? { get }
    var lastGlucoseDate: Date? { get }
    var lastGlucoseTrendFormatted: String? { get }
    var sessionRunning: Bool { get }
    var dateFormatter: DateFormatter { get }
    var completionHandler: () -> Void { get }
    var calibrationCompletionHandler: (HKQuantity) -> Void { get }
    var newSensorCompletionHandler: (SensorCode?) -> Void { get }
    var stopSensorCompletionHandler: () -> Void { get }
    var deleteCGMManagerHandler: () -> Void { get }
}
