//
//  CalibrationViewModelProtocol.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-21.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import HealthKit

protocol CalibrationViewModelProtocol: ObservableObject {
    var glucoseUnit: HKUnit { get }
    var completionHandler: (HKQuantity) -> Void { get }
    func performCalibration(_ calibrationDouble: Double?) -> Bool
}

