//
//  NotificationListViewModelProtocol.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-02.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import HealthKit
import DexcomCGMKit

protocol NotificationListViewModelProtocol: ObservableObject {
    var configurations: [DexcomAlertConfiguration] { get set }
    var glucoseUnit: HKUnit { get }
    var configurationUpdateHandler: (DexcomAlertConfiguration) -> Void { get }
    func value(forConfiguration configuration: DexcomAlertConfiguration) -> String?
}
