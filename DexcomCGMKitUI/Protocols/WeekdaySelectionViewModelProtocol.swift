//
//  WeekdaySelectionViewModelProtocol.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-24.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import DexcomCGMKit

protocol WeekdaySelectionViewModelProtocol: ObservableObject {
    var selectedDays: Set<Days> { get set }
    var selectableDays: [Days] { get }
    var updateHandler: (Days) -> Void { get }
    var daysAsString: (Days) -> String { get }
}
