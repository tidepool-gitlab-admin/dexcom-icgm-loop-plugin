//
//  SoundViewModelProtocol.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-19.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import DexcomCGMKit

protocol SoundViewModelProtocol: ObservableObject {
    var updateHandler: (AlertSound) -> Void { get }
    var sounds: [String] { get }
    var selectedSound: String? { get set }
}
