//
//  HorizonalSizeClassOverride.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-03-27.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import SwiftUI

protocol HorizontalSizeClassOverride {
    var horizontalOverride: UserInterfaceSizeClass { get }
}

extension HorizontalSizeClassOverride {
    var horizontalOverride: UserInterfaceSizeClass {
        if UIScreen.main.bounds.height <= 640 {
            return .compact
        } else {
            return .regular
        }
    }
}
