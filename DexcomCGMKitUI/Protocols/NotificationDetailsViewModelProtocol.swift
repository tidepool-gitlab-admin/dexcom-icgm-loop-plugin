//
//  NotificationDetailsViewModelProtocol.swift
//  DexcomCGMKitUI
//
//  Created by Nathaniel Hamming on 2020-02-18.
//  Copyright © 2020 Tidepool Project. All rights reserved.
//

import Foundation
import HealthKit
import DexcomCGMKit

protocol NotificationDetailsViewModelProtocol: ObservableObject {
    var updateHandler: (DexcomAlertConfiguration) -> Void { get }
    var glucoseUnit: HKUnit { get }
    var configuration: DexcomAlertConfiguration { get set }
    var configurationDescription: String { get }
    var threshold: Double { get set }
    var repeatValue: TimeInterval { get set }
    var displaysDetails: Bool { get }
    var isUrgentLow: Bool { get }
    var soundUpdateHandler: (AlertSound) -> Void { get }

    func thresholdValues() -> [Double]
    
    func repeatValues() -> [TimeInterval]

    func formattedThreshold(_ threshold: Double) -> String?
    
    func formattedRepeat(_ repeatValue: TimeInterval) -> String
    
    func formattedSelectedRepeat(_ selectedRepeat: TimeInterval) -> String
}
