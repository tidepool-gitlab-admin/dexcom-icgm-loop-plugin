//
//  CGMConsecutiveLocalSessionPeriodPublisher.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-11-26.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Alerts

class CGMConsecutiveLocalSessionPeriodPublisher: Publisher<ConsecutiveLocalSessionPeriodUpdateType> { }
