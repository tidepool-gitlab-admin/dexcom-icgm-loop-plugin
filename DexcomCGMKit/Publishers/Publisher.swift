//
//  Publisher.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-11-26.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import DexcomCommon

class Publisher<DataType>: Publishing {
    
    var subscribers = [AnySubscriber<DataType>]()
    
    func subscribe(subscriber: AnySubscriber<DataType>) {
        subscribers.append(subscriber)
    }

    func unsubscribe(subscriber: AnySubscriber<DataType>) {
        subscribers = subscribers.filter({ $0.identifier != subscriber.identifier })
    }

    func unsubscribe(subscriberId: UUID) {
        subscribers = subscribers.filter({ $0.identifier != subscriberId })
    }
    
    func update(data: DataType) {
        for subscriber in subscribers {
            subscriber.update(data: data)
        }
    }
}
