//
//  TransmitterBleCommunicationProtocol.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-10-01.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation
import Transmitter
import TransmitterCore

public protocol TransmitterBleCommunicationProtocol {
    
    var connectionDelegate: ConnectionDelegate? { get set }
    
    var cgmDelegate: CgmDelegate? { get set }
    
    var debugMessageDelegate: DebugMessageDelegate? { get set }
    
    var cgmCommandDelegate: CgmCommandDelegate? { get set }
    
    var transmitterStatusDelegate: TransmitterStatusDelegate? { get set }
    
    var communicationStreamQueue: DispatchQueue { get set }
    
    func start() throws
    
    func close()
    
    func addCgmCommand(_ command: TransmitterCore.CgmCommand)
    
    func removeCgmCommand(_ command: TransmitterCore.CgmCommand)
    
}

extension TransmitterBleCommunication: TransmitterBleCommunicationProtocol { }
