//
//  DexcomCGMKit.h
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-08-20.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DexcomCGMKit.
FOUNDATION_EXPORT double DexcomCGMKitVersionNumber;

//! Project version string for DexcomCGMKit.
FOUNDATION_EXPORT const unsigned char DexcomCGMKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DexcomCGMKit/PublicHeader.h>


