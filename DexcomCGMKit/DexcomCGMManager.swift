//
//  DexcomCGMManager.swift
//  DexcomCGMPlugin
//
//  Created by Nathaniel Hamming on 2019-08-16.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation
import HealthKit
import UserNotifications
import LoopKit
import Logging
import LoggingCore
import Transmitter
import TransmitterCore
import TransmitterLoggingCore
import os.log

public protocol DexcomCGMManagerObserver: class {
    func cgmManagerDidUpdateLatestTransmitterData(_ manager: DexcomCGMManager)
    func cgmManagerDidFindTransmitter(_ manager: DexcomCGMManager)
}

public extension DexcomCGMManagerObserver {
    func cgmManagerDidUpdateLatestTransmitterData(_ manager: DexcomCGMManager) {
        //optional
    }
    func cgmManagerDidFindTransmitter(_ manager: DexcomCGMManager) {
        //optional
    }
}

public class DexcomCGMManager: CGMManager {
    
    private let logger = OSLog(category: "DexcomCGMManager")

    var transmitterCommunication: TransmitterBleCommunicationProtocol?
    
    var backgroundUpdateTask: UIBackgroundTaskIdentifier?
    
    private let observers = WeakSynchronizedSet<DexcomCGMManagerObserver>()
    
    private let cgmDelegate = WeakSynchronizedDelegate<CGMManagerDelegate>()
    
    private var lockedTransmitterData: Locked<TransmitterData?>
    
    private let jsonEncoder = JSONEncoder()
    
    public var latestTransmitterData: TransmitterData? {
        get {
            return lockedTransmitterData.value
        }
        set {
            lockedTransmitterData.value = newValue
            notifyObserversOfLatestTransmitterData()
        }
    }
    
    public static var managerIdentifier = "DexcomCGM"
    
    public var authenticationKeyServiceIdentifier: String? {
        guard let credentialStoragePrefix = cgmManagerDelegate?.credentialStoragePrefix(for: self) else {
            return nil
        }
        return credentialStoragePrefix + "." + DexcomCGMManager.managerIdentifier + ".keyService"
    }
    
    public var cgmManagerDelegate: CGMManagerDelegate? {
        get {
            return cgmDelegate.delegate
        }
        set {
            cgmDelegate.delegate = newValue
        }
    }
    
    public var delegateQueue: DispatchQueue! {
        get {
            return cgmDelegate.queue
        }
        set {
            cgmDelegate.queue = newValue
        }
    }
    
    public var providesBLEHeartbeat: Bool
    
    public var managedDataInterval: TimeInterval?
    
    public var shouldSyncToRemoteService: Bool
    
    public var transmitterHasMoreSessions: Bool {
        if let estimatedSessionsForTransmitter = estimatedSessionsForTransmitter(),
            estimatedSessionsForTransmitter > 0 {
            return true
        }
        return false
    }
    
    public var sensorState: SensorDisplayable? {
        return latestTransmitterData
    }
    
    public var device: HKDevice? {
        return HKDevice(
            name: type(of: self).managerIdentifier,
            manufacturer: "Dexcom",
            model: state.transmitterStaticInfo.dexcomTransmitterID.model,
            hardwareVersion: nil,
            firmwareVersion: state.transmitterStaticInfo.transmitterVersion,
            softwareVersion: String(DexcomCGMKitVersionNumber),
            localIdentifier: state.transmitterStaticInfo.transmitterId,
            udiDeviceIdentifier: nil
        )
    }
    
    public func fetchNewDataIfNeeded(_ completion: @escaping (CGMResult) -> Void) {
        //TODO just a placeholder to ensure frameworks are setup correctly
    }
    
    public var localizedTitle : String {
        get {
            if let model = state.transmitterStaticInfo.dexcomTransmitterID.model {
                return "Dexcom " + model
            } else {
                return DexcomCGMManager.localizedTitle
            }
        }
    }
    
    public static var localizedTitle = LocalizedString("Dexcom G6", comment: "Generic title of the Dexcom iCGM manager")

    public let alertConfigurationManager: DexcomAlertConfigurationManager
    
    private let newUUID: () -> UUID
    private let now: () -> Date
    
    public typealias FindTransmitterCommunication = (String, FindConfigurationProtocol) -> TransmitterBleCommunicationProtocol?
    public typealias ReconnectTransmitterCommunication = (TransmitterInfo, ReconnectConfigurationProtocol) -> TransmitterBleCommunicationProtocol?
    private let findNewTransmitterCommunication: FindTransmitterCommunication
    private let reconnectTransmitterCommunication: ReconnectTransmitterCommunication

    // Time interval before scanning for the transmitter gives up and alerts the user that it is not found
    static private let defaultTransmitterNotFoundTimeout = TimeInterval(minutes: 30)
    private let scanTimerTimeout: TimeInterval
    
    public init(state: DexcomCGMManagerState,
                // The remaining injected arguments are seams for testinng
                initialTransmitterCommunication: TransmitterBleCommunicationProtocol? = nil,
                findNewTransmitterCommunication: @escaping FindTransmitterCommunication = TransmitterBleClient.find,
                reconnectTransmitterCommunication: @escaping ReconnectTransmitterCommunication = TransmitterBleClient.reconnect,
                newUUIDFunc: @escaping () -> UUID = { UUID() },
                nowFunc: @escaping () -> Date = { Date() },
                scanTimerTimeout: TimeInterval? = nil) {
        self.state = state
        self.findNewTransmitterCommunication = findNewTransmitterCommunication
        self.reconnectTransmitterCommunication = reconnectTransmitterCommunication
        self.transmitterCommunication = initialTransmitterCommunication
        self.newUUID = newUUIDFunc
        self.now = nowFunc
        self.scanTimerTimeout = scanTimerTimeout ?? DexcomCGMManager.defaultTransmitterNotFoundTimeout
        lockedTransmitterData = Locked(nil)
        providesBLEHeartbeat = true
        shouldSyncToRemoteService = false
        alertConfigurationManager = DexcomAlertConfigurationManager(userMutableAlertConfigurations: state.userMutableAlertConfigurations ?? [:], alertSchedule: state.alertSchedule)
        alertConfigurationManager.delegate = self
        addObserver(alertConfigurationManager, queue: .main)
    }
    
    public convenience required init?(rawState: CGMManager.RawStateValue) {
        guard let state = DexcomCGMManagerState(rawValue: rawState) else {
            return nil
        }
        self.init(state: state)
        
        // loading from rawState, so start communications to reconnect to transmitter
        startCommunication()
    }
    
    public var rawState: CGMManager.RawStateValue {
        return state.rawValue
    }
    
    private(set) public var state: DexcomCGMManagerState {
        didSet {
            if self.state != oldValue {
                cgmDelegate.notify { delegate in
                    delegate?.cgmManagerDidUpdateState(self)
                }
            }
        }
    }
    
    public var debugDescription: String {
        return "Dexcom G6 Manager"
    }
    
    private var needsSessionStart: Bool = false
    
    public var pendingSensorCode: SensorCode? {
        didSet {
            needsSessionStart = true
        }
    }
    
    // MARK: - Transmitter communication
    
    public func startCommunication() {
        // need to assign the logging destination before connecting with transmitter
        TransmitterBleClient.logger = ActiveQueueLogger(destinations: [self])
        
        // close any open communications
        closeCommunication()
        
        let connectionState = state.connectionState
        logConnectionEvent("connectionState: \(connectionState)")

        switch connectionState {
        case .awaitingScan:
            scanForTransmitter()
        case .awaitingReconnect:
            reconnectToTransmitter()
        default:
            logger.default("startCommunication: no actions for connection state %{public}@", String(describing: connectionState))
        }
        precondition(transmitterCommunication != nil)
        // assign transmitter delegates
        transmitterCommunication?.connectionDelegate = self
        transmitterCommunication?.cgmDelegate = self
        transmitterCommunication?.cgmCommandDelegate = self
        transmitterCommunication?.debugMessageDelegate = self
        transmitterCommunication?.transmitterStatusDelegate = self
    }
    
    private func scanForTransmitter() {
        // Connection Info values are currently abritary/default
        let transmitterTime = TransmitterTimeStruct(seconds: 0)
        let diagConnectionData = DiagConnectionInfo(isEnabled: true,
                                                    isManifestDownloaded: false,
                                                    isEncryptionInfoDownloaded: false,
                                                    diagnosticResumeTimestamp: transmitterTime,
                                                    diagnosticCatchupTimestamp: transmitterTime,
                                                    minimumDiagnosticTime: nil)
        let findConfiguration = TransmitterFindConfiguration(diagnosticConnectionData: diagConnectionData,
                                                             backfillConnectionData: nil)
        transmitterCommunication = findNewTransmitterCommunication( state.transmitterStaticInfo.transmitterId, findConfiguration)
        transmitterCommunication?.communicationStreamQueue = cgmDelegate.queue
        updateConnectionState(.scanning)
        startScanningForTransmitter()
    }
    
    private func startScanningForTransmitter() {
        do {
            alertConfigurationManager.cancelTransmitterNotFoundNotification()
            try transmitterCommunication?.start()
            logConnectionEvent("communication started")
            // TODO: It would be helpful to log a "not found" when it happens, but it is unlikely the app will be
            // running after 30 minutes.  Is it worth it to set up a Timer anyway?
            alertConfigurationManager.scheduleTransmitterNotFoundNotification(timeout: scanTimerTimeout)
        } catch {
            // TODO display error to user
            logErrorEvent("communication start failed")
        }
    }

    private func reconnectToTransmitter() {
        guard let authenticationKey = state.transmitterAuthenticationKey,
            let keyTimestamp = state.transmitterAuthenticationKeyTimestamp else {
            
            logErrorEvent("invalid transmitter auth key")
            return
        }
        
        let transmitterTime = TransmitterTimeStruct(seconds: 0)
        let diagConnectionData = DiagConnectionInfo(isEnabled: true,
                                                    isManifestDownloaded: false,
                                                    isEncryptionInfoDownloaded: false,
                                                    diagnosticResumeTimestamp: transmitterTime,
                                                    diagnosticCatchupTimestamp: transmitterTime,
                                                    minimumDiagnosticTime: nil)
        let authConnectionData = AuthConnectionInfo(authenticationKey: authenticationKey,
                                                    authenticationKeyTimestamp: keyTimestamp)
        let backfillConnectionData = TransmitterBackfillConnectionData(isEnabled: true,
                                                                       cgmCache: state.backfillCache,
                                                                       minimumBackfillTime: nil)
        let reconnectConfiguration = TransmitterReconnectConfiguration(authenticationConnectionData: authConnectionData,
                                                                       diagnosticConnectionData: diagConnectionData,
                                                                       backfillConnectionData: backfillConnectionData)
        
        transmitterCommunication = reconnectTransmitterCommunication(state.transmitterStaticInfo,
                                                                     reconnectConfiguration)
        transmitterCommunication?.communicationStreamQueue = delegateQueue
        startScanningForTransmitter()
    }

    public func closeCommunication() {
        alertConfigurationManager.cancelTransmitterNotFoundNotification()
        if var transmitterCommunication = transmitterCommunication {
            transmitterCommunication.close()
            transmitterCommunication.cgmDelegate = nil
            transmitterCommunication.connectionDelegate = nil
            transmitterCommunication.cgmCommandDelegate = nil
            transmitterCommunication.debugMessageDelegate = nil
            transmitterCommunication.transmitterStatusDelegate = nil
            self.transmitterCommunication = nil
            logConnectionEvent("communication closed")
        } else {
            logger.default("closeCommunication: Transmitter Communication is already closed")
        }
    }
    
    public func replaceTransmitter(_ dexcomTransmitterID: DexcomTransmitterID) {
        logConnectionEvent("replacing transmitter with \(dexcomTransmitterID)")
        closeCommunication()
        state = DexcomCGMManagerState(dexcomTransmitterID: dexcomTransmitterID)
        resetState()
        startCommunication()
        logConnectionEvent("started communication with transmitter replacement")
    }
    
    private func resetState(justSession: Bool = false) {
        alertConfigurationManager.resetState()
        if justSession {
            latestTransmitterData?.session = nil
        } else {
            latestTransmitterData = nil
            state.emptyBackfillCache()
        }
    }
    
    private func completeTransmitterFoundActions() {
        alertConfigurationManager.cancelTransmitterNotFoundNotification()
        if needsSessionStart {
            startSession(pendingSensorCode)
            needsSessionStart = false
        }
    }

    private func updateConnectionState(_ newState: DexcomCGMManagerState.ConnectionState, function: StaticString = #function) {
        // We pass along the caller's function so we know who is updating the state in the logs
        logConnectionEvent(function: function, "connectionState: \(state.connectionState) ->  \(newState)")
        state.connectionState = newState
    }
    
    struct TransmitterTimeStruct: TransmitterTime {
        var seconds: UInt
    }
    
    struct DiagConnectionInfo: DiagnosticConnectionDataProtocol {
        var isEnabled: Bool
        var isManifestDownloaded: Bool
        var isEncryptionInfoDownloaded: Bool
        var diagnosticResumeTimestamp: TransmitterTime
        var diagnosticCatchupTimestamp: TransmitterTime
        var minimumDiagnosticTime: Date?
    }
    
    struct TransmitterFindConfiguration: FindConfigurationProtocol {
        var diagnosticConnectionData: DiagnosticConnectionDataProtocol?
        var backfillConnectionData: BackfillConnectionDataProtocol?
    }
    
    struct AuthConnectionInfo: AuthenticationConnectionDataProtocol {
        var authenticationKey: String
        var authenticationKeyTimestamp: Date
    }
    
    struct TransmitterBackfillConnectionData:  BackfillConnectionDataProtocol {
        var isEnabled: Bool
        var cgmCache: [TransmitterCgmReading]
        var minimumBackfillTime: Date?
    }
    
    struct TransmitterReconnectConfiguration: ReconnectConfigurationProtocol {
        var authenticationConnectionData: AuthenticationConnectionDataProtocol?
        var diagnosticConnectionData: DiagnosticConnectionDataProtocol?
        var backfillConnectionData: BackfillConnectionDataProtocol?
    }
}

//MARK: - Transmitter Log Destination

extension DexcomCGMManager: LogDestinationProtocol {
    public func log(payload: Loggable) {
        // currently just logging to the console, but will redirect as necessary
        
        let timeStampString = payload.timeStamp.description
        let logMessage: String
        let logable = payload.payload
        let jsonData: Data?
        
        switch logable {
        case let logPayload as BLEStateUpdateData:
            logMessage = "BLEStateUpdateData:" + timeStampString + ":" +  logPayload.transmitterId + ":" + logPayload.event
            jsonData = try? jsonEncoder.encode(logPayload)
        case let logPayload as TransmitterConnectData:
            logMessage  = "TransmitterConnectData:" + timeStampString + ":" +  logPayload.transmitterId + ":" + logPayload.deviceAddress
            jsonData = try? jsonEncoder.encode(logPayload)
        case let logPayload as TransmitterOnDisconnectData :
            logMessage  = "TransmitterOnDisconnectData:" + timeStampString + ":" +  logPayload.transmitterId + ":" + logPayload.connectLogMessage + " connectionDuration: " + String(logPayload.connectionDuration) + " rssi: " + String(describing: logPayload.rssi)
            jsonData = try? jsonEncoder.encode(logPayload)
        case let logPayload as TransmitterOnConnectData:
            logMessage  = "TransmitterOnConnectData:" + timeStampString + ":" +  logPayload.transmitterId + ": RSSI=" + String(logPayload.rssi)
            jsonData = try? jsonEncoder.encode(logPayload)
        case let logPayload as TransmitterScanData:
            logMessage  = "TransmitterScanData:" + timeStampString + ":" +  logPayload.transmitterId + ":" + logPayload.transmitterId
            jsonData = try? jsonEncoder.encode(logPayload)
        case let logPayload as TransmitterDisconnectData:
            logMessage  = "TransmitterDisconnectData:" + timeStampString + ":" +  logPayload.transmitterId + ":" + logPayload.reason
            jsonData = try? jsonEncoder.encode(logPayload)
        default:
            logMessage = "Error: Unknown log"
            jsonData = nil
        }
        
        logger.default("%{public}@", logMessage)
        
        if let jsonData = jsonData, let jsonString = String(data: jsonData, encoding: .utf8) {
            logConnectionEvent("Dexcom log: \(timeStampString): \(jsonString)")
        } else {
            logConnectionEvent("Dexcom log: \(timeStampString): unknown")
        }
    }
}

// MARK: - CGM Commands

extension DexcomCGMManager {
    public func startSession(_ sensorCode: SensorCode?) {
        guard let transmitterCommunication = transmitterCommunication else {
            logger.debug("Communication stream does not exist. Establish a connection with the transmitter before sending commands")
            return
        }
        
        guard transmitterHasMoreSessions else {
            logger.debug("No sessions remaining for this transmitter. Do not try to start a new sensor")
            return
        }
        
        let startSensorCmd = CgmCommand.start(uuid: newUUID(),
                                              systemTime: now(),
                                              transmitterTime: nil,
                                              code: sensorCode,
                                              response: nil)
        
        transmitterCommunication.addCgmCommand(startSensorCmd)
        
        // RSP TODO: should we make CgmCommand conform to Encodable so we can JSON-ify this as well?  Or is that overkill?
        logSendEvent("\(startSensorCmd)")
    }
    
    public func stopSession() {
        guard let transmitterCommunication = transmitterCommunication else {
            logger.debug("Communication stream does not exist. Establish a connection with the transmitter before sending commands")
            return
        }
        
        let stopSensorCmd = CgmCommand.stop(uuid: newUUID(),
                                            systemTime: now(),
                                            transmitterTime: nil,
                                            response: nil)
        transmitterCommunication.addCgmCommand(stopSensorCmd)
        resetState(justSession: true)
        logSendEvent("\(stopSensorCmd)")
    }
    
    public func calibrate(value:HKQuantity) {
        guard let transmitterCommunication = transmitterCommunication else {
            logger.debug("Communication stream does not exist. Establish a connection with the transmitter before sending commands")
            return
        }
        let valueInMgdl = value.doubleValue(for: HKUnit.milligramsPerDeciliter) // Dexcom SDK expects values in mg/dL
        let calibrateCmd = CgmCommand.calibrate(uuid: newUUID(),
                                                systemTime: now(),
                                                transmitterTime: nil,
                                                meterValue: Int32(valueInMgdl),
                                                response: nil)
        transmitterCommunication.addCgmCommand(calibrateCmd)
        logSendEvent("\(calibrateCmd)")
    }
}

// MARK: - CGM Command Delegate

extension DexcomCGMManager: CgmCommandDelegate {
    public func onCommandQueued(_ command: CgmCommand) {
        // TODO RSP: I bet this is duplicate to the above explicit calls to addCgmCommand, but this might be more
        // valuable if other commands are added (or are added by other actors)
        logDelegateEvent("\(command)")
    }
    
    public func onCommandCancelled(_ command: CgmCommand, _ reason: CgmCommandCancelReason) {
        logDelegateEvent("\(command) \(reason)")
    }
    
    public func onCommandCompleted(_ command: CgmCommand) {
        switch command {
        case .start( _, _, _, _, let response):
            logDelegateResponseEvent("start command response: \(String(describing: response))")
            if response == StartSensorResponseCode.transmitterEndOfLife {
                // transmitter at EOL
                alertConfigurationManager.triggerNoMoreSessionsNotification()
            }
        case .stop( _, _, _, let response):
            logDelegateResponseEvent("stop command response: \(String(describing: response))")
        case .calibrate( _, _, _, _, let response):
            logDelegateResponseEvent("calibrate command response: \(String(describing: response))")
        }
    }
}

// MARK: - CGM Delegate

extension DexcomCGMManager: CgmDelegate {
    public func onCgmData(_ cgmData: TransmitterCgmData) {
        // RSP TODO: Similar question to above: should we make TransmitterCgmData conform to Encodable so we can JSON-ify this as well?  Or is that overkill?
        logReceiveEvent("\(String(describing: cgmData))")

        if let oldCalibrationData = latestTransmitterData?.lastCalibration {
            // compare with new calibration value
            if let newCalibrationData = cgmData.lastCalibration,
                oldCalibrationData.calibrationTransmitterTime.seconds != newCalibrationData.calibrationTransmitterTime.seconds {
                storeCalibration(newCalibrationData, forTransmitter: cgmData.transmitterId)
            }
        } else if let newCalibrationData = cgmData.lastCalibration {
            storeCalibration(newCalibrationData, forTransmitter: cgmData.transmitterId)
        }
                
        // provide all the new CGM readings, include any potential backfill, to the CGMManagerDelegate in a single call
        var allCGMReadings = [cgmData.cgmReading]
        allCGMReadings.append(contentsOf: cgmData.backfillCgmReading)
        processCGMReadings(allCGMReadings)

        latestTransmitterData = TransmitterData(dexcomTransmitterID: DexcomTransmitterID(cgmData.transmitterId)!,
                                                session: cgmData.session,
                                                cgmReading: cgmData.cgmReading,
                                                backfillCgmReading: cgmData.backfillCgmReading,
                                                lastCalibration: cgmData.lastCalibration,
                                                calibrationBounds: cgmData.calibrationBounds,
                                                autoCalibration: cgmData.autoCalibration,
                                                sessionDurationDays: cgmData.sessionDurationDays,
                                                sensorWarmupDuration: cgmData.sensorWarmupDuration)
    }
    
    private func storeCalibration(_ calibration: TransmitterCalibration, forTransmitter transmitterID: String) {
        if calibration.isValid {
            publishGlucoseSamples([calibration.glucoseSample(fromDevice: device, withTransmitterID: transmitterID)])
        }
    }
    
    private func processNewCGMReadings(_ newCGMReadings: [DexcomCGMReading]) {
        var validGlucoseSamples: [NewGlucoseSample] = []
        newCGMReadings.forEach { cgmReading in
            if let validGlucoseSample = cgmReading.validGlucoseSample(fromDevice: device) {
                validGlucoseSamples.append(validGlucoseSample)
            }
        }
        publishGlucoseSamples(validGlucoseSamples)
        state.updateBackfillCache(newCGMReadings)
    }
    
    private func publishGlucoseSamples(_ glucoseSamples: [NewGlucoseSample]) {
        if !glucoseSamples.isEmpty {
            cgmDelegate.notify { delegate in
                delegate?.cgmManager(self, didUpdateWith: .newData(glucoseSamples))
            }
        }
    }
    
    private func processCGMReadings(_ allCGMReadings: [TransmitterCgmReading]) {
        if !allCGMReadings.isEmpty {
            var newCGMReadings = allCGMReadings.map { DexcomCGMReading($0) }
            // remove known readings from the transmitter provided backfill.
            newCGMReadings = newCGMReadings.filter { !state.backfillCache.contains($0) }
            processNewCGMReadings(newCGMReadings)
        }
    }
}

// MARK: - Connection Delegate

extension DexcomCGMManager: ConnectionDelegate {
    
    public func onConnect() {
        logConnectionEvent()
        // must begin background task to allow requests for more time to perform transmitter's communication
        backgroundUpdateTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            if let task = self?.backgroundUpdateTask {
                UIApplication.shared.endBackgroundTask(task)
                self?.backgroundUpdateTask = .invalid
            }
        }
        
        if state.connectionState != .transmitterFound &&
            state.transmitterStaticInfo.activatedOn != nil {
            // reconnected to transmitter. OnTransmitterFound will not be called
            updateConnectionState(.transmitterFound)
            completeTransmitterFoundActions()
        }
    }
    
    public func onDisconnect() {
        logConnectionEvent()
    }
    
    public func onAuthenticationConnectionData(_ authenticationConnectionData: AuthenticationConnectionDataProtocol) {
        logConnectionEvent()
        // about to update the authentication key of the state manager. Make sure a unique identifier is set
        if state.authenticationKeyServiceIdentifier == nil, let authenticationKeyServiceIdentifier = authenticationKeyServiceIdentifier {
            state.authenticationKeyServiceIdentifier = authenticationKeyServiceIdentifier
        }
        
        // persist authentication data to support reconnecting to a device
        state.transmitterAuthenticationKey = authenticationConnectionData.authenticationKey
        state.transmitterAuthenticationKeyTimestamp = authenticationConnectionData.authenticationKeyTimestamp
    }
    
    public func onDiagnosticConnectionData(_ diagnosticConnectionData: DiagnosticConnectionDataProtocol) {
        logDelegateEvent("\(diagnosticConnectionData)")
    }
    
    public func onTransmitterFound(_ transmitterInfo: TransmitterInfo) {
        logConnectionEvent("\(transmitterInfo)")

        // Use this information for staging re-connects to the same Transmitter.
        state.transmitterStaticInfo = TransmitterStaticInfo(dexcomTransmitterID: DexcomTransmitterID(transmitterInfo.transmitterId)!,
                                                            deviceUUID: transmitterInfo.deviceUUID,
                                                            transmitterVersion: transmitterInfo.transmitterVersion,
                                                            softwareNumber: transmitterInfo.softwareNumber,
                                                            apiVersion: transmitterInfo.apiVersion,
                                                            storageTimeDays: transmitterInfo.storageTimeDays,
                                                            maxStorageTimeDays: transmitterInfo.maxStorageTimeDays,
                                                            maxRuntimeDays: transmitterInfo.maxRuntimeDays,
                                                            sessionTimeDays: transmitterInfo.sessionTimeDays,
                                                            isDiagnosticDataDownloadSupported: transmitterInfo.isDiagnosticDataDownloadSupported,
                                                            isEgvBackfillSupported: transmitterInfo.isEgvBackfillSupported,
                                                            isPredictedEgvSupported: transmitterInfo.isPredictedEgvSupported,
                                                            activatedOn: now())
        updateConnectionState(.transmitterFound)
        completeTransmitterFoundActions()
        notifyObserversOfTransmitterFound()
    }
    
    public func onBackgroundBondRequestAlert() {
        logConnectionEvent()
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.isAppInBackground() {
                strongSelf.alertConfigurationManager.triggerTransmitterFoundNotification()
            }
        }
    }
    
    public func backgroundReconnectRequested() {
        logConnectionEvent()
        if let backgroundUpdateTask = backgroundUpdateTask {
            UIApplication.shared.endBackgroundTask(backgroundUpdateTask)
            self.backgroundUpdateTask = .invalid
        }
    }
    
    public func isAppInBackground() -> Bool {
        print("DexcomCGMManager isAppInBackground:")
        return UIApplication.shared.applicationState == UIApplication.State.background
    }
    
    public func onDiagnosticData(_ diagnosticData: DiagnosticData) {
        logDelegateEvent("\(diagnosticData)")
    }
}

// MARK: - Observer management

extension DexcomCGMManager {
    public func addObserver(_ observer: DexcomCGMManagerObserver, queue: DispatchQueue) {
        observers.insert(observer, queue: queue)
    }
    
    public func removeObserver(_ observer: DexcomCGMManagerObserver) {
        observers.removeElement(observer)
    }
    
    private func notifyObserversOfLatestTransmitterData() {
        observers.forEach { observer in
            observer.cgmManagerDidUpdateLatestTransmitterData(self)
        }
    }
    
    private func notifyObserversOfTransmitterFound() {
        observers.forEach { observer in
            observer.cgmManagerDidFindTransmitter(self)
        }
    }
}

// MARK: - Dexcom Alert Manager Delegate

extension DexcomCGMManager: DexcomAlertConfigurationManagerDelegate {
    
    public func estimatedSessionsForTransmitter() -> Int? {
        guard let transmitterEOL = state.transmitterStaticInfo.transmitterEOL else {
            return nil
        }
        
        guard state.transmitterStaticInfo.sessionDuration > 0 else {
            logger.error("state.transmitterStaticInfo.sessionDuration <= 0")
            return nil
        }
        
        let timeRemaining = transmitterEOL.timeIntervalSince(now())
        // calculate an estimate of the remaining sessions
        return Int(floor(timeRemaining/state.transmitterStaticInfo.sessionDuration))
    }
    
    public func alertConfigurationsUpdated(_ manager: DexcomAlertConfigurationManager, userMutableAlertConfigurations: [DexcomAlertType: DexcomAlertConfiguration], alertSchedule: DexcomAlertSchedule?) {
        state.userMutableAlertConfigurations = userMutableAlertConfigurations
        state.alertSchedule = alertSchedule
    }
    
    public func issueAlert(_ alert: DeviceAlert) {
        // TODO: do we still need these "notify"s?
        cgmDelegate.notify { delegate in
            // Only log this if trigger is "soon", otherwise it is misleading that this is logged now, instead of at trigger time.
            if case .immediate = alert.trigger {
                self.logDelegateEvent("\(alert.identifier.alertIdentifier)")
            }
            delegate?.issueAlert(alert)
        }
    }
    
    public func removePendingAlert(identifier: DeviceAlert.Identifier) {
        cgmDelegate.notify { delegate in
            self.logDelegateEvent("\(identifier.alertIdentifier)")
            delegate?.removePendingAlert(identifier: identifier)
        }
    }
    
    public func removeDeliveredAlert(identifier: DeviceAlert.Identifier) {
        cgmDelegate.notify { delegate in
            self.logDelegateEvent("\(identifier.alertIdentifier)")
            delegate?.removeDeliveredAlert(identifier: identifier)
        }
    }
    
    public func acknowledgeAlert(alertIdentifier: DeviceAlert.AlertIdentifier) {
        logEvent(type: .delegateResponse, message: "\(alertIdentifier) acknowledged")
        alertConfigurationManager.acknowledgeAlert(with: alertIdentifier)
    }
    
    public func onTransmitterFoundAcknowledged() {
        logConnectionEvent("connectionState is \(state.connectionState), transmitterCommunication is \(String(describing: transmitterCommunication))")
    }
    
    public func didSuspendAlert(alertIdentifier: Int, alertType: Int, activeAlarmingCount: Int?, suspended: Bool) {
        logDelegateEvent("did suspend alert: " +
            "id: \(String(describing: DexcomAlertType.init(rawValue: alertIdentifier))), " +
            "type: \(alertType), " +
            "active alarming count: \(String(describing: activeAlarmingCount)) " +
            "suspended \(suspended)"
        )
    }
    
    public func didAcknowledgeAlert(alertIdentifier: Int, alertType: Int, activeAlarmingCount: Int?, at time: Date) {
        logDelegateEvent("did acknowledge alert: " +
            "id: \(String(describing: DexcomAlertType.init(rawValue: alertIdentifier))), " +
            "type: \(alertType), " +
            "active alarming count: \(String(describing: activeAlarmingCount)) " +
            "at time \(time)"
        )
    }
    
}

// MARK: - Debug Message Delegate
extension DexcomCGMManager: DebugMessageDelegate {
    public func onCriticalError(_ reason: String) {
        logErrorEvent("reason: \(reason)")
    }
    
    public func onInfo(_ message: String) {
        logDelegateEvent(message)
    }
}

// MARK: - Transmitter Status Delegate
extension DexcomCGMManager: TransmitterStatusDelegate {
    public func onTransmitterLowBattery() {
        logDelegateEvent()
    }
    
    public func onTransmitterUnrecoverableError(_ statusCode: Int) {
        logErrorEvent("statusCode: \(statusCode)")
    }
}


// MARK: - Logging device comms for forensics
extension DexcomCGMManager {
    
    private func logConnectionEvent(function: StaticString = #function, _ message: String = "") {
        logEvent(function: function, type: .connection, message: message)
    }
    
    private func logSendEvent(function: StaticString = #function, _ message: String = "") {
        logEvent(function: function, type: .send, message: message)
    }
    
    private func logReceiveEvent(function: StaticString = #function, _ message: String = "") {
        logEvent(function: function, type: .receive, message: message)
    }

    private func logErrorEvent(function: StaticString = #function, _ message: String = "") {
        logEvent(function: function, type: .error, message: message)
    }
    
    private func logDelegateEvent(function: StaticString = #function, _ message: String = "") {
        logEvent(function: function, type: .delegate, message: message)
    }
    
    private func logDelegateResponseEvent(function: StaticString = #function, _ message: String = "") {
        logEvent(function: function, type: .delegateResponse, message: message)
    }
    
    private func logEvent(function: StaticString = #function, type: DeviceLogEntryType, message: String) {
        switch type {
        case .error:
            logger.error("%{public}@: %{public}@", function.description, message)
        default:
            logger.info("%{public}@: %{public}@", function.description, message)
        }
        cgmDelegate.notify { (delegate) in
            delegate?.deviceManager(self, logEventForDeviceIdentifier: DexcomCGMManager.managerIdentifier, type: type, message: "\(function): \(message)", completion: nil)
        }
    }
}
