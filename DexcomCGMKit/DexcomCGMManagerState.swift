//
//  DexcomCGMManagerState.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-08-30.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation
import LoopKit
import TransmitterCore

protocol SecurePersistAuthenticationKey {
    func setDexcomCGMTransmitterKey(_ key: String?, for keyService: String?) throws
    func getDexcomCGMTransmitterKey(for keyService: String?) -> String?
}

public struct DexcomCGMManagerState: RawRepresentable, Equatable {

    public typealias RawValue = CGMManager.RawStateValue
    
    public static let version = 1
    
    private enum DexcomCGMManagerStateKey {
        static let transmitterInfo = "transmitterInfo"
        static let transmitterKeyTimestamp = "transmitterKeyTimestamp"
        static let authenticationKeyServiceIdentifier = "authenticationKeyServiceIdentifier"
        static let userMutableAlertConfigurations = "userMutableAlertConfigurations"
        static let alertSchedule = "alertSchedule"
        static let backfillCache = "backfillCache"
    }
    
    static var keychainManager: () -> SecurePersistAuthenticationKey = { return KeychainManager() }
    
    public var authenticationKeyServiceIdentifier: String?
    
    public var transmitterStaticInfo: TransmitterStaticInfo
    
    public var connectionState: ConnectionState
    
    public var transmitterAuthenticationKeyTimestamp: Date?
    
    public var transmitterAuthenticationKey : String? {
        get {
            return DexcomCGMManagerState.keychainManager().getDexcomCGMTransmitterKey(for: self.authenticationKeyServiceIdentifier)
        }
        set {
            try? DexcomCGMManagerState.keychainManager().setDexcomCGMTransmitterKey(newValue, for: self.authenticationKeyServiceIdentifier)
        }
    }

    public var userMutableAlertConfigurations: [DexcomAlertType: DexcomAlertConfiguration]?
    
    public var alertSchedule: DexcomAlertSchedule?
    
    fileprivate(set) var backfillCache: [DexcomCGMReading] = []
    
    mutating func updateBackfillCache(_ backfillCache: [DexcomCGMReading]) {
        // only keep the last 3 hours (36 readings @ 5 min inteval) in the backfill cache, since the transmitter only backfills 3 hours
        let maxBackfillCacheLength = 36
        var tempBackfillCache = backfillCache
        if tempBackfillCache.count > maxBackfillCacheLength {
            tempBackfillCache = tempBackfillCache.sorted(by: { $0.egvTransmitterTime.seconds < $1.egvTransmitterTime.seconds })
            tempBackfillCache.removeFirst(tempBackfillCache.count - maxBackfillCacheLength)
        }
        self.backfillCache = tempBackfillCache
    }
    
    mutating func emptyBackfillCache() {
        self.backfillCache.removeAll()
    }
    
    public init(dexcomTransmitterID: DexcomTransmitterID, connectionState: ConnectionState = .awaitingScan) {
        transmitterStaticInfo = TransmitterStaticInfo(dexcomTransmitterID: dexcomTransmitterID,
                                                      deviceUUID: "",
                                                      transmitterVersion: "",
                                                      softwareNumber: 0,
                                                      apiVersion: 0,
                                                      storageTimeDays: 0,
                                                      maxStorageTimeDays: 0,
                                                      maxRuntimeDays: 0,
                                                      sessionTimeDays: 0,
                                                      isDiagnosticDataDownloadSupported: false,
                                                      isEgvBackfillSupported: false,
                                                      isPredictedEgvSupported: false,
                                                      activatedOn: nil)
        self.connectionState = connectionState
    }
    
    public init?(rawValue: RawValue) {
        guard let rawTransmitterInfo = rawValue[DexcomCGMManagerStateKey.transmitterInfo] as? Data,
            let transmitterStaticInfo = try? PropertyListDecoder().decode(TransmitterStaticInfo.self, from: rawTransmitterInfo) else {
                return nil
        }
        
        self.transmitterStaticInfo = transmitterStaticInfo
        
        if let transmitterAuthenticationKeyTimestamp = rawValue[DexcomCGMManagerStateKey.transmitterKeyTimestamp] as? Date {
            self.transmitterAuthenticationKeyTimestamp = transmitterAuthenticationKeyTimestamp
        }

        if let authenticationKeyServiceIdentifier = rawValue[DexcomCGMManagerStateKey.authenticationKeyServiceIdentifier] as? String {
            self.authenticationKeyServiceIdentifier = authenticationKeyServiceIdentifier
        }
        
        if let rawUserMutableAlertConfigurations = rawValue[DexcomCGMManagerStateKey.userMutableAlertConfigurations] as? Data,
            let userMutableAlertConfigurations = try? PropertyListDecoder().decode([DexcomAlertType: DexcomAlertConfiguration].self, from: rawUserMutableAlertConfigurations) {
            self.userMutableAlertConfigurations = userMutableAlertConfigurations
        }

        if let rawAlertSchedule = rawValue[DexcomCGMManagerStateKey.alertSchedule] as? Data,
            let alertSchedule = try? PropertyListDecoder().decode(DexcomAlertSchedule.self, from: rawAlertSchedule) {
            self.alertSchedule = alertSchedule
        }

        if let rawBackfillCache = rawValue[DexcomCGMManagerStateKey.backfillCache] as? Data,
            let backfillCache = try? PropertyListDecoder().decode([DexcomCGMReading].self, from: rawBackfillCache) {
            self.backfillCache = backfillCache
        }
        
        connectionState = .awaitingReconnect
        if transmitterStaticInfo.transmitterId.isEmpty || transmitterStaticInfo.deviceUUID.isEmpty {
            connectionState = .awaitingScan
        }
    }
    
    public var rawValue: RawValue {
        var raw: RawValue = [:]
        
        if let rawTransmitterInfo = try? PropertyListEncoder().encode(transmitterStaticInfo) {
            raw[DexcomCGMManagerStateKey.transmitterInfo] = rawTransmitterInfo
        }
        
        if let transmitterAuthenticationKeyTimestamp = transmitterAuthenticationKeyTimestamp {
            raw[DexcomCGMManagerStateKey.transmitterKeyTimestamp] = transmitterAuthenticationKeyTimestamp
        }
        
        if let authenticationKeyServiceIdentifier = authenticationKeyServiceIdentifier {
            raw[DexcomCGMManagerStateKey.authenticationKeyServiceIdentifier] = authenticationKeyServiceIdentifier
        }
        
        if let rawUserMutableAlertConfigurations = try? PropertyListEncoder().encode(userMutableAlertConfigurations) {
            raw[DexcomCGMManagerStateKey.userMutableAlertConfigurations] = rawUserMutableAlertConfigurations
        }

        if let rawAlertSchedule = try? PropertyListEncoder().encode(alertSchedule) {
            raw[DexcomCGMManagerStateKey.alertSchedule] = rawAlertSchedule
        }
        
        if let rawBackfillCache = try? PropertyListEncoder().encode(backfillCache) {
            raw[DexcomCGMManagerStateKey.backfillCache] = rawBackfillCache
        }
        return raw
    }
    
    public enum ConnectionState: String, Equatable {
        case awaitingScan
        case awaitingReconnect
        case scanning
        case transmitterFound
    }
}

extension KeychainManager: SecurePersistAuthenticationKey {
    func setDexcomCGMTransmitterKey(_ key: String?, for keyService: String?) throws {
        if let keyService = keyService {
            try replaceGenericPassword(key, forService: keyService)
        }
    }
    
    func getDexcomCGMTransmitterKey(for keyService: String?) -> String? {
        guard let keyService = keyService else {
            return nil
        }
        return try? getGenericPasswordForService(keyService)
    }
}

