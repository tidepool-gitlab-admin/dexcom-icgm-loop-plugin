//
//  DexcomAlertConfigurationManager.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-10-03.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import Foundation
import HealthKit
import UserNotifications
import LoopKit
import Alerts
import AlertsCore
import DexcomCommon
import TransmitterCore
import GlucoseCore

public protocol DexcomAlertConfigurationManagerDelegate: DeviceAlertPresenter {
    func alertConfigurationsUpdated(_ manager: DexcomAlertConfigurationManager, userMutableAlertConfigurations: [DexcomAlertType: DexcomAlertConfiguration], alertSchedule: DexcomAlertSchedule?)
    func estimatedSessionsForTransmitter() -> Int?
    func onTransmitterFoundAcknowledged()
    func didSuspendAlert(alertIdentifier: Int, alertType: Int, activeAlarmingCount: Int?, suspended: Bool)
    func didAcknowledgeAlert(alertIdentifier: Int, alertType: Int, activeAlarmingCount: Int?, at time: Date)
}

public class DexcomAlertConfigurationManager {
    
    // MARK - properties to schedule alerts
    
    private let alertsManager = AlertsManager()
    
    private let cgmReadingPublisher = CGMReadingPublisher()
    
    private let cgmSystemStatusPublisher = CGMSystemStatusPublisher()
    
    private var cgmConsecutiveReadingStatesPublisher = CGMConsecutiveReadingStatePublisher()
    
    private var cgmConsecutiveLocalSessionPeriodPublisher = CGMConsecutiveLocalSessionPeriodPublisher()
    
    private var currentTimePublisher = CurrentTimePublisher(currentTime: Date())
    
    private var currentPublishedSystemStatusData: CGMSystemStatus?
    
    private var transmitterFailedAlertCondition = TransmitterFailedAlertCondition()
    
    private var noReadingsAlertFirstDetectedAt: Date? = nil
    
    public var noReadingsAlertDelay: TimeInterval = TimeInterval(minutes: 20) // wait 20 minutes before triggering no readings alert
    
    // MARK - user configurable properties
    
    public var alwaysSoundEnabled: Bool {
        get {
            return userMutableAlerts.alwaysSoundEnabled
        }
        set {
            userMutableAlerts.alwaysSoundEnabled = newValue
            userStaticAlerts.alwaysSoundEnabled = newValue
        }
    }
    
    private var userMutableAlerts: DexcomUserMutableAlerts {
        didSet {
            delegate?.alertConfigurationsUpdated(self, userMutableAlertConfigurations: userMutableAlerts.configurations, alertSchedule: alertSchedule)
        }
    }
    
    public var alertSchedule: DexcomAlertSchedule? {
        didSet {
            delegate?.alertConfigurationsUpdated(self, userMutableAlertConfigurations: userMutableAlerts.configurations, alertSchedule: alertSchedule)
        }
    }
    
    // MARK - remaining properties and functions for class behavior
    
    weak var delegate: DexcomAlertConfigurationManagerDelegate? {
        didSet {
            if !userMutableAlerts.configurations.isEmpty ||
                hasAlertSchedule {
                delegate?.alertConfigurationsUpdated(self, userMutableAlertConfigurations: userMutableAlerts.configurations, alertSchedule: alertSchedule)
            }
        }
    }
    
    private var userStaticAlerts = DexcomUserStaticAlerts.defaultAlerts()
    
    public var alertScheduleActive = false
    
    public var hasAlertSchedule: Bool {
        return alertSchedule != nil
    }
    
    public init(userMutableAlertConfigurations: [DexcomAlertType: DexcomAlertConfiguration] = [:], alertSchedule: DexcomAlertSchedule? = nil) {
        if userMutableAlertConfigurations.isEmpty {
            // if no configurations were restored, create new ones
            userMutableAlerts = DexcomUserMutableAlerts.defaultAlerts()
        } else {
            userMutableAlerts = DexcomUserMutableAlerts(configurations: userMutableAlertConfigurations)
        }
        
        // restore always sound state
        alwaysSoundEnabled = userMutableAlerts.alwaysSoundEnabled
        
        self.alertSchedule = alertSchedule
        alertsManager.alertsManagerDelegate = self
        
        createAlerts()
    }
    
    public func resetState() {
        currentPublishedSystemStatusData = nil
    }
    
    public func resetAlerts() {
        userMutableAlerts = DexcomUserMutableAlerts.defaultAlerts()
        userMutableAlerts.configurations.values.forEach { updateAlert(withConfiguration: $0) }
        alertSchedule = nil
        alwaysSoundEnabled = userMutableAlerts.alwaysSoundEnabled
    }
    
    private func createAlerts() {
        createMutableAlerts()
        createStaticAlerts()
    }
    
    private func createMutableAlerts() {
        createAlerts(forConfigurations: userMutableAlerts.configurations)
    }
    
    private func createStaticAlerts() {
        createAlerts(forConfigurations: userStaticAlerts.configurations)
    }
    
    private func createAlerts(forConfigurations configurations: [DexcomAlertType: DexcomAlertConfiguration]) {
        configurations.values.forEach { configuration in
            if configuration.enabled {
                newAlert(fromConfiguration: configuration)
            }
        }
    }
    
    private func removeAlert(ofType type: DexcomAlertType) {
        if let alert = getAlert(forType: type) {
            alertsManager.remove(alert: alert)
        }
    }
    
    private func newAlert(fromConfiguration configuration: DexcomAlertConfiguration) {
        if let alert = createAlert(fromConfiguration: configuration) {
            alertsManager.add(alert: alert)
        }
    }
    
    private func createAlert(fromConfiguration configuration: DexcomAlertConfiguration) -> AlertProtocol? {
        currentTimePublisher.currentTime = Date()
        
        // To help ensure the alert fires on the correct connection interval, the configuration snooze frequency is adjusted from the user configuration
        switch configuration.type {
        case .glucoseUrgentLow:
            return GlucoseAlerts.urgentLow(configuration.type.id,
                                           configuration.alertFrequency,
                                           configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                           AnyPublisher(cgmReadingPublisher),
                                           currentTimePublisher.currentTime)
        case .glucoseLow:
            return GlucoseAlerts.low(configuration.type.id,
                                     Mgdl(configuration.threshold.doubleValue(for: HKUnit.milligramsPerDeciliter)),
                                     configuration.alertFrequency,
                                     configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                     AnyPublisher(cgmReadingPublisher),
                                     currentTimePublisher.currentTime)
        case .glucoseHigh:
            return GlucoseAlerts.high(configuration.type.id,
                                      Mgdl(configuration.threshold.doubleValue(for: HKUnit.milligramsPerDeciliter)),
                                      configuration.alertFrequency,
                                      configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                      AnyPublisher(cgmReadingPublisher),
                                      currentTimePublisher.currentTime)
        case .glucoseRateRise:
            return GlucoseAlerts.risingRate(configuration.type.id,
                                            configuration.threshold.doubleValue(for: HKUnit.milligramsPerDeciliterPerMinute),
                                            configuration.alertFrequency,
                                            configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                            AnyPublisher(cgmReadingPublisher),
                                            currentTimePublisher.currentTime)
        case .glucoseRateFall:
            return GlucoseAlerts.fallingRate(configuration.type.id,
                                             configuration.threshold.doubleValue(for: HKUnit.milligramsPerDeciliterPerMinute),
                                             configuration.alertFrequency,
                                             configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                             AnyPublisher(cgmReadingPublisher),
                                             currentTimePublisher.currentTime)
        case .noReadings:
            return SystemAlerts.noReadings(configuration.type.id,
                                           configuration.alertFrequency,
                                           configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                           AnyPublisher(cgmReadingPublisher),
                                           currentTimePublisher.currentTime)
        case .signalLoss:
            return SystemAlerts.signalLoss(configuration.type.id,
                                           configuration.alertFrequency,
                                           configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                           AnyPublisher(cgmSystemStatusPublisher),
                                           currentTimePublisher.currentTime)
        case .warmupComplete:
            return SystemAlerts.warmupComplete(configuration.type.id,
                                               configuration.alertFrequency,
                                               configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                               AnyPublisher(cgmConsecutiveReadingStatesPublisher),
                                               currentTimePublisher.currentTime)
        case .calibrationRequest:
            return SystemAlerts.calibrationRequest(configuration.type.id,
                                                   configuration.alertFrequency,
                                                   configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                                   AnyPublisher(cgmReadingPublisher),
                                                   currentTimePublisher.currentTime)
        case .firstCalibration:
            return SystemAlerts.firstCalibration(configuration.type.id,
                                                 configuration.alertFrequency,
                                                 configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                                 AnyPublisher(cgmReadingPublisher),
                                                 currentTimePublisher.currentTime)
        case .secondCalibration:
            return SystemAlerts.secondCalibration(configuration.type.id,
                                                  configuration.alertFrequency,
                                                  configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                                  AnyPublisher(cgmReadingPublisher),
                                                  currentTimePublisher.currentTime)
        case .calibrationRequired:
            return SystemAlerts.calibrationRequired(configuration.type.id,
                                                    configuration.alertFrequency,
                                                    configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                                    AnyPublisher(cgmReadingPublisher),
                                                    currentTimePublisher.currentTime)
        case .calibrationRequiredLater:
            return SystemAlerts.calibrationRequiredLater(configuration.type.id,
                                                         configuration.alertFrequency,
                                                         configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                                         AnyPublisher(cgmReadingPublisher),
                                                         currentTimePublisher.currentTime)
        case .sensorExpired:
            return SystemAlerts.sensorExpired(configuration.type.id,
                                              configuration.alertFrequency,
                                              configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                              AnyPublisher(cgmConsecutiveLocalSessionPeriodPublisher),
                                              currentTimePublisher.currentTime)
        case .sensorExpiresSoon24H:
            let timeRange24H = (TimeInterval(hours: 6.0)+1)...TimeInterval(hours: 24.0) // 6 hours 1 second -> 24 hours
            return SystemAlerts.sensorExpireSoon(configuration.type.id,
                                                 timeRange24H,
                                                 configuration.alertFrequency,
                                                 configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                                 AnyPublisher(cgmSystemStatusPublisher),
                                                 currentTimePublisher)
        case .sensorExpiresSoon6H:
            let timeRange6H = (TimeInterval(hours: 2.0)+1)...TimeInterval(hours: 6.0) // 2 hours 1 second -> 6 hours
            return SystemAlerts.sensorExpireSoon(configuration.type.id,
                                                 timeRange6H,
                                                 configuration.alertFrequency,
                                                 configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                                 AnyPublisher(cgmSystemStatusPublisher),
                                                 currentTimePublisher)
        case .sensorExpiresSoon2H:
            let timeRange2H = (TimeInterval(minutes: 30.0)+1)...TimeInterval(hours: 2.0) // 30 minutes 1 second -> 2 hours
            return SystemAlerts.sensorExpireSoon(configuration.type.id,
                                                 timeRange2H,
                                                 configuration.alertFrequency,
                                                 configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                                 AnyPublisher(cgmSystemStatusPublisher),
                                                 currentTimePublisher)
        case .sensorExpiresSoon30M:
            let timeRange30M = TimeInterval(seconds: 0.0)...TimeInterval(minutes: 30.0) // 0 seconds -> 30 minutes
            return SystemAlerts.sensorExpireSoon(configuration.type.id,
                                                 timeRange30M,
                                                 configuration.alertFrequency,
                                                 configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                                 AnyPublisher(cgmSystemStatusPublisher),
                                                 currentTimePublisher)
        case .sensorFailed:
            return SystemAlerts.sensorFailed(configuration.type.id,
                                             configuration.alertFrequency,
                                             configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                             AnyPublisher(cgmReadingPublisher),
                                             currentTimePublisher.currentTime)
        case .sensorFailedDueToRestart:
            return SystemAlerts.sensorFailedDueToRestart(configuration.type.id,
                                                         configuration.alertFrequency,
                                                         configuration.snoozeFrequency.adjustedForConnectionInterval(),
                                                         AnyPublisher(cgmReadingPublisher),
                                                         currentTimePublisher.currentTime)
        case .transmitterFailed:
            // transmitter failed alert. There is no convenience system alert initializer in the SDK for transmitter failed. It will be triggered in Loop code
            transmitterFailedAlertCondition.conditionChangedDelegate = self
            return DexcomAlert(configuration.type.id,
                               transmitterFailedAlertCondition,
                               nil,
                               configuration.alertFrequency,
                               configuration.snoozeFrequency.adjustedForConnectionInterval(),
                               currentTimePublisher.currentTime,
                               configuration.type.value)
        default:
            return nil
        }
    }
    
    private func deactivateScheduledAlerts() {
        if let alertSchedule = alertSchedule {
            // activate the corresponding mutable alerts
            let userMutableConfigurationsToActivate = userMutableAlerts.configurations.filter { (mutableAlertConfigType, _) in
                alertSchedule.configurations.contains { (alertScheduleConfigType, _) in
                    alertScheduleConfigType == mutableAlertConfigType
                }
            }
            userMutableConfigurationsToActivate.values.forEach { updateAlert(withConfiguration: $0) }
        }
        alertScheduleActive = false
    }
    
    private func activateScheduledAlerts() {
        if let alertSchedule = alertSchedule {
            alertSchedule.configurations.values.forEach { updateAlert(withConfiguration: $0) }
            alertScheduleActive = true
        }
    }
    
    private func updateAlert(withConfiguration configuration: DexcomAlertConfiguration) {
        if let alert = getAlert(forConfiguration: configuration) {
            alertsManager.remove(alert: alert)
        }
        
        if configuration.enabled {
            newAlert(fromConfiguration: configuration)
        }
        
        if configuration.type == DexcomAlertType.signalLoss {
            scheduleSignalLossAlert()
        }
    }
    
    public func updateMutableAlert(withConfiguration configuration: DexcomAlertConfiguration) {
        userMutableAlerts.configurations[configuration.type] = configuration
        updateAlert(withConfiguration: configuration)
    }
    
    public func updateAlertSchedule(withConfiguration configuration: DexcomAlertConfiguration) {
        alertSchedule?.configurations[configuration.type] = configuration
        if alertScheduleActive {
            updateAlert(withConfiguration: configuration)
        }
    }
    
    private var allActiveAlertConfigurations: [DexcomAlertType: DexcomAlertConfiguration] {
        var allActiveAlertConfigurations = userMutableAlerts.configurations
        allActiveAlertConfigurations.merge(userStaticAlerts.configurations) { (current, _) in current }
        if alertScheduleActive,
            let alertScheduleConfigurations = alertSchedule?.configurations {
            // allow scheduled alerts to override mutable alerts
            allActiveAlertConfigurations.merge(alertScheduleConfigurations) { (_, new) in new }
        }
        
        return allActiveAlertConfigurations
    }
    
    public func mutableAlertConfiguration(forType alertType: DexcomAlertType) -> DexcomAlertConfiguration? {
        return userMutableAlerts.configurations[alertType]
    }
    
    public func alertScheduleConfiguration(forType alertType: DexcomAlertType) -> DexcomAlertConfiguration? {
        guard let alertSchedule = alertSchedule else {
            return nil
        }
        return alertSchedule.configurations[alertType]
    }
    
    public func activeAlertConfiguration(forAlert alert: AlertProtocol) -> DexcomAlertConfiguration? {
        if let type = DexcomAlertType.type(forAlert: alert) {
            return allActiveAlertConfigurations[type]
        }
        return nil
    }
    
    private func getAlert(forConfiguration configuration: DexcomAlertConfiguration) -> AlertProtocol? {
        return alertsManager.alerts.first(where: { $0.identifier == configuration.type.id })
    }
    
    private func getAlert(forType alertType: DexcomAlertType) -> AlertProtocol? {
        return alertsManager.alerts.first(where: { $0.identifier == alertType.id })
    }
    
    public func newCGMReading(_ transmitterCGMReading: TransmitterCgmReading, forSession sessionPeriod: CGMSessionPeriodModel? = nil) {
        // update the current time publisher
        currentTimePublisher.currentTime = Date()
        let currentTime = currentTimePublisher.currentTime
        
        // reschedule the signal loss alert with each CGM data received
        scheduleSignalLossAlert()
        
        updateAlertScheduleActivation(egvSystemTime: transmitterCGMReading.egvSystemTime)
        
        guard checkForTransmitterError(transmitterCGMReading: transmitterCGMReading) == false else {
            return
        }
        
        // translate the cgm reading data to expected types
        let cgmReading = transmitterCGMReading.mappedToCGMReadingModel
        
        updateNoReadingsAlertState(cgmReading: cgmReading, currentTime: currentTime)
        
        // publish new cgm reading to trigger alerts
        cgmReadingPublisher.update(data: (cgmReading, cgmReading.timestamp))
        
        checkAfterWarmupCompleted(cgmReading: cgmReading, currentTime: currentTime, sessionPeriod: sessionPeriod)
        
        checkSensorExpired(for: sessionPeriod, currentTime: currentTime, transmitterId: transmitterCGMReading.transmitterId)
        
        publishSystemStatus(cgmReading: cgmReading, sessionPeriod: sessionPeriod, egvSystemTime: transmitterCGMReading.egvSystemTime)
    }
    
    private func updateAlertScheduleActivation(egvSystemTime: Date) {
        // check if scheduled alerts should be activated or deactivated based on the egv time
        guard let alertSchedule = alertSchedule else { return }
        if alertSchedule.enabled &&
            !alertScheduleActive &&
            alertSchedule.weeklySchedule.contains(egvSystemTime) {
            activateScheduledAlerts()
        } else if alertScheduleActive &&
            (!alertSchedule.weeklySchedule.contains(egvSystemTime) || !alertSchedule.enabled) {
            deactivateScheduledAlerts()
        }
    }
    
    private func checkForTransmitterError(transmitterCGMReading: TransmitterCgmReading) -> Bool {
        switch transmitterCGMReading.algorithmState {
        case .sessionFailedDueToTransmitterError:
            transmitterFailedAlertCondition.triggerAlert(conditionMet: true)
            return true
        default:
            transmitterFailedAlertCondition.triggerAlert(conditionMet: false)
            return false
        }
    }
    
    private func updateNoReadingsAlertState(cgmReading: CGMReadingModel, currentTime: Date) {
        // since the Dexcom SDK doesn't delay the initial no readings alert, this is handled here
        switch cgmReading.cgmReadingState {
        case .aberrationError(.noReadings):
            if noReadingsAlertFirstDetectedAt == nil {
                noReadingsAlertFirstDetectedAt = currentTime
            }
        default:
            // any communication without no reading error state resets the no reading delay
            noReadingsAlertFirstDetectedAt = nil
        }
    }
    
    private func checkAfterWarmupCompleted(cgmReading: CGMReadingModel, currentTime: Date, sessionPeriod: CGMSessionPeriodModel?) {
        guard let previousSystemStatusData = currentPublishedSystemStatusData else {
            return
        }
        
        let firstReading = previousSystemStatusData.cgmReading?.cgmReadingState
        let secondReading = cgmReading.cgmReadingState
        let consecutiveReadings: ConsecutiveCGMReadingStateType = (firstReading, secondReading)
        cgmConsecutiveReadingStatesPublisher.update(data: (consecutiveReadings, currentTime))
        
        // check if user should be notified of remaining sessions
        if firstReading != .warmup(.sensorWarmup),
            secondReading == .warmup(.sensorWarmup) {
            // session started
            triggerTransmitterEOLNotificationIfNeeded()
        } else if firstReading != .warmup(.sessionStopped),
            secondReading == .warmup(.sessionStopped),
            delegate?.estimatedSessionsForTransmitter() == 0 {
            // last session ended
            triggerTransmitterEOLNotificationIfNeeded()
        }
    }
    
    private func checkSensorExpired(for sessionPeriod: CGMSessionPeriodModel?, currentTime: Date, transmitterId: String) {
        guard let previousSystemStatusData = currentPublishedSystemStatusData else {
            return
        }
        
        if let previousCGMSessionPeriod = previousSystemStatusData.cgmSessionPeriod,
            sessionPeriod == nil || currentTime > sessionPeriod!.sessionStartTime.addingTimeInterval(Double(sessionPeriod!.sessionDuration)) {
            let firstLocalSessionPeriod = LocalSessionPeriod(source: .transmitter,
                                                             startTimestamp: previousCGMSessionPeriod.sessionStartTime,
                                                             transmitterId: transmitterId,
                                                             cgmSessionPeriod: previousCGMSessionPeriod)
            let secondLocalSessionPeriod = LocalSessionPeriod(source: .transmitter,
                                                              startTimestamp: currentTime,
                                                              transmitterId: transmitterId,
                                                              cgmSessionPeriod: sessionPeriod)
            let consecutiveLocalSessionPeriod = ConsecutiveLocalSessionPeriodType(firstLocalSessionPeriod, secondLocalSessionPeriod)
            cgmConsecutiveLocalSessionPeriodPublisher.update(data: (consecutiveLocalSessionPeriod, currentTime))
        }
    }
    
    private func publishSystemStatus(cgmReading: CGMReadingModel, sessionPeriod: CGMSessionPeriodModel?, egvSystemTime: Date) {
        let cgmSystemStatus = CGMSystemStatus(identifier: cgmReading.identifier,
                                              timestamp: cgmReading.timestamp,
                                              cgmSystemStatusType: .readingAvailable,
                                              cgmReading: cgmReading,
                                              cgmSessionPeriod: sessionPeriod)
        currentPublishedSystemStatusData = cgmSystemStatus
        
        if cgmReading.cgmReadingState != .warmup(.sessionStopped) &&
            cgmReading.cgmReadingState != .warmup(.sensorWarmup) {
            // publish current system status to detect when the session will expire
            // currentTime is set to the egvSystemTime, since the egvSystemTime is on the same transmitter timeline as cgmSystemStatus and egvSystemTime will always be earlier then Date()
            cgmSystemStatusPublisher.update(data: (systemStatus: cgmSystemStatus, currentTime: egvSystemTime))
        }
    }
    
    private func triggerTransmitterEOLNotificationIfNeeded() {
        guard let estimatedSessions = delegate?.estimatedSessionsForTransmitter(),
            estimatedSessions >= 0 else {
                return
        }
        var transmitterEOLAlertConfig: DexcomAlertConfiguration?
        switch estimatedSessions {
        case 0:
            transmitterEOLAlertConfig = DexcomAlertConfiguration(type: .sessionsNoMore, enabled: true, alwaysSoundEnabled: alwaysSoundEnabled, sound: .defaultSound)
        case 1:
            transmitterEOLAlertConfig = DexcomAlertConfiguration(type: .sessionStartedLast, enabled: true, alwaysSoundEnabled: alwaysSoundEnabled, sound: .defaultSound)
        case 2:
            transmitterEOLAlertConfig = DexcomAlertConfiguration(type: .sessionStartedSecondLast, enabled: true, alwaysSoundEnabled: alwaysSoundEnabled, sound: .defaultSound)
        case 3:
            transmitterEOLAlertConfig = DexcomAlertConfiguration(type: .sessionStartedThirdLast, enabled: true, alwaysSoundEnabled: alwaysSoundEnabled, sound: .defaultSound)
        default:
            break
        }
        if let transmitterEOLAlertConfig = transmitterEOLAlertConfig {
            scheduleAlert(for: transmitterEOLAlertConfig)
        }
    }
    
    public func triggerNoMoreSessionsNotification() {
        scheduleAlert(for: DexcomAlertConfiguration(type: .sessionsNoMore, enabled: true, alwaysSoundEnabled: alwaysSoundEnabled, sound: .defaultSound))
    }
    
    public func triggerTransmitterFoundNotification() {
        scheduleAlert(for: DexcomAlertConfiguration(type: .transmitterFound, enabled: true, alwaysSoundEnabled: alwaysSoundEnabled, sound: .defaultSound))
    }
    
    public func scheduleTransmitterNotFoundNotification(timeout: TimeInterval) {
        let trigger = DeviceAlert.Trigger.delayed(interval: timeout)
        scheduleAlert(for: DexcomAlertConfiguration(type: .transmitterNotFound, enabled: true, alwaysSoundEnabled: alwaysSoundEnabled, sound: .defaultSound), withTrigger: trigger)
    }
    
    public func cancelTransmitterNotFoundNotification() {
        cancelAlert(for: .transmitterNotFound)
    }
    
    private func scheduleSignalLossAlert() {
        // remove any pending notifications
        cancelAlert(for: .signalLoss)
        if let signalLossConfig = mutableAlertConfiguration(forType: DexcomAlertType.signalLoss),
            signalLossConfig.enabled {
            // the signal loss alert fires after .thresholdValue minutes of no transmitter communication
            let triggerDelayInterval = signalLossConfig.threshold.doubleValue(for: HKUnit.second())
            let trigger = triggerDelayInterval > 0 ? DeviceAlert.Trigger.repeating(repeatInterval: triggerDelayInterval) : .immediate
            scheduleAlert(for: signalLossConfig, withTrigger: trigger)
        }
    }
    
    public func acknowledgeAlert(with identifier: DeviceAlert.AlertIdentifier) {
        if let dexcomAlert = DexcomAlertType.type(forIdentifier: identifier), let alert = getAlert(forAlertID: dexcomAlert.id) {
            alert.acknowledge(at: Date())
            if dexcomAlert == .transmitterFound {
                delegate?.onTransmitterFoundAcknowledged()
            }
        }
    }
    
    private func getAlert(forAlertID alertID: Int) -> AlertProtocol? {
        return alertsManager.alerts.first(where: { $0.identifier == alertID })
    }
}

// MARK: - Alerts Manager Delegate

extension DexcomAlertConfigurationManager: AlertsManagerDelegate {
    public func alert(_ alert: AlertProtocol, shouldAlert: Bool) {
        if shouldAlert {
            if let alertConfiguration = activeAlertConfiguration(forAlert: alert) {
                if alertConfiguration.type == .noReadings {
                    // For initial detection, wait the alert delay before presenting the no reading alert to the user. The Dexcom SDK does not handle this delay
                    if let noReadingsAlertFirstDetectedAt = noReadingsAlertFirstDetectedAt,
                        abs(noReadingsAlertFirstDetectedAt.timeIntervalSinceNow) >= noReadingsAlertDelay {
                        self.scheduleAlert(for: alertConfiguration)
                    }
                } else {
                    scheduleAlert(for: alertConfiguration)
                }
            }
        }
    }
    
    public static let cgmAlertNotificationIdentifier = "Dexcom:\(LoopNotificationCategory.alert.rawValue)"
    
    private func scheduleAlert(for alertConfiguration: DexcomAlertConfiguration, withTrigger trigger: DeviceAlert.Trigger = .immediate) {
        delegate?.issueAlert(alertConfiguration.asUserAlert(with: trigger))
    }
    
    private func cancelAlert(for alertType: DexcomAlertType) {
        delegate?.removeDeliveredAlert(identifier: DeviceAlert.Identifier(managerIdentifier: DexcomCGMManager.managerIdentifier, alertIdentifier: alertType.identifier))
    }
    
    public func alert(_ alert: AlertProtocol, didSuspend: Bool) {
        delegate?.didSuspendAlert(alertIdentifier: alert.identifier, alertType: alert.alertType, activeAlarmingCount: alert.activeAlarmingCount(), suspended: didSuspend)
    }
    
    public func didAcknowledge(alert: AlertProtocol, at time: Date) {
        delegate?.didAcknowledgeAlert(alertIdentifier: alert.identifier, alertType: alert.alertType, activeAlarmingCount: alert.activeAlarmingCount(), at: time)
    }
}

extension DexcomAlertConfigurationManager: DexcomCGMManagerObserver {
    public func cgmManagerDidUpdateLatestTransmitterData(_ manager: DexcomCGMManager) {
        if let cgmData = manager.latestTransmitterData {
            // record new cgm reading and update alert publishers
            newCGMReading(cgmData.cgmReading, forSession: cgmData.sessionPeriod)
        }
    }
    
    public func cgmManagerDidFindTransmitter(_ manager: DexcomCGMManager) {
        triggerTransmitterEOLNotificationIfNeeded()
    }
}

extension DexcomAlertConfigurationManager: AlertConditionChangedDelegate {
    public func didUpdateCondition(conditionMet: Bool, currentTime: Date) {
        if transmitterFailedAlertCondition.alertConditionMet(),
            let transmitterFailedAlert = getAlert(forType: DexcomAlertType.transmitterFailed) {
            alert(transmitterFailedAlert, shouldAlert: true)
        }
    }
}

extension AlertConfiguration {
    public func asUserAlert(with trigger: DeviceAlert.Trigger) -> DeviceAlert {
        let userAlertInAppContent = type.hasInApp ? DeviceAlert.Content(title: type.localizedInAppTitle,
                                                                        body: type.localizedInAppMessage,
                                                                        acknowledgeActionButtonLabel: type.localizedDismissAction,
                                                                        isCritical: alwaysSoundEnabled) : nil
        let userAlertNotificationContent = type.hasUserNotification ? DeviceAlert.Content(title: type.localizedNotificationTitle,
                                                                                          body: type.localizedNotificationMessage,
                                                                                          acknowledgeActionButtonLabel: type.localizedDismissAction,
                                                                                          isCritical: alwaysSoundEnabled) : nil
        return DeviceAlert(identifier: DeviceAlert.Identifier(managerIdentifier: DexcomCGMManager.managerIdentifier,
                                                              alertIdentifier: type.identifier),
                           foregroundContent: userAlertInAppContent,
                           backgroundContent: userAlertNotificationContent,
                           trigger: trigger)
    }
}
