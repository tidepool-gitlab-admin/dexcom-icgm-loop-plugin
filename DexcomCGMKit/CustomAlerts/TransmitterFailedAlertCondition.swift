//
//  TransmitterFailedAlertCondition.swift
//  DexcomCGMKit
//
//  Created by Nathaniel Hamming on 2019-11-14.
//  Copyright © 2019 Tidepool Project. All rights reserved.
//

import AlertsCore

class TransmitterFailedAlertCondition: AlertConditionProtocol {
    weak var conditionChangedDelegate: AlertConditionChangedDelegate?
    
    private var conditionMet: Bool = false
    
    func triggerAlert(conditionMet: Bool) {
        if self.conditionMet != conditionMet {
            self.conditionMet = conditionMet
            conditionChangedDelegate?.didUpdateCondition(conditionMet: conditionMet, currentTime: Date())
        }
    }
    
    func alertConditionMet() -> Bool {
        return conditionMet
    }
}
