//
//  CurrentTimePublisher.swift
//  AlertsDemo
//
//  Created by Jenny Wu on 3/29/18.
//
//  Copyright (c) 2018 Dexcom, Inc.
//  Licenses to third-party material that may be incorporated into this software are listed at www.dexcom.com/notices
//

import Foundation
import Alerts

internal class CurrentTimePublisher: CurrentTimePublishing {
    var currentTime: Date {
        didSet {
            for subscriber in subscribers {
                subscriber.update(currentTime: currentTime)
            }
        }
    }
    
    var subscribers = [CurrentTimeSubscribing]()
    
    func addSubscriber(_ subscriber: CurrentTimeSubscribing) {
        subscribers.append(subscriber)
        subscriber.update(currentTime: currentTime)
    }
    
    func removeSubscriber(_ subscriber: CurrentTimeSubscribing) {
        subscribers = subscribers.filter({ $0.subscriberId != subscriber.subscriberId })
    }
    
    init(currentTime: Date) {
        self.currentTime = currentTime
    }
}
