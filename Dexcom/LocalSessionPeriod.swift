//
//  LocalSessionPeriod.swift
//  AlertsDemo
//
//  Created by Jenny Wu on 4/25/18.
//
//  Copyright (c) 2018 Dexcom, Inc.
//  Licenses to third-party material that may be incorporated into this software are listed at www.dexcom.com/notices
//

import Foundation
import GlucoseCore
import CgmSystemCore

internal struct LocalSessionPeriod: LocalSessionPeriodProtocol {
    
    var source: CgmSource
    
    var startTimestamp: Date
    
    var endTimestamp: Date?
    
    var transmitterId: String
    
    var transmitterSessionSignature: UInt?
    
    var cgmSessionPeriod: CGMSessionPeriodModel?
    
    init(source: CgmSource, startTimestamp: Date, endTimestamp: Date? = nil, transmitterId: String, transmitterSessionSignature: UInt? = 0, cgmSessionPeriod: CGMSessionPeriodModel?) {
        self.source = source
        self.startTimestamp = startTimestamp
        self.endTimestamp = endTimestamp
        self.transmitterId = transmitterId
        self.transmitterSessionSignature = transmitterSessionSignature
        self.cgmSessionPeriod = cgmSessionPeriod
    }
}
